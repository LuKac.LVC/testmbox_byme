package se.searchofskills;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("se.searchofskills");

        noClasses()
            .that()
                .resideInAnyPackage("se.searchofskills.service..")
            .or()
                .resideInAnyPackage("se.searchofskills.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..se.searchofskills.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
