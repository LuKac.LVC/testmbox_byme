package se.searchofskills.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.SearchOfSkillsApp;
import se.searchofskills.domain.ManagerialPosition;
import se.searchofskills.repository.ManagerialPositionRepository;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.dto.managerial_position.ManagerialPositionOutputDTO;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = SearchOfSkillsApp.class)
@WithMockUser(authorities = AuthoritiesConstants.ADMIN)
@Transactional
public class ManagerialPositionServiceIT {
    @Autowired
    ManagerialPositionRepository managerialPositionRepository;

    @Autowired
    ManagerialPositionService managerialPositionService;

    @BeforeEach
    public void init() {
        List<ManagerialPosition> managerialPositions = new ArrayList<>();
        ManagerialPosition managerialPosition = new ManagerialPosition();
        managerialPosition.setName("lead");
        managerialPositions.add(managerialPosition);

        ManagerialPosition managerialPosition1 = new ManagerialPosition();
        managerialPosition1.setName("lead1");
        managerialPositions.add(managerialPosition1);

        ManagerialPosition managerialPosition2 = new ManagerialPosition();
        managerialPosition2.setName("chairman");
        managerialPositions.add(managerialPosition2);

        managerialPositionRepository.saveAll(managerialPositions);
    }

    @Test
    public void testGetAllWithNameIsNull() {
        Pageable pageable = PageRequest.of(0, 3);
        String name = null;

        Page<ManagerialPositionOutputDTO> result = managerialPositionService.getAll(pageable, name);
        //then
        assertThat(result.getContent().size()).isEqualTo(3);
        assertThat(result.getContent().get(0).getName()).isEqualTo("lead");
        assertThat(result.getContent().get(1).getName()).isEqualTo("lead1");
        assertThat(result.getContent().get(2).getName()).isEqualTo("chairman");
    }

    @Test
    public void testGetAllWithNameIsNotNull() {
        Pageable pageable = PageRequest.of(0, 3);
        String name = "lead";

        Page<ManagerialPositionOutputDTO> result = managerialPositionService.getAll(pageable, name);
        //then
        assertThat(result.getContent().size()).isEqualTo(2);
        assertThat(result.getContent().get(0).getName()).isEqualTo("lead");
        assertThat(result.getContent().get(1).getName()).isEqualTo("lead1");
    }

    @AfterEach
    public void destroy() {
        managerialPositionRepository.deleteAll();
    }
}
