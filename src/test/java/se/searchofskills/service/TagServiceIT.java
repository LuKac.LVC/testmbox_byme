package se.searchofskills.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.SearchOfSkillsApp;
import se.searchofskills.domain.Category;
import se.searchofskills.domain.Tag;
import se.searchofskills.domain.TagCategory;
import se.searchofskills.repository.CategoryRepository;
import se.searchofskills.repository.TagCategoryRepository;
import se.searchofskills.repository.TagRepository;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.dto.tag.TagOutputDTO;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = SearchOfSkillsApp.class)
@WithMockUser(authorities = AuthoritiesConstants.ADMIN)
@Transactional
public class TagServiceIT {
    @Autowired
    TagRepository tagRepository;

    @Autowired
    TagCategoryRepository tagCategoryRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    TagService tagService;

    @BeforeEach
    public void init() {
        List<Tag> tagList = new ArrayList<>();
        Tag tag = new Tag();
        tag.setName("phong");
        tag.setActive(true);
        tag.setBlock(false);
        tagList.add(tag);

        Tag tag1 = new Tag();
        tag1.setName("phong1");
        tag1.setActive(true);
        tag1.setBlock(false);
        tagList.add(tag1);

        tagRepository.saveAll(tagList);

        List<Category> categoryList = new ArrayList<>();
        Category category = new Category();
        category.setName("cate1");
        categoryList.add(category);

        Category category1 = new Category();
        category1.setName("cate2");
        categoryList.add(category1);

        categoryRepository.saveAll(categoryList);

        List<TagCategory> tagCategoryList = new ArrayList<>();

        TagCategory tagCategory = new TagCategory();
        tagCategory.setTag(tag);
        tagCategory.setCategory(category);
        tagCategoryList.add(tagCategory);

        TagCategory tagCategory1 = new TagCategory();
        tagCategory1.setTag(tag);
        tagCategory1.setCategory(category1);
        tagCategoryList.add(tagCategory1);

        TagCategory tagCategory2 = new TagCategory();
        tagCategory2.setTag(tag1);
        tagCategory2.setCategory(category);
        tagCategoryList.add(tagCategory2);

        TagCategory tagCategory3 = new TagCategory();
        tagCategory3.setTag(tag1);
        tagCategory3.setCategory(category1);
        tagCategoryList.add(tagCategory3);

        tagCategoryRepository.saveAll(tagCategoryList);
    }

    @Test
    public void testGetAllTag() {
        Pageable pageable = PageRequest.of(0, 2);
        String name = "";
        Page<TagOutputDTO> page = tagService.getALl(pageable, false, name);
        assertThat(page.getContent().get(0).getName()).isEqualTo("phong");
        assertThat(page.getContent().get(0).getCategories().size()).isEqualTo(2);
        assertThat(page.getContent().get(1).getName()).isEqualTo("phong1");
        assertThat(page.getContent().get(1).getCategories().get(0).getName()).isEqualTo("cate1");
        assertThat(page.getContent().get(0).getCategories().size()).isEqualTo(2);
    }

    @AfterEach
    public void destroy() {
        categoryRepository.deleteAll();
        tagRepository.deleteAll();
        tagCategoryRepository.deleteAll();
    }
}
