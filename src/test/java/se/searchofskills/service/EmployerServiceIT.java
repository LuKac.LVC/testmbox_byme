package se.searchofskills.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.SearchOfSkillsApp;
import se.searchofskills.domain.Employer;
import se.searchofskills.repository.EmployerRepository;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.dto.employer.EmployerOutputDTO;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = SearchOfSkillsApp.class)
@WithMockUser(authorities = AuthoritiesConstants.ADMIN)
@Transactional
public class EmployerServiceIT {
    @Autowired
    EmployerRepository employerRepository;

    @Autowired
    EmployerService employerService;

    @BeforeEach
    public void init() {
        employerRepository.deleteAll();
    }

    @AfterEach
    public void destroy() {
        employerRepository.deleteAll();
    }

    @Test
    public void testEmployerWithNameIsNull() {
        String name = null;

        Pageable pageable = PageRequest.of(0, 3);

        List<Employer> employerList = new ArrayList<>();

        Employer employer = new Employer();
        employer.setName("phong1");
        employerList.add(employer);

        Employer employer1 = new Employer();
        employer1.setName("phong2");
        employerList.add(employer1);

        Employer employer2 = new Employer();
        employer2.setName("techlead");
        employerList.add(employer2);

        employerRepository.saveAll(employerList);

        Page<EmployerOutputDTO> result = employerService.getAll(pageable, name);
        assertThat(result.getContent().size()).isEqualTo(3);
        assertThat(result.getContent().get(0).getName()).isEqualTo("phong1");
        assertThat(result.getContent().get(1).getName()).isEqualTo("phong2");
        assertThat(result.getContent().get(2).getName()).isEqualTo("techlead");
    }

    @Test
    public void testEmployerWithNameIsNotNull() {
        String name = "phong";

        Pageable pageable = PageRequest.of(0, 3);

        List<Employer> employerList = new ArrayList<>();

        Employer employer = new Employer();
        employer.setName("phong1");
        employerList.add(employer);

        Employer employer1 = new Employer();
        employer1.setName("phong2");
        employerList.add(employer1);

        Employer employer2 = new Employer();
        employer2.setName("techlead");
        employerList.add(employer2);

        employerRepository.saveAll(employerList);

        Page<EmployerOutputDTO> result = employerService.getAll(pageable, name);
        assertThat(result.getContent().size()).isEqualTo(2);
        assertThat(result.getContent().get(0).getName()).isEqualTo("phong1");
        assertThat(result.getContent().get(1).getName()).isEqualTo("phong2");
    }

}
