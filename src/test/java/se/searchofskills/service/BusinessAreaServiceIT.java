package se.searchofskills.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.SearchOfSkillsApp;
import se.searchofskills.domain.BusinessArea;
import se.searchofskills.repository.BusinessAreaRepository;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.dto.business_area.BusinessAreaOutputDTO;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = SearchOfSkillsApp.class)
@WithMockUser(authorities = AuthoritiesConstants.ADMIN)
@Transactional
public class BusinessAreaServiceIT {
    @Autowired
    BusinessAreaRepository businessAreaRepository;

    @Autowired
    BusinessAreaService businessAreaService;

    @BeforeEach
    public void init() {
        List<BusinessArea> businessAreas = new ArrayList<>();
        BusinessArea businessArea = new BusinessArea();
        businessArea.setName("lead");
        businessAreas.add(businessArea);

        BusinessArea businessArea1 = new BusinessArea();
        businessArea1.setName("lead1");
        businessAreas.add(businessArea1);

        BusinessArea businessArea2 = new BusinessArea();
        businessArea2.setName("chairman");
        businessAreas.add(businessArea2);

        businessAreaRepository.saveAll(businessAreas);
    }

    @Test
    public void testGetAllWithNameIsNull() {
        Pageable pageable = PageRequest.of(0, 3);
        String name = null;

        Page<BusinessAreaOutputDTO> result = businessAreaService.getAll(pageable, name);
        //then
        assertThat(result.getContent().size()).isEqualTo(3);
        assertThat(result.getContent().get(0).getName()).isEqualTo("lead");
        assertThat(result.getContent().get(1).getName()).isEqualTo("lead1");
        assertThat(result.getContent().get(2).getName()).isEqualTo("chairman");
    }

    @Test
    public void testGetAllWithNameIsNotNull() {
        Pageable pageable = PageRequest.of(0, 3);
        String name = "lead";

        Page<BusinessAreaOutputDTO> result = businessAreaService.getAll(pageable, name);
        //then
        assertThat(result.getContent().size()).isEqualTo(2);
        assertThat(result.getContent().get(0).getName()).isEqualTo("lead");
        assertThat(result.getContent().get(1).getName()).isEqualTo("lead1");
    }

    @AfterEach
    public void destroy() {
        businessAreaRepository.deleteAll();
    }
}
