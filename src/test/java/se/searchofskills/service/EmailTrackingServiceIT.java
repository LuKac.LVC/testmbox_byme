package se.searchofskills.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.SearchOfSkillsApp;
import se.searchofskills.domain.*;
import se.searchofskills.domain.enumeration.CronJobStatus;
import se.searchofskills.domain.enumeration.EmailStatus;
import se.searchofskills.domain.enumeration.Frequency;
import se.searchofskills.domain.enumeration.WeekDay;
import se.searchofskills.repository.CronJobRepository;
import se.searchofskills.repository.EmailTrackingRepository;
import se.searchofskills.repository.RecipientListRepository;
import se.searchofskills.repository.RecipientRepository;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.dto.EmailTrackingDTO;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(classes = SearchOfSkillsApp.class)
@WithMockUser(authorities = AuthoritiesConstants.ADMIN)
@Transactional
public class EmailTrackingServiceIT {
    @Autowired
    RecipientRepository recipientRepository;

    @Autowired
    EmailTrackingService emailTrackingService;

    @Autowired
    EmailTrackingRepository emailTrackingRepository;

    @Autowired
    RecipientListRepository recipientListRepository;

    @Autowired
    CronJobRepository cronJobRepository;


    @BeforeEach
    public void init() {
        emailTrackingRepository.deleteAll();
    }

    @AfterEach
    public void destroy(){
        emailTrackingRepository.deleteAll();
    }

    @Test
    public void testFindAllById() {

        Instant endDate
            = Instant.parse("2021-11-12T00:00:00Z");
        Instant startDate
            = Instant.parse("2021-10-22T00:00:00Z");


        RecipientList recipientList = new RecipientList();
        recipientList.setName("Recipient List");
        recipientListRepository.save(recipientList);

        Recipient recipient = new Recipient();
        recipient.setCounty("average");
        recipient.setTitle("sw");
        recipient.setEmail("pt@gmail.com");
        recipient.setReferenceNumber("1234");
        recipient.setRegistrar("pt@gmail.com");
        recipient.setProcurementUrl("ab@gmail.com");
        recipient.setProcuringAgency("new york");
        recipient.setRecipientList(recipientList);

        Recipient recipient1 = new Recipient();
        recipient1.setCounty("fail");
        recipient1.setTitle("sa");
        recipient1.setReferenceNumber("1234");
        recipient1.setEmail("pt@gmail.com");
        recipient1.setRegistrar("pt@gmail.com");
        recipient1.setProcurementUrl("ab@gmail.com");
        recipient1.setProcuringAgency("paris");
        recipient1.setRecipientList(recipientList);

        Recipient recipient2 = new Recipient();
        recipient2.setCounty("success");
        recipient2.setTitle("sv");
        recipient2.setEmail("pt@gmail.com");
        recipient2.setReferenceNumber("1234");
        recipient2.setRegistrar("pt@gmail.com");
        recipient2.setProcurementUrl("ab@gmail.com");
        recipient2.setProcuringAgency("tokyo");
        recipient2.setRecipientList(recipientList);

        List<Recipient> recipients = new ArrayList<>();
        recipients.add(recipient);
        recipients.add(recipient1);
        recipients.add(recipient2);
        recipientRepository.saveAll(recipients);

        CronJob cronJob = new CronJob();
        cronJob.setName("cronjob 1");
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setWeekday(WeekDay.THU);
        cronJob.setNextSendDate(Instant.now());
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setTemplateReminderTime2(7);
        cronJob.setTemplateReminderTime2(14);
        cronJob.setArchived(false);
        cronJob.setDeleted(false);
        cronJob.setStartDate(startDate);
        cronJob.setEndDate(endDate);
        cronJob.setRecipientList(recipientList);
        cronJob.setChunkNumber(20);
        cronJob.setSendHour(2);
        cronJobRepository.save(cronJob);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setRecipient(recipient);
        emailTracking.setCronJob(cronJob);
        emailTracking.setStatus(EmailStatus.SENT);
        emailTracking.setDeleted(false);

        EmailTracking emailTracking1 = new EmailTracking();
        emailTracking.setId(2L);
        emailTracking1.setCronJob(cronJob);
        emailTracking1.setRecipient(recipient1);
        emailTracking1.setStatus(EmailStatus.SENT);
        emailTracking1.setDeleted(false);

        EmailTracking emailTracking2 = new EmailTracking();
        emailTracking.setId(3L);
        emailTracking2.setCronJob(cronJob);
        emailTracking2.setRecipient(recipient2);
        emailTracking2.setStatus(EmailStatus.SENT);
        emailTracking2.setDeleted(false);

        EmailTracking emailTracking3 = new EmailTracking();
        emailTracking.setId(4L);
        emailTracking3.setCronJob(cronJob);
        emailTracking3.setRecipient(recipient);
        emailTracking3.setStatus(EmailStatus.SENT);
        emailTracking3.setDeleted(false);

        EmailTracking emailTracking4 = new EmailTracking();
        emailTracking.setId(5L);
        emailTracking4.setCronJob(cronJob);
        emailTracking4.setRecipient(recipient1);
        emailTracking4.setStatus(EmailStatus.SENT);
        emailTracking4.setDeleted(false);

        EmailTracking emailTracking5 = new EmailTracking();
        emailTracking.setId(6L);
        emailTracking5.setCronJob(cronJob);
        emailTracking5.setRecipient(recipient2);
        emailTracking5.setStatus(EmailStatus.SENT);
        emailTracking5.setDeleted(false);

        EmailTracking emailTracking6 = new EmailTracking();
        emailTracking.setId(7L);
        emailTracking6.setCronJob(cronJob);
        emailTracking6.setRecipient(recipient);
        emailTracking6.setStatus(EmailStatus.SENT);
        emailTracking6.setDeleted(false);

        EmailTracking emailTracking7 = new EmailTracking();
        emailTracking.setId(8L);
        emailTracking7.setCronJob(cronJob);
        emailTracking7.setRecipient(recipient1);
        emailTracking7.setStatus(EmailStatus.SENT);
        emailTracking7.setDeleted(false);

        EmailTracking emailTracking8 = new EmailTracking();
        emailTracking.setId(9L);
        emailTracking8.setCronJob(cronJob);
        emailTracking8.setRecipient(recipient2);
        emailTracking8.setStatus(EmailStatus.SENT);
        emailTracking8.setDeleted(false);

        EmailTracking emailTracking9 = new EmailTracking();
        emailTracking.setId(10L);
        emailTracking9.setCronJob(cronJob);
        emailTracking9.setRecipient(recipient1);
        emailTracking9.setStatus(EmailStatus.SENT);
        emailTracking9.setDeleted(false);

        List<EmailTracking> list = new ArrayList<>();
        list.add(emailTracking);
        list.add(emailTracking1);
        list.add(emailTracking2);
        list.add(emailTracking3);
        list.add(emailTracking4);
        list.add(emailTracking5);
        list.add(emailTracking6);
        list.add(emailTracking7);
        list.add(emailTracking8);
        list.add(emailTracking9);
        emailTrackingRepository.saveAll(list);

        Pageable pageable = PageRequest.of(0, 20, Sort.by(EmailTracking_.ID).ascending());

        Page<EmailTrackingDTO> result = emailTrackingService.findAllById(pageable, recipient.getId());
        assertThat(result.getContent().size()).isEqualTo(3);
        assertThat(result.getContent().get(0).getRecipient().getTitle()).isEqualTo("sw");
        assertThat(result.getContent().get(0).getRecipient().getCounty()).isEqualTo("average");

    }

    /**
     * Get list email tracking with case recipientId and keyword null
     */
    @Test
    public void testGetEmailTrackingWithCaseAll(){
        Instant endDate
            = Instant.parse("2021-11-12T00:00:00Z");
        Instant startDate
            = Instant.parse("2021-10-22T00:00:00Z");


        RecipientList recipientList = new RecipientList();
        recipientList.setName("Recipient List");
        recipientListRepository.save(recipientList);

        Recipient recipient = new Recipient();
        recipient.setCounty("average");
        recipient.setTitle("sw");
        recipient.setEmail("pt@gmail.com");
        recipient.setReferenceNumber("1234");
        recipient.setRegistrar("pt@gmail.com");
        recipient.setProcurementUrl("ab@gmail.com");
        recipient.setProcuringAgency("new york");
        recipient.setRecipientList(recipientList);

        Recipient recipient1 = new Recipient();
        recipient1.setCounty("fail");
        recipient1.setTitle("sa");
        recipient1.setReferenceNumber("1234");
        recipient1.setEmail("pt@gmail.com");
        recipient1.setRegistrar("pt@gmail.com");
        recipient1.setProcurementUrl("ab@gmail.com");
        recipient1.setProcuringAgency("paris");
        recipient1.setRecipientList(recipientList);

        Recipient recipient2 = new Recipient();
        recipient2.setCounty("success");
        recipient2.setTitle("sv");
        recipient2.setEmail("pt@gmail.com");
        recipient2.setReferenceNumber("1234");
        recipient2.setRegistrar("pt@gmail.com");
        recipient2.setProcurementUrl("ab@gmail.com");
        recipient2.setProcuringAgency("tokyo");
        recipient2.setRecipientList(recipientList);

        List<Recipient> recipients = new ArrayList<>();
        recipients.add(recipient);
        recipients.add(recipient1);
        recipients.add(recipient2);
        recipientRepository.saveAll(recipients);

        CronJob cronJob = new CronJob();
        cronJob.setName("cronjob 1");
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setWeekday(WeekDay.THU);
        cronJob.setNextSendDate(Instant.now());
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setTemplateReminderTime2(7);
        cronJob.setTemplateReminderTime2(14);
        cronJob.setArchived(false);
        cronJob.setDeleted(false);
        cronJob.setStartDate(startDate);
        cronJob.setEndDate(endDate);
        cronJob.setRecipientList(recipientList);
        cronJob.setChunkNumber(20);
        cronJob.setSendHour(2);
        cronJobRepository.save(cronJob);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setRecipient(recipient);
        emailTracking.setCronJob(cronJob);
        emailTracking.setStatus(EmailStatus.SENT);
        emailTracking.setDeleted(false);

        EmailTracking emailTracking1 = new EmailTracking();
        emailTracking.setId(2L);
        emailTracking1.setCronJob(cronJob);
        emailTracking1.setRecipient(recipient1);
        emailTracking1.setStatus(EmailStatus.SENT);
        emailTracking1.setDeleted(false);

        EmailTracking emailTracking2 = new EmailTracking();
        emailTracking.setId(3L);
        emailTracking2.setCronJob(cronJob);
        emailTracking2.setRecipient(recipient2);
        emailTracking2.setStatus(EmailStatus.SENT);
        emailTracking2.setDeleted(false);

        EmailTracking emailTracking3 = new EmailTracking();
        emailTracking.setId(4L);
        emailTracking3.setCronJob(cronJob);
        emailTracking3.setRecipient(recipient);
        emailTracking3.setStatus(EmailStatus.SENT);
        emailTracking3.setDeleted(false);

        EmailTracking emailTracking4 = new EmailTracking();
        emailTracking.setId(5L);
        emailTracking4.setCronJob(cronJob);
        emailTracking4.setRecipient(recipient1);
        emailTracking4.setStatus(EmailStatus.SENT);
        emailTracking4.setDeleted(false);

        EmailTracking emailTracking5 = new EmailTracking();
        emailTracking.setId(6L);
        emailTracking5.setCronJob(cronJob);
        emailTracking5.setRecipient(recipient2);
        emailTracking5.setStatus(EmailStatus.SENT);
        emailTracking5.setDeleted(false);

        EmailTracking emailTracking6 = new EmailTracking();
        emailTracking.setId(7L);
        emailTracking6.setCronJob(cronJob);
        emailTracking6.setRecipient(recipient);
        emailTracking6.setStatus(EmailStatus.SENT);
        emailTracking6.setDeleted(false);

        EmailTracking emailTracking7 = new EmailTracking();
        emailTracking.setId(8L);
        emailTracking7.setCronJob(cronJob);
        emailTracking7.setRecipient(recipient1);
        emailTracking7.setStatus(EmailStatus.SENT);
        emailTracking7.setDeleted(false);

        EmailTracking emailTracking8 = new EmailTracking();
        emailTracking.setId(9L);
        emailTracking8.setCronJob(cronJob);
        emailTracking8.setRecipient(recipient2);
        emailTracking8.setStatus(EmailStatus.SENT);
        emailTracking8.setDeleted(false);

        EmailTracking emailTracking9 = new EmailTracking();
        emailTracking.setId(10L);
        emailTracking9.setCronJob(cronJob);
        emailTracking9.setRecipient(recipient1);
        emailTracking9.setStatus(EmailStatus.SENT);
        emailTracking9.setDeleted(false);

        List<EmailTracking> list = new ArrayList<>();
        list.add(emailTracking);
        list.add(emailTracking1);
        list.add(emailTracking2);
        list.add(emailTracking3);
        list.add(emailTracking4);
        list.add(emailTracking5);
        list.add(emailTracking6);
        list.add(emailTracking7);
        list.add(emailTracking8);
        list.add(emailTracking9);
        emailTrackingRepository.saveAll(list);

        Pageable pageable = PageRequest.of(0, 20, Sort.by(EmailTracking_.ID).ascending());

        Page<EmailTrackingDTO> result = emailTrackingService.getListOfEmailTracking(pageable, null, null, false);
        assertThat(result.getContent().size()).isEqualTo(10);
        assertThat(result.getContent().get(0).getRecipient().getTitle()).isEqualTo("sw");
        assertThat(result.getContent().get(0).getRecipient().getCounty()).isEqualTo("average");

    }

    /**
     * Get list email tracking with recipientId
     */
    @Test
    public void testGetEmailTrackingWithRecipientId(){
        Instant endDate
            = Instant.parse("2021-11-12T00:00:00Z");
        Instant startDate
            = Instant.parse("2021-10-22T00:00:00Z");


        RecipientList recipientList = new RecipientList();
        recipientList.setName("Recipient List");
        recipientListRepository.save(recipientList);

        Recipient recipient = new Recipient();
        recipient.setCounty("average");
        recipient.setTitle("sw");
        recipient.setEmail("pt@gmail.com");
        recipient.setReferenceNumber("1234");
        recipient.setRegistrar("pt@gmail.com");
        recipient.setProcurementUrl("ab@gmail.com");
        recipient.setProcuringAgency("new york");
        recipient.setRecipientList(recipientList);

        Recipient recipient1 = new Recipient();
        recipient1.setCounty("fail");
        recipient1.setTitle("sa");
        recipient1.setReferenceNumber("1234");
        recipient1.setEmail("pt@gmail.com");
        recipient1.setRegistrar("pt@gmail.com");
        recipient1.setProcurementUrl("ab@gmail.com");
        recipient1.setProcuringAgency("paris");
        recipient1.setRecipientList(recipientList);

        Recipient recipient2 = new Recipient();
        recipient2.setCounty("success");
        recipient2.setTitle("sv");
        recipient2.setEmail("pt@gmail.com");
        recipient2.setReferenceNumber("1234");
        recipient2.setRegistrar("pt@gmail.com");
        recipient2.setProcurementUrl("ab@gmail.com");
        recipient2.setProcuringAgency("tokyo");
        recipient2.setRecipientList(recipientList);

        List<Recipient> recipients = new ArrayList<>();
        recipients.add(recipient);
        recipients.add(recipient1);
        recipients.add(recipient2);
        recipientRepository.saveAll(recipients);

        CronJob cronJob = new CronJob();
        cronJob.setName("cronjob 1");
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setWeekday(WeekDay.THU);
        cronJob.setNextSendDate(Instant.now());
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setTemplateReminderTime2(7);
        cronJob.setTemplateReminderTime2(14);
        cronJob.setArchived(false);
        cronJob.setDeleted(false);
        cronJob.setStartDate(startDate);
        cronJob.setEndDate(endDate);
        cronJob.setRecipientList(recipientList);
        cronJob.setChunkNumber(20);
        cronJob.setSendHour(2);
        cronJobRepository.save(cronJob);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setRecipient(recipient);
        emailTracking.setCronJob(cronJob);
        emailTracking.setStatus(EmailStatus.SENT);
        emailTracking.setDeleted(false);

        EmailTracking emailTracking1 = new EmailTracking();
        emailTracking.setId(2L);
        emailTracking1.setCronJob(cronJob);
        emailTracking1.setRecipient(recipient1);
        emailTracking1.setStatus(EmailStatus.SENT);
        emailTracking1.setDeleted(false);

        EmailTracking emailTracking2 = new EmailTracking();
        emailTracking.setId(3L);
        emailTracking2.setCronJob(cronJob);
        emailTracking2.setRecipient(recipient2);
        emailTracking2.setStatus(EmailStatus.SENT);
        emailTracking2.setDeleted(false);

        EmailTracking emailTracking3 = new EmailTracking();
        emailTracking.setId(4L);
        emailTracking3.setCronJob(cronJob);
        emailTracking3.setRecipient(recipient);
        emailTracking3.setStatus(EmailStatus.SENT);
        emailTracking3.setDeleted(false);

        EmailTracking emailTracking4 = new EmailTracking();
        emailTracking.setId(5L);
        emailTracking4.setCronJob(cronJob);
        emailTracking4.setRecipient(recipient1);
        emailTracking4.setStatus(EmailStatus.SENT);
        emailTracking4.setDeleted(false);

        EmailTracking emailTracking5 = new EmailTracking();
        emailTracking.setId(6L);
        emailTracking5.setCronJob(cronJob);
        emailTracking5.setRecipient(recipient2);
        emailTracking5.setStatus(EmailStatus.SENT);
        emailTracking5.setDeleted(false);

        EmailTracking emailTracking6 = new EmailTracking();
        emailTracking.setId(7L);
        emailTracking6.setCronJob(cronJob);
        emailTracking6.setRecipient(recipient);
        emailTracking6.setStatus(EmailStatus.SENT);
        emailTracking6.setDeleted(false);

        EmailTracking emailTracking7 = new EmailTracking();
        emailTracking.setId(8L);
        emailTracking7.setCronJob(cronJob);
        emailTracking7.setRecipient(recipient1);
        emailTracking7.setStatus(EmailStatus.SENT);
        emailTracking7.setDeleted(false);

        EmailTracking emailTracking8 = new EmailTracking();
        emailTracking.setId(9L);
        emailTracking8.setCronJob(cronJob);
        emailTracking8.setRecipient(recipient2);
        emailTracking8.setStatus(EmailStatus.SENT);
        emailTracking8.setDeleted(false);

        EmailTracking emailTracking9 = new EmailTracking();
        emailTracking.setId(10L);
        emailTracking9.setCronJob(cronJob);
        emailTracking9.setRecipient(recipient1);
        emailTracking9.setStatus(EmailStatus.SENT);
        emailTracking9.setDeleted(false);

        List<EmailTracking> list = new ArrayList<>();
        list.add(emailTracking);
        list.add(emailTracking1);
        list.add(emailTracking2);
        list.add(emailTracking3);
        list.add(emailTracking4);
        list.add(emailTracking5);
        list.add(emailTracking6);
        list.add(emailTracking7);
        list.add(emailTracking8);
        list.add(emailTracking9);
        emailTrackingRepository.saveAll(list);

        Pageable pageable = PageRequest.of(0, 20, Sort.by(EmailTracking_.ID).ascending());

        Page<EmailTrackingDTO> result = emailTrackingService.getListOfEmailTracking(pageable, recipient.getId(), null, false);
        assertThat(result.getContent().size()).isEqualTo(3);
        assertThat(result.getContent().get(0).getRecipient().getTitle()).isEqualTo("sw");
        assertThat(result.getContent().get(0).getRecipient().getCounty()).isEqualTo("average");
    }

    /**
     * Get list email tracking with keyword
     */
    @Test
    public void testGetEmailTrackingWithKeyword(){
        Instant endDate
            = Instant.parse("2021-11-12T00:00:00Z");
        Instant startDate
            = Instant.parse("2021-10-22T00:00:00Z");


        RecipientList recipientList = new RecipientList();
        recipientList.setName("Recipient List");
        recipientListRepository.save(recipientList);

        Recipient recipient = new Recipient();
        recipient.setCounty("average");
        recipient.setTitle("sw");
        recipient.setEmail("pt@gmail.com");
        recipient.setReferenceNumber("1234");
        recipient.setRegistrar("pt@gmail.com");
        recipient.setProcurementUrl("ab@gmail.com");
        recipient.setProcuringAgency("new york");
        recipient.setRecipientList(recipientList);

        Recipient recipient1 = new Recipient();
        recipient1.setCounty("fail");
        recipient1.setTitle("sa");
        recipient1.setReferenceNumber("1234");
        recipient1.setEmail("pt@gmail.com");
        recipient1.setRegistrar("pt@gmail.com");
        recipient1.setProcurementUrl("ab@gmail.com");
        recipient1.setProcuringAgency("paris");
        recipient1.setRecipientList(recipientList);

        Recipient recipient2 = new Recipient();
        recipient2.setCounty("success");
        recipient2.setTitle("sv");
        recipient2.setEmail("pt@gmail.com");
        recipient2.setReferenceNumber("1234");
        recipient2.setRegistrar("pt@gmail.com");
        recipient2.setProcurementUrl("ab@gmail.com");
        recipient2.setProcuringAgency("tokyo");
        recipient2.setRecipientList(recipientList);

        List<Recipient> recipients = new ArrayList<>();
        recipients.add(recipient);
        recipients.add(recipient1);
        recipients.add(recipient2);
        recipientRepository.saveAll(recipients);

        CronJob cronJob = new CronJob();
        cronJob.setName("cronjob 1");
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setWeekday(WeekDay.THU);
        cronJob.setNextSendDate(Instant.now());
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setTemplateReminderTime2(7);
        cronJob.setTemplateReminderTime2(14);
        cronJob.setArchived(false);
        cronJob.setDeleted(false);
        cronJob.setStartDate(startDate);
        cronJob.setEndDate(endDate);
        cronJob.setRecipientList(recipientList);
        cronJob.setChunkNumber(20);
        cronJob.setSendHour(2);
        cronJobRepository.save(cronJob);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setRecipient(recipient);
        emailTracking.setCronJob(cronJob);
        emailTracking.setStatus(EmailStatus.SENT);
        emailTracking.setDeleted(false);

        EmailTracking emailTracking1 = new EmailTracking();
        emailTracking.setId(2L);
        emailTracking1.setCronJob(cronJob);
        emailTracking1.setRecipient(recipient1);
        emailTracking1.setStatus(EmailStatus.SENT);
        emailTracking1.setDeleted(false);

        EmailTracking emailTracking2 = new EmailTracking();
        emailTracking.setId(3L);
        emailTracking2.setCronJob(cronJob);
        emailTracking2.setRecipient(recipient2);
        emailTracking2.setStatus(EmailStatus.SENT);
        emailTracking2.setDeleted(false);

        EmailTracking emailTracking3 = new EmailTracking();
        emailTracking.setId(4L);
        emailTracking3.setCronJob(cronJob);
        emailTracking3.setRecipient(recipient);
        emailTracking3.setStatus(EmailStatus.SENT);
        emailTracking3.setDeleted(false);

        EmailTracking emailTracking4 = new EmailTracking();
        emailTracking.setId(5L);
        emailTracking4.setCronJob(cronJob);
        emailTracking4.setRecipient(recipient1);
        emailTracking4.setStatus(EmailStatus.SENT);
        emailTracking4.setDeleted(false);

        EmailTracking emailTracking5 = new EmailTracking();
        emailTracking.setId(6L);
        emailTracking5.setCronJob(cronJob);
        emailTracking5.setRecipient(recipient2);
        emailTracking5.setStatus(EmailStatus.SENT);
        emailTracking5.setDeleted(false);

        EmailTracking emailTracking6 = new EmailTracking();
        emailTracking.setId(7L);
        emailTracking6.setCronJob(cronJob);
        emailTracking6.setRecipient(recipient);
        emailTracking6.setStatus(EmailStatus.SENT);
        emailTracking6.setDeleted(false);

        EmailTracking emailTracking7 = new EmailTracking();
        emailTracking.setId(8L);
        emailTracking7.setCronJob(cronJob);
        emailTracking7.setRecipient(recipient1);
        emailTracking7.setStatus(EmailStatus.SENT);
        emailTracking7.setDeleted(false);

        EmailTracking emailTracking8 = new EmailTracking();
        emailTracking.setId(9L);
        emailTracking8.setCronJob(cronJob);
        emailTracking8.setRecipient(recipient2);
        emailTracking8.setStatus(EmailStatus.SENT);
        emailTracking8.setDeleted(false);

        EmailTracking emailTracking9 = new EmailTracking();
        emailTracking.setId(10L);
        emailTracking9.setCronJob(cronJob);
        emailTracking9.setRecipient(recipient1);
        emailTracking9.setStatus(EmailStatus.SENT);
        emailTracking9.setDeleted(false);

        List<EmailTracking> list = new ArrayList<>();
        list.add(emailTracking);
        list.add(emailTracking1);
        list.add(emailTracking2);
        list.add(emailTracking3);
        list.add(emailTracking4);
        list.add(emailTracking5);
        list.add(emailTracking6);
        list.add(emailTracking7);
        list.add(emailTracking8);
        list.add(emailTracking9);
        emailTrackingRepository.saveAll(list);

        Pageable pageable = PageRequest.of(0, 20, Sort.by(EmailTracking_.ID).ascending());

        Page<EmailTrackingDTO> result = emailTrackingService.getListOfEmailTracking(pageable, null, "success", false);
        assertThat(result.getContent().size()).isEqualTo(3);
        assertThat(result.getContent().get(0).getRecipient().getTitle()).isEqualTo("sv");
        assertThat(result.getContent().get(0).getRecipient().getCounty()).isEqualTo("success");
    }

    /**
     * Get list email tracking with keyword and archived
     */
    @Test
    public void testGetEmailTrackingWithKeywordAndArchived(){
        Instant endDate
            = Instant.parse("2021-11-12T00:00:00Z");
        Instant startDate
            = Instant.parse("2021-10-22T00:00:00Z");


        RecipientList recipientList = new RecipientList();
        recipientList.setName("Recipient List");
        recipientListRepository.save(recipientList);

        Recipient recipient = new Recipient();
        recipient.setCounty("average");
        recipient.setTitle("sw");
        recipient.setEmail("pt@gmail.com");
        recipient.setReferenceNumber("1234");
        recipient.setRegistrar("pt@gmail.com");
        recipient.setProcurementUrl("ab@gmail.com");
        recipient.setProcuringAgency("new york");
        recipient.setRecipientList(recipientList);

        Recipient recipient1 = new Recipient();
        recipient1.setCounty("fail");
        recipient1.setTitle("sa");
        recipient1.setReferenceNumber("1234");
        recipient1.setEmail("pt@gmail.com");
        recipient1.setRegistrar("pt@gmail.com");
        recipient1.setProcurementUrl("ab@gmail.com");
        recipient1.setProcuringAgency("paris");
        recipient1.setRecipientList(recipientList);

        Recipient recipient2 = new Recipient();
        recipient2.setCounty("success");
        recipient2.setTitle("sv");
        recipient2.setEmail("pt@gmail.com");
        recipient2.setReferenceNumber("1234");
        recipient2.setRegistrar("pt@gmail.com");
        recipient2.setProcurementUrl("ab@gmail.com");
        recipient2.setProcuringAgency("tokyo");
        recipient2.setRecipientList(recipientList);

        List<Recipient> recipients = new ArrayList<>();
        recipients.add(recipient);
        recipients.add(recipient1);
        recipients.add(recipient2);
        recipientRepository.saveAll(recipients);

        CronJob cronJob = new CronJob();
        cronJob.setName("cronjob 1");
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setWeekday(WeekDay.THU);
        cronJob.setNextSendDate(Instant.now());
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setTemplateReminderTime2(7);
        cronJob.setTemplateReminderTime2(14);
        cronJob.setArchived(false);
        cronJob.setDeleted(false);
        cronJob.setStartDate(startDate);
        cronJob.setEndDate(endDate);
        cronJob.setRecipientList(recipientList);
        cronJob.setChunkNumber(20);
        cronJob.setSendHour(2);
        cronJobRepository.save(cronJob);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setRecipient(recipient);
        emailTracking.setCronJob(cronJob);
        emailTracking.setStatus(EmailStatus.SENT);
        emailTracking.setDeleted(false);

        EmailTracking emailTracking1 = new EmailTracking();
        emailTracking.setId(2L);
        emailTracking1.setCronJob(cronJob);
        emailTracking1.setRecipient(recipient1);
        emailTracking1.setStatus(EmailStatus.SENT);
        emailTracking1.setDeleted(false);

        EmailTracking emailTracking2 = new EmailTracking();
        emailTracking.setId(3L);
        emailTracking2.setCronJob(cronJob);
        emailTracking2.setRecipient(recipient2);
        emailTracking2.setArchived(true);
        emailTracking2.setStatus(EmailStatus.SENT);
        emailTracking2.setDeleted(false);

        EmailTracking emailTracking3 = new EmailTracking();
        emailTracking.setId(4L);
        emailTracking3.setCronJob(cronJob);
        emailTracking3.setRecipient(recipient);
        emailTracking3.setStatus(EmailStatus.SENT);
        emailTracking3.setDeleted(false);

        EmailTracking emailTracking4 = new EmailTracking();
        emailTracking.setId(5L);
        emailTracking4.setCronJob(cronJob);
        emailTracking4.setRecipient(recipient1);
        emailTracking4.setStatus(EmailStatus.SENT);
        emailTracking4.setDeleted(false);

        EmailTracking emailTracking5 = new EmailTracking();
        emailTracking.setId(6L);
        emailTracking5.setCronJob(cronJob);
        emailTracking5.setRecipient(recipient2);
        emailTracking5.setStatus(EmailStatus.SENT);
        emailTracking5.setDeleted(false);

        EmailTracking emailTracking6 = new EmailTracking();
        emailTracking.setId(7L);
        emailTracking6.setCronJob(cronJob);
        emailTracking6.setRecipient(recipient);
        emailTracking6.setStatus(EmailStatus.SENT);
        emailTracking6.setDeleted(false);

        EmailTracking emailTracking7 = new EmailTracking();
        emailTracking.setId(8L);
        emailTracking7.setCronJob(cronJob);
        emailTracking7.setRecipient(recipient1);
        emailTracking7.setStatus(EmailStatus.SENT);
        emailTracking7.setDeleted(false);

        EmailTracking emailTracking8 = new EmailTracking();
        emailTracking.setId(9L);
        emailTracking8.setCronJob(cronJob);
        emailTracking8.setRecipient(recipient2);
        emailTracking8.setStatus(EmailStatus.SENT);
        emailTracking8.setDeleted(false);

        EmailTracking emailTracking9 = new EmailTracking();
        emailTracking.setId(10L);
        emailTracking9.setCronJob(cronJob);
        emailTracking9.setRecipient(recipient1);
        emailTracking9.setStatus(EmailStatus.SENT);
        emailTracking9.setDeleted(false);

        List<EmailTracking> list = new ArrayList<>();
        list.add(emailTracking);
        list.add(emailTracking1);
        list.add(emailTracking2);
        list.add(emailTracking3);
        list.add(emailTracking4);
        list.add(emailTracking5);
        list.add(emailTracking6);
        list.add(emailTracking7);
        list.add(emailTracking8);
        list.add(emailTracking9);
        emailTrackingRepository.saveAll(list);

        Pageable pageable = PageRequest.of(0, 20, Sort.by(EmailTracking_.ID).ascending());

        Page<EmailTrackingDTO> result = emailTrackingService.getListOfEmailTracking(pageable, null, "success", true);
        assertThat(result.getContent().size()).isEqualTo(1);
        assertThat(result.getContent().get(0).getRecipient().getTitle()).isEqualTo("sv");
        assertThat(result.getContent().get(0).getRecipient().getCounty()).isEqualTo("success");
    }

    /**
     * Get list email tracking with archived
     */
    @Test
    public void testGetEmailTrackingWithArchived(){
        Instant endDate
            = Instant.parse("2021-11-12T00:00:00Z");
        Instant startDate
            = Instant.parse("2021-10-22T00:00:00Z");


        RecipientList recipientList = new RecipientList();
        recipientList.setName("Recipient List");
        recipientListRepository.save(recipientList);

        Recipient recipient = new Recipient();
        recipient.setCounty("average");
        recipient.setTitle("sw");
        recipient.setEmail("pt@gmail.com");
        recipient.setReferenceNumber("1234");
        recipient.setRegistrar("pt@gmail.com");
        recipient.setProcurementUrl("ab@gmail.com");
        recipient.setProcuringAgency("new york");
        recipient.setRecipientList(recipientList);

        Recipient recipient1 = new Recipient();
        recipient1.setCounty("fail");
        recipient1.setTitle("sa");
        recipient1.setReferenceNumber("1234");
        recipient1.setEmail("pt@gmail.com");
        recipient1.setRegistrar("pt@gmail.com");
        recipient1.setProcurementUrl("ab@gmail.com");
        recipient1.setProcuringAgency("paris");
        recipient1.setRecipientList(recipientList);

        Recipient recipient2 = new Recipient();
        recipient2.setCounty("success");
        recipient2.setTitle("sv");
        recipient2.setEmail("pt@gmail.com");
        recipient2.setReferenceNumber("1234");
        recipient2.setRegistrar("pt@gmail.com");
        recipient2.setProcurementUrl("ab@gmail.com");
        recipient2.setProcuringAgency("tokyo");
        recipient2.setRecipientList(recipientList);

        List<Recipient> recipients = new ArrayList<>();
        recipients.add(recipient);
        recipients.add(recipient1);
        recipients.add(recipient2);
        recipientRepository.saveAll(recipients);

        CronJob cronJob = new CronJob();
        cronJob.setName("cronjob 1");
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setWeekday(WeekDay.THU);
        cronJob.setNextSendDate(Instant.now());
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setTemplateReminderTime2(7);
        cronJob.setTemplateReminderTime2(14);
        cronJob.setArchived(false);
        cronJob.setDeleted(false);
        cronJob.setStartDate(startDate);
        cronJob.setEndDate(endDate);
        cronJob.setRecipientList(recipientList);
        cronJob.setChunkNumber(20);
        cronJob.setSendHour(2);
        cronJobRepository.save(cronJob);

        EmailTracking emailTracking = new EmailTracking();
        emailTracking.setId(1L);
        emailTracking.setRecipient(recipient);
        emailTracking.setCronJob(cronJob);
        emailTracking.setStatus(EmailStatus.SENT);
        emailTracking.setDeleted(false);

        EmailTracking emailTracking1 = new EmailTracking();
        emailTracking.setId(2L);
        emailTracking1.setCronJob(cronJob);
        emailTracking1.setRecipient(recipient1);
        emailTracking1.setStatus(EmailStatus.SENT);
        emailTracking1.setDeleted(false);

        EmailTracking emailTracking2 = new EmailTracking();
        emailTracking.setId(3L);
        emailTracking2.setCronJob(cronJob);
        emailTracking2.setRecipient(recipient2);
        emailTracking2.setArchived(true);
        emailTracking2.setStatus(EmailStatus.SENT);
        emailTracking2.setDeleted(false);

        EmailTracking emailTracking3 = new EmailTracking();
        emailTracking.setId(4L);
        emailTracking3.setCronJob(cronJob);
        emailTracking3.setRecipient(recipient);
        emailTracking3.setArchived(true);
        emailTracking3.setStatus(EmailStatus.SENT);
        emailTracking3.setDeleted(false);

        EmailTracking emailTracking4 = new EmailTracking();
        emailTracking.setId(5L);
        emailTracking4.setCronJob(cronJob);
        emailTracking4.setRecipient(recipient1);
        emailTracking4.setArchived(true);
        emailTracking4.setStatus(EmailStatus.SENT);
        emailTracking4.setDeleted(false);

        EmailTracking emailTracking5 = new EmailTracking();
        emailTracking.setId(6L);
        emailTracking5.setCronJob(cronJob);
        emailTracking5.setRecipient(recipient2);
        emailTracking5.setStatus(EmailStatus.SENT);
        emailTracking5.setDeleted(false);

        EmailTracking emailTracking6 = new EmailTracking();
        emailTracking.setId(7L);
        emailTracking6.setCronJob(cronJob);
        emailTracking6.setRecipient(recipient);
        emailTracking6.setStatus(EmailStatus.SENT);
        emailTracking6.setDeleted(false);

        EmailTracking emailTracking7 = new EmailTracking();
        emailTracking.setId(8L);
        emailTracking7.setCronJob(cronJob);
        emailTracking7.setRecipient(recipient1);
        emailTracking7.setStatus(EmailStatus.SENT);
        emailTracking7.setDeleted(false);

        EmailTracking emailTracking8 = new EmailTracking();
        emailTracking.setId(9L);
        emailTracking8.setCronJob(cronJob);
        emailTracking8.setRecipient(recipient2);
        emailTracking8.setStatus(EmailStatus.SENT);
        emailTracking8.setDeleted(false);

        EmailTracking emailTracking9 = new EmailTracking();
        emailTracking.setId(10L);
        emailTracking9.setCronJob(cronJob);
        emailTracking9.setRecipient(recipient1);
        emailTracking9.setStatus(EmailStatus.SENT);
        emailTracking9.setDeleted(false);

        List<EmailTracking> list = new ArrayList<>();
        list.add(emailTracking);
        list.add(emailTracking1);
        list.add(emailTracking2);
        list.add(emailTracking3);
        list.add(emailTracking4);
        list.add(emailTracking5);
        list.add(emailTracking6);
        list.add(emailTracking7);
        list.add(emailTracking8);
        list.add(emailTracking9);
        emailTrackingRepository.saveAll(list);

        Pageable pageable = PageRequest.of(0, 20, Sort.by(EmailTracking_.ID).ascending());

        Page<EmailTrackingDTO> result = emailTrackingService.getListOfEmailTracking(pageable, null, "success", true);
        assertThat(result.getContent().size()).isEqualTo(1);
        assertThat(result.getContent().get(0).getRecipient().getTitle()).isEqualTo("sv");
        assertThat(result.getContent().get(0).getRecipient().getCounty()).isEqualTo("success");
    }

}
