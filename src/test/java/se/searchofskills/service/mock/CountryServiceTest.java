package se.searchofskills.service.mock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.searchofskills.domain.Country;
import se.searchofskills.repository.CountryRepository;
import se.searchofskills.service.CountryService;
import se.searchofskills.service.UserACL;
import se.searchofskills.service.dto.country.CountryCreateDTO;
import se.searchofskills.service.dto.country.CountryOutputDTO;
import se.searchofskills.service.dto.country.CountryUpdateDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.ResourceNotFoundException;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class CountryServiceTest {
    @Mock
    UserACL userACL;

    @Mock
    CountryRepository countryRepository;

    @InjectMocks
    CountryService countryService;

    @Test
    public void testCreateWithNotPermission() {
        given(userACL.isAdmin()).willReturn(false);
        CountryCreateDTO countryCreateDTO = new CountryCreateDTO();

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> countryService.create(countryCreateDTO));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testCreateWithCountryNameIsBlank() {
        given(userACL.isAdmin()).willReturn(true);
        CountryCreateDTO countryCreateDTO = new CountryCreateDTO();
        countryCreateDTO.setName("    ");

        BadRequestException result = assertThrows(BadRequestException.class, () -> countryService.create(countryCreateDTO));
        assertThat(result.getMessage()).isEqualTo("error.nameEmptyOrBlank");
    }

    @Test
    public void testCreateWithCountryExisted() {
        given(userACL.isAdmin()).willReturn(true);
        CountryCreateDTO countryCreateDTO = new CountryCreateDTO();
        countryCreateDTO.setName("japan");

        Country country = new Country();
        country.setId(1L);
        country.setName("Japan");
        given(countryRepository.findByName("japan")).willReturn(Optional.of(country));

        BadRequestException result = assertThrows(BadRequestException.class, () -> countryService.create(countryCreateDTO));
        assertThat(result.getMessage()).isEqualTo("error.countryNameExisted");
    }

    @Test
    public void testUpdateWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);
        CountryUpdateDTO countryUpdateDTO = new CountryUpdateDTO();
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> countryService.update(countryUpdateDTO));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testUpdateCountryBlankOrEmpty() {
        given(userACL.isAdmin()).willReturn(true);
        CountryUpdateDTO businessAreaUpdateDTO = new CountryUpdateDTO();
        businessAreaUpdateDTO.setId(1L);
        businessAreaUpdateDTO.setName("     ");

        Country country = new Country();
        country.setId(1L);
        country.setName("viet nam");

        given(countryRepository.findById(1L)).willReturn(Optional.of(country));

        BadRequestException result = assertThrows(BadRequestException.class, () -> countryService.update(businessAreaUpdateDTO));
        assertThat(result.getMessage()).isEqualTo("error.nameEmptyOrBlank");
    }

    @Test
    public void testUpdateCountryNotExisted() {
        given(userACL.isAdmin()).willReturn(true);
        CountryUpdateDTO countryUpdateDTO = new CountryUpdateDTO();
        countryUpdateDTO.setId(1L);
        countryUpdateDTO.setName("viet nam");

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> countryService.update(countryUpdateDTO));
        assertThat(result.getMessage()).isEqualTo("error.countryNotFound");
    }

    @Test
    public void testUpdateCountryNameExisted() {
        given(userACL.isAdmin()).willReturn(true);
        CountryUpdateDTO countryUpdateDTO = new CountryUpdateDTO();
        countryUpdateDTO.setId(2L);
        countryUpdateDTO.setName("viet nam");

        Country country1 = new Country();
        country1.setId(2L);
        country1.setName("japan");
        given(countryRepository.findById(2L)).willReturn(Optional.of(country1));

        Country country = new Country();
        country.setId(1L);
        country.setName("Viet nam");

        given(countryRepository.findByName("viet nam")).willReturn(Optional.of(country));
        BadRequestException result = assertThrows(BadRequestException.class, () -> countryService.update(countryUpdateDTO));
        assertThat(result.getMessage()).isEqualTo("error.countryNameExisted");
    }

    @Test
    public void testUpdateSuccessfully() {
        given(userACL.isAdmin()).willReturn(true);
        CountryUpdateDTO countryUpdateDTO = new CountryUpdateDTO();
        countryUpdateDTO.setId(1L);
        countryUpdateDTO.setName("viet nam");

        Country country = new Country();
        country.setId(1L);
        country.setName("japan");

        given(countryRepository.findById(1L)).willReturn(Optional.of(country));

        CountryOutputDTO result = countryService.update(countryUpdateDTO);
        assertThat(result.getName()).isEqualTo("viet nam");
    }

    @Test
    public void testDeleteWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> countryService.delete(1L));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testDeleteCountryNotExisted() {
        given(userACL.isAdmin()).willReturn(true);

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> countryService.delete(1L));
        assertThat(result.getMessage()).isEqualTo("error.countryNotFound");
    }
}
