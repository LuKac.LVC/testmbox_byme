package se.searchofskills.service.mock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.searchofskills.domain.ManagerialPosition;
import se.searchofskills.repository.ManagerialPositionRepository;
import se.searchofskills.service.ManagerialPositionService;
import se.searchofskills.service.UserACL;
import se.searchofskills.service.dto.managerial_position.ManagerialPositionCreateDTO;
import se.searchofskills.service.dto.managerial_position.ManagerialPositionOutputDTO;
import se.searchofskills.service.dto.managerial_position.ManagerialPositionUpdateDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.ResourceNotFoundException;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class ManagerialPositionServiceTest {
    @Mock
    ManagerialPositionRepository managerialPositionRepository;

    @Mock
    UserACL userACL;

    @InjectMocks
    ManagerialPositionService managerialPositionService;

    @Test
    public void testCreateWithNotPermission() {
        given(userACL.isAdmin()).willReturn(false);
        ManagerialPositionCreateDTO managerialPositionCreateDTO = new ManagerialPositionCreateDTO();

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> managerialPositionService.create(managerialPositionCreateDTO));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testCreateWithManagerialNameIsNull() {
        given(userACL.isAdmin()).willReturn(true);
        ManagerialPositionCreateDTO managerialPositionCreateDTO = new ManagerialPositionCreateDTO();
        managerialPositionCreateDTO.setNames(Arrays.asList("    "));

        BadRequestException result = assertThrows(BadRequestException.class, () -> managerialPositionService.create(managerialPositionCreateDTO));
        assertThat(result.getMessage()).isEqualTo("error.listNameEmpty");
    }

    @Test
    public void testUpdateWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);
        ManagerialPositionUpdateDTO managerialPositionUpdateDTO = new ManagerialPositionUpdateDTO();
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> managerialPositionService.update(managerialPositionUpdateDTO));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testUpdateManagerialNameBlankOrEmpty() {
        given(userACL.isAdmin()).willReturn(true);
        ManagerialPositionUpdateDTO managerialPositionUpdateDTO = new ManagerialPositionUpdateDTO();
        managerialPositionUpdateDTO.setId(1L);
        managerialPositionUpdateDTO.setName("     ");

        ManagerialPosition managerialPosition = new ManagerialPosition();
        managerialPosition.setId(1L);
        managerialPosition.setName("phong");

        given(managerialPositionRepository.findById(1L)).willReturn(Optional.of(managerialPosition));

        BadRequestException result = assertThrows(BadRequestException.class, () -> managerialPositionService.update(managerialPositionUpdateDTO));
        assertThat(result.getMessage()).isEqualTo("error.nameEmptyOrBlank");
    }

    @Test
    public void testUpdateManagerialNotExisted() {
        given(userACL.isAdmin()).willReturn(true);
        ManagerialPositionUpdateDTO managerialPositionUpdateDTO = new ManagerialPositionUpdateDTO();
        managerialPositionUpdateDTO.setId(1L);
        managerialPositionUpdateDTO.setName("phong");

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> managerialPositionService.update(managerialPositionUpdateDTO));
        assertThat(result.getMessage()).isEqualTo("error.managerialPositionNotFound");
    }

    @Test
    public void testUpdateSuccessfully() {
        given(userACL.isAdmin()).willReturn(true);
        ManagerialPositionUpdateDTO managerialPositionUpdateDTO = new ManagerialPositionUpdateDTO();
        managerialPositionUpdateDTO.setId(1L);
        managerialPositionUpdateDTO.setName("nam");

        ManagerialPosition managerialPosition = new ManagerialPosition();
        managerialPosition.setId(1L);
        managerialPosition.setName("phong");

        given(managerialPositionRepository.findById(1L)).willReturn(Optional.of(managerialPosition));

        ManagerialPositionOutputDTO result = managerialPositionService.update(managerialPositionUpdateDTO);
        assertThat(result.getName()).isEqualTo("nam");
    }

    @Test
    public void testDeleteWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> managerialPositionService.delete(1L));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testDeleteManagerialNotExisted() {
        given(userACL.isAdmin()).willReturn(true);

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> managerialPositionService.delete(1L));
        assertThat(result.getMessage()).isEqualTo("error.managerialPositionNotFound");
    }
}
