package se.searchofskills.service.mock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.searchofskills.domain.Category;
import se.searchofskills.domain.Employer;
import se.searchofskills.repository.EmployerRepository;
import se.searchofskills.service.EmployerService;
import se.searchofskills.service.UserACL;
import se.searchofskills.service.dto.category.CategoryCreatedDTO;
import se.searchofskills.service.dto.category.CategoryUpdateDTO;
import se.searchofskills.service.dto.employer.EmployerCreateDTO;
import se.searchofskills.service.dto.employer.EmployerOutputDTO;
import se.searchofskills.service.dto.employer.EmployerUpdateDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.ResourceNotFoundException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class EmployerServiceTest {
    @Mock
    EmployerRepository employerRepository;

    @Mock
    UserACL userACL;

    @InjectMocks
    EmployerService employerService;

    @Test
    public void testCreateWithNotPermission() {
        given(userACL.isAdmin()).willReturn(false);
        EmployerCreateDTO employerCreateDTO = new EmployerCreateDTO();

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> employerService.create(employerCreateDTO));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testCreateWithEmployerNameIsNull() {
        given(userACL.isAdmin()).willReturn(true);
        EmployerCreateDTO employerCreateDTO = new EmployerCreateDTO();
        employerCreateDTO.setNames(Arrays.asList("    "));

        BadRequestException result = assertThrows(BadRequestException.class, () -> employerService.create(employerCreateDTO));
        assertThat(result.getMessage()).isEqualTo("error.listNameEmpty");
    }

    @Test
    public void testUpdateWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);
        EmployerUpdateDTO employerUpdateDTO = new EmployerUpdateDTO();
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> employerService.update(employerUpdateDTO));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testUpdateNameBlankOrEmpty() {
        given(userACL.isAdmin()).willReturn(true);
        EmployerUpdateDTO employerUpdateDTO = new EmployerUpdateDTO();
        employerUpdateDTO.setId(1L);
        employerUpdateDTO.setName("     ");

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("phong");

        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));

        BadRequestException result = assertThrows(BadRequestException.class, () -> employerService.update(employerUpdateDTO));
        assertThat(result.getMessage()).isEqualTo("error.nameEmptyOrBlank");
    }

    @Test
    public void testUpdateEmployerNotExisted() {
        given(userACL.isAdmin()).willReturn(true);
        EmployerUpdateDTO employerUpdateDTO = new EmployerUpdateDTO();
        employerUpdateDTO.setId(1L);
        employerUpdateDTO.setName("phong");

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> employerService.update(employerUpdateDTO));
        assertThat(result.getMessage()).isEqualTo("error.employerNotFound");
    }

    @Test
    public void testUpdateSuccessfully() {
        given(userACL.isAdmin()).willReturn(true);
        EmployerUpdateDTO employerUpdateDTO = new EmployerUpdateDTO();
        employerUpdateDTO.setId(1L);
        employerUpdateDTO.setName("nam");

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("phong");

        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));

        EmployerOutputDTO result = employerService.update(employerUpdateDTO);
        assertThat(result.getName()).isEqualTo("nam");
    }

    @Test
    public void testDeleteWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> employerService.delete(1L));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testDeleteEmployerNotExisted() {
        given(userACL.isAdmin()).willReturn(true);

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> employerService.delete(1L));
        assertThat(result.getMessage()).isEqualTo("error.employerNotFound");
    }
}
