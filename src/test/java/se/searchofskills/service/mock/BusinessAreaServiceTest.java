package se.searchofskills.service.mock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.searchofskills.domain.BusinessArea;
import se.searchofskills.repository.BusinessAreaRepository;
import se.searchofskills.service.BusinessAreaService;
import se.searchofskills.service.UserACL;
import se.searchofskills.service.dto.business_area.BusinessAreaCreateDTO;
import se.searchofskills.service.dto.business_area.BusinessAreaOutputDTO;
import se.searchofskills.service.dto.business_area.BusinessAreaUpdateDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.ResourceNotFoundException;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class BusinessAreaServiceTest {
    @Mock
    BusinessAreaRepository businessAreaRepository;

    @Mock
    UserACL userACL;

    @InjectMocks
    BusinessAreaService businessAreaService;

    @Test
    public void testCreateWithNotPermission() {
        given(userACL.isAdmin()).willReturn(false);
        BusinessAreaCreateDTO businessAreaCreateDTO = new BusinessAreaCreateDTO();

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> businessAreaService.create(businessAreaCreateDTO));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testCreateWithManagerialNameIsNull() {
        given(userACL.isAdmin()).willReturn(true);
        BusinessAreaCreateDTO businessAreaCreateDTO = new BusinessAreaCreateDTO();
        businessAreaCreateDTO.setNames(Arrays.asList("    "));

        BadRequestException result = assertThrows(BadRequestException.class, () -> businessAreaService.create(businessAreaCreateDTO));
        assertThat(result.getMessage()).isEqualTo("error.listNameEmpty");
    }

    @Test
    public void testUpdateWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);
        BusinessAreaUpdateDTO businessAreaUpdateDTO = new BusinessAreaUpdateDTO();
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> businessAreaService.update(businessAreaUpdateDTO));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testUpdateManagerialNameBlankOrEmpty() {
        given(userACL.isAdmin()).willReturn(true);
        BusinessAreaUpdateDTO businessAreaUpdateDTO = new BusinessAreaUpdateDTO();
        businessAreaUpdateDTO.setId(1L);
        businessAreaUpdateDTO.setName("     ");

        BusinessArea businessArea = new BusinessArea();
        businessArea.setId(1L);
        businessArea.setName("phong");

        given(businessAreaRepository.findById(1L)).willReturn(Optional.of(businessArea));

        BadRequestException result = assertThrows(BadRequestException.class, () -> businessAreaService.update(businessAreaUpdateDTO));
        assertThat(result.getMessage()).isEqualTo("error.nameEmptyOrBlank");
    }

    @Test
    public void testUpdateManagerialNotExisted() {
        given(userACL.isAdmin()).willReturn(true);
        BusinessAreaUpdateDTO businessAreaUpdateDTO = new BusinessAreaUpdateDTO();
        businessAreaUpdateDTO.setId(1L);
        businessAreaUpdateDTO.setName("phong");

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> businessAreaService.update(businessAreaUpdateDTO));
        assertThat(result.getMessage()).isEqualTo("error.businessAreaNotFound");
    }

    @Test
    public void testUpdateSuccessfully() {
        given(userACL.isAdmin()).willReturn(true);
        BusinessAreaUpdateDTO businessAreaUpdateDTO = new BusinessAreaUpdateDTO();
        businessAreaUpdateDTO.setId(1L);
        businessAreaUpdateDTO.setName("nam");

        BusinessArea businessArea = new BusinessArea();
        businessArea.setId(1L);
        businessArea.setName("phong");

        given(businessAreaRepository.findById(1L)).willReturn(Optional.of(businessArea));

        BusinessAreaOutputDTO result = businessAreaService.update(businessAreaUpdateDTO);
        assertThat(result.getName()).isEqualTo("nam");
    }

    @Test
    public void testDeleteWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> businessAreaService.delete(1L));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testDeleteManagerialNotExisted() {
        given(userACL.isAdmin()).willReturn(true);

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> businessAreaService.delete(1L));
        assertThat(result.getMessage()).isEqualTo("error.businessAreaNotFound");
    }
}
