package se.searchofskills.service.mock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.searchofskills.domain.*;
import se.searchofskills.domain.enumeration.Gender;
import se.searchofskills.repository.*;
import se.searchofskills.service.CandidateService;
import se.searchofskills.service.UserACL;
import se.searchofskills.service.dto.candidate.CandidateCreateInputDTO;
import se.searchofskills.service.dto.candidate.CandidateOutputDTO;
import se.searchofskills.service.dto.candidate.CandidateUpdateInputDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.NotFoundException;
import se.searchofskills.service.error.ResourceNotFoundException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class CandidateServiceTest {
    @Mock
    CandidateEmployerRepository candidateEmployerRepository;

    @Mock
    CandidateRepository candidateRepository;

    @Mock
    CandidateBusinessAreaRepository candidateBusinessAreaRepository;

    @Mock
    CandidateManagerialPositionRepository candidateManagerialPositionRepository;

    @Mock
    CountryRepository countryRepository;

    @Mock
    RegionRepository regionRepository;

    @Mock
    UserACL userACL;

    @Mock
    CvRepository cvRepository;

    @Mock
    ManagerialPositionRepository managerialPositionRepository;

    @Mock
    BusinessAreaRepository businessAreaRepository;

    @Mock
    EmployerRepository employerRepository;

    @InjectMocks
    CandidateService candidateService;

    @Test
    public void testSaveCandidateWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        //when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testSaveCandidateWithEmployerListEmpty() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        candidateCreateInputDTO.setEmployerIds(employerIds);
        //when
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.employerListEmpty");
    }

    @Test
    public void testSaveCandidateWithEmployerNotFound() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = Arrays.asList(1L);
        candidateCreateInputDTO.setEmployerIds(employerIds);
        //when
        NotFoundException result = assertThrows(NotFoundException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.employerNotFound: [1]");
    }

    @Test
    public void testSaveCandidateWithBusinessListEmpty() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        employerIds.add(1L);
        List<Long> businessAreasIds = new ArrayList<>();
        candidateCreateInputDTO.setEmployerIds(employerIds);
        candidateCreateInputDTO.setBusinessAreaIds(businessAreasIds);

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("tech");

        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));
        //when
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.businessAreasEmpty");
    }

    @Test
    public void testSaveCandidateWithBusinessNotFound() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        employerIds.add(1L);
        List<Long> businessAreasIds = new ArrayList<>();
        businessAreasIds.add(1L);
        candidateCreateInputDTO.setEmployerIds(employerIds);
        candidateCreateInputDTO.setBusinessAreaIds(businessAreasIds);

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("tech");

        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));
        //when
        NotFoundException result = assertThrows(NotFoundException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.businessAreaNotFound: [1]");
    }

    @Test
    public void testCreateCandidateWithManagerListEmpty() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        employerIds.add(1L);
        List<Long> businessAreasIds = new ArrayList<>();
        businessAreasIds.add(1L);
        List<Long> managerialPostionIds = new ArrayList<>();
        candidateCreateInputDTO.setEmployerIds(employerIds);
        candidateCreateInputDTO.setBusinessAreaIds(businessAreasIds);
        candidateCreateInputDTO.setManagerialPositionIds(managerialPostionIds);

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("tech");

        BusinessArea businessArea = new BusinessArea();
        businessArea.setId(1L);
        businessArea.setName("hn");
        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));
        given(businessAreaRepository.findById(1L)).willReturn(Optional.of(businessArea));
        //when
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.managerialPositionListEmpty");
    }

    @Test
    public void testCreateCandidateWithManagerNotFound() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        employerIds.add(1L);
        List<Long> businessAreasIds = new ArrayList<>();
        businessAreasIds.add(1L);
        List<Long> managerialPostionIds = new ArrayList<>();
        managerialPostionIds.add(1L);
        candidateCreateInputDTO.setEmployerIds(employerIds);
        candidateCreateInputDTO.setBusinessAreaIds(businessAreasIds);
        candidateCreateInputDTO.setManagerialPositionIds(managerialPostionIds);

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("tech");

        BusinessArea businessArea = new BusinessArea();
        businessArea.setId(1L);
        businessArea.setName("hn");
        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));
        given(businessAreaRepository.findById(1L)).willReturn(Optional.of(businessArea));
        //when
        NotFoundException result = assertThrows(NotFoundException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.managerialPositionNotFound: [1]");
    }

    @Test
    public void testCreateCandidateWithFirstNameBlank() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        employerIds.add(1L);
        List<Long> businessAreasIds = new ArrayList<>();
        businessAreasIds.add(1L);
        List<Long> managerialPostionIds = new ArrayList<>();
        managerialPostionIds.add(1L);
        candidateCreateInputDTO.setEmployerIds(employerIds);
        candidateCreateInputDTO.setBusinessAreaIds(businessAreasIds);
        candidateCreateInputDTO.setManagerialPositionIds(managerialPostionIds);
        candidateCreateInputDTO.setFirstName("     ");

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("tech");

        BusinessArea businessArea = new BusinessArea();
        businessArea.setId(1L);
        businessArea.setName("hn");

        ManagerialPosition managerialPosition = new ManagerialPosition();
        managerialPosition.setId(1L);
        managerialPosition.setName("dev");

        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));
        given(businessAreaRepository.findById(1L)).willReturn(Optional.of(businessArea));
        given(managerialPositionRepository.findById(1L)).willReturn(Optional.of(managerialPosition));
        //when
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.firstNameEmptyOrBlank");
    }

    @Test
    public void testCreateCandidateWithLastNameBlank() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        employerIds.add(1L);
        List<Long> businessAreasIds = new ArrayList<>();
        businessAreasIds.add(1L);
        List<Long> managerialPostionIds = new ArrayList<>();
        managerialPostionIds.add(1L);
        candidateCreateInputDTO.setEmployerIds(employerIds);
        candidateCreateInputDTO.setBusinessAreaIds(businessAreasIds);
        candidateCreateInputDTO.setManagerialPositionIds(managerialPostionIds);
        candidateCreateInputDTO.setFirstName("phong");
        candidateCreateInputDTO.setLastName("    ");

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("tech");

        BusinessArea businessArea = new BusinessArea();
        businessArea.setId(1L);
        businessArea.setName("hn");

        ManagerialPosition managerialPosition = new ManagerialPosition();
        managerialPosition.setId(1L);
        managerialPosition.setName("dev");

        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));
        given(businessAreaRepository.findById(1L)).willReturn(Optional.of(businessArea));
        given(managerialPositionRepository.findById(1L)).willReturn(Optional.of(managerialPosition));
        //when
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.lastNameEmptyOrBlank");
    }

    @Test
    public void testCreateCandidateWithBirthYearInvalid() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        employerIds.add(1L);
        List<Long> businessAreasIds = new ArrayList<>();
        businessAreasIds.add(1L);
        List<Long> managerialPostionIds = new ArrayList<>();
        managerialPostionIds.add(1L);
        candidateCreateInputDTO.setEmployerIds(employerIds);
        candidateCreateInputDTO.setBusinessAreaIds(businessAreasIds);
        candidateCreateInputDTO.setManagerialPositionIds(managerialPostionIds);
        candidateCreateInputDTO.setFirstName("phong");
        candidateCreateInputDTO.setLastName("tran");
        candidateCreateInputDTO.setBirthYear("2k");

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("tech");

        BusinessArea businessArea = new BusinessArea();
        businessArea.setId(1L);
        businessArea.setName("hn");

        ManagerialPosition managerialPosition = new ManagerialPosition();
        managerialPosition.setId(1L);
        managerialPosition.setName("dev");

        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));
        given(businessAreaRepository.findById(1L)).willReturn(Optional.of(businessArea));
        given(managerialPositionRepository.findById(1L)).willReturn(Optional.of(managerialPosition));
        //when
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.birthYearInvalid");
    }

    @Test
    public void testCreateCandidateWithPersonalNumberInvalid() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        employerIds.add(1L);
        List<Long> businessAreasIds = new ArrayList<>();
        businessAreasIds.add(1L);
        List<Long> managerialPostionIds = new ArrayList<>();
        managerialPostionIds.add(1L);
        candidateCreateInputDTO.setEmployerIds(employerIds);
        candidateCreateInputDTO.setBusinessAreaIds(businessAreasIds);
        candidateCreateInputDTO.setManagerialPositionIds(managerialPostionIds);
        candidateCreateInputDTO.setFirstName("phong");
        candidateCreateInputDTO.setLastName("tran");
        candidateCreateInputDTO.setBirthYear("2000");
        candidateCreateInputDTO.setPersonalNumber("asfgeghthdgsghd");

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("tech");

        BusinessArea businessArea = new BusinessArea();
        businessArea.setId(1L);
        businessArea.setName("hn");

        ManagerialPosition managerialPosition = new ManagerialPosition();
        managerialPosition.setId(1L);
        managerialPosition.setName("dev");

        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));
        given(businessAreaRepository.findById(1L)).willReturn(Optional.of(businessArea));
        given(managerialPositionRepository.findById(1L)).willReturn(Optional.of(managerialPosition));
        //when
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.personalNumberInvalid");
    }

    @Test
    public void testCreateCandidateWithPhoneNumberInvalid() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        employerIds.add(1L);
        List<Long> businessAreasIds = new ArrayList<>();
        businessAreasIds.add(1L);
        List<Long> managerialPostionIds = new ArrayList<>();
        managerialPostionIds.add(1L);
        candidateCreateInputDTO.setEmployerIds(employerIds);
        candidateCreateInputDTO.setBusinessAreaIds(businessAreasIds);
        candidateCreateInputDTO.setManagerialPositionIds(managerialPostionIds);
        candidateCreateInputDTO.setFirstName("phong");
        candidateCreateInputDTO.setLastName("tran");
        candidateCreateInputDTO.setBirthYear("2000");
        candidateCreateInputDTO.setPersonalNumber("123456789012");
        candidateCreateInputDTO.setPhoneNumber("012345566a");

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("tech");

        BusinessArea businessArea = new BusinessArea();
        businessArea.setId(1L);
        businessArea.setName("hn");

        ManagerialPosition managerialPosition = new ManagerialPosition();
        managerialPosition.setId(1L);
        managerialPosition.setName("dev");

        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));
        given(businessAreaRepository.findById(1L)).willReturn(Optional.of(businessArea));
        given(managerialPositionRepository.findById(1L)).willReturn(Optional.of(managerialPosition));
        //when
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.phoneNumberInvalid");
    }

    @Test
    public void testCreateCandidateWithSecondPhoneNumberInvalid() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        employerIds.add(1L);
        List<Long> businessAreasIds = new ArrayList<>();
        businessAreasIds.add(1L);
        List<Long> managerialPostionIds = new ArrayList<>();
        managerialPostionIds.add(1L);
        candidateCreateInputDTO.setEmployerIds(employerIds);
        candidateCreateInputDTO.setBusinessAreaIds(businessAreasIds);
        candidateCreateInputDTO.setManagerialPositionIds(managerialPostionIds);
        candidateCreateInputDTO.setFirstName("phong");
        candidateCreateInputDTO.setLastName("tran");
        candidateCreateInputDTO.setBirthYear("2000");
        candidateCreateInputDTO.setPersonalNumber("123456789012");
        candidateCreateInputDTO.setPhoneNumber("012345566");
        candidateCreateInputDTO.setSecondPhoneNumber("  ");

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("tech");

        BusinessArea businessArea = new BusinessArea();
        businessArea.setId(1L);
        businessArea.setName("hn");

        ManagerialPosition managerialPosition = new ManagerialPosition();
        managerialPosition.setId(1L);
        managerialPosition.setName("dev");

        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));
        given(businessAreaRepository.findById(1L)).willReturn(Optional.of(businessArea));
        given(managerialPositionRepository.findById(1L)).willReturn(Optional.of(managerialPosition));
        //when
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.secondPhoneNumberInvalid");
    }

    @Test
    public void testCreateCandidateWithEmailPrivateInvalid() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        employerIds.add(1L);
        List<Long> businessAreasIds = new ArrayList<>();
        businessAreasIds.add(1L);
        List<Long> managerialPostionIds = new ArrayList<>();
        managerialPostionIds.add(1L);
        candidateCreateInputDTO.setEmployerIds(employerIds);
        candidateCreateInputDTO.setBusinessAreaIds(businessAreasIds);
        candidateCreateInputDTO.setManagerialPositionIds(managerialPostionIds);
        candidateCreateInputDTO.setFirstName("phong");
        candidateCreateInputDTO.setLastName("tran");
        candidateCreateInputDTO.setBirthYear("2000");
        candidateCreateInputDTO.setPersonalNumber("123456789012");
        candidateCreateInputDTO.setPhoneNumber("012345566");
        candidateCreateInputDTO.setSecondPhoneNumber("012345566");
        candidateCreateInputDTO.setEmailPrivate("phong.com");

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("tech");

        BusinessArea businessArea = new BusinessArea();
        businessArea.setId(1L);
        businessArea.setName("hn");

        ManagerialPosition managerialPosition = new ManagerialPosition();
        managerialPosition.setId(1L);
        managerialPosition.setName("dev");

        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));
        given(businessAreaRepository.findById(1L)).willReturn(Optional.of(businessArea));
        given(managerialPositionRepository.findById(1L)).willReturn(Optional.of(managerialPosition));
        //when
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.emailPrivateInvalid");
    }

    @Test
    public void testCreateCandidateWithEmailWorkInvalid() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        employerIds.add(1L);
        List<Long> businessAreasIds = new ArrayList<>();
        businessAreasIds.add(1L);
        List<Long> managerialPostionIds = new ArrayList<>();
        managerialPostionIds.add(1L);
        candidateCreateInputDTO.setEmployerIds(employerIds);
        candidateCreateInputDTO.setBusinessAreaIds(businessAreasIds);
        candidateCreateInputDTO.setManagerialPositionIds(managerialPostionIds);
        candidateCreateInputDTO.setFirstName("phong");
        candidateCreateInputDTO.setLastName("tran");
        candidateCreateInputDTO.setBirthYear("2000");
        candidateCreateInputDTO.setPersonalNumber("123456789012");
        candidateCreateInputDTO.setPhoneNumber("012345566");
        candidateCreateInputDTO.setSecondPhoneNumber("012345566");
        candidateCreateInputDTO.setEmailPrivate("phongtrantph@gmail.com");
        candidateCreateInputDTO.setEmailWork("phongtechlead.vn");

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("tech");

        BusinessArea businessArea = new BusinessArea();
        businessArea.setId(1L);
        businessArea.setName("hn");

        ManagerialPosition managerialPosition = new ManagerialPosition();
        managerialPosition.setId(1L);
        managerialPosition.setName("dev");

        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));
        given(businessAreaRepository.findById(1L)).willReturn(Optional.of(businessArea));
        given(managerialPositionRepository.findById(1L)).willReturn(Optional.of(managerialPosition));
        //when
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.emailWorkInvalid");
    }

    @Test
    public void testCreateCandidateWithRegionNotFound() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        employerIds.add(1L);
        List<Long> businessAreasIds = new ArrayList<>();
        businessAreasIds.add(1L);
        List<Long> managerialPostionIds = new ArrayList<>();
        managerialPostionIds.add(1L);
        candidateCreateInputDTO.setEmployerIds(employerIds);
        candidateCreateInputDTO.setBusinessAreaIds(businessAreasIds);
        candidateCreateInputDTO.setManagerialPositionIds(managerialPostionIds);
        candidateCreateInputDTO.setFirstName("phong");
        candidateCreateInputDTO.setLastName("tran");
        candidateCreateInputDTO.setBirthYear("2000");
        candidateCreateInputDTO.setPersonalNumber("123456789012");
        candidateCreateInputDTO.setPhoneNumber("012345566");
        candidateCreateInputDTO.setSecondPhoneNumber("012345566");
        candidateCreateInputDTO.setEmailPrivate("phongtrantph@gmail.com");
        candidateCreateInputDTO.setEmailWork("phong@techlead.vn");
        candidateCreateInputDTO.setCountryId(1L);

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("tech");

        BusinessArea businessArea = new BusinessArea();
        businessArea.setId(1L);
        businessArea.setName("hn");

        ManagerialPosition managerialPosition = new ManagerialPosition();
        managerialPosition.setId(1L);
        managerialPosition.setName("dev");

        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));
        given(businessAreaRepository.findById(1L)).willReturn(Optional.of(businessArea));
        given(managerialPositionRepository.findById(1L)).willReturn(Optional.of(managerialPosition));
        //when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.countryNotFound");
    }

    @Test
    public void testCreateCandidateWithCountryNotFound() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateCreateInputDTO candidateCreateInputDTO = new CandidateCreateInputDTO();
        List<Long> employerIds = new ArrayList<>();
        employerIds.add(1L);
        List<Long> businessAreasIds = new ArrayList<>();
        businessAreasIds.add(1L);
        List<Long> managerialPostionIds = new ArrayList<>();
        managerialPostionIds.add(1L);
        candidateCreateInputDTO.setEmployerIds(employerIds);
        candidateCreateInputDTO.setBusinessAreaIds(businessAreasIds);
        candidateCreateInputDTO.setManagerialPositionIds(managerialPostionIds);
        candidateCreateInputDTO.setFirstName("phong");
        candidateCreateInputDTO.setLastName("tran");
        candidateCreateInputDTO.setBirthYear("2000");
        candidateCreateInputDTO.setPersonalNumber("123456789012");
        candidateCreateInputDTO.setPhoneNumber("012345566");
        candidateCreateInputDTO.setSecondPhoneNumber("012345566");
        candidateCreateInputDTO.setEmailPrivate("phongtrantph@gmail.com");
        candidateCreateInputDTO.setEmailWork("phong@techlead.vn");
        candidateCreateInputDTO.setRegionId(1L);
        candidateCreateInputDTO.setCountryId(1L);

        Employer employer = new Employer();
        employer.setId(1L);
        employer.setName("tech");

        BusinessArea businessArea = new BusinessArea();
        businessArea.setId(1L);
        businessArea.setName("hn");

        ManagerialPosition managerialPosition = new ManagerialPosition();
        managerialPosition.setId(1L);
        managerialPosition.setName("dev");

        Country country = new Country();
        country.setId(1L);
        country.setName("usa");


        given(employerRepository.findById(1L)).willReturn(Optional.of(employer));
        given(businessAreaRepository.findById(1L)).willReturn(Optional.of(businessArea));
        given(managerialPositionRepository.findById(1L)).willReturn(Optional.of(managerialPosition));
        given(countryRepository.findById(1L)).willReturn(Optional.of(country));
        //when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> candidateService.create(candidateCreateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.regionNotFound");
    }

    @Test
    public void testUpdateWithCandidateNotFound() {
        given(userACL.isAdmin()).willReturn(true);
        CandidateUpdateInputDTO candidateUpdateInputDTO = new CandidateUpdateInputDTO();
        candidateUpdateInputDTO.setId(1L);

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> candidateService.update(candidateUpdateInputDTO));
        //then
        assertThat(result.getMessage()).isEqualTo("error.candidateNotFound");
    }

    @Test
    public void deleteSuccessfully() {
        given(userACL.isAdmin()).willReturn(true);

        Candidate candidate = new Candidate();
        candidate.setId(1L);
        candidate.setDeleted(false);

        given(candidateRepository.findById(1L)).willReturn(Optional.of(candidate));
        //when
        candidateService.delete(1L);
        //then
        assertThat(candidate.isDeleted()).isTrue();

    }


}
