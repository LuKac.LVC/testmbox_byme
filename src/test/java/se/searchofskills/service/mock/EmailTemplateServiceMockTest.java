package se.searchofskills.service.mock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import se.searchofskills.domain.EmailTemplate;
import se.searchofskills.domain.enumeration.EmailTemplateType;
import se.searchofskills.repository.EmailTemplateRepository;
import se.searchofskills.service.EmailTemplateService;
import se.searchofskills.service.UserACL;
import se.searchofskills.service.dto.EmailTemplateDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.ResourceNotFoundException;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class EmailTemplateServiceMockTest {

    @InjectMocks
    EmailTemplateService emailTemplateService;

    @Mock
    EmailTemplateRepository emailTemplateRepository;

    @Mock
    UserACL userACL;

    /**
     * create email template from non admin permission
     */
    @Test
    public void testCreateOrUpdateEmailTemplateWithNonPermissionUser() {
        // given
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> emailTemplateService.createOrUpdate(new EmailTemplateDTO()));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * delete email template from non admin permission
     */
    @Test
    public void testDeleteEmailTemplateWithNonPermissionUser() {
        // given
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> emailTemplateService.deleteEmailTemplate(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * Create new email template success
     */
    @Test
    public void testCreateEmailTemplateSuccess() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        EmailTemplateDTO emailTemplateDTO = new EmailTemplateDTO();
        emailTemplateDTO.setContent("&lt;reference_number&gt;");
        emailTemplateDTO.setName("  Email  template  1 ");
        emailTemplateDTO.setType(EmailTemplateType.MAIN);

        // when
        EmailTemplateDTO result = emailTemplateService.createOrUpdate(emailTemplateDTO);

        // then
        assertThat(result.getContent()).isEqualTo("&lt;reference_number&gt;");
        assertThat(result.getName()).isEqualTo("Email template 1");
        assertThat(result.getType()).isEqualTo(EmailTemplateType.MAIN);
    }

    /**
     * Update email template from non exist data
     */
    @Test
    public void testUpdateEmailTemplateCaseNonExistData() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        EmailTemplateDTO emailTemplateDTO = new EmailTemplateDTO();
        emailTemplateDTO.setId(1L);
        emailTemplateDTO.setContent("<p>This is a text</p>");
        emailTemplateDTO.setName("Email template 1");
        emailTemplateDTO.setType(EmailTemplateType.MAIN);

        given(emailTemplateRepository.findById(eq(1L))).willReturn(Optional.empty());

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> emailTemplateService.createOrUpdate(emailTemplateDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    /**
     * Update email template success
     */
    @Test
    public void testUpdateEmailTemplateSuccess() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        // input
        EmailTemplateDTO emailTemplateDTO = new EmailTemplateDTO();
        emailTemplateDTO.setId(1L);
        emailTemplateDTO.setContent("&lt;reference_number&gt;");
        emailTemplateDTO.setName("Email template 1");
        emailTemplateDTO.setType(EmailTemplateType.MAIN);

        // exist email template
        EmailTemplate emailTemplate = new EmailTemplate();
        emailTemplate.setId(1L);
        emailTemplate.setContent("&lt;request_date&gt;");
        emailTemplate.setType(EmailTemplateType.REMINDER_1);
        emailTemplate.setName("some name");

        given(emailTemplateRepository.findById(eq(1L))).willReturn(Optional.of(emailTemplate));

        // when
        EmailTemplateDTO result = emailTemplateService.createOrUpdate(emailTemplateDTO);

        // then
        assertThat(result.getContent()).isEqualTo("&lt;reference_number&gt;");
        assertThat(result.getName()).isEqualTo("Email template 1");
        assertThat(result.getType()).isEqualTo(EmailTemplateType.MAIN);
    }

    /**
     * delete email template from non exist data
     */
    @Test
    public void testDeleteEmailTemplateCaseNonExistData() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> emailTemplateService.deleteEmailTemplate(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    /**
     * delete email template success
     */
    @Test
    public void testDeleteEmailTemplateSuccess() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        EmailTemplate emailTemplate = new EmailTemplate();
        emailTemplate.setId(1L);
        emailTemplate.setContent("some content");
        emailTemplate.setType(EmailTemplateType.REMINDER_1);
        emailTemplate.setName("some name");
        emailTemplate.setDeleted(false);

        given(emailTemplateRepository.findByIdAndDeletedIsFalse(eq(1L))).willReturn(Optional.of(emailTemplate));

        // when
        emailTemplateService.deleteEmailTemplate(1L);

        // then
        assertThat(emailTemplate.isDeleted()).isEqualTo(true);
    }

    /**
     * Find email templates by name
     */
    @Test
    public void testFindEmailTemplatesByName() {
        // given
        given(userACL.isAdmin()).willReturn(true);
        EmailTemplate emailTemplate = new EmailTemplate();
        emailTemplate.setName("Name");
        emailTemplate.setContent("Content");
        emailTemplate.setType(EmailTemplateType.MAIN);

        given(emailTemplateRepository.findAllByDeletedAndNameContainingIgnoreCase(PageRequest.of(1, 20), false, "a")).willReturn(new PageImpl<>(Collections.singletonList(emailTemplate)));

        // when
        Page<EmailTemplateDTO> page = emailTemplateService.findEmailTemplatesByName(PageRequest.of(1, 20), "a", null);

        // then
        assertThat(page.getContent().size()).isEqualTo(1);
        assertThat(page.getContent().get(0).getName().contains("a")).isTrue();
        assertThat(page.getContent().get(0).getContent().equals("Content")).isTrue();
    }

    /**
     * Find email templates without name params
     */
    @Test
    public void testFindEmailTemplatesWithoutNameParams() {
        // given
        given(userACL.isAdmin()).willReturn(true);
        EmailTemplate emailTemplate = new EmailTemplate();
        emailTemplate.setName("Name");
        emailTemplate.setContent("Content");
        emailTemplate.setType(EmailTemplateType.MAIN);

        given(emailTemplateRepository.findAllByDeleted(PageRequest.of(1, 20), false)).willReturn(new PageImpl<>(Collections.singletonList(emailTemplate)));

        // when
        Page<EmailTemplateDTO> page = emailTemplateService.findEmailTemplatesByName(PageRequest.of(1, 20), "", null);

        // then
        assertThat(page.getContent().size()).isEqualTo(1);
        assertThat(page.getContent().get(0).getName().equals("Name")).isTrue();
        assertThat(page.getContent().get(0).getContent().equals("Content")).isTrue();
    }

    /**
     * create email template with invalid default content of main email template
     */
    @Test
    public void testCreateOrUpdateInvalidDefaultContentOfMainEmailTemplate() {
        // given
        given(userACL.isAdmin()).willReturn(true);
        EmailTemplateDTO emailTemplateDTO = new EmailTemplateDTO();
        emailTemplateDTO.setContent("<p>This is a text</p>");
        emailTemplateDTO.setName("Email template 1");
        emailTemplateDTO.setType(EmailTemplateType.MAIN);

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTemplateService.createOrUpdate(emailTemplateDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.requiredContentMainTemplate");
    }

    /**
     * create email template with invalid default content of reminder email template
     */
    @Test
    public void testCreateOrUpdateInvalidDefaultContentOfReminderEmailTemplate() {
        // given
        given(userACL.isAdmin()).willReturn(true);
        EmailTemplateDTO emailTemplateDTO = new EmailTemplateDTO();
        emailTemplateDTO.setContent("<p>This is a text</p>");
        emailTemplateDTO.setName("Email template 1");
        emailTemplateDTO.setType(EmailTemplateType.REMINDER_1);

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTemplateService.createOrUpdate(emailTemplateDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.requiredContentReminderTemplate");
    }

    /**
     * Create new email template failed by all white spaces
     */
    @Test
    public void testCreateEmailTemplateFailedByAllWhiteSpaces() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        EmailTemplateDTO emailTemplateDTO = new EmailTemplateDTO();
        emailTemplateDTO.setContent("&lt;reference_number&gt;");
        emailTemplateDTO.setName("   ");
        emailTemplateDTO.setType(EmailTemplateType.MAIN);

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> emailTemplateService.createOrUpdate(emailTemplateDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.noWhiteSpace");
    }


}
