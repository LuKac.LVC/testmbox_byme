package se.searchofskills.service.mock;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import se.searchofskills.domain.Recipient;
import se.searchofskills.domain.RecipientList;
import se.searchofskills.domain.enumeration.CronJobStatus;
import se.searchofskills.repository.RecipientListRepository;
import se.searchofskills.repository.RecipientRepository;
import se.searchofskills.service.RecipientService;
import se.searchofskills.service.UserACL;
import se.searchofskills.service.dto.RecipientListDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.ResourceNotFoundException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class RecipientServiceMockTest {

    @InjectMocks
    RecipientService recipientService;

    @Mock
    RecipientRepository recipientRepository;

    @Mock
    RecipientListRepository recipientListRepository;

    @Mock
    UserACL userACL;

    /**
     * Import recipient list from non admin permission user
     */
    @Test
    public void testImportRecipientListWithNonAdmin() {
        // given
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> recipientService.importRecipientList(null, ""));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * Import recipient list from invalid excel file
     */
    @Test
    public void testImportRecipientListSuccess() throws IOException {
        // given
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel.xlsx");

        given(recipientListRepository.save(any(RecipientList.class))).willReturn(recipientList);

        // when
        List<Recipient> result = recipientService.importRecipientList(new FileInputStream(file), "testing_file.xlsx");

        // then
        assertThat(result.size()).isEqualTo(10);
        assertThat(result.get(0).getRecipientList().getId()).isEqualTo(1L);
        assertThat(result.get(0).getEmail()).isEqualTo("sebastian.fjall@vallentuna.se");
        assertThat(result.get(3).getReferenceNumber()).isEqualTo("TOP-2020/40");
    }

    /**
     * Import recipient list with an excel has redundant row
     */
    @Test
    public void testImportRecipientListCaseAddMoreRedundantColumn() throws IOException {
        // given
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_add_more_column.xlsx");

        given(recipientListRepository.save(any(RecipientList.class))).willReturn(recipientList);

        // when
        List<Recipient> result = recipientService.importRecipientList(new FileInputStream(file), "testing_excel_add_more_column.xlsx");

        // then
        // still save success because we exclude all redundant fields in the logic code
        assertThat(result.size()).isEqualTo(10);
        assertThat(result.get(0).getRecipientList().getId()).isEqualTo(1L);
        assertThat(result.get(0).getEmail()).isEqualTo("sebastian.fjall@vallentuna.se");
        assertThat(result.get(3).getReferenceNumber()).isEqualTo("TOP-2020/40");
    }

    /**
     * Import recipient list with an Excel has empty data
     */
    @Test
    public void testImportRecipientListCaseEmptyData() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_empty_data.xlsx");

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> recipientService.importRecipientList(new FileInputStream(file), "testing_excel_empty_column.xlsx"));

        // then
        assertThat(result.getMessage()).isEqualTo("error.uploadExcelDataEmpty");
    }

    /**
     * create recipient with field null or empty
     * return handle exception
     */
    @Test
    public void testImportRecipientListEmptyAnyColumnRow() {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_empty_any_column.xlsx");

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> recipientService.importRecipientList(new FileInputStream(file), "testing_excel_empty_any_column.xlsx"));

        // then
        assertThat(result.getMessage()).isEqualTo("error.cellContentError: 2-7");
    }

    /**
     * create recipient with import excel with field not invalid email
     * return handle exception
     */
    @Test
    public void testImportRecipientListWithNonInvalidEmail() {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_non_invalid_email.xlsx");

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> recipientService.importRecipientList(new FileInputStream(file), "testing_excel_non_invalid_email.xlsx"));

        // then
        assertThat(result.getMessage()).isEqualTo("error.invalidEmailFromExcel: 2-6");
    }

    /**
     * create recipient with import excel with field not invalid registrar
     * return handle exception
     */
    @Test
    public void testImportRecipientListWithNonInvalidRegistrar() {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_non_invalid_registrar.xlsx");

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> recipientService.importRecipientList(new FileInputStream(file), "testing_excel_non_invalid_registrar.xlsx"));

        // then
        assertThat(result.getMessage()).isEqualTo("error.invalidEmailFromExcel: 3-5");
    }

    /**
     * create recipient with import excel with upload excel wrong format
     * return handle exception
     */
    @Test
    public void testImportRecipientListWithUploadExcelWrongFormat() {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/~$testing_excel_non_invalid_email.xlsx");

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> recipientService.importRecipientList(new FileInputStream(file), "~$testing_excel_non_invalid_email.xlsx"));

        // then
        assertThat(result.getMessage()).isEqualTo("error.uploadExcelWrongFormat");
    }

    /**
     * create recipient with import excel with registrar null
     * return handle exception
     */
    @Test
    public void testImportRecipientListWithRegistrarNull() throws FileNotFoundException {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_registrar_null.xlsx");

        // when
        List<Recipient> result = recipientService.importRecipientList(new FileInputStream(file), "testing_excel_registrar_null.xlsx");

        // then
        assertThat(result.get(3).getTitle()).isEqualTo("UPPHANDLING LOKALANPASSNING GULDET, GÄLLIVARE");
        assertThat(result.get(3).getRegistrar()).isNull();
    }

    /**
     * create recipient with import excel with reference_number null
     * return handle exception
     */
    @Test
    public void testImportRecipientListWithReferenceNumberNull() throws FileNotFoundException {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_reference_number_null.xlsx");

        // when
        List<Recipient> result = recipientService.importRecipientList(new FileInputStream(file), "testing_excel_reference_number_null.xlsx");

        // then
        assertThat(result.get(0).getTitle()).isEqualTo("GC-väg Brottby");
        assertThat(result.get(0).getReferenceNumber()).isNull();
    }

    /**
     * create recipient with import excel with email null
     * return handle exception
     */
    @Test
    public void testImportRecipientListWithEmailNull() throws FileNotFoundException {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_enail_null.xlsx");

        // when
        List<Recipient> result = recipientService.importRecipientList(new FileInputStream(file), "testing_excel_enail_null.xlsx");

        // then
        assertThat(result.get(0).getTitle()).isEqualTo("GC-väg Brottby");
        assertThat(result.get(0).getReferenceNumber()).isEqualTo("KS 2020.142");
        assertThat(result.get(0).getProcuringAgency()).isEqualTo("Vallentuna kommun");
        assertThat(result.get(0).getRegistrar()).isEqualTo("kund@vasyd.se");
        assertThat(result.get(0).getEmail()).isNull();
    }

    /**
     * create recipient with import excel with register and email null
     * return handle exception
     */
    @Test
    public void testImportRecipientListWithBothRegisterEmailNull() throws FileNotFoundException {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_registrar_and_email_null.xlsx");

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> recipientService.importRecipientList(new FileInputStream(file), "testing_excel_registrar_and_email_null.xlsx"));

        // then
        assertThat(result.getMessage()).isEqualTo("error.bothEmailAndRegisterCannotEmpty: 2-5");
    }

    /**
     * create recipient with import excel with any column is number
     * return handle exception
     */
    @Test
    public void testImportRecipientListWithColumnIsNumber() throws FileNotFoundException {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_any_colum_is_number.xlsx");

        // when
        List<Recipient> result = recipientService.importRecipientList(new FileInputStream(file), "testing_excel_registrar_null.xlsx");

        // then
        assertThat(result.size()).isEqualTo(10);
        assertThat(result.get(1).getProcuringAgency()).isEqualTo("1.0");
    }

    /**
     * create recipient with import excel with all column is number
     * return handle exception
     */
    @Test
    public void testImportRecipientListWithAllColumnIsNumber() throws FileNotFoundException {
        given(userACL.isAdmin()).willReturn(true);

        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);

        File file = new File("src/main/resources/testing_excel_all_colum_is_number.xlsx");

        // when
        List<Recipient> result = recipientService.importRecipientList(new FileInputStream(file), "testing_excel_registrar_null.xlsx");

        // then
        assertThat(result.size()).isEqualTo(10);
        assertThat(result.get(1).getTitle()).isEqualTo("1.0");
        assertThat(result.get(1).getReferenceNumber()).isEqualTo("1.0");
        assertThat(result.get(1).getProcuringAgency()).isEqualTo("1.0");
        assertThat(result.get(1).getCounty()).isEqualTo("1.0");
        assertThat(result.get(1).getProcurementUrl()).isEqualTo("1.0");
    }

    /**
     * remove empty row in Excel with sheet have cell number different rule case row 5 have cells blank
     * the rule must be 1 row with 7 cell
     * return valid sheet
     */
    @Test
    public void testRemoveEmptyWithValueCellIsBlank() throws IOException {
        //given
        File file = new File("src/main/resources/test_excel_correct_rules_cell_blank.xlsx");
        FileInputStream fileInputStream = new FileInputStream(file);
        Workbook workbook = WorkbookFactory.create(fileInputStream);
        Sheet sheet = workbook.getSheetAt(0);

        //when
        Sheet result = recipientService.removeEmptyRow(sheet);

        //then remove row 5 bacause row 5 has cells blank
        assertThat(result.getLastRowNum()).isEqualTo(10);
        assertThat(result.getRow(5)).isNull();
        assertThat(result.getRow(1)).isNotNull();
        assertThat(result.getRow(2)).isNotNull();
        assertThat(result.getRow(3)).isNotNull();
        assertThat(result.getRow(4)).isNotNull();
        assertThat(result.getRow(6)).isNotNull();
        assertThat(result.getRow(7)).isNotNull();
        assertThat(result.getRow(8)).isNotNull();
        assertThat(result.getRow(9)).isNotNull();
        assertThat(result.getRow(10)).isNotNull();

    }

    /*
     * remove empty row in Excel with sheet have cell number different rule case rows correct rule so no rows are deleted
     * the rule must be 1 row with 7 cell
     * return valid sheet
     */
    @Test
    public void testRemoveEmptyWithSheetCorrectRules() throws IOException {
        //given
        File file = new File("src/main/resources/testing_excel.xlsx");
        FileInputStream fileInputStream = new FileInputStream(file);
        Workbook workbook = WorkbookFactory.create(fileInputStream);
        Sheet sheet = workbook.getSheetAt(0);

        //when
        Sheet result = recipientService.removeEmptyRow(sheet);

        //then
        assertThat(result.getLastRowNum()).isEqualTo(10);
        assertThat(result.getRow(1)).isNotNull();
        assertThat(result.getRow(2)).isNotNull();
        assertThat(result.getRow(3)).isNotNull();
        assertThat(result.getRow(4)).isNotNull();
        assertThat(result.getRow(5)).isNotNull();
        assertThat(result.getRow(6)).isNotNull();
        assertThat(result.getRow(7)).isNotNull();
        assertThat(result.getRow(8)).isNotNull();
        assertThat(result.getRow(9)).isNotNull();
        assertThat(result.getRow(10)).isNotNull();

    }

    /*
     * remove empty row in Excel with sheet have cell number different rule case any row is empty
     * the rule must be 1 row with 7 cell
     * return valid sheet
     */
    @Test
    public void testRemoveEmptyWithSheetRowEmpty() throws IOException {
        //given
        File file = new File("src/main/resources/test_excel_row_empty.xlsx");
        FileInputStream fileInputStream = new FileInputStream(file);
        Workbook workbook = WorkbookFactory.create(fileInputStream);
        Sheet sheet = workbook.getSheetAt(0);

        //when
        Sheet result = recipientService.removeEmptyRow(sheet);

        //then remove row 5 bacause row 5 is empty
        assertThat(result.getLastRowNum()).isEqualTo(10);
        assertThat(result.getRow(1)).isNotNull();
        assertThat(result.getRow(2)).isNotNull();
        assertThat(result.getRow(3)).isNotNull();
        assertThat(result.getRow(4)).isNotNull();
        assertThat(result.getRow(5)).isNull();
        assertThat(result.getRow(6)).isNotNull();
        assertThat(result.getRow(7)).isNotNull();
        assertThat(result.getRow(8)).isNotNull();
        assertThat(result.getRow(9)).isNotNull();
        assertThat(result.getRow(10)).isNotNull();

    }

    /**
     * Update recipient list name with non admin user
     */
    @Test
    public void testUpdateRecipientListNameCaseUserNonPermission() {
        // given
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> recipientService.updateRecipientList(new RecipientListDTO()));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * Delete recipient list name with non admin user
     */
    @Test
    public void testDeleteRecipientListNameCaseUserNonPermission() {
        // given
        given(userACL.isAdmin()).willReturn(false);

        // when
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> recipientService.deleteRecipientList(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * Update recipient list name with non exist recipient list
     */
    @Test
    public void testUpdateRecipientListNameCaseRecipientNotFound() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> recipientService.updateRecipientList(new RecipientListDTO()));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    /**
     * Delete recipient list name with non exist recipient list
     */
    @Test
    public void testDeleteRecipientListNameCaseRecipientNotFound() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        // when
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> recipientService.deleteRecipientList(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    /**
     * Update recipient list name success
     */
    @Test
    public void testUpdateRecipientListNameSuccess() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        // input
        RecipientListDTO recipientListDTO = new RecipientListDTO();
        recipientListDTO.setId(1L);
        recipientListDTO.setName("  new   Name ");

        // Exist recipient list
        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("oldName");

        given(recipientListRepository.findById(eq(1L))).willReturn(Optional.of(recipientList));

        // when
        recipientService.updateRecipientList(recipientListDTO);

        // then
        assertThat(recipientList.getName()).isEqualTo("new Name");
    }

    /**
     * Delete recipient list by id success
     */
    @Test
    public void testDeleteRecipientListSuccess() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        // Exist recipient list
        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setDeleted(false);

        given(recipientListRepository.findByIdAndDeletedIsFalse(eq(1L))).willReturn(Optional.of(recipientList));
        given(recipientListRepository.countAllCrobjobIsUsingRecipientById(CronJobStatus.RUNNING, 1L)).willReturn(0L);

        // when
        recipientService.deleteRecipientList(1L);

        // then
        assertThat(recipientList.isDeleted()).isEqualTo(true);
    }

    /**
     * Find all recipientList by name containing
     */
    @Test
    public void testFindAllRecipientListsByName() {
        // given
        given(userACL.isAdmin()).willReturn(true);
        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("Name");

        given(recipientListRepository.findAllByDeletedAndNameContainingIgnoreCase(PageRequest.of(1, 20), false, "a")).willReturn(new PageImpl<>(Collections.singletonList(recipientList)));

        // when
        Page<RecipientListDTO> page = recipientService.findAllRecipientListsByName(PageRequest.of(1, 20), "a");

        // then
        assertThat(page.getContent().size()).isEqualTo(1);
        assertThat(page.getContent().get(0).getId()).isEqualTo(1);
        assertThat(page.getContent().get(0).getName().contains("a")).isTrue();
    }

    /**
     * Find all recipientList without name containing
     */
    @Test
    public void testFindAllRecipientListsWithoutNameParams() {
        // given
        given(userACL.isAdmin()).willReturn(true);
        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("Name");

        given(recipientListRepository.findAllByDeleted(PageRequest.of(1, 20), false)).willReturn(new PageImpl<>(Collections.singletonList(recipientList)));

        // when
        Page<RecipientListDTO> page = recipientService.findAllRecipientListsByName(PageRequest.of(1, 20), "");

        // then
        assertThat(page.getContent().size()).isEqualTo(1);
        assertThat(page.getContent().get(0).getName().equals("Name")).isTrue();
        assertThat(page.getContent().get(0).getId()).isEqualTo(1);
    }

    /**
     * Delete recipient list is using by running Cronjob
     */
    @Test
    public void testDeleteRecipientListIsUsingByRunningCronjob() {
        // given
        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setDeleted(false);

        given(userACL.isAdmin()).willReturn(true);
        given(recipientListRepository.findByIdAndDeletedIsFalse(1L)).willReturn(Optional.of(recipientList));
        given(recipientListRepository.countAllCrobjobIsUsingRecipientById(CronJobStatus.RUNNING, 1L)).willReturn(1L);

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> recipientService.deleteRecipientList(1L));

        // then
        assertThat(result.getMessage()).isEqualTo("error.recipientListAlreadyInUse");
    }

    /**
     * Update recipient list name failed with all white space
     */
    @Test
    public void testUpdateRecipientListNameCaseAllWhiteSpace() {
        // given
        given(userACL.isAdmin()).willReturn(true);

        RecipientListDTO recipientListDTO = new RecipientListDTO();
        recipientListDTO.setId(1L);
        recipientListDTO.setName("  ");

        // Exist recipient list
        RecipientList recipientList = new RecipientList();
        recipientList.setId(1L);
        recipientList.setName("oldName");

        given(recipientListRepository.findById(eq(1L))).willReturn(Optional.of(recipientList));

        // when
        BadRequestException result = assertThrows(BadRequestException.class, () -> recipientService.updateRecipientList(recipientListDTO));

        // then
        assertThat(result.getMessage()).isEqualTo("error.noWhiteSpace");
    }
}
