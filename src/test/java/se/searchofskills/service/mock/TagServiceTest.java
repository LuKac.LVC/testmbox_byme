package se.searchofskills.service.mock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.searchofskills.domain.Category;
import se.searchofskills.domain.Tag;
import se.searchofskills.repository.CategoryRepository;
import se.searchofskills.repository.TagCategoryRepository;
import se.searchofskills.repository.TagRepository;
import se.searchofskills.service.TagService;
import se.searchofskills.service.UserACL;
import se.searchofskills.service.dto.tag.TagCreateInputDTO;
import se.searchofskills.service.dto.tag.TagOutputDTO;
import se.searchofskills.service.dto.tag.TagUpdateInputDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.ResourceNotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class TagServiceTest {
    @Mock
    TagRepository tagRepository;

    @Mock
    CategoryRepository categoryRepository;

    @Mock
    TagCategoryRepository tagCategoryRepository;

    @Mock
    UserACL userACL;

    @InjectMocks
    TagService tagService;

    /**
     *  test create with not admin permission
     */
    @Test
    public void testCreateNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);
        List<String> tags = Arrays.asList("nam", "nu");
        List<Long> cateIds = Arrays.asList(1L, 2L);
        TagCreateInputDTO tagCreateInputDTO = new TagCreateInputDTO();
        tagCreateInputDTO.setTags(tags);
        tagCreateInputDTO.setCategories(cateIds);
        tagCreateInputDTO.setBlock(false);
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> tagService.create(tagCreateInputDTO));

        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     *  test create with tag name is all null
     */
    @Test
    public void testCreateWithTagNamesAllBlank() {
        given(userACL.isAdmin()).willReturn(true);
        List<String> tags = Arrays.asList("     ");
        TagCreateInputDTO tagCreateInputDTO = new TagCreateInputDTO();
        tagCreateInputDTO.setTags(tags);

        BadRequestException result = assertThrows(BadRequestException.class, () -> tagService.create(tagCreateInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.tagListEmpty");
    }

    /**
     *  test create tag name when no category selected
     */
    @Test
    public void testCreateWithTagNamesWithNoCategorySelected() {
        given(userACL.isAdmin()).willReturn(true);
        List<String> tags = Arrays.asList("nam", "nu");
        List<Long> cateIds = Arrays.asList(1L);
        TagCreateInputDTO tagCreateInputDTO = new TagCreateInputDTO();
        tagCreateInputDTO.setTags(tags);
        tagCreateInputDTO.setCategories(cateIds);
        tagCreateInputDTO.setBlock(false);

        BadRequestException result = assertThrows(BadRequestException.class, () -> tagService.create(tagCreateInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.categoryListEmpty");
    }

    /**
     * test create tag when block tag name existed
     */
    @Test
    public void testCreateWithBlockTagNamesExisted() {
        given(userACL.isAdmin()).willReturn(true);
        List<String> tags = Arrays.asList("nam");
        List<Long> cateIds = Arrays.asList(1L);
        TagCreateInputDTO tagCreateInputDTO = new TagCreateInputDTO();
        tagCreateInputDTO.setTags(tags);
        tagCreateInputDTO.setCategories(cateIds);
        tagCreateInputDTO.setBlock(false);

        Category category = new Category();
        category.setId(1L);
        category.setName("cate1");

        Tag tag = new Tag();
        tag.setName("nam");
        tag.setActive(true);
        tag.setBlock(true);

        given(categoryRepository.findById(1L)).willReturn(Optional.of(category));
        given(tagRepository.findByName("nam")).willReturn(Optional.of(tag));

        BadRequestException result = assertThrows(BadRequestException.class, () -> tagService.create(tagCreateInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.blockTagNameExist");
    }

    /**
     * test created tag successfully
     */
    @Test
    public void testCreateSuccessful() {
        given(userACL.isAdmin()).willReturn(true);
        List<String> tags = Arrays.asList("nam");
        List<Long> cateIds = Arrays.asList(1L, 2L);
        TagCreateInputDTO tagCreateInputDTO = new TagCreateInputDTO();
        tagCreateInputDTO.setTags(tags);
        tagCreateInputDTO.setCategories(cateIds);
        tagCreateInputDTO.setBlock(false);

        Category category = new Category();
        category.setId(1L);
        category.setName("cate1");

        Category category2 = new Category();
        category2.setId(2L);
        category2.setName("cate2");

        Tag tag = new Tag();
        tag.setId(1L);
        tag.setName("nam");
        tag.setBlock(tagCreateInputDTO.isBlock());
        tag.setActive(false);
        tag.setSearch(0);

        given(categoryRepository.findById(1L)).willReturn(Optional.of(category));
        given(categoryRepository.findById(2L)).willReturn(Optional.of(category2));
        given(tagRepository.findByName("nam")).willReturn(Optional.of(tag));

        List<TagOutputDTO> result = tagService.create(tagCreateInputDTO);

        assertThat(result.get(0).getName()).isEqualTo("nam");
        assertThat(result.get(0).getCategories().size()).isEqualTo(2);
    }

    /**
     * test update with not admin permission
     */
    @Test
    public void testUpdateNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);
        TagUpdateInputDTO tagUpdateInputDTO = new TagUpdateInputDTO();

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> tagService.update(tagUpdateInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * test update with tag not existed
     */
    @Test
    public void testUpdateTagNotExisted() {
        given(userACL.isAdmin()).willReturn(true);
        TagUpdateInputDTO tagUpdateInputDTO = new TagUpdateInputDTO();
        tagUpdateInputDTO.setId(1L);

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> tagService.update(tagUpdateInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.tagNotFound");
    }

    /**
     * test update tag is all space
     */
    @Test
    public void testUpdateTagIsAllSpace() {
        given(userACL.isAdmin()).willReturn(true);
        TagUpdateInputDTO tagUpdateInputDTO = new TagUpdateInputDTO();
        tagUpdateInputDTO.setId(1L);
        tagUpdateInputDTO.setName("    ");
        tagUpdateInputDTO.setCategories(Arrays.asList(1L));

        Tag tag = new Tag();
        tag.setId(1L);
        given(tagRepository.findById(1L)).willReturn(Optional.of(tag));

        BadRequestException result = assertThrows(BadRequestException.class, () -> tagService.update(tagUpdateInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.tagNameEmptyOrBlank");
    }

    /**
     * test update tag name existed
     */
    @Test
    public void testUpdateTagNameExisted() {
        given(userACL.isAdmin()).willReturn(true);
        TagUpdateInputDTO tagUpdateInputDTO = new TagUpdateInputDTO();
        tagUpdateInputDTO.setId(1L);
        tagUpdateInputDTO.setName("nam");
        tagUpdateInputDTO.setBlock(false);

        Tag tag = new Tag();
        tag.setId(1L);
        given(tagRepository.findById(1L)).willReturn(Optional.of(tag));

        Tag tag1 = new Tag();
        tag1.setId(2L);
        tag1.setBlock(false);
        tag1.setActive(true);
        given(tagRepository.findByName("nam")).willReturn(Optional.of(tag1));

        BadRequestException result = assertThrows(BadRequestException.class, () -> tagService.update(tagUpdateInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.tagNameExisted");
    }

    /**
     * test update block tag name existed
     */
    @Test
    public void testUpdateBlockTagNameExisted() {
        given(userACL.isAdmin()).willReturn(true);
        TagUpdateInputDTO tagUpdateInputDTO = new TagUpdateInputDTO();
        tagUpdateInputDTO.setId(1L);
        tagUpdateInputDTO.setName("nam");
        tagUpdateInputDTO.setBlock(false);

        Tag tag = new Tag();
        tag.setId(1L);
        given(tagRepository.findById(1L)).willReturn(Optional.of(tag));

        Tag tag1 = new Tag();
        tag1.setId(2L);
        tag1.setBlock(true);
        tag1.setActive(true);
        given(tagRepository.findByName("nam")).willReturn(Optional.of(tag1));

        BadRequestException result = assertThrows(BadRequestException.class, () -> tagService.update(tagUpdateInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.blockTagNameExisted");
    }

    /**
     * test update with no category selected
     */
    @Test
    public void testUpdateWithNotCategorySelected() {
        given(userACL.isAdmin()).willReturn(true);
        TagUpdateInputDTO tagUpdateInputDTO = new TagUpdateInputDTO();
        tagUpdateInputDTO.setId(1L);
        tagUpdateInputDTO.setName("nam");
        tagUpdateInputDTO.setCategories(Arrays.asList(1L, 2L));
        tagUpdateInputDTO.setBlock(false);

        Tag tag = new Tag();
        tag.setId(1L);
        given(tagRepository.findById(1L)).willReturn(Optional.of(tag));

        TagOutputDTO result = tagService.update(tagUpdateInputDTO);
        assertThat(result.getName()).isEqualTo("nam");
        assertThat(result.getCategories().size()).isEqualTo(0);
    }

    /**
     * test update successfully
     */
    @Test
    public void testUpdateSuccessfully() {
        given(userACL.isAdmin()).willReturn(true);
        TagUpdateInputDTO tagUpdateInputDTO = new TagUpdateInputDTO();
        tagUpdateInputDTO.setId(1L);
        tagUpdateInputDTO.setName("nam");
        tagUpdateInputDTO.setCategories(Arrays.asList(1L, 2L));
        tagUpdateInputDTO.setBlock(false);

        Category category = new Category();
        category.setId(1L);
        category.setName("cate1");

        Category category2 = new Category();
        category2.setId(2L);
        category2.setName("cate2");

        Tag tag = new Tag();
        tag.setId(1L);
        tag.setName("nu");

        given(tagRepository.findById(1L)).willReturn(Optional.of(tag));
        given(categoryRepository.findById(1L)).willReturn(Optional.of(category));
        given(categoryRepository.findById(2L)).willReturn(Optional.of(category2));

        TagOutputDTO result = tagService.update(tagUpdateInputDTO);
        assertThat(result.getName()).isEqualTo("nam");
        assertThat(result.getCategories().get(0).getName()).isEqualTo("cate1");
    }

    /**
     * test delete with not admin permission
     */
    @Test
    public void testDeleteWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> tagService.delete(1L));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /**
     * test delete with tag not existed
     */
    @Test
    public void testDeleteTagNotExisted() {
        given(userACL.isAdmin()).willReturn(true);

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> tagService.delete(1L));
        assertThat(result.getMessage()).isEqualTo("error.tagOrBlockTagNotFound");
    }

    @Test
    public void testSwitchBlockTagWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> tagService.block(1L));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testSwitchBlockTagWithTagNotExisted() {
        given(userACL.isAdmin()).willReturn(true);

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> tagService.block(1L));
        assertThat(result.getMessage()).isEqualTo("error.tagNotExisted");
    }

    @Test
    public void testSwitchBLockTagNotExisted() {
        given(userACL.isAdmin()).willReturn(true);

        Tag tag = new Tag();
        tag.setId(1L);
        tag.setBlock(false);

        given(tagRepository.findById(1L)).willReturn(Optional.of(tag));

        tagService.block(1L);
        assertThat(tag.isBlock()).isTrue();
    }

    @Test
    public void testSwitchTagNotExisted() {
        given(userACL.isAdmin()).willReturn(true);

        Tag tag = new Tag();
        tag.setId(1L);
        tag.setBlock(true);

        given(tagRepository.findById(1L)).willReturn(Optional.of(tag));

        tagService.unblock(1L);
        assertThat(tag.isBlock()).isFalse();
    }

    @Test
    public void testCheckExistedWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> tagService.checkExisted("name"));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testCheckWhenTagNameExisted() {
        given(userACL.isAdmin()).willReturn(true);
        Tag tag = new Tag();
        tag.setId(1L);
        tag.setName("name");
        tag.setActive(true);

        given(tagRepository.findByActiveIsTrueAndName("name")).willReturn(Optional.of(tag));

        boolean result = tagService.checkExisted("name");
        assertThat(result).isTrue();
    }

    @Test
    public void testCheckWhenTagNameNotExisted() {
        given(userACL.isAdmin()).willReturn(true);
        Tag tag = new Tag();
        tag.setId(1L);
        tag.setName("name");
        tag.setActive(true);

        boolean result = tagService.checkExisted("name");
        assertThat(result).isFalse();
    }
}
