package se.searchofskills.service.mock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.searchofskills.domain.Country;
import se.searchofskills.domain.Region;
import se.searchofskills.repository.CountryRepository;
import se.searchofskills.repository.RegionRepository;
import se.searchofskills.service.RegionService;
import se.searchofskills.service.UserACL;
import se.searchofskills.service.dto.region.RegionInputDTO;
import se.searchofskills.service.dto.region.RegionOutputDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.ResourceNotFoundException;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class RegionServiceTest {
    @Mock
    RegionRepository regionRepository;

    @Mock
    UserACL userACL;

    @Mock
    CountryRepository countryRepository;

    @InjectMocks
    RegionService regionService;

    @Test
    public void testCreateWithNotPermission() {
        given(userACL.isAdmin()).willReturn(false);
        RegionInputDTO regionInputDTO = new RegionInputDTO();
        regionInputDTO.setName("vn");

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> regionService.create(regionInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testCreateWithRegionNameIsNull() {
        given(userACL.isAdmin()).willReturn(true);
        RegionInputDTO regionInputDTO = new RegionInputDTO();
        regionInputDTO.setName("     ");
        regionInputDTO.setCountryId(1L);

        BadRequestException result = assertThrows(BadRequestException.class, () -> regionService.create(regionInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.nameEmptyOrBlank");
    }

    @Test
    public void testCreateWithRegionNameExisted() {
        given(userACL.isAdmin()).willReturn(true);
        RegionInputDTO regionInputDTO = new RegionInputDTO();
        regionInputDTO.setName("southern");
        regionInputDTO.setCountryId(1L);

        Country country = new Country();
        country.setId(1L);

        Region region = new Region();
        region.setId(1L);
        region.setName("SOuthern");
        region.setCountry(country);

        given(countryRepository.findById(1L)).willReturn(Optional.of(country));

        given(regionRepository.findByCountryIdAndName(1L,"southern")).willReturn(Optional.of(region));

        BadRequestException result = assertThrows(BadRequestException.class, () -> regionService.create(regionInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.regionExisted");
    }

    @Test
    public void testCreateWithCountryNotFound() {
        given(userACL.isAdmin()).willReturn(true);
        RegionInputDTO regionInputDTO = new RegionInputDTO();
        regionInputDTO.setName("southern");
        regionInputDTO.setCountryId(1L);

        Country country = new Country();
        country.setId(1L);

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> regionService.create(regionInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.countryNotFound");
    }

    @Test
    public void testUpdateWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);
        RegionInputDTO regionInputDTO = new RegionInputDTO();
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> regionService.update(regionInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testUpdateRegionNotFound() {
        given(userACL.isAdmin()).willReturn(true);
        RegionInputDTO regionInputDTO = new RegionInputDTO();
        regionInputDTO.setId(1L);
        regionInputDTO.setName("vn");
        regionInputDTO.setCountryId(1L);

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> regionService.update(regionInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.regionNotFound");
    }

    @Test
    public void testUpdateCountryNotExisted() {
        given(userACL.isAdmin()).willReturn(true);
        RegionInputDTO regionInputDTO = new RegionInputDTO();
        regionInputDTO.setId(1L);
        regionInputDTO.setName("vn");
        regionInputDTO.setCountryId(1L);

        Region region = new Region();
        region.setId(1L);
        region.setName("japan");
        given(regionRepository.findById(regionInputDTO.getId())).willReturn(Optional.of(region));

        Country country = new Country();
        country.setId(1L);
        country.setName("viet nam");

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> regionService.update(regionInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.countryNotFound");
    }

    @Test
    public void testUpdateWithRegionNameExisted() {
        given(userACL.isAdmin()).willReturn(true);
        RegionInputDTO regionInputDTO = new RegionInputDTO();
        regionInputDTO.setId(1L);
        regionInputDTO.setName("northern");
        regionInputDTO.setCountryId(1L);

        Region region = new Region();
        region.setId(1L);
        region.setName("southern");

        Region region1 = new Region();
        region1.setId(2L);
        region1.setName("northern");

        Country country = new Country();
        country.setId(1L);
        country.setName("vn");

        given(countryRepository.findById(1L)).willReturn(Optional.of(country));
        given(regionRepository.findById(1L)).willReturn(Optional.of(region));

        given(regionRepository.findByCountryIdAndName(1L,"northern")).willReturn(Optional.of(region1));

        BadRequestException result = assertThrows(BadRequestException.class, () -> regionService.update(regionInputDTO));
        assertThat(result.getMessage()).isEqualTo("error.regionExisted");
    }

    @Test
    public void testUpdateSuccessfully() {
        given(userACL.isAdmin()).willReturn(true);
        RegionInputDTO regionInputDTO = new RegionInputDTO();
        regionInputDTO.setId(1L);
        regionInputDTO.setName("southern");
        regionInputDTO.setCountryId(1L);

        Country country = new Country();
        country.setId(1L);
        country.setName("vn");

        Region region = new Region();
        region.setId(1L);
        region.setName("northern");
        region.setCountry(country);

        given(countryRepository.findById(1L)).willReturn(Optional.of(country));
        given(regionRepository.findById(1L)).willReturn(Optional.of(region));

        RegionOutputDTO result = regionService.update(regionInputDTO);
        assertThat(result.getName()).isEqualTo("southern");
        assertThat(result.getCountry().getName()).isEqualTo("vn");
    }

    @Test
    public void testDeleteWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> regionService.delete(1L));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testDeleteRegionNotExisted() {
        given(userACL.isAdmin()).willReturn(true);

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> regionService.delete(1L));
        assertThat(result.getMessage()).isEqualTo("error.regionNotFound");
    }
}
