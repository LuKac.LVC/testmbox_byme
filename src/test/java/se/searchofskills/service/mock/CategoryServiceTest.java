package se.searchofskills.service.mock;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.searchofskills.domain.Category;
import se.searchofskills.repository.CategoryRepository;
import se.searchofskills.service.CategoryService;
import se.searchofskills.service.UserACL;
import se.searchofskills.service.dto.category.CategoryCreatedDTO;
import se.searchofskills.service.dto.category.CategoryUpdateDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {
    @Mock
    CategoryRepository categoryRepository;

    @Mock
    UserACL userACL;

    @InjectMocks
    CategoryService categoryService;

    @Test
    public void testCreateWithNotPermission() {
        given(userACL.isAdmin()).willReturn(false);
        CategoryCreatedDTO categoryCreatedDTO = new CategoryCreatedDTO();

        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> categoryService.create(categoryCreatedDTO));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testCreateWithCategoryNameIsNull() {
        given(userACL.isAdmin()).willReturn(true);
        CategoryCreatedDTO categoryCreatedDTO = new CategoryCreatedDTO();
        categoryCreatedDTO.setNames(Arrays.asList("     "));

        BadRequestException result = assertThrows(BadRequestException.class, () -> categoryService.create(categoryCreatedDTO));
        assertThat(result.getMessage()).isEqualTo("error.listNameEmpty");
    }

    @Test
    public void testCreateWithCategoryNameExist() {
        given(userACL.isAdmin()).willReturn(true);
        CategoryCreatedDTO categoryCreatedDTO = new CategoryCreatedDTO();
        categoryCreatedDTO.setNames(Arrays.asList("cate1"));

        Category category = new Category();
        category.setId(1L);
        category.setName("cate1");

        given(categoryRepository.findByName("cate1")).willReturn(Optional.of(category));
        BadRequestException result = assertThrows(BadRequestException.class, () -> categoryService.create(categoryCreatedDTO));
        assertThat(result.getMessage()).isEqualTo("error.categoryNameExisted");
    }

    @Test
    public void testUpdateWithNotPermission() {
        given(userACL.isAdmin()).willReturn(false);
        CategoryUpdateDTO categoryUpdateDTO = new CategoryUpdateDTO();
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> categoryService.update(categoryUpdateDTO));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testCheckCategoryExistWithNotAdminPermission() {
        given(userACL.isAdmin()).willReturn(false);
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class, () -> categoryService.checkExist("nam"));
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testCheckCategoryExisted() {
        given(userACL.isAdmin()).willReturn(true);
        Category category = new Category();
        category.setName("nam");
        category.setId(1L);
        given(categoryRepository.findByName("nam")).willReturn(Optional.of(category));
        Boolean result = categoryService.checkExist("nam");
        assertThat(result).isTrue();
    }

}
