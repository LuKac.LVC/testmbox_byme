package se.searchofskills.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.SearchOfSkillsApp;
import se.searchofskills.domain.Category;
import se.searchofskills.repository.CategoryRepository;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.dto.category.CategoryOutputDTO;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = SearchOfSkillsApp.class)
@WithMockUser(authorities = AuthoritiesConstants.ADMIN)
@Transactional
public class CategoryServiceIT {
    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    CategoryService categoryService;

    @BeforeEach
    public void init() {
        categoryRepository.deleteAll();
    }

    @Test
    public void getAllWithNameNotNull() {
        List<Category> categoryList = new ArrayList<>();
        Category category = new Category();
        category.setName("cate1");
        categoryList.add(category);

        Category category1 = new Category();
        category1.setName("cate2");
        categoryList.add(category1);

        Category category2 = new Category();
        category2.setName("ca2");
        categoryList.add(category2);

        categoryRepository.saveAll(categoryList);

        Pageable pageable = PageRequest.of(0, 3);
        String name = "cate";
        Page<CategoryOutputDTO> page = categoryService.getAll(pageable, name);
        assertThat(page.getContent().get(0).getName()).isEqualTo("cate1");
        assertThat(page.getContent().get(1).getName()).isEqualTo("cate2");
        assertThat(page.getContent().size()).isEqualTo(2);
    }

    @Test
    public void getAllWithNameIsNull() {
        List<Category> categoryList = new ArrayList<>();
        Category category = new Category();
        category.setName("cate1");
        categoryList.add(category);

        Category category1 = new Category();
        category1.setName("cate2");
        categoryList.add(category1);

        Category category2 = new Category();
        category2.setName("ca2");
        categoryList.add(category2);

        categoryRepository.saveAll(categoryList);
        String name = null;
        Pageable pageable = PageRequest.of(0, 3);
        Page<CategoryOutputDTO> page = categoryService.getAll(pageable, name);
        assertThat(page.getContent().get(0).getName()).isEqualTo("cate1");
        assertThat(page.getContent().get(1).getName()).isEqualTo("cate2");
        assertThat(page.getContent().get(2).getName()).isEqualTo("ca2");
        assertThat(page.getContent().size()).isEqualTo(3);
    }

    @AfterEach
    public void destroy() {
        categoryRepository.deleteAll();
    }
}
