package se.searchofskills.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.SearchOfSkillsApp;
import se.searchofskills.domain.CronJob;
import se.searchofskills.domain.RecipientList;
import se.searchofskills.domain.enumeration.CronJobStatus;
import se.searchofskills.domain.enumeration.Frequency;
import se.searchofskills.domain.enumeration.WeekDay;
import se.searchofskills.repository.CronJobRepository;
import se.searchofskills.repository.RecipientListRepository;
import se.searchofskills.security.AuthoritiesConstants;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest(classes = SearchOfSkillsApp.class)
@WithMockUser(authorities = AuthoritiesConstants.ADMIN)
@Transactional
public class CronJobServiceIT {

    @Autowired
    CronJobRepository cronJobRepository;

    @Autowired
    RecipientListRepository recipientListRepository;


    @BeforeEach
    public void init(){
        RecipientList recipientList = new RecipientList();
        recipientList.setName("recipient list");
        recipientListRepository.save(recipientList);


        List<CronJob> cronJobs = new ArrayList<>();
        CronJob cronJob1 = new CronJob();
        cronJob1.setName("Test");
        cronJob1.setRecipientList(recipientList);
        cronJob1.setDeleted(false);
        cronJob1.setChunkNumber(10);
        cronJob1.setWeekday(WeekDay.FRI);
        cronJob1.setSendHour(1);
        cronJob1.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob1.setStartDate(Instant.now());
        cronJob1.setEndDate(Instant.parse("2023-11-12T00:00:00Z"));
        cronJob1.setStatus(CronJobStatus.RUNNING);
        cronJobs.add(cronJob1);

        CronJob cronJob2 = new CronJob();
        cronJob2.setName("Test second");
        cronJob2.setRecipientList(recipientList);
        cronJob2.setDeleted(true);
        cronJob2.setChunkNumber(10);
        cronJob2.setWeekday(WeekDay.MON);
        cronJob2.setSendHour(1);
        cronJob2.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob2.setStartDate(Instant.now());
        cronJob2.setEndDate(Instant.parse("2023-11-12T00:00:00Z"));
        cronJob2.setStatus(CronJobStatus.RUNNING);
        cronJobs.add(cronJob2);

        CronJob cronJob3 = new CronJob();
        cronJob3.setName("Send candidate");
        cronJob3.setRecipientList(recipientList);
        cronJob3.setDeleted(false);
        cronJob3.setChunkNumber(10);
        cronJob3.setWeekday(WeekDay.SAT);
        cronJob3.setSendHour(1);
        cronJob3.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob3.setStartDate(Instant.now());
        cronJob3.setEndDate(Instant.parse("2023-11-12T00:00:00Z"));
        cronJob3.setStatus(CronJobStatus.RUNNING);
        cronJobs.add(cronJob3);

        cronJobRepository.saveAll(cronJobs);
    }

    @AfterEach
    public void destroy(){
        cronJobRepository.deleteAll();
    }

    @Test
    public void testGetListCronjobWithNameNull(){
        Page<CronJob> result = cronJobRepository.findCronJobAllByDeletedIsFalse(PageRequest.of(0, 5));

        assertThat(result.getContent().size()).isEqualTo(2);
        assertThat(result.getContent().get(0).getName()).isEqualTo("Test");
        assertThat(result.getContent().get(1).getName()).isEqualTo("Send candidate");
    }

    @Test
    public void testGetListCronjobWithNameNotNull(){
        Page<CronJob> result = cronJobRepository.findAllByNameContainingAndDeletedIsFalse(PageRequest.of(0, 5), "candi");

        assertThat(result.getContent().size()).isEqualTo(1);
        assertThat(result.getContent().get(0).getName()).isEqualTo("Send candidate");
    }

}
