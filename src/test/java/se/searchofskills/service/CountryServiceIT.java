package se.searchofskills.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.SearchOfSkillsApp;
import se.searchofskills.domain.Country;
import se.searchofskills.repository.CountryRepository;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.dto.country.CountryOutputDTO;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = SearchOfSkillsApp.class)
@WithMockUser(authorities = AuthoritiesConstants.ADMIN)
@Transactional
public class CountryServiceIT {
    @Autowired
    CountryRepository countryRepository;

    @Autowired
    CountryService countryService;

    @BeforeEach
    public void init() {
        List<Country> businessAreas = new ArrayList<>();
        Country country = new Country();
        country.setName("vn");
        businessAreas.add(country);

        Country country1 = new Country();
        country1.setName("vn2");
        businessAreas.add(country1);

        Country country2 = new Country();
        country2.setName("usa");
        businessAreas.add(country2);

        countryRepository.saveAll(businessAreas);
    }

    @AfterEach
    public void destroy() {
        countryRepository.deleteAll();
    }

    @Test
    public void testGetAllWithNameIsNull() {
        Pageable pageable = PageRequest.of(0, 3);
        String name = null;

        Page<CountryOutputDTO> result = countryService.getAll(pageable, name);
        //then
        assertThat(result.getContent().size()).isEqualTo(3);
        assertThat(result.getContent().get(0).getName()).isEqualTo("vn");
        assertThat(result.getContent().get(1).getName()).isEqualTo("vn2");
        assertThat(result.getContent().get(2).getName()).isEqualTo("usa");
    }

    @Test
    public void testGetAllWithNameIsNotNull() {
        Pageable pageable = PageRequest.of(0, 3);
        String name = "vn";

        Page<CountryOutputDTO> result = countryService.getAll(pageable, name);
        //then
        assertThat(result.getContent().size()).isEqualTo(2);
        assertThat(result.getContent().get(0).getName()).isEqualTo("vn");
        assertThat(result.getContent().get(1).getName()).isEqualTo("vn2");
    }
}


