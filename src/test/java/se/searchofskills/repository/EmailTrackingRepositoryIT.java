package se.searchofskills.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.SearchOfSkillsApp;
import se.searchofskills.domain.CronJob;
import se.searchofskills.domain.EmailTracking;
import se.searchofskills.domain.Recipient;
import se.searchofskills.domain.RecipientList;
import se.searchofskills.domain.enumeration.CronJobStatus;
import se.searchofskills.domain.enumeration.EmailStatus;
import se.searchofskills.domain.enumeration.Frequency;
import se.searchofskills.domain.enumeration.WeekDay;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for {@link EmailTrackingRepository}.
 */
@SpringBootTest(classes = SearchOfSkillsApp.class)
@Transactional
public class EmailTrackingRepositoryIT {

    @Autowired
    private EmailTrackingRepository emailTrackingRepository;

    @Autowired
    private CronJobRepository cronJobRepository;

    @Autowired
    private RecipientListRepository recipientListRepository;

    @Autowired
    private RecipientRepository recipientRepository;

    @BeforeEach
    public void setup() {
        RecipientList recipientList = new RecipientList();
        recipientList.setName("Recipient List");
        recipientList = recipientListRepository.save(recipientList);

        CronJob cronJob = new CronJob();
        cronJob.setName("Cronjob");
        cronJob.setStatus(CronJobStatus.RUNNING);
        cronJob.setEndDate(Instant.now());
        cronJob.setStartDate(Instant.now());
        cronJob.setChunkNumber(10);
        cronJob.setRecipientList(recipientList);
        cronJob.setWeekOfMonth(1);
        cronJob.setFrequency(Frequency.ONCE_PER_WEEK);
        cronJob.setSendHour(1);
        cronJob.setWeekday(WeekDay.MON);
        cronJob = cronJobRepository.save(cronJob);

        Recipient recipient = new Recipient();
        recipient.setTitle("Recipient");
        recipient.setEmail("recipient@gmail.com");
        recipient.setTitle("title");
        recipient.setCounty("county");
        recipient.setRegistrar("registrar@gmail.com");
        recipient.setProcurementUrl("procurementUrl");
        recipient.setProcuringAgency("procuringAgency");
        recipient.setRecipientList(recipientList);
        recipient.setReferenceNumber("ReferenceNumber");

        recipient = recipientRepository.save(recipient);

        List<EmailTracking> emailTrackingList = new ArrayList<>();

        Instant dateTime
            = Instant.parse("2021-11-12T00:00:00Z");

        // Email reminder 1 has reached time to send
        EmailTracking emailTracking1 = new EmailTracking();
        emailTracking1.setSentReminder1(false);
        emailTracking1.setReminderDate1(dateTime);
        emailTracking1.setArchived(false);
        emailTracking1.setReminderDate2(dateTime.plusSeconds(7 * 86400));
        emailTracking1.setSentReminder2(false);
        emailTracking1.setCronJob(cronJob);
        emailTracking1.setStatus(EmailStatus.SENT);
        emailTracking1.setRecipient(recipient);
        emailTrackingList.add(emailTracking1);

        // Email reminder 2 has not reached time to send
        EmailTracking emailTracking2 = new EmailTracking();
        emailTracking2.setSentReminder1(true);
        emailTracking2.setReminderDate1(dateTime);
        emailTracking2.setArchived(false);
        emailTracking2.setReminderDate2(dateTime.plusSeconds(7 * 86400));
        emailTracking2.setSentReminder2(false);
        emailTracking2.setCronJob(cronJob);
        emailTracking2.setStatus(EmailStatus.SENT);
        emailTracking2.setRecipient(recipient);
        emailTrackingList.add(emailTracking2);

        // Email sent all reminders
        EmailTracking emailTracking3 = new EmailTracking();
        emailTracking3.setSentReminder1(true);
        emailTracking3.setArchived(false);
        emailTracking3.setReminderDate1(dateTime.minusSeconds(7 * 86400));
        emailTracking3.setReminderDate2(dateTime.minusSeconds(86400));
        emailTracking3.setSentReminder2(true);
        emailTracking3.setStatus(EmailStatus.SENT);
        emailTracking3.setCronJob(cronJob);
        emailTracking3.setRecipient(recipient);
        emailTrackingList.add(emailTracking3);

        // Email reminder 1 has not reached time to send
        EmailTracking emailTracking4 = new EmailTracking();
        emailTracking4.setSentReminder1(false);
        emailTracking4.setArchived(false);
        emailTracking4.setReminderDate1(dateTime.plusSeconds(7 * 86400));
        emailTracking4.setReminderDate2(dateTime.plusSeconds(14 * 86400));
        emailTracking4.setSentReminder2(false);
        emailTracking4.setCronJob(cronJob);
        emailTracking4.setStatus(EmailStatus.SENT);
        emailTracking4.setRecipient(recipient);
        emailTrackingList.add(emailTracking4);

        // Email reminder 2 has reached time to send
        EmailTracking emailTracking5 = new EmailTracking();
        emailTracking5.setSentReminder1(true);
        emailTracking5.setArchived(false);
        emailTracking5.setReminderDate1(dateTime.minusSeconds(86400));
        emailTracking5.setReminderDate2(dateTime);
        emailTracking5.setSentReminder2(false);
        emailTracking5.setArchived(true);
        emailTracking5.setCronJob(cronJob);
        emailTracking5.setStatus(EmailStatus.SENT);
        emailTracking5.setRecipient(recipient);
        emailTrackingList.add(emailTracking5);

        emailTrackingRepository.saveAll(emailTrackingList);
    }

    @Test
    public void testFindAllEmailTrackingHavingReminder() {
        Instant dateTime
            = Instant.parse("2021-11-12T00:00:00Z");
        List<EmailTracking> emailTrackingList = emailTrackingRepository.findAllEmailTrackingHavingReminder(EmailStatus.SENT, EmailStatus.INCOMPLETE, dateTime);
        assertThat(emailTrackingList.size()).isEqualTo(1);
        for (EmailTracking emailTracking: emailTrackingList) {
            assertThat(emailTracking.getStatus()).isEqualTo(EmailStatus.SENT);
            assertThat(!emailTracking.isSentReminder1() || !emailTracking.isSentReminder2()).isTrue();
            assertThat((!emailTracking.isSentReminder1() && emailTracking.getReminderDate1().compareTo(dateTime) <= 0) || (emailTracking.isSentReminder1() && !emailTracking.isSentReminder2() && emailTracking.getReminderDate2().compareTo(dateTime) <= 0)).isTrue();
        }
    }

}
