package se.searchofskills.service;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import io.github.jhipster.config.JHipsterProperties;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import se.searchofskills.domain.*;
import se.searchofskills.domain.enumeration.CandidateColumn;
import se.searchofskills.domain.enumeration.FilterCondition;
import se.searchofskills.domain.enumeration.Operator;
import se.searchofskills.repository.*;
import se.searchofskills.security.SecurityUtils;
import se.searchofskills.service.dto.business_area.BusinessAreaOutputDTO;
import se.searchofskills.service.dto.candidate.CandidateCreateInputDTO;
import se.searchofskills.service.dto.candidate.CandidateExportExelFileDTO;
import se.searchofskills.service.dto.candidate.CandidateOutputDTO;
import se.searchofskills.service.dto.candidate.CandidateUpdateInputDTO;
import se.searchofskills.service.dto.cv.CvOutputDTO;
import se.searchofskills.service.dto.employer.EmployerOutputDTO;
import se.searchofskills.service.dto.filter.FilterDTO;
import se.searchofskills.service.dto.filter.FilterWithConditionDTO;
import se.searchofskills.service.dto.managerial_position.ManagerialPositionOutputDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.error.NotFoundException;
import se.searchofskills.service.utils.Utils;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class CandidateService {
    private final CandidateRepository candidateRepository;

    private final JHipsterProperties jHipsterProperties;

    private final UserACL userACL;

    private final RegionRepository regionRepository;

    private final CountryRepository countryRepository;

    private final BusinessAreaRepository businessAreaRepository;

    private final ManagerialPositionRepository managerialPositionRepository;

    private final CandidateEmployerRepository candidateEmployerRepository;

    private final CandidateManagerialPositionRepository candidateManagerialPositionRepository;

    private final CandidateBusinessAreaRepository candidateBusinessAreaRepository;

    private final CvRepository cvRepository;

    private final EmployerRepository employerRepository;

    public static final String FILE_DIRECTORY =  System.getProperty("user.dir") + File.separator + "storage/candidate/";

    private final FilterListRepository filterListRepository;

    private final CandidateFilterListRepository candidateFilterListRepository;

    private final WordService wordService;

    private final CustomCandidateRepository customCandidateRepository;

    private final UserRepository userRepository;

    private final TagRepository tagRepository;

    public CandidateService(CandidateRepository candidateRepository, JHipsterProperties jHipsterProperties, UserACL userACL, RegionRepository regionRepository, CountryRepository countryRepository, BusinessAreaRepository businessAreaRepository, ManagerialPositionRepository managerialPositionRepository, CandidateEmployerRepository candidateEmployerRepository, CandidateManagerialPositionRepository candidateManagerialPositionRepository, CandidateBusinessAreaRepository candidateBusinessAreaRepository, CvRepository cvRepository, EmployerRepository employerRepository, FilterListRepository filterListRepository, CandidateFilterListRepository candidateFilterListRepository, WordService wordService, CustomCandidateRepository customCandidateRepository, UserRepository userRepository, TagRepository tagRepository) {
        this.candidateRepository = candidateRepository;
        this.jHipsterProperties = jHipsterProperties;
        this.userACL = userACL;
        this.regionRepository = regionRepository;
        this.countryRepository = countryRepository;
        this.businessAreaRepository = businessAreaRepository;
        this.managerialPositionRepository = managerialPositionRepository;
        this.candidateEmployerRepository = candidateEmployerRepository;
        this.candidateManagerialPositionRepository = candidateManagerialPositionRepository;
        this.candidateBusinessAreaRepository = candidateBusinessAreaRepository;
        this.cvRepository = cvRepository;
        this.employerRepository = employerRepository;
        this.filterListRepository = filterListRepository;
        this.candidateFilterListRepository = candidateFilterListRepository;
        this.wordService = wordService;
        this.customCandidateRepository = customCandidateRepository;
        this.userRepository = userRepository;
        this.tagRepository = tagRepository;
    }

    public CandidateOutputDTO create(CandidateCreateInputDTO candidateCreateInputDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Candidate candidate = new Candidate();
        //throw exception employer list
        List<Employer> employerList = new ArrayList<>();
        if (candidateCreateInputDTO.getEmployerIds().size() == 0) {
            //status 400 employer list empty
            throw new BadRequestException("error.employerListEmpty", null);
        }
        List<Long> employerIdExisted = new ArrayList<>();
        for (Long employerId : candidateCreateInputDTO.getEmployerIds()) {
            Optional<Employer> employerOptional = employerRepository.findById(employerId);
            if (employerOptional.isPresent()) {
                employerIdExisted.add(employerId);
                employerList.add(employerOptional.get());
            }
        }
        candidateCreateInputDTO.getEmployerIds().removeAll(employerIdExisted);
        if (candidateCreateInputDTO.getEmployerIds().size() > 0) {
            //status 404 employer not found
            throw new NotFoundException("error.employerNotFound", "" + candidateCreateInputDTO.getEmployerIds());
        }
        //throw exception businessAreasIds
        if (candidateCreateInputDTO.getBusinessAreaIds().size() == 0) {
            //status 400 business list empty
            throw new BadRequestException("error.businessAreasEmpty", null);
        }
        List<BusinessArea> businessAreas = new ArrayList<>();
        List<Long> businessAreasIdsExisted = new ArrayList<>();
        for (Long businessAreaId : candidateCreateInputDTO.getBusinessAreaIds()) {
            Optional<BusinessArea> businessAreaOptional = businessAreaRepository.findById(businessAreaId);
            if (businessAreaOptional.isPresent()) {
                businessAreasIdsExisted.add(businessAreaId);
                businessAreas.add(businessAreaOptional.get());
            }
        }
        candidateCreateInputDTO.getBusinessAreaIds().removeAll(businessAreasIdsExisted);
        if (candidateCreateInputDTO.getBusinessAreaIds().size() > 0) {
            //status 404 business list empty
            throw new NotFoundException("error.businessAreaNotFound", "" + candidateCreateInputDTO.getBusinessAreaIds());
        }
        //throw exception managerial position
        if (candidateCreateInputDTO.getManagerialPositionIds().size() == 0) {
            //status 400 managerial position list empty
            throw new BadRequestException("error.managerialPositionListEmpty", null);
        }
        List<ManagerialPosition> managerialPositions = new ArrayList<>();
        List<Long> managerialIdsExisted = new ArrayList<>();
        for (Long managerialPositionId : candidateCreateInputDTO.getManagerialPositionIds()) {
            Optional<ManagerialPosition> managerialPositionOptional = managerialPositionRepository.findById(managerialPositionId);
            if (managerialPositionOptional.isPresent()) {
                managerialIdsExisted.add(managerialPositionId);
                managerialPositions.add(managerialPositionOptional.get());
            }
        }
        candidateCreateInputDTO.getManagerialPositionIds().removeAll(managerialIdsExisted);
        if (candidateCreateInputDTO.getManagerialPositionIds().size() > 0) {
            //status 404 managerial position not found
            throw new NotFoundException("error.managerialPositionNotFound", "" + candidateCreateInputDTO.getManagerialPositionIds());
        }
        //status 400 first name blank
        String firstName = candidateCreateInputDTO.getFirstName();
        if (firstName.isEmpty() || Utils.isAllSpaces(firstName)) {
            throw new BadRequestException("error.firstNameEmptyOrBlank", null);
        }
        if (!Utils.nameRegex(firstName)) {
            throw new BadRequestException("error.firstNameInvalid", null);
        }
        candidate.setFirstName(Utils.handleWhitespace(firstName));
        String lastName = candidateCreateInputDTO.getLastName();
        if (lastName.isEmpty() || Utils.isAllSpaces(lastName)) {
            //status 400 last name empty or blank
            throw new BadRequestException("error.lastNameEmptyOrBlank", null);
        }
        if (!Utils.nameRegex(lastName)) {
            throw new BadRequestException("error.lastNameInvalid", null);
        }
        candidate.setLastName(Utils.handleWhitespace(lastName));
        String regexBirthYear = "^[0-9]{4}$";
        Calendar calendar = Calendar.getInstance();
        if (candidateCreateInputDTO.getBirthYear() != null) {
            if (!candidateCreateInputDTO.getBirthYear().matches(regexBirthYear) || Integer.parseInt(candidateCreateInputDTO.getBirthYear()) < 1900) {
                //status 400 birth year invalid
                throw new BadRequestException("error.birthYearInvalid", null);
            }
            candidate.setBirthYear(candidateCreateInputDTO.getBirthYear());
        }
        String regexPersonalNumber = "^[0-9]{12}$";
        if (!Utils.handleWhitespace(candidateCreateInputDTO.getPersonalNumber()).matches(regexPersonalNumber)) {
            //status 400 personal number invalid
            throw new BadRequestException("error.personalNumberInvalid", null);
        }
        candidate.setPersonalNumber(candidateCreateInputDTO.getPersonalNumber());
        String regexPhoneNumber = "^[0-9]{8,14}$";
        if (!candidateCreateInputDTO.getPhoneNumber().matches(regexPhoneNumber)) {
            //status 400 phone number invalid
            throw new BadRequestException("error.phoneNumberInvalid", null);
        }
        candidate.setPhoneNumber(candidateCreateInputDTO.getPhoneNumber());
        if (candidateCreateInputDTO.getSecondPhoneNumber() == null) {
            candidate.setSecondPhoneNumber(null);
        }
        else if (candidateCreateInputDTO.getSecondPhoneNumber().matches(regexPhoneNumber)) {
            candidate.setSecondPhoneNumber(candidateCreateInputDTO.getSecondPhoneNumber());
        } else {
            //status 400 second phone invalid
            throw new BadRequestException("error.secondPhoneNumberInvalid", null);
        }
        if (!Utils.isValidEmail(Utils.handleWhitespace(candidateCreateInputDTO.getEmailPrivate()))) {
            //status 400 email private invalid
            throw new BadRequestException("error.emailPrivateInvalid", null);
        }
        candidate.setEmailPrivate(Utils.handleWhitespace(candidateCreateInputDTO.getEmailPrivate()));
        if (!Utils.isValidEmail(Utils.handleWhitespace(candidateCreateInputDTO.getEmailWork()))) {
            //status 400 email work invalid
            throw new BadRequestException("error.emailWorkInvalid", null);
        }
        candidate.setEmailWork(Utils.handleWhitespace(candidateCreateInputDTO.getEmailWork()));
        //status 404 country not found
        Country country = Utils.requireExists(countryRepository.findById(candidateCreateInputDTO.getCountryId()), "error.countryNotFound");
        candidate.setCountry(country);
        //status 404 region not found
        Region region = Utils.requireExists(regionRepository.findById(candidateCreateInputDTO.getRegionId()), "error.regionNotFound");
        if (!region.getCountry().getId().equals(country.getId())) {
            throw new NotFoundException("error.regionNotInCountry", null);
        }
        candidate.setRegion(region);
        //get gender
        candidate.setGender(candidateCreateInputDTO.getGender());
        //get linkedLnLink
        candidate.setLinkedLnLink(Utils.handleWhitespace(candidateCreateInputDTO.getLinkedLnLink()));
        //get first employment
        if (candidateCreateInputDTO.getFirstEmployment() <= 0) {
            throw new BadRequestException("error.firstEmploymentInvalid", null);
        }
        candidate.setFirstEmployment(candidateCreateInputDTO.getFirstEmployment());
        //get zip code
        String zipCode = candidateCreateInputDTO.getZipCode();
        if (zipCode.isEmpty() || Utils.isAllSpaces(zipCode)) {
            throw new BadRequestException("error.zipCodeEmptyOrBlank", null);
        }
        if (!Utils.nameRegex(zipCode)) {
            throw new BadRequestException("error.zipCodeInvalid", null);
        }
        candidate.setZipCode(Utils.handleWhitespace(zipCode));
        //get county
        String county = candidateCreateInputDTO.getCounty();
        if (county.isEmpty() || Utils.isAllSpaces(county)) {
            throw new BadRequestException("error.countyEmptyOrBlank", null);
        }
        if (!Utils.nameRegex(county)) {
            throw new BadRequestException("error.countyInvalid", null);
        }
        candidate.setCounty(Utils.handleWhitespace(county));
        //get city
        String city = candidateCreateInputDTO.getCity();
        if (city.isEmpty() || Utils.isAllSpaces(city)) {
            throw new BadRequestException("error.cityEmptyOrBlank", null);
        }
        if (!Utils.nameRegex(city)) {
            throw new BadRequestException("error.cityInvalid", null);
        }
        candidate.setCity(Utils.handleWhitespace(city));
        //get street address
        String streetAddress = candidateCreateInputDTO.getStreetAddress();
        if (streetAddress.isEmpty() || Utils.isAllSpaces(streetAddress)) {
            throw new BadRequestException("error.streetAddressEmptyOrBlank", null);
        }
        candidate.setStreetAddress(Utils.handleWhitespace(streetAddress));
        candidateRepository.save(candidate);
        CandidateOutputDTO candidateOutputDTO = new CandidateOutputDTO(candidate);
        for (Employer employer : employerList) {
            Optional<CandidateEmployer> candidateEmployerOptional = candidateEmployerRepository.findByEmployerIdAndCandidateId(employer.getId(), candidate.getId());
            CandidateEmployer candidateEmployer = candidateEmployerOptional.orElseGet(CandidateEmployer::new);
            candidateEmployer.setCandidate(candidate);
            candidateEmployer.setEmployer(employer);
            candidateEmployerRepository.save(candidateEmployer);
        }
        List<EmployerOutputDTO> employerOutputDTOS = employerList.stream().map(EmployerOutputDTO::new).collect(Collectors.toList());
        candidateOutputDTO.setEmployerDTOs(employerOutputDTOS);
        for (BusinessArea businessArea : businessAreas) {
            Optional<CandidateBusinessArea> candidateEmployerOptional = candidateBusinessAreaRepository.findByBusinessAreaIdAndCandidateId(businessArea.getId(), candidate.getId());
            CandidateBusinessArea candidateBusinessArea = candidateEmployerOptional.orElseGet(CandidateBusinessArea::new);
            candidateBusinessArea.setBusinessArea(businessArea);
            candidateBusinessArea.setCandidate(candidate);
            candidateBusinessAreaRepository.save(candidateBusinessArea);
        }
        List<BusinessAreaOutputDTO> businessAreaOutputDTOS = businessAreas.stream().map(BusinessAreaOutputDTO::new).collect(Collectors.toList());
        candidateOutputDTO.setBusinessAreaDTOs(businessAreaOutputDTOS);
        for (ManagerialPosition managerialPosition : managerialPositions) {
            Optional<CandidateManagerialPosition> candidateManagerialPositionOptional = candidateManagerialPositionRepository.findByCandidateIdAndManagerialPositionId(candidate.getId(), managerialPosition.getId());
            CandidateManagerialPosition candidateManagerialPosition = candidateManagerialPositionOptional.orElseGet(CandidateManagerialPosition::new);
            candidateManagerialPosition.setCandidate(candidate);
            candidateManagerialPosition.setManagerialPosition(managerialPosition);
            candidateManagerialPositionRepository.save(candidateManagerialPosition);
        }
        List<ManagerialPositionOutputDTO> managerialPositionOutputDTOS = managerialPositions.stream().map(ManagerialPositionOutputDTO::new).collect(Collectors.toList());
        candidateOutputDTO.setManagerialPositionDTOs(managerialPositionOutputDTOS);
        return candidateOutputDTO;
    }

    public CandidateOutputDTO update(CandidateUpdateInputDTO candidateUpdateInputDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Candidate candidate = Utils.requireExists(candidateRepository.findById(candidateUpdateInputDTO.getId()), "error.candidateNotFound");
        //throw exception employer list
        List<Employer> employerList = new ArrayList<>();
        if (candidateUpdateInputDTO.getEmployerIds().size() == 0) {
            //status 400 employer list empty
            throw new BadRequestException("error.employerListEmpty", null);
        }
        List<Long> employerIdExisted = new ArrayList<>();
        for (Long employerId : candidateUpdateInputDTO.getEmployerIds()) {
            Optional<Employer> employerOptional = employerRepository.findById(employerId);
            if (employerOptional.isPresent()) {
                employerIdExisted.add(employerId);
                employerList.add(employerOptional.get());
            }
        }
        candidateUpdateInputDTO.getEmployerIds().removeAll(employerIdExisted);
        if (candidateUpdateInputDTO.getEmployerIds().size() > 0) {
            //status 404 employer not found
            throw new NotFoundException("error.employerNotFound", "" + candidateUpdateInputDTO.getEmployerIds());
        }
        //throw exception businessAreasIds
        if (candidateUpdateInputDTO.getBusinessAreaIds().size() == 0) {
            //status 400 business list empty
            throw new BadRequestException("error.businessAreasEmpty", null);
        }
        List<BusinessArea> businessAreas = new ArrayList<>();
        List<Long> businessAreasIdsExisted = new ArrayList<>();
        for (Long businessAreaId : candidateUpdateInputDTO.getBusinessAreaIds()) {
            Optional<BusinessArea> businessAreaOptional = businessAreaRepository.findById(businessAreaId);
            if (businessAreaOptional.isPresent()) {
                businessAreasIdsExisted.add(businessAreaId);
                businessAreas.add(businessAreaOptional.get());
            }
        }
        candidateUpdateInputDTO.getBusinessAreaIds().removeAll(businessAreasIdsExisted);
        if (candidateUpdateInputDTO.getBusinessAreaIds().size() > 0) {
            //status 404 business not found
            throw new NotFoundException("error.businessAreaNotFound", "" + candidateUpdateInputDTO.getBusinessAreaIds());
        }
        //throw exception managerial position
        if (candidateUpdateInputDTO.getManagerialPositionIds().size() == 0) {
            //status 400 managerial position list empty
            throw new BadRequestException("error.managerialPositionListEmpty", null);
        }
        List<ManagerialPosition> managerialPositions = new ArrayList<>();
        List<Long> managerialIdsExisted = new ArrayList<>();
        for (Long managerialPositionId : candidateUpdateInputDTO.getManagerialPositionIds()) {
            Optional<ManagerialPosition> managerialPositionOptional = managerialPositionRepository.findById(managerialPositionId);
            if (managerialPositionOptional.isPresent()) {
                managerialIdsExisted.add(managerialPositionId);
                managerialPositions.add(managerialPositionOptional.get());
            }
        }
        candidateUpdateInputDTO.getManagerialPositionIds().removeAll(managerialIdsExisted);
        if (candidateUpdateInputDTO.getManagerialPositionIds().size() > 0) {
            //status 404 managerial position not found
            throw new NotFoundException("error.managerialPositionNotFound", "" + candidateUpdateInputDTO.getManagerialPositionIds());
        }
        //get firstName
        String firstName = candidateUpdateInputDTO.getFirstName();
        if (firstName != null) {
            if (Utils.isAllSpaces(firstName) || firstName.isEmpty()) {
                //status 400 first name blank
                throw new BadRequestException("error.firstNameEmptyOrBlank", null);
            }
            if (!Utils.nameRegex(firstName)) {
                throw new BadRequestException("error.firstNameInvalid", null);
            }
            candidate.setFirstName(Utils.handleWhitespace(firstName));
        }
        //get lastName
        String lastName = candidateUpdateInputDTO.getLastName();
        if (lastName != null) {
            if (lastName.isEmpty() || Utils.isAllSpaces(lastName)) {
                //status 400 last name empty or blank
                throw new BadRequestException("error.lastNameEmptyOrBlank", null);
            }
            if (!Utils.nameRegex(lastName)) {
                throw new BadRequestException("error.lastNameInvalid", null);
            }
            candidate.setLastName(Utils.handleWhitespace(lastName));
        }
        //get birthYear
        String regexBirthYear = "^[0-9]{4}$";
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        if (candidateUpdateInputDTO.getBirthYear() != null) {
            if (!candidateUpdateInputDTO.getBirthYear().matches(regexBirthYear) || Integer.parseInt(candidateUpdateInputDTO.getBirthYear()) < 1900) {
                //status 400 birth year invalid
                throw new BadRequestException("error.birthYearInvalid", null);
            }
            candidate.setBirthYear(candidateUpdateInputDTO.getBirthYear());
        }
        //get personal number
        String regexPersonalNumber = "^[0-9]{12}$";
        if (candidateUpdateInputDTO.getPersonalNumber() != null) {
            if (!Utils.handleWhitespace(candidateUpdateInputDTO.getPersonalNumber()).matches(regexPersonalNumber)) {
                //status 400 personal number invalid
                throw new BadRequestException("error.personalNumberInvalid", null);
            }
            candidate.setPersonalNumber(candidateUpdateInputDTO.getPersonalNumber());
        }
        //get phone number
        String regexPhoneNumber = "^[0-9]{8,14}$";
        if (candidateUpdateInputDTO.getPhoneNumber() != null) {
            if (!candidateUpdateInputDTO.getPhoneNumber().matches(regexPhoneNumber)) {
                //status 400 phone number invalid
                throw new BadRequestException("error.phoneNumberInvalid", null);
            }
            candidate.setPhoneNumber(Utils.handleWhitespace(candidateUpdateInputDTO.getPhoneNumber()));
        }
        //get second phone number
        if (candidateUpdateInputDTO.getSecondPhoneNumber() != null) {
            if (!candidateUpdateInputDTO.getSecondPhoneNumber().matches(regexPhoneNumber)) {
                //status 400 second phone number invalid
                throw new BadRequestException("error.secondPhoneNumberInvalid", null);
            }
            candidateUpdateInputDTO.setSecondPhoneNumber(candidateUpdateInputDTO.getSecondPhoneNumber());
        }
        //get email private
        if (candidateUpdateInputDTO.getEmailPrivate() != null) {
            if (!Utils.isValidEmail(Utils.handleWhitespace(candidateUpdateInputDTO.getEmailPrivate()))) {
                //status 400 email private invalid
                throw new BadRequestException("error.emailPrivateInvalid", null);
            }
            candidate.setEmailPrivate(Utils.handleWhitespace(candidateUpdateInputDTO.getEmailPrivate()));
        }
        if (candidateUpdateInputDTO.getEmailWork() != null) {
            if (!Utils.isValidEmail(Utils.handleWhitespace(candidateUpdateInputDTO.getEmailWork()))) {
                //status 400 email work invalid
                throw new BadRequestException("error.emailWorkInvalid", null);
            }
            candidate.setEmailWork(Utils.handleWhitespace(candidateUpdateInputDTO.getEmailWork()));
        }
        //status 404 country not found
        Country country = Utils.requireExists(countryRepository.findById(candidateUpdateInputDTO.getCountryId()), "error.countryNotFound");
        candidate.setCountry(country);
        //status 404 region not found
        Region region = Utils.requireExists(regionRepository.findById(candidateUpdateInputDTO.getRegionId()), "error.regionNotFound");
        candidate.setRegion(region);
        if (!region.getCountry().getId().equals(country.getId())) {
            throw new NotFoundException("error.regionNotInCountry", null);
        }
        //get city
        String city = candidateUpdateInputDTO.getCity();
        if (city != null) {
            if (city.isEmpty() || Utils.isAllSpaces(city)) {
                throw new BadRequestException("error.cityEmptyOrBlank", null);
            }
            if (!Utils.nameRegex(city)) {
                throw new BadRequestException("error.cityInvalid", null);
            }
            candidate.setCity(Utils.handleWhitespace(city));
        }
        //get gender
        if (candidateUpdateInputDTO.getGender() != null) {
            candidate.setGender(candidateUpdateInputDTO.getGender());
        }
        //get linkednLn
        if (candidateUpdateInputDTO.getLinkedLnLink() != null) {
            candidate.setLinkedLnLink(Utils.handleWhitespace(candidateUpdateInputDTO.getLinkedLnLink()));
        }
        //get zipcode
        String zipCode = candidateUpdateInputDTO.getZipCode();
        if (zipCode != null) {
            if (zipCode.isEmpty() || Utils.isAllSpaces(zipCode)) {
                throw new BadRequestException("error.zipCodeEmptyOrBlank", null);
            }
            if (!Utils.nameRegex(zipCode)) {
                throw new BadRequestException("error.zipCodeInvalid", null);
            }
            candidate.setZipCode(Utils.handleWhitespace(zipCode));
        }
        //get county
        String county = candidateUpdateInputDTO.getCounty();
        if (county != null) {
            if (county.isEmpty() || Utils.isAllSpaces(county)) {
                throw new BadRequestException("error.countyEmptyOrBlank", null);
            }
            if (!Utils.nameRegex(county)) {
                throw new BadRequestException("error.countyInvalid", null);
            }
            candidate.setCounty(Utils.handleWhitespace(county));
        }
        //get street address
        String streetAddress = candidateUpdateInputDTO.getStreetAddress();
        if (streetAddress != null) {
            if (streetAddress.isEmpty() || Utils.isAllSpaces(streetAddress)) {
                throw new BadRequestException("error.streetAddressEmptyOrBlank", null);
            }
            candidate.setStreetAddress(Utils.handleWhitespace(streetAddress));
        }
        //get first employment
        if (candidateUpdateInputDTO.getFirstEmployment() <= 0) {
            throw new BadRequestException("error.firstEmploymentInvalid", null);
        }
        candidate.setFirstEmployment(candidateUpdateInputDTO.getFirstEmployment());
        candidateRepository.save(candidate);
        CandidateOutputDTO candidateOutputDTO = new CandidateOutputDTO(candidate);
        for (Employer employer : employerList) {
            Optional<CandidateEmployer> candidateEmployerOptional = candidateEmployerRepository.findByEmployerIdAndCandidateId(employer.getId(), candidate.getId());
            CandidateEmployer candidateEmployer = candidateEmployerOptional.orElseGet(CandidateEmployer::new);
            candidateEmployer.setCandidate(candidate);
            candidateEmployer.setEmployer(employer);
            candidateEmployerRepository.save(candidateEmployer);
        }
        List<EmployerOutputDTO> employerOutputDTOS = employerList.stream().map(EmployerOutputDTO::new).collect(Collectors.toList());
        candidateOutputDTO.setEmployerDTOs(employerOutputDTOS);
        for (BusinessArea businessArea : businessAreas) {
            Optional<CandidateBusinessArea> candidateEmployerOptional = candidateBusinessAreaRepository.findByBusinessAreaIdAndCandidateId(businessArea.getId(), candidate.getId());
            CandidateBusinessArea candidateBusinessArea = candidateEmployerOptional.orElseGet(CandidateBusinessArea::new);
            candidateBusinessArea.setBusinessArea(businessArea);
            candidateBusinessArea.setCandidate(candidate);
            candidateBusinessAreaRepository.save(candidateBusinessArea);
        }
        List<BusinessAreaOutputDTO> businessAreaOutputDTOS = businessAreas.stream().map(BusinessAreaOutputDTO::new).collect(Collectors.toList());
        candidateOutputDTO.setBusinessAreaDTOs(businessAreaOutputDTOS);
        for (ManagerialPosition managerialPosition : managerialPositions) {
            Optional<CandidateManagerialPosition> candidateManagerialPositionOptional = candidateManagerialPositionRepository.findByCandidateIdAndManagerialPositionId(candidate.getId(), managerialPosition.getId());
            CandidateManagerialPosition candidateManagerialPosition = candidateManagerialPositionOptional.orElseGet(CandidateManagerialPosition::new);
            candidateManagerialPosition.setCandidate(candidate);
            candidateManagerialPosition.setManagerialPosition(managerialPosition);
            candidateManagerialPositionRepository.save(candidateManagerialPosition);
        }
        List<ManagerialPositionOutputDTO> managerialPositionOutputDTOS = managerialPositions.stream().map(ManagerialPositionOutputDTO::new).collect(Collectors.toList());
        candidateOutputDTO.setManagerialPositionDTOs(managerialPositionOutputDTOS);
        return candidateOutputDTO;
    }

    public void delete(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Candidate candidate = Utils.requireExists(candidateRepository.findById(id), "error.candidateNotFound");
        candidate.setDeleted(true);
        candidateRepository.save(candidate);
        List<Cv> cvs = cvRepository.findAllByCandidateIdOrderByCreatedDateDesc(id);
        for (Cv cv : cvs) {
            String path = cv.getFilePath();
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
        }
    }

    public Page<CandidateOutputDTO> getAll(Pageable pageable, String filter, List<Long> tags) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        User user = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin).get();
        Page<CandidateOutputDTO> candidateOutputDTOS;
        FilterDTO filterDTO = new FilterDTO(new ArrayList<>(), new ArrayList<>(), "",  0);
        FilterList filterList = null;
        List<Tag> tagsExist = new ArrayList<>();
        if (filter != null) {
            try {
                //convert to object
                JSONObject jsonObject = new JSONObject(filter);
                //get free text
                String freeText = jsonObject.getString("freeText").trim();
                JSONArray operatorsOrderJson = jsonObject.getJSONArray("operatorsOrder");
                // convert to array filter condition
                JSONArray filterWithConditionDTOSJson = jsonObject.getJSONArray("filterWithConditions");
                List<FilterWithConditionDTO> filterWithConditionDTOS = new ArrayList<>();
                //get filter with condition list
                if (filterWithConditionDTOSJson.length() > 0) {
                    for (int i = 0; i < filterWithConditionDTOSJson.length(); i++) {
                        if (filterWithConditionDTOSJson.getJSONObject(0).length() == 0) {
                            break;
                        }
                        JSONObject filterWithConditionDTOJson = filterWithConditionDTOSJson.getJSONObject(i);
                        FilterCondition filterCondition = FilterCondition.valueOf(filterWithConditionDTOJson.getString("condition"));
                        CandidateColumn candidateColumn = CandidateColumn.valueOf(filterWithConditionDTOJson.getString("field"));
                        String value = filterWithConditionDTOJson.getString("value").trim();
                        FilterWithConditionDTO filterWithConditionDTO = new FilterWithConditionDTO(candidateColumn, filterCondition, value);
                        filterWithConditionDTOS.add(filterWithConditionDTO);
                    }
                }
                //get operatorsOrder list
                List<Operator> operatorsOrders = new ArrayList<>();
                if (operatorsOrderJson.length() > 0) {
                    for (int i = 0; i < operatorsOrderJson.length(); i++) {
                        String operatorsOrder = operatorsOrderJson.get(i).toString();
                        operatorsOrders.add(Operator.valueOf(operatorsOrder));
                    }
                }
                //get candidates from filter list
                long filterListId = jsonObject.getLong("filterListId");
                if (filterListId > 0) {
                    filterList = Utils.requireExists(filterListRepository.findById(filterListId), "error.filterListNotFound");
                    if (!filterList.getUser().getId().equals(user.getId())) {
                        throw new NotFoundException("filterListNotUser", null);
                    }
                }
                filterDTO = new FilterDTO(filterWithConditionDTOS, operatorsOrders, freeText, filterListId);
            } catch (JSONException e) {
                throw new BadRequestException("error.filterInvalid", null);
            }
        }
        //get list tags
        tagsExist = tagRepository.findAllByIdIn(tags);
        for (Tag tag : tagsExist) {
            tag.setSearch(tag.getSearch() + 1);
        }
        tagRepository.saveAll(tagsExist);
        List<String> tagNames = tagsExist.stream().map(Tag::getName).collect(Collectors.toList());

        //get list candidate
        candidateOutputDTOS = customCandidateRepository.searchCandidate(pageable, filterDTO, filterList, tagNames).map(CandidateOutputDTO::new);

        for (CandidateOutputDTO candidateOutputDTO : candidateOutputDTOS) {
            //get business area
            List<BusinessAreaOutputDTO> businessAreas = new ArrayList<>();
            List<Long> businessIds = candidateBusinessAreaRepository.findAllByCandidateId(candidateOutputDTO.getId());
            if (businessIds.size() > 0) {
                businessAreas = businessAreaRepository.findAllByIdIn(businessIds).stream().map(BusinessAreaOutputDTO::new).collect(Collectors.toList());
            }
            //get employers
            List<EmployerOutputDTO> employerList = new ArrayList<>();
            List<Long> employerIds = candidateEmployerRepository.findAllByCandidateId(candidateOutputDTO.getId());
            if (employerIds.size() > 0) {
                employerList = employerRepository.findAllByIdIn(employerIds).stream().map(EmployerOutputDTO::new).collect(Collectors.toList());
            }
            //get managerial position
            List<ManagerialPositionOutputDTO> managerialPositions = new ArrayList<>();
            List<Long> managerIds = candidateManagerialPositionRepository.findAllByCandidateId(candidateOutputDTO.getId());
            if (managerIds.size() > 0) {
                managerialPositions = managerialPositionRepository.findAllByIdIn(managerIds).stream().map(ManagerialPositionOutputDTO::new).collect(Collectors.toList());
            }
            //get cv
            Optional<Cv> cv = cvRepository.findTopByCandidateIdOrderByCreatedDateDesc(candidateOutputDTO.getId());
            CvOutputDTO cvDTO = null;
            if (cv.isPresent()) {
                cvDTO = new CvOutputDTO(cv.get(), jHipsterProperties.getMail().getBaseUrl());
            }
            candidateOutputDTO.setBusinessAreaDTOs(businessAreas);
            candidateOutputDTO.setManagerialPositionDTOs(managerialPositions);
            candidateOutputDTO.setEmployerDTOs(employerList);
            candidateOutputDTO.setCvDTO(cvDTO);
        }
        return candidateOutputDTOS;
    }

    public CandidateOutputDTO getOne(long id) {
        Candidate candidate = Utils.requireExists(candidateRepository.findById(id), "error.candidateNotFound");
        CandidateOutputDTO candidateOutputDTO = new CandidateOutputDTO(candidate);
        //get business area
        List<Long> businessIds = candidateBusinessAreaRepository.findAllByCandidateId(candidateOutputDTO.getId());
        List<BusinessAreaOutputDTO> businessAreas = businessAreaRepository.findAllByIdIn(businessIds).stream().map(BusinessAreaOutputDTO::new).collect(Collectors.toList());
        //get employers
        List<Long> employerIds = candidateEmployerRepository.findAllByCandidateId(candidateOutputDTO.getId());
        List<EmployerOutputDTO> employerList = employerRepository.findAllByIdIn(employerIds).stream().map(EmployerOutputDTO::new).collect(Collectors.toList());
        //get managerial position
        List<Long> managerIds = candidateManagerialPositionRepository.findAllByCandidateId(candidateOutputDTO.getId());
        List<ManagerialPositionOutputDTO> managerialPositions = managerialPositionRepository.findAllByIdIn(managerIds).stream().map(ManagerialPositionOutputDTO::new).collect(Collectors.toList());
        //get cv
        Optional<Cv> cv = cvRepository.findTopByCandidateIdOrderByCreatedDateDesc(candidateOutputDTO.getId());
        CvOutputDTO cvDTO = null;
        if (cv.isPresent()) {
            cvDTO = new CvOutputDTO(cv.get(), jHipsterProperties.getMail().getBaseUrl());
        }
        //
        candidateOutputDTO.setBusinessAreaDTOs(businessAreas);
        candidateOutputDTO.setManagerialPositionDTOs(managerialPositions);
        candidateOutputDTO.setEmployerDTOs(employerList);
        candidateOutputDTO.setCvDTO(cvDTO);
        return candidateOutputDTO;
    }

    public List<String> getColumns() {
        Field[] fields = CandidateOutputDTO.class.getDeclaredFields();
        List<String> columns = new ArrayList<>();
        for (Field field : fields) {
            String value = field.getName();
            columns.add(value);
        }
        return columns;
    }

    public void uploadCvs(List<MultipartFile> files, long candidateId) throws IOException {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        String filePath = FILE_DIRECTORY + candidateId + "/cv";
        File fileDirectory = new File(filePath);
        if (!fileDirectory.exists()) {
            fileDirectory.mkdirs();
        }
        List<File> fileList = files.stream().map(file -> {
            try {
                return convertMultiPartToFile(file, filePath);
            } catch (IOException e) {
                return null;
            }
        }).collect(Collectors.toList());
        Candidate candidate = Utils.requireExists(candidateRepository.findById(candidateId), "error.candidateNotFound");
        for (File file : fileList) {
            try {
                PdfReader pdfReader = new PdfReader(String.valueOf(file));
                PdfTextExtractor.getTextFromPage(pdfReader, 1);
            } catch (IOException e) {
               throw new BadRequestException("error.fileFormatInvalid", null);
            }
        }
        for (File file : fileList) {
            //save cv
            InputStream inputStream = new FileInputStream(file);
            String content = wordService.readPDFFile(inputStream);
            Cv cv = new Cv();
            cv.setCandidate(candidate);
            cv.setFileName(file.getName());
            cv.setFilePath(file.getAbsolutePath());
            cv.setContent(content);
            cvRepository.save(cv);
            //save words
            wordService.saveWord(content);
        }

    }

    private File convertMultiPartToFile(MultipartFile file, String filePath) throws IOException {
        String fileNameWithoutExtension = FilenameUtils.removeExtension(file.getOriginalFilename());
        File convFile = new File(filePath, fileNameWithoutExtension + ".pdf");
        int num = 0;
        while (convFile.exists()) {
            convFile = new File(filePath, fileNameWithoutExtension + "(" + (++num) + ").pdf");
        }
        FileOutputStream fos = new FileOutputStream( convFile );
        fos.write( file.getBytes() );
        fos.close();
        return convFile;
    }

    public void deleteCv(long candidateId, long cvId) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Candidate candidate = Utils.requireExists(candidateRepository.findById(candidateId), "error.candidateNotFound");
        Cv cv = Utils.requireExists(cvRepository.findById(cvId), "error.cvNotFound");
        File file = new File(cv.getFilePath());
        if (file.exists()) {
            file.delete();
        }
        cvRepository.delete(cv);
    }

    public CvOutputDTO getCv(long candidateId, long cvId) {
        Candidate candidate = Utils.requireExists(candidateRepository.findById(candidateId), "error.candidateNotFound");
        Cv cv = Utils.requireExists(cvRepository.findById(cvId), "error.cvNotFound");
        return new CvOutputDTO(cv, jHipsterProperties.getMail().getBaseUrl());
    }

    public InputStreamResource viewPdf(long candidateId, String fileName) throws FileNotFoundException {
        Candidate candidate = Utils.requireExists(candidateRepository.findById(candidateId), "error.candidateNotFound");
        Cv cv = Utils.requireExists(cvRepository.findByCandidateIdAndFileName(candidate.getId(), fileName), "error.cvNotFound");
        File file = new File(cv.getFilePath());
        FileInputStream fileInputStream = new FileInputStream(file);
        return new InputStreamResource(fileInputStream);
    }

    public Page<CvOutputDTO> getCvs(Pageable pageable, long candidateId) {
        Candidate candidate = Utils.requireExists(candidateRepository.findById(candidateId), "error.candidateNotFound");
        Page<CvOutputDTO> page = cvRepository.findAllByCandidateIdOrderByCreatedDateDesc(pageable, candidateId).map(cv -> new CvOutputDTO(cv, jHipsterProperties.getMail().getBaseUrl()));
        return page;
    }

    public InputStreamResource exportCvs(long filterListId, int numberOfCvs, String fileName) throws IOException {
        FilterList filterList = Utils.requireExists(filterListRepository.findById(filterListId), "error.filterListNotFound");
        List<Long> candidateIds = candidateFilterListRepository.findAllByFilterListId(filterList.getId());
        List<Candidate> candidates = candidateRepository.findAllByIdIn(candidateIds);
        if (numberOfCvs <= 0) {
            throw new BadRequestException("error.numberOfCVInvalid", null);
        }
        List<Cv> cvsList = new ArrayList<>();
        for (Candidate candidate : candidates) {
            List<Cv> cvs = cvRepository.findAllByCandidateIdOrderByCreatedDateDesc(candidate.getId());
            int maxNumberOfCv = Math.min(cvs.size(), numberOfCvs);
            for (int i = 0; i < maxNumberOfCv; i++) {
                cvsList.add(cvs.get(i));
            }
        }
        PDFMergerUtility obj = new PDFMergerUtility();
        File newFile = new File(fileName);
        if (!newFile.exists()) {
            newFile.createNewFile();
        }
        obj.setDestinationFileName(newFile.getAbsolutePath());
        for (Cv cv : cvsList) {
            File file = new File(cv.getFilePath());
            obj.addSource(file);
        }
        obj.mergeDocuments();
        FileInputStream fileInputStream = new FileInputStream(newFile);
        return new InputStreamResource(fileInputStream);
    }

    public ByteArrayInputStream exportExcel(long filerListId, List<String> fieldsSelected) throws Exception {
        FilterList filterList = Utils.requireExists(filterListRepository.findById(filerListId), "error.filterListNotFound");
        List<Long> candidateIds = candidateFilterListRepository.findAllByFilterListId(filterList.getId());
        List<CandidateExportExelFileDTO> candidateOutputDTOS = candidateRepository.findAllByIdIn(candidateIds).stream().map(CandidateExportExelFileDTO::new).collect(Collectors.toList());
        for (CandidateExportExelFileDTO candidateExportExelFileDTO: candidateOutputDTOS) {
            //get business area
            List<Long> businessIds = candidateBusinessAreaRepository.findAllByCandidateId(candidateExportExelFileDTO.getId());
            List<BusinessArea> businessAreas = businessAreaRepository.findAllByIdIn(businessIds);
            String businessDTO = null;
            if (businessAreas.size() > 0) {
                businessDTO = businessAreas.stream().map(BusinessArea::getName).collect(Collectors.joining(","));
            }
            //get employers
            List<Long> employerIds = candidateEmployerRepository.findAllByCandidateId(candidateExportExelFileDTO.getId());
            List<Employer> employerList = employerRepository.findAllByIdIn(employerIds);
            String employerDTO = null;
            if (employerList.size() > 0) {
                employerDTO = employerList.stream().map(Employer::getName).collect(Collectors.joining(","));
            }
            //get managerial position
            List<Long> managerIds = candidateManagerialPositionRepository.findAllByCandidateId(candidateExportExelFileDTO.getId());
            List<ManagerialPosition> managerialPositions = managerialPositionRepository.findAllByIdIn(managerIds);
            String managers = null;
            if (managerialPositions.size() > 0) {
                managers = managerialPositions.stream().map(ManagerialPosition::getName).collect(Collectors.joining(","));
            }
            //get cv
            Optional<Cv> cv = cvRepository.findTopByCandidateIdOrderByCreatedDateDesc(candidateExportExelFileDTO.getId());
            String cvDTO = "";
            if (cv.isPresent()) {
                CvOutputDTO cvOutputDTO = new CvOutputDTO(cv.get(), jHipsterProperties.getMail().getBaseUrl());
                cvDTO = cvOutputDTO.getUrl();

            }
            candidateExportExelFileDTO.setBusinessAreaDTOs(businessDTO);
            candidateExportExelFileDTO.setManagerialPositionDTOs(managers);
            candidateExportExelFileDTO.setEmployerDTOs(employerDTO);
            candidateExportExelFileDTO.setCvDTO(cvDTO);
        }
        if (fieldsSelected.isEmpty()) {
            throw new BadRequestException("error.fieldsSelectedEmpty", null);
        }
        //write to excel file
        ByteArrayOutputStream byteArrayOutputStream = writeToExcel(candidateOutputDTOS, fieldsSelected);
        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }

    public ByteArrayOutputStream writeToExcel(List<CandidateExportExelFileDTO> candidateOutputDTOS, List<String> fieldsSelected) throws IllegalAccessException, IOException {
        Workbook workbook = new XSSFWorkbook();
        //create sheet
        Sheet sheet = workbook.createSheet("Candidates");
        int rowIndex = 0;
        Field[] fields = CandidateExportExelFileDTO.class.getDeclaredFields();
        List<Field> fieldsColumns = Arrays.stream(fields).filter(field -> fieldsSelected.contains(field.getName())).collect(Collectors.toList());
        //write header
        writeHeader(sheet, rowIndex, fieldsColumns);
        rowIndex++;
        for (CandidateExportExelFileDTO book : candidateOutputDTOS) {
            // Create row
            Row row = sheet.createRow(rowIndex);
            // Write data on row
            writeCandidate(book, row, fieldsColumns);
            rowIndex++;
        }
        // Auto resize column width
        int numberOfColumn = sheet.getRow(0).getPhysicalNumberOfCells();
        autosizeColumn(sheet, numberOfColumn);
        //write to excel
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        workbook.write(outputStream);
        return outputStream;
    }

    private void writeHeader(Sheet sheet, int rowIndex, List<Field> fieldsColumn) {
        // create CellStyle
        CellStyle cellStyle = createStyleForHeader(sheet);

        // Create row
        Row row = sheet.createRow(rowIndex);

        // Create cells
        for (int i = 0; i < fieldsColumn.size(); i++) {
            Cell cell = row.createCell(i);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(headerName(fieldsColumn.get(i).getName()));
        }
    }

    private String headerName(String fieldColumn) {
        String column = "";
        switch (fieldColumn) {
            case "id" :
                column = "ID";
                break;
            case "firstName":
                column = "First Name";
                break;
            case "lastName":
                column = "Last Name";
                break;
            case "birthYear":
                column = "Birth Year";
                break;
            case "personalNumber":
                column = "Personal Number";
                break;
            case "gender":
                column = "Gender";
                break;
            case "regionDTO":
                column = "Region";
                break;
            case "countryDTO":
                column = "Country";
                break;
            case "linkedLnLink":
                column = "LinkedLn Link";
                break;
            case "employerDTOs" :
                column = "Employers";
                break;
            case "firstEmployment":
                column = "First Employment";
                break;
            case "managerialPositionDTOs":
                column = "Managerial Positions";
                break;
            case "businessAreaDTOs":
                column = "Business Areas";
                break;
            case "city":
                column = "City";
                break;
            case "zipCode":
                column = "Zip Code";
                break;
            case "county":
                column = "County";
                break;
            case "streetAddress":
                column = "Street Address";
                break;
            case "phoneNumber":
                column = "Phone Number";
                break;
            case "secondPhoneNumber":
                column = "Second Phone Number";
                break;
            case "emailPrivate":
                column = "Private Email";
                break;
            case "emailWork":
                column = "Email Work";
                break;
            case "cvDTO":
                column = "CV";
                break;
            case "createdDate":
                column = "Create Date";
                break;
            case "createdBy":
                column = "Created By";
                break;
            case "lastModifiedDate":
                column = "Last Modified Date";
                break;
            case "lastModifiedBy":
                column = "Last Modified By";
                break;
        }
        return column;
    }

    // Auto resize column width
    private static void autosizeColumn(Sheet sheet, int lastColumn) {
        for (int columnIndex = 0; columnIndex < lastColumn; columnIndex++) {
            sheet.autoSizeColumn(columnIndex);
        }
    }

    // Write data
    private static void writeCandidate(CandidateExportExelFileDTO candidateOutputDTO, Row row, List<Field> fieldsColumn) throws IllegalAccessException {
        for (int i = 0; i < fieldsColumn.size(); i++) {
            Cell cell = row.createCell(i);
            String value = FieldUtils.readField(candidateOutputDTO, fieldsColumn.get(i).getName(), true) != null ? FieldUtils.readField(candidateOutputDTO, fieldsColumn.get(i).getName(), true).toString() : "";
            cell.setCellValue(value);
        }
    }

    // Create CellStyle for header
    private static CellStyle createStyleForHeader(Sheet sheet) {
        // Create font
        Font font = sheet.getWorkbook().createFont();
        font.setFontName("Times New Roman");
        font.setBold(true);
        font.setFontHeightInPoints((short) 14); // font size
        font.setColor(IndexedColors.WHITE.getIndex()); // text color

        // Create CellStyle
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        return cellStyle;
    }
}
