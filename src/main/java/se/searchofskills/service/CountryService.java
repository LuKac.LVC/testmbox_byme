package se.searchofskills.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.domain.Country;
import se.searchofskills.repository.CountryRepository;
import se.searchofskills.service.dto.country.CountryCreateDTO;
import se.searchofskills.service.dto.country.CountryOutputDTO;
import se.searchofskills.service.dto.country.CountryUpdateDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CountryService {
    private final CountryRepository countryRepository;

    private final UserACL userACL;

    public CountryService(CountryRepository countryRepository, UserACL userACL) {
        this.countryRepository = countryRepository;
        this.userACL = userACL;
    }

    public CountryOutputDTO create(CountryCreateDTO countryCreateDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        //get country name remove invalid tag name
        String name = countryCreateDTO.getName();
        if (name == null || name.isEmpty() || Utils.isAllSpaces(name)) {
            throw new BadRequestException("error.nameEmptyOrBlank", null);
        }
        if (!Utils.nameRegex(name)) {
            throw new BadRequestException("error.countryNameInvalid", null);
        }
        Optional<Country> countryOptional = countryRepository.findByName(name);
        if (countryOptional.isPresent()) {
            throw new BadRequestException("error.countryNameExisted", null);
        }
        Country country = new Country();
        country.setName(Utils.handleWhitespace(countryCreateDTO.getName()));
        countryRepository.save(country);
        return new CountryOutputDTO(country);
    }

    public CountryOutputDTO update(CountryUpdateDTO countryUpdateDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        // get existed country from id
        Country country = Utils.requireExists(countryRepository.findById(countryUpdateDTO.getId()), "error.countryNotFound");
        String name = countryUpdateDTO.getName();
        if (name != null) {
            if (Utils.isAllSpaces(name) || name.isEmpty()) {
                //throw country name invalid
                throw new BadRequestException("error.nameEmptyOrBlank", null);
            }
            if (!Utils.nameRegex(name)) {
                throw new BadRequestException("error.countryNameInvalid", null);
            }
            Optional<Country> countryOptional = countryRepository.findByName(Utils.handleWhitespace(name));
            if (countryOptional.isPresent() && !countryOptional.get().getId().equals(country.getId())) {
                throw new BadRequestException("error.countryNameExisted", null);
            }
            country.setName(Utils.handleWhitespace(name));
            countryRepository.save(country);
        }
        return new CountryOutputDTO(country);
    }

    public void delete(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Country country = Utils.requireExists(countryRepository.findById(id), "error.countryNotFound");
        countryRepository.delete(country);
    }

    public Page<CountryOutputDTO> getAll(Pageable pageable, String name) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Page<CountryOutputDTO> page;
        if (name == null) {
            page = countryRepository.findAll(pageable).map(CountryOutputDTO::new);
        } else {
            page = countryRepository.findAllByNameContainingIgnoreCase(pageable, name).map(CountryOutputDTO::new);
        }
        return page;
    }
}
