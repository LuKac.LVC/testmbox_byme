package se.searchofskills.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.domain.Employer;
import se.searchofskills.repository.EmployerRepository;
import se.searchofskills.service.dto.employer.EmployerCreateDTO;
import se.searchofskills.service.dto.employer.EmployerOutputDTO;
import se.searchofskills.service.dto.employer.EmployerUpdateDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class EmployerService {
    private final EmployerRepository employerRepository;

    private final UserACL userACL;

    public EmployerService(EmployerRepository employerRepository, UserACL userACL) {
        this.employerRepository = employerRepository;
        this.userACL = userACL;
    }

    public List<EmployerOutputDTO> create(EmployerCreateDTO employerCreateDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        List<EmployerOutputDTO> employerOutputDTOS = new ArrayList<>();
        List<String> employerNames = new ArrayList<>();
        for (int i = 0; i < employerCreateDTO.getNames().size(); i++) {
            String employerName = employerCreateDTO.getNames().get(i);
            if (!Utils.nameRegex(employerName)) {
                throw new BadRequestException("error.employersNameInvalid", null);
            }
            if (!Utils.isAllSpaces(employerName) && !employerName.isEmpty()) {
                employerNames.add(Utils.handleWhitespace(employerName));
            }
        }
        //get employer name remove invalid tag name
        if (employerNames.size() == 0) {
            throw new BadRequestException("error.listNameEmpty", null);
        }
        List<Employer> employerList = new ArrayList<>();
        for (String employerName : employerNames) {
            Employer employer = new Employer();
            employer.setName(employerName);
            employerList.add(employer);
        }
        employerRepository.saveAll(employerList);
        employerOutputDTOS = employerList.stream().map(EmployerOutputDTO::new).collect(Collectors.toList());
        return employerOutputDTOS;
    }

    public EmployerOutputDTO update(EmployerUpdateDTO employerDTO) {
        // check admin role
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        // get existed employer from id
        Employer employer = Utils.requireExists(employerRepository.findById(employerDTO.getId()), "error.employerNotFound");
        String name = employerDTO.getName();
        if (name.isEmpty() || Utils.isAllSpaces(name)) {
            //throw employer name invalid
            throw new BadRequestException("error.nameEmptyOrBlank", null);
        }
        if (!Utils.nameRegex(name)) {
            throw new BadRequestException("error.employersNameInvalid", null);
        }
        employer.setName(Utils.handleWhitespace(name));
        employerRepository.save(employer);
        return new EmployerOutputDTO(employer);
    }

    public void delete(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Employer employer = Utils.requireExists(employerRepository.findById(id), "error.employerNotFound");
        employerRepository.delete(employer);
    }

    public Page<EmployerOutputDTO> getAll(Pageable pageable, String name) {
        Page<EmployerOutputDTO> page;
        if (name == null) {
            page = employerRepository.findAll(pageable).map(EmployerOutputDTO::new);
        } else {
            page = employerRepository.findAllByNameContainingIgnoreCase(pageable, name).map(EmployerOutputDTO::new);
        }
        return page;
    }
}
