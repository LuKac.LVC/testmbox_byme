package se.searchofskills.service;

import se.searchofskills.domain.EmailTemplate;
import se.searchofskills.domain.EmailTracking;
import se.searchofskills.domain.Recipient;
import se.searchofskills.domain.enumeration.EmailTemplateType;

import java.time.Instant;

public class AutoSendEmailEvent {
    private final Recipient recipient;
    private final EmailTemplate emailTemplate;
    private final EmailTemplateType type;
    private final EmailTemplate mainEmailTemplate;
    private final EmailTemplate reminder1Template;
    private final Instant requestDate; // belong to reminder
    private final EmailTracking emailTracking;

    public AutoSendEmailEvent(Recipient recipient,
                              EmailTemplate emailTemplate,
                              EmailTemplateType type,
                              Instant requestDate,
                              EmailTemplate mainEmailTemplate,
                              EmailTemplate reminder1Template,
                              EmailTracking emailTracking) {
        this.recipient = recipient;
        this.emailTemplate = emailTemplate;
        this.type = type;
        this.requestDate = requestDate;
        this.mainEmailTemplate = mainEmailTemplate;
        this.reminder1Template = reminder1Template;
        this.emailTracking = emailTracking;
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public EmailTemplate getEmailTemplate() {
        return emailTemplate;
    }

    public EmailTemplateType getType() {
        return type;
    }

    public Instant getRequestDate() {
        return requestDate;
    }

    public EmailTemplate getMainEmailTemplate() {
        return mainEmailTemplate;
    }

    public EmailTemplate getReminder1Template() {
        return reminder1Template;
    }

    public EmailTracking getEmailTracking() {
        return emailTracking;
    }
}
