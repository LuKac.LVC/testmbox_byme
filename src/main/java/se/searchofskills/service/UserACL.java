package se.searchofskills.service;

import org.springframework.stereotype.Service;
import se.searchofskills.domain.User;
import se.searchofskills.repository.UserRepository;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.security.SecurityUtils;
import se.searchofskills.service.utils.Utils;

@Service
public class UserACL {
    private final UserRepository userRepository;

    public UserACL(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Check if current user is admin
     * @return true if user is logged in and has role 'admin', false otherwise
     */
    public boolean isAdmin() {
        return SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN);
    }

    public boolean canUpdate(Long userId) {
        User user = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin).get();
        User userInput = Utils.requireExists(userRepository.findById(userId), "error.userNotFound");
        return user.getId().equals(userInput.getId());
    }
}
