package se.searchofskills.service;

import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class TimeService {

    /**
     * get current time
     */
    public Instant getCurrentTime() {
        return Instant.now();
    }
}
