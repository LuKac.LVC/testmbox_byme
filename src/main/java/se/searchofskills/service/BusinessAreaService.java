package se.searchofskills.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.domain.BusinessArea;
import se.searchofskills.repository.BusinessAreaRepository;
import se.searchofskills.service.dto.business_area.BusinessAreaCreateDTO;
import se.searchofskills.service.dto.business_area.BusinessAreaOutputDTO;
import se.searchofskills.service.dto.business_area.BusinessAreaUpdateDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class BusinessAreaService {
    private final UserACL userACL;

    private final BusinessAreaRepository businessAreaRepository;

    public BusinessAreaService(UserACL userACL, BusinessAreaRepository businessAreaRepository) {
        this.userACL = userACL;
        this.businessAreaRepository = businessAreaRepository;
    }

    public List<BusinessAreaOutputDTO> create(BusinessAreaCreateDTO businessAreaCreateDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        List<BusinessAreaOutputDTO> businessAreaOutputDTOS = new ArrayList<>();
        List<String> businessAreaNames = new ArrayList<>();
        for (int i = 0; i < businessAreaCreateDTO.getNames().size(); i++) {
            String businessAreaName = businessAreaCreateDTO.getNames().get(i);
            if (businessAreaName == null ) {
                continue;
            }
            if (!Utils.isAllSpaces(businessAreaName) && !businessAreaName.isEmpty()) {
                businessAreaNames.add(Utils.handleWhitespace(businessAreaName));
            }
            if (!Utils.nameRegex(businessAreaName)) {
                throw new BadRequestException("error.businessAreasInvalid", null);
            }
        }
        //get business area name remove invalid tag name
        if (businessAreaNames.size() == 0) {
            throw new BadRequestException("error.listNameEmpty", null);
        }
        List<BusinessArea> businessAreas = new ArrayList<>();
        for (String managerialPositionName : businessAreaNames) {
            BusinessArea businessArea = new BusinessArea();
            businessArea.setName(managerialPositionName);
            businessAreas.add(businessArea);
        }
        businessAreaRepository.saveAll(businessAreas);
        businessAreaOutputDTOS = businessAreas.stream().map(BusinessAreaOutputDTO::new).collect(Collectors.toList());
        return businessAreaOutputDTOS;
    }

    public BusinessAreaOutputDTO update(BusinessAreaUpdateDTO businessAreaUpdateDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        // get existed business area from id
        BusinessArea businessArea = Utils.requireExists(businessAreaRepository.findById(businessAreaUpdateDTO.getId()), "error.businessAreaNotFound");
        String name = businessAreaUpdateDTO.getName();
        if (name != null) {
            if (name.isEmpty() || Utils.isAllSpaces(name)) {
                //throw business name invalid
                throw new BadRequestException("error.nameEmptyOrBlank", null);
            }
            if (!Utils.nameRegex(name)) {
                throw new BadRequestException("error.businessAreasInvalid", null);
            }
            businessArea.setName(Utils.handleWhitespace(name));
            businessAreaRepository.save(businessArea);
        }
        return new BusinessAreaOutputDTO(businessArea);
    }

    public void delete(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        BusinessArea managerialPosition = Utils.requireExists(businessAreaRepository.findById(id), "error.businessAreaNotFound");
        businessAreaRepository.delete(managerialPosition);
    }

    public Page<BusinessAreaOutputDTO> getAll(Pageable pageable, String name) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Page<BusinessAreaOutputDTO> businessAreaOutputDTOS;
        if (name == null) {
            businessAreaOutputDTOS = businessAreaRepository.findAll(pageable).map(BusinessAreaOutputDTO::new);
        } else {
            businessAreaOutputDTOS = businessAreaRepository.findAllByNameContainingIgnoreCase(pageable, name).map(BusinessAreaOutputDTO::new);
        }
        return businessAreaOutputDTOS;
    }
}
