package se.searchofskills.service.dto;

import se.searchofskills.domain.RecipientList;

import javax.validation.constraints.NotBlank;
import java.time.Instant;

public class RecipientListDTO {
    private long id;
    private String name;
    private Instant createdDate;

    public RecipientListDTO() {
    }

    public RecipientListDTO(RecipientList recipientList) {
        this.id = recipientList.getId();
        this.name = recipientList.getName();
        this.createdDate = recipientList.getCreatedDate();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }
}
