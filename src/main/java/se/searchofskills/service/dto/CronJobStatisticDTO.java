package se.searchofskills.service.dto;

import se.searchofskills.domain.CronJobStatistic;

public class CronJobStatisticDTO {
    private Long id;
    private int totalEmailToSend;
    private int emailSent;
    private int reminderSent1;
    private int reminderSent2;
    private int unansweredEmail;

    public CronJobStatisticDTO(CronJobStatistic cronJobStatistic) {
        this.id = cronJobStatistic.getId();
        this.totalEmailToSend = cronJobStatistic.getTotalEmailToSend();
        this.emailSent = cronJobStatistic.getEmailSent();
        this.reminderSent1 = cronJobStatistic.getReminderSent1();
        this.reminderSent2 = cronJobStatistic.getReminderSent2();
        this.unansweredEmail = cronJobStatistic.getUnansweredEmail();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTotalEmailToSend() {
        return totalEmailToSend;
    }

    public void setTotalEmailToSend(int totalEmailToSend) {
        this.totalEmailToSend = totalEmailToSend;
    }

    public int getEmailSent() {
        return emailSent;
    }

    public void setEmailSent(int emailSent) {
        this.emailSent = emailSent;
    }

    public int getReminderSent1() {
        return reminderSent1;
    }

    public void setReminderSent1(int reminderSent1) {
        this.reminderSent1 = reminderSent1;
    }

    public int getReminderSent2() {
        return reminderSent2;
    }

    public void setReminderSent2(int reminderSent2) {
        this.reminderSent2 = reminderSent2;
    }

    public int getUnansweredEmail() {
        return unansweredEmail;
    }

    public void setUnansweredEmail(int unansweredEmail) {
        this.unansweredEmail = unansweredEmail;
    }
}
