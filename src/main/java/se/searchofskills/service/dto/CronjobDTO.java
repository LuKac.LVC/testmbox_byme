package se.searchofskills.service.dto;

import se.searchofskills.domain.CronJob;
import se.searchofskills.domain.enumeration.Frequency;
import se.searchofskills.domain.enumeration.WeekDay;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;

public class CronjobDTO {
    private Long id;

    @NotNull
    private String name;

    @NotNull
    Long recipientListId;

    @NotNull
    Long templateMainId;

    @NotNull
    Long templateReminder1Id;

    @NotNull
    Long templateReminder2Id;

    @Min(1)
    int templateReminderTime1;

    @Min(1)
    int templateReminderTime2;

    @Min(1)
    private int chunkNumber;

    @NotNull
    private WeekDay weekday;

    @Min(0)
    private int sendHour;

    @NotNull
    private Frequency frequency;

    @Max(4)
    @Min(0)
    private int weekOfMonth;

    @NotNull
    private Instant startDate;

    @NotNull
    private Instant endDate;

    public CronjobDTO() {
    }

    public CronjobDTO(CronJob cronJob) {
        this.id = cronJob.getId();
        this.recipientListId = cronJob.getRecipientList() != null ? cronJob.getRecipientList().getId() : null;
        this.chunkNumber = cronJob.getChunkNumber();
        this.endDate = cronJob.getEndDate();
        this.frequency = cronJob.getFrequency();
        this.sendHour = cronJob.getSendHour();
        this.weekday = cronJob.getWeekday();
        this.weekOfMonth = cronJob.getWeekOfMonth();
        this.startDate = cronJob.getStartDate();
        this.name = cronJob.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Long getRecipientListId() {
        return recipientListId;
    }

    public void setRecipientListId(Long recipientListId) {
        this.recipientListId = recipientListId;
    }

    public Long getTemplateMainId() {
        return templateMainId;
    }

    public void setTemplateMainId(Long templateMainId) {
        this.templateMainId = templateMainId;
    }

    public Long getTemplateReminder1Id() {
        return templateReminder1Id;
    }

    public void setTemplateReminder1Id(Long templateReminder1Id) {
        this.templateReminder1Id = templateReminder1Id;
    }

    public Long getTemplateReminder2Id() {
        return templateReminder2Id;
    }

    public void setTemplateReminder2Id(Long templateReminder2Id) {
        this.templateReminder2Id = templateReminder2Id;
    }

    public int getTemplateReminderTime1() {
        return templateReminderTime1;
    }

    public void setTemplateReminderTime1(int templateReminderTime1) {
        this.templateReminderTime1 = templateReminderTime1;
    }

    public int getTemplateReminderTime2() {
        return templateReminderTime2;
    }

    public void setTemplateReminderTime2(int templateReminderTime2) {
        this.templateReminderTime2 = templateReminderTime2;
    }

    public int getChunkNumber() {
        return chunkNumber;
    }

    public void setChunkNumber(int chunkNumber) {
        this.chunkNumber = chunkNumber;
    }

    public WeekDay getWeekday() {
        return weekday;
    }

    public void setWeekday(WeekDay weekday) {
        this.weekday = weekday;
    }

    public int getSendHour() {
        return sendHour;
    }

    public void setSendHour(int sendHour) {
        this.sendHour = sendHour;
    }

    public Frequency getFrequency() {
        return frequency;
    }

    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
    }

    public int getWeekOfMonth() {
        return weekOfMonth;
    }

    public void setWeekOfMonth(int weekOfMonth) {
        this.weekOfMonth = weekOfMonth;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }
}
