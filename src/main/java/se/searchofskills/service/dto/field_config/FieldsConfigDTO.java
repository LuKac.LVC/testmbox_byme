package se.searchofskills.service.dto.field_config;

import java.util.List;

public class FieldsConfigDTO {
    List<String> fieldConfig;

    public List<String> getFieldConfig() {
        return fieldConfig;
    }

    public void setFieldConfig(List<String> fieldConfig) {
        this.fieldConfig = fieldConfig;
    }

    public FieldsConfigDTO() {
    }
}
