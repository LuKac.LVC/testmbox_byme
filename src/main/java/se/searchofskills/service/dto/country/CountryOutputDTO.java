package se.searchofskills.service.dto.country;

import se.searchofskills.domain.Country;

import java.time.Instant;

public class CountryOutputDTO {
    private long id;
    private String name;
    private Instant createdDate;
    private String createdBy;
    private Instant lastModifiedDate;
    private String lastModifiedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public CountryOutputDTO(Country country) {
        this.id = country.getId();
        this.name = country.getName();
        this.createdDate = country.getCreatedDate();
        this.createdBy = country.getCreatedBy();
        this.lastModifiedDate = country.getLastModifiedDate();
        this.lastModifiedBy = country.getLastModifiedBy();
    }
}
