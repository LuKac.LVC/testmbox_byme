package se.searchofskills.service.dto.country;

import java.util.List;

public class CountryCreateDTO {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CountryCreateDTO() {
    }
}
