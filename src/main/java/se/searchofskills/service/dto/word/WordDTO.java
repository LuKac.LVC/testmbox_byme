package se.searchofskills.service.dto.word;

import se.searchofskills.domain.Word;

public class WordDTO {
    private long id;
    private String value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public WordDTO(Word word) {
        this.id = word.getId();
        this.value = word.getName();
    }
}
