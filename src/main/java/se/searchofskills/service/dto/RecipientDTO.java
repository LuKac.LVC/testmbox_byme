package se.searchofskills.service.dto;

import se.searchofskills.domain.Recipient;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RecipientDTO {
    private long id;
    private RecipientListDTO recipientListDTO;
    private long recipientIdentify;
    private String county;
    private String title;
    private String procuringAgency;
    private String referenceNumber;
    @NotBlank
    @Email
    @Pattern(regexp = "^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$")
    @Size(min = 5, max = 254)
    private String registrar;
    @NotBlank
    @Email
    @Pattern(regexp = "^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$")
    @Size(min = 5, max = 254)
    private String email;
    private String procurementUrl;

    public RecipientDTO(Recipient recipient) {
        this.id = recipient.getId();
        this.recipientListDTO = new RecipientListDTO(recipient.getRecipientList());
        this.county = recipient.getCounty();
        this.title = recipient.getTitle();
        this.procuringAgency = recipient.getProcuringAgency();
        this.referenceNumber = recipient.getReferenceNumber();
        this.registrar = recipient.getRegistrar();
        this.email = recipient.getEmail();
        this.procurementUrl = recipient.getProcurementUrl();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RecipientListDTO getRecipientListDTO() {
        return recipientListDTO;
    }

    public void setRecipientListDTO(RecipientListDTO recipientListDTO) {
        this.recipientListDTO = recipientListDTO;
    }

    public long getRecipientIdentify() {
        return recipientIdentify;
    }

    public void setRecipientIdentify(long recipientIdentify) {
        this.recipientIdentify = recipientIdentify;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProcuringAgency() {
        return procuringAgency;
    }

    public void setProcuringAgency(String procuringAgency) {
        this.procuringAgency = procuringAgency;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProcurementUrl() {
        return procurementUrl;
    }

    public void setProcurementUrl(String procurementUrl) {
        this.procurementUrl = procurementUrl;
    }
}
