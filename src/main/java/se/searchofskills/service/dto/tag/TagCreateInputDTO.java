package se.searchofskills.service.dto.tag;

import java.util.List;

public class TagCreateInputDTO {
    private long id;
    private List<String> tags;
    private List<Long> categories;
    private boolean block;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<Long> getCategories() {
        return categories;
    }

    public void setCategories(List<Long> categories) {
        this.categories = categories;
    }

    public boolean isBlock() {
        return block;
    }

    public void setBlock(boolean block) {
        this.block = block;
    }

    public TagCreateInputDTO() {
    }
}
