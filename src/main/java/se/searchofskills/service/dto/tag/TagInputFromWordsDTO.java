package se.searchofskills.service.dto.tag;

import java.util.List;

public class TagInputFromWordsDTO {
    private List<String> tags;

    private List<String> blockTags;

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getBlockTags() {
        return blockTags;
    }

    public void setBlockTags(List<String> blockTags) {
        this.blockTags = blockTags;
    }
}
