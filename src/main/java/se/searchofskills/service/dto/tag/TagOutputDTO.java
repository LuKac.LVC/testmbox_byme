package se.searchofskills.service.dto.tag;

import se.searchofskills.domain.Tag;
import se.searchofskills.service.dto.category.CategoryOutputDTO;

import java.util.List;

public class TagOutputDTO {
    private long id;
    private String name;
    private int search;
    private List<CategoryOutputDTO> categories;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CategoryOutputDTO> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryOutputDTO> categories) {
        this.categories = categories;
    }

    public int getSearch() {
        return search;
    }

    public void setSearch(int search) {
        this.search = search;
    }

    public TagOutputDTO(Tag tag) {
        this.id = tag.getId();
        this.name = tag.getName();
        this.search = tag.getSearch();
    }
}
