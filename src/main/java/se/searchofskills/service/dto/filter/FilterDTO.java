package se.searchofskills.service.dto.filter;

import se.searchofskills.domain.enumeration.Operator;

import java.util.List;

public class FilterDTO {
    private List<FilterWithConditionDTO> filterWithConditions;

    private List<Operator> operatorsOrder;

    private String freeText;

    private long filterListId;

    public List<FilterWithConditionDTO> getFilterWithConditions() {
        return filterWithConditions;
    }

    public void setFilterWithConditions(List<FilterWithConditionDTO> filterWithConditions) {
        this.filterWithConditions = filterWithConditions;
    }

    public List<Operator> getOperatorsOrder() {
        return operatorsOrder;
    }

    public void setOperatorsOrder(List<Operator> operatorsOrder) {
        this.operatorsOrder = operatorsOrder;
    }

    public String getFreeText() {
        return freeText;
    }

    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

    public long getFilterListId() {
        return filterListId;
    }

    public void setFilterListId(long filterListId) {
        this.filterListId = filterListId;
    }

    public FilterDTO(List<FilterWithConditionDTO> filterWithConditions, List<Operator> operatorsOrder, String freeText, long filterListId) {
        this.filterWithConditions = filterWithConditions;
        this.operatorsOrder = operatorsOrder;
        this.freeText = freeText;
        this.filterListId = filterListId;
    }
}
