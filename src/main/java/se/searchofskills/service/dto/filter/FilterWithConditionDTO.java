package se.searchofskills.service.dto.filter;

import se.searchofskills.domain.enumeration.CandidateColumn;
import se.searchofskills.domain.enumeration.FilterCondition;

public class FilterWithConditionDTO {
    private CandidateColumn field;

    private FilterCondition condition;

    private String value;

    public CandidateColumn getField() {
        return field;
    }

    public void setField(CandidateColumn field) {
        this.field = field;
    }

    public FilterCondition getCondition() {
        return condition;
    }

    public void setCondition(FilterCondition condition) {
        this.condition = condition;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public FilterWithConditionDTO(CandidateColumn field, FilterCondition condition, String value) {
        this.field = field;
        this.condition = condition;
        this.value = value;
    }

    public FilterWithConditionDTO() {
    }
}
