package se.searchofskills.service.dto;

import se.searchofskills.domain.enumeration.CronJobStatus;
import se.searchofskills.domain.enumeration.Frequency;
import se.searchofskills.domain.enumeration.WeekDay;

import java.time.Instant;

public class CronjobDataDTO {
    private Long id;

    private String name;

    private RecipientListDTO recipientList;

    private EmailTemplateDTO templateMain;

    private EmailTemplateDTO templateReminder1;

    private EmailTemplateDTO templateReminder2;

    private int chunkNumber;

    private WeekDay weekday;

    private CronJobStatus status;

    private int sendHour;

    private Instant startDate;

    private Instant endDate;

    private int weekOfMonth;

    private Frequency frequency;

    public CronjobDataDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getWeekOfMonth() {
        return weekOfMonth;
    }

    public void setWeekOfMonth(int weekOfMonth) {
        this.weekOfMonth = weekOfMonth;
    }

    public RecipientListDTO getRecipientList() {
        return recipientList;
    }

    public void setRecipientList(RecipientListDTO recipientList) {
        this.recipientList = recipientList;
    }

    public EmailTemplateDTO getTemplateMain() {
        return templateMain;
    }

    public void setTemplateMain(EmailTemplateDTO templateMain) {
        this.templateMain = templateMain;
    }

    public EmailTemplateDTO getTemplateReminder1() {
        return templateReminder1;
    }

    public void setTemplateReminder1(EmailTemplateDTO templateReminder1) {
        this.templateReminder1 = templateReminder1;
    }

    public EmailTemplateDTO getTemplateReminder2() {
        return templateReminder2;
    }

    public void setTemplateReminder2(EmailTemplateDTO templateReminder2) {
        this.templateReminder2 = templateReminder2;
    }

    public int getChunkNumber() {
        return chunkNumber;
    }

    public void setChunkNumber(int chunkNumber) {
        this.chunkNumber = chunkNumber;
    }

    public WeekDay getWeekday() {
        return weekday;
    }

    public void setWeekday(WeekDay weekday) {
        this.weekday = weekday;
    }

    public CronJobStatus getStatus() {
        return status;
    }

    public void setStatus(CronJobStatus status) {
        this.status = status;
    }

    public int getSendHour() {
        return sendHour;
    }

    public void setSendHour(int sendHour) {
        this.sendHour = sendHour;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Frequency getFrequency() {
        return frequency;
    }

    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
    }
}
