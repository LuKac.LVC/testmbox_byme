package se.searchofskills.service.dto.category;

import se.searchofskills.domain.Category;

public class CategoryUpdateDTO {
    private long id;

    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CategoryUpdateDTO() {
    }
}
