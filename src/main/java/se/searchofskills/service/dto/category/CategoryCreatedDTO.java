package se.searchofskills.service.dto.category;

import java.util.List;

public class CategoryCreatedDTO {
    private long id;

    private List<String> names;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public CategoryCreatedDTO() {
    }
}
