package se.searchofskills.service.dto.category;

import se.searchofskills.domain.Category;

import java.time.Instant;

public class CategoryOutputDTO {
    private long id;

    private String name;

    private Instant createdDate;
    private String createdBy;
    private Instant lastModifiedDate;
    private String lastModifiedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return lastModifiedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.lastModifiedDate = updatedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public CategoryOutputDTO(Category category) {
        this.id = category.getId();
        this.name = category.getName();
        this.createdDate = category.getCreatedDate();
        this.createdBy = category.getCreatedBy();
        this.lastModifiedDate = category.getLastModifiedDate();
        this.lastModifiedBy = category.getLastModifiedBy();
    }

    public CategoryOutputDTO() {
    }
}
