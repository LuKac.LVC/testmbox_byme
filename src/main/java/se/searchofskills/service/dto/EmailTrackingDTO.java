package se.searchofskills.service.dto;

import se.searchofskills.domain.EmailTracking;
import se.searchofskills.domain.enumeration.EmailStatus;

import javax.validation.constraints.NotNull;
import java.time.Instant;

public class EmailTrackingDTO {
    private Long id;

    @NotNull
    private CronjobDTO cronJob;

    @NotNull
    private RecipientDTO recipient;

    private String registrar;

    @NotNull
    private Instant reminderDate1;

    @NotNull
    private Instant reminderDate2;

    private boolean sentReminder1;

    private boolean sentReminder2;

    @NotNull
    private EmailStatus status;

    private boolean deleted;

    private boolean archived;

    private String comment;

    @NotNull
    private Instant createdDate;

    public EmailTrackingDTO() {
    }

    public EmailTrackingDTO(EmailTracking emailTracking) {
        this.id = emailTracking.getId();
        this.recipient = emailTracking.getRecipient() != null ? new RecipientDTO(emailTracking.getRecipient()) : null;
        this.cronJob = emailTracking.getCronJob() != null ? new CronjobDTO(emailTracking.getCronJob()) : null;
        this.status = emailTracking.getStatus();
        this.reminderDate1 = emailTracking.getReminderDate1();
        this.reminderDate2 = emailTracking.getReminderDate2();
        this.createdDate = emailTracking.getCreatedDate();
        this.sentReminder1 = emailTracking.isSentReminder1();
        this.sentReminder2 = emailTracking.isSentReminder2();
        this.comment = emailTracking.getComment();
        this.registrar = emailTracking.getRegistrar();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CronjobDTO getCronJob() {
        return cronJob;
    }

    public void setCronJob(CronjobDTO cronJob) {
        this.cronJob = cronJob;
    }

    public RecipientDTO getRecipient() {
        return recipient;
    }

    public void setRecipient(RecipientDTO recipient) {
        this.recipient = recipient;
    }

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }

    public Instant getReminderDate1() {
        return reminderDate1;
    }

    public void setReminderDate1(Instant reminderDate1) {
        this.reminderDate1 = reminderDate1;
    }

    public Instant getReminderDate2() {
        return reminderDate2;
    }

    public void setReminderDate2(Instant reminderDate2) {
        this.reminderDate2 = reminderDate2;
    }

    public boolean isSentReminder1() {
        return sentReminder1;
    }

    public void setSentReminder1(boolean sentReminder1) {
        this.sentReminder1 = sentReminder1;
    }

    public boolean isSentReminder2() {
        return sentReminder2;
    }

    public void setSentReminder2(boolean sentReminder2) {
        this.sentReminder2 = sentReminder2;
    }

    public EmailStatus getStatus() {
        return status;
    }

    public void setStatus(EmailStatus status) {
        this.status = status;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }
}
