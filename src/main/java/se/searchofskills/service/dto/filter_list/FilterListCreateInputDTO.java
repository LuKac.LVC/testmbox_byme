package se.searchofskills.service.dto.filter_list;

public class FilterListCreateInputDTO {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
