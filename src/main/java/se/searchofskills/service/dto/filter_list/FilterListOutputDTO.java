package se.searchofskills.service.dto.filter_list;

import se.searchofskills.domain.FilterList;

import java.util.List;

public class FilterListOutputDTO {
    private long id;

    private String name;

    private List<Long> candidateIds;

    private Long userId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getCandidateIds() {
        return candidateIds;
    }

    public void setCandidateIds(List<Long> candidateIds) {
        this.candidateIds = candidateIds;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public FilterListOutputDTO(FilterList filterList) {
        this.id = filterList.getId();
        this.name = filterList.getName();
        this.userId = filterList.getUser().getId();
    }
}
