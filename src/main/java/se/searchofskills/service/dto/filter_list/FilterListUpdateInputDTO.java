package se.searchofskills.service.dto.filter_list;

import se.searchofskills.domain.enumeration.Action;

import java.util.List;

public class FilterListUpdateInputDTO {
    private long id;

    private List<Long> candidateIds;

    private Action action;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Long> getCandidateIds() {
        return candidateIds;
    }

    public void setCandidateIds(List<Long> candidateIds) {
        this.candidateIds = candidateIds;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
}
