package se.searchofskills.service.dto.managerial_position;

import se.searchofskills.domain.ManagerialPosition;

import java.time.Instant;

public class ManagerialPositionOutputDTO {
    private long id;

    private String name;

    private Instant createdDate;

    private String createdBy;

    private Instant updatedDate;

    private String lastModifyBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getLastModifyBy() {
        return lastModifyBy;
    }

    public void setLastModifyBy(String lastModifyBy) {
        this.lastModifyBy = lastModifyBy;
    }

    public ManagerialPositionOutputDTO(ManagerialPosition managerialPosition) {
        this.id = managerialPosition.getId();
        this.name = managerialPosition.getName();
        this.createdDate = managerialPosition.getCreatedDate();
        this.createdBy = managerialPosition.getCreatedBy();
        this.updatedDate = managerialPosition.getLastModifiedDate();
        this.lastModifyBy = managerialPosition.getLastModifiedBy();
    }
}
