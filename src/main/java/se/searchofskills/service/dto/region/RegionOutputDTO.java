package se.searchofskills.service.dto.region;

import se.searchofskills.domain.Region;
import se.searchofskills.service.dto.country.CountryOutputDTO;

import java.time.Instant;

public class RegionOutputDTO {
    private long id;
    private String name;
    private CountryOutputDTO country;

    private Instant createdDate;
    private String createdBy;
    private Instant updatedDate;
    private String lastModifyBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CountryOutputDTO getCountry() {
        return country;
    }

    public void setCountry(CountryOutputDTO country) {
        this.country = country;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getLastModifyBy() {
        return lastModifyBy;
    }

    public void setLastModifyBy(String lastModifyBy) {
        this.lastModifyBy = lastModifyBy;
    }

    public RegionOutputDTO(Region region) {
        this.id = region.getId();
        this.name = region.getName();
        this.country = new CountryOutputDTO(region.getCountry());
        this.createdDate = region.getCreatedDate();
        this.createdBy = region.getCreatedBy();
        this.updatedDate = region.getCreatedDate();
        this.lastModifyBy = region.getLastModifiedBy();
    }
}
