package se.searchofskills.service.dto.candidate;

import se.searchofskills.domain.Candidate;
import se.searchofskills.domain.enumeration.Gender;

import java.time.Instant;

public class CandidateExportExelFileDTO {
    private long id;

    private String firstName;

    private String lastName;

    private String birthYear;

    private String personalNumber;

    private Gender gender;

    private String regionDTO;

    private String countryDTO;

    private String linkedLnLink;

    private String employerDTOs;

    private int firstEmployment;

    private String managerialPositionDTOs;

    private String businessAreaDTOs;

    private String city;

    private String zipCode;

    private String county;

    private String streetAddress;

    private String phoneNumber;

    private String secondPhoneNumber;

    private String emailPrivate;

    private String emailWork;

    private String cvDTO;

    private String createdDate;

    private String createdBy;

    private String lastModifiedDate;

    private String lastModifiedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getRegionDTO() {
        return regionDTO;
    }

    public void setRegionDTO(String regionDTO) {
        this.regionDTO = regionDTO;
    }

    public String getCountryDTO() {
        return countryDTO;
    }

    public void setCountryDTO(String countryDTO) {
        this.countryDTO = countryDTO;
    }

    public String getLinkedLnLink() {
        return linkedLnLink;
    }

    public void setLinkedLnLink(String linkedLnLink) {
        this.linkedLnLink = linkedLnLink;
    }

    public String getEmployerDTOs() {
        return employerDTOs;
    }

    public void setEmployerDTOs(String employerDTOs) {
        this.employerDTOs = employerDTOs;
    }

    public int getFirstEmployment() {
        return firstEmployment;
    }

    public void setFirstEmployment(int firstEmployment) {
        this.firstEmployment = firstEmployment;
    }

    public String getManagerialPositionDTOs() {
        return managerialPositionDTOs;
    }

    public void setManagerialPositionDTOs(String managerialPositionDTOs) {
        this.managerialPositionDTOs = managerialPositionDTOs;
    }

    public String getBusinessAreaDTOs() {
        return businessAreaDTOs;
    }

    public void setBusinessAreaDTOs(String businessAreaDTOs) {
        this.businessAreaDTOs = businessAreaDTOs;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSecondPhoneNumber() {
        return secondPhoneNumber;
    }

    public void setSecondPhoneNumber(String secondPhoneNumber) {
        this.secondPhoneNumber = secondPhoneNumber;
    }

    public String getEmailPrivate() {
        return emailPrivate;
    }

    public void setEmailPrivate(String emailPrivate) {
        this.emailPrivate = emailPrivate;
    }

    public String getEmailWork() {
        return emailWork;
    }

    public void setEmailWork(String emailWork) {
        this.emailWork = emailWork;
    }

    public String getCvDTO() {
        return cvDTO;
    }

    public void setCvDTO(String cvDTO) {
        this.cvDTO = cvDTO;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public CandidateExportExelFileDTO(Candidate candidate) {
        this.id = candidate.getId();
        this.firstName = candidate.getFirstName();
        this.lastName = candidate.getLastName();
        this.birthYear = candidate.getBirthYear();
        this.personalNumber = candidate.getPersonalNumber();
        this.gender = candidate.getGender();
        this.linkedLnLink = candidate.getLinkedLnLink();
        this.firstEmployment = candidate.getFirstEmployment();
        this.city = candidate.getCity();
        this.regionDTO = candidate.getRegion().getName();
        this.countryDTO = candidate.getCountry().getName() ;
        this.zipCode = candidate.getZipCode();
        this.county = candidate.getCounty();
        this.streetAddress = candidate.getStreetAddress();
        this.phoneNumber = candidate.getPhoneNumber();
        this.secondPhoneNumber = candidate.getSecondPhoneNumber();
        this.emailPrivate = candidate.getEmailPrivate();
        this.emailWork = candidate.getEmailWork();
        this.createdDate = candidate.getCreatedDate().toString();
        this.createdBy = candidate.getCreatedBy();
        this.lastModifiedDate = candidate.getLastModifiedDate().toString();
        this.lastModifiedBy = candidate.getLastModifiedBy();
    }
}
