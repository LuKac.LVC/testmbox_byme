package se.searchofskills.service.dto.candidate;

import se.searchofskills.domain.enumeration.Gender;

import java.util.List;

public class CandidateCreateInputDTO {
    private long id;

    private String firstName;

    private String lastName;

    private String birthYear;

    private String personalNumber;

    private Gender gender;

    private long regionId;

    private long countryId;

    private String linkedLnLink;

    private List<Long> employerIds;

    private int firstEmployment;

    private List<Long> managerialPositionIds;

    private List<Long> businessAreaIds;

    private String city;

    private String zipCode;

    private String county;

    private String streetAddress;

    private String phoneNumber;

    private String secondPhoneNumber;

    private String emailPrivate;

    private String emailWork;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public long getRegionId() {
        return regionId;
    }

    public void setRegionId(long regionId) {
        this.regionId = regionId;
    }

    public long getCountryId() {
        return countryId;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }

    public String getLinkedLnLink() {
        return linkedLnLink;
    }

    public void setLinkedLnLink(String linkedLnLink) {
        this.linkedLnLink = linkedLnLink;
    }

    public List<Long> getEmployerIds() {
        return employerIds;
    }

    public void setEmployerIds(List<Long> employerIds) {
        this.employerIds = employerIds;
    }

    public int getFirstEmployment() {
        return firstEmployment;
    }

    public void setFirstEmployment(int firstEmployment) {
        this.firstEmployment = firstEmployment;
    }

    public List<Long> getManagerialPositionIds() {
        return managerialPositionIds;
    }

    public void setManagerialPositionIds(List<Long> managerialPositionIds) {
        this.managerialPositionIds = managerialPositionIds;
    }

    public List<Long> getBusinessAreaIds() {
        return businessAreaIds;
    }

    public void setBusinessAreaIds(List<Long> businessAreaIds) {
        this.businessAreaIds = businessAreaIds;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSecondPhoneNumber() {
        return secondPhoneNumber;
    }

    public void setSecondPhoneNumber(String secondPhoneNumber) {
        this.secondPhoneNumber = secondPhoneNumber;
    }

    public String getEmailPrivate() {
        return emailPrivate;
    }

    public void setEmailPrivate(String emailPrivate) {
        this.emailPrivate = emailPrivate;
    }

    public String getEmailWork() {
        return emailWork;
    }

    public void setEmailWork(String emailWork) {
        this.emailWork = emailWork;
    }
}
