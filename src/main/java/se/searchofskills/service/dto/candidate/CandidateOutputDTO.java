package se.searchofskills.service.dto.candidate;

import se.searchofskills.domain.Candidate;
import se.searchofskills.domain.enumeration.Gender;
import se.searchofskills.service.dto.business_area.BusinessAreaOutputDTO;
import se.searchofskills.service.dto.country.CountryOutputDTO;
import se.searchofskills.service.dto.cv.CvOutputDTO;
import se.searchofskills.service.dto.employer.EmployerOutputDTO;
import se.searchofskills.service.dto.managerial_position.ManagerialPositionOutputDTO;
import se.searchofskills.service.dto.region.RegionOutputDTO;

import java.time.Instant;
import java.util.List;

public class CandidateOutputDTO {
    private long id;

    private String firstName;

    private String lastName;

    private String birthYear;

    private String personalNumber;

    private Gender gender;

    private RegionOutputDTO regionDTO;

    private CountryOutputDTO countryDTO;

    private String linkedLnLink;

    private List<EmployerOutputDTO> employerDTOs;

    private int firstEmployment;

    private List<ManagerialPositionOutputDTO> managerialPositionDTOs;

    private List<BusinessAreaOutputDTO> businessAreaDTOs;

    private String city;

    private String zipCode;

    private String county;

    private String streetAddress;

    private String phoneNumber;

    private String secondPhoneNumber;

    private String emailPrivate;

    private String emailWork;

    private CvOutputDTO cvDTO;

    private Instant createdDate;

    private String createdBy;

    private Instant lastModifiedDate;

    private String lastModifiedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public RegionOutputDTO getRegionDTO() {
        return regionDTO;
    }

    public void setRegionDTO(RegionOutputDTO regionDTO) {
        this.regionDTO = regionDTO;
    }

    public CountryOutputDTO getCountryDTO() {
        return countryDTO;
    }

    public void setCountryDTO(CountryOutputDTO countryDTO) {
        this.countryDTO = countryDTO;
    }

    public String getLinkedLnLink() {
        return linkedLnLink;
    }

    public void setLinkedLnLink(String linkedLnLink) {
        this.linkedLnLink = linkedLnLink;
    }

    public List<EmployerOutputDTO> getEmployerDTOs() {
        return employerDTOs;
    }

    public void setEmployerDTOs(List<EmployerOutputDTO> employerDTOs) {
        this.employerDTOs = employerDTOs;
    }

    public int getFirstEmployment() {
        return firstEmployment;
    }

    public void setFirstEmployment(int firstEmployment) {
        this.firstEmployment = firstEmployment;
    }

    public List<ManagerialPositionOutputDTO> getManagerialPositionDTOs() {
        return managerialPositionDTOs;
    }

    public void setManagerialPositionDTOs(List<ManagerialPositionOutputDTO> managerialPositionDTOs) {
        this.managerialPositionDTOs = managerialPositionDTOs;
    }

    public List<BusinessAreaOutputDTO> getBusinessAreaDTOs() {
        return businessAreaDTOs;
    }

    public void setBusinessAreaDTOs(List<BusinessAreaOutputDTO> businessAreaDTOs) {
        this.businessAreaDTOs = businessAreaDTOs;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSecondPhoneNumber() {
        return secondPhoneNumber;
    }

    public void setSecondPhoneNumber(String secondPhoneNumber) {
        this.secondPhoneNumber = secondPhoneNumber;
    }

    public String getEmailPrivate() {
        return emailPrivate;
    }

    public void setEmailPrivate(String emailPrivate) {
        this.emailPrivate = emailPrivate;
    }

    public String getEmailWork() {
        return emailWork;
    }

    public void setEmailWork(String emailWork) {
        this.emailWork = emailWork;
    }

    public CvOutputDTO getCvDTO() {
        return cvDTO;
    }

    public void setCvDTO(CvOutputDTO cvDTO) {
        this.cvDTO = cvDTO;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public CandidateOutputDTO(Candidate candidate) {
        this.id = candidate.getId();
        this.firstName = candidate.getFirstName();
        this.lastName = candidate.getLastName();
        this.birthYear = candidate.getBirthYear();
        this.personalNumber = candidate.getPersonalNumber();
        this.gender = candidate.getGender();
        this.linkedLnLink = candidate.getLinkedLnLink();
        this.firstEmployment = candidate.getFirstEmployment();
        this.city = candidate.getCity();
        this.regionDTO = candidate.getRegion() != null ? new RegionOutputDTO(candidate.getRegion()) : null;
        this.countryDTO = candidate.getCountry() != null ? new CountryOutputDTO(candidate.getCountry()) : null;
        this.zipCode = candidate.getZipCode();
        this.county = candidate.getCounty();
        this.streetAddress = candidate.getStreetAddress();
        this.phoneNumber = candidate.getPhoneNumber();
        this.secondPhoneNumber = candidate.getSecondPhoneNumber();
        this.emailPrivate = candidate.getEmailPrivate();
        this.emailWork = candidate.getEmailWork();
        this.createdDate = candidate.getCreatedDate();
        this.createdBy = candidate.getCreatedBy();
        this.lastModifiedBy = candidate.getLastModifiedBy();
        this.lastModifiedDate = candidate.getLastModifiedDate();
    }
}
