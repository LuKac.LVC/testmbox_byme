package se.searchofskills.service.dto.business_area;

import java.util.List;

public class BusinessAreaCreateDTO {
    private List<String> names;

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public BusinessAreaCreateDTO() {
    }
}
