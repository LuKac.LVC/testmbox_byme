package se.searchofskills.service.dto.business_area;

import se.searchofskills.domain.BusinessArea;

import java.time.Instant;

public class BusinessAreaOutputDTO {
    private long id;
    private String name;
    private Instant createdDate;
    private String createdBy;
    private Instant updatedDate;
    private String lastModifyBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getLastModifyBy() {
        return lastModifyBy;
    }

    public void setLastModifyBy(String lastModifyBy) {
        this.lastModifyBy = lastModifyBy;
    }

    public BusinessAreaOutputDTO(BusinessArea businessArea) {
        this.id = businessArea.getId();
        this.name = businessArea.getName();
        this.createdDate = businessArea.getCreatedDate();
        this.createdBy = businessArea.getCreatedBy();
        this.updatedDate = businessArea.getLastModifiedDate();
        this.lastModifyBy = businessArea.getLastModifiedBy();
    }
}
