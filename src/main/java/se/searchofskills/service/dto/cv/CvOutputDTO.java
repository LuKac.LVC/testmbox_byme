package se.searchofskills.service.dto.cv;

import se.searchofskills.domain.Cv;

import java.time.Instant;

public class CvOutputDTO {
    private long id;
    private String fileName;
    private String url;
    private long candidateId;
    private Instant createdDate;
    private String createdBy;
    private Instant lastModifiedDate;
    private String lastModifiedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(long candidateId) {
        this.candidateId = candidateId;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public CvOutputDTO(Cv cv, String baseUrl) {
        this.id = cv.getId();
        this.fileName = cv.getFileName();
        this.candidateId = cv.getCandidate().getId();
        this.url = baseUrl + "/api/v2/candidates/" + candidateId + "/cv/" + fileName;
        this.createdDate = cv.getCreatedDate();
        this.createdBy = cv.getCreatedBy();
        this.lastModifiedDate = cv.getLastModifiedDate();
        this.lastModifiedBy = cv.getLastModifiedBy();
    }
}
