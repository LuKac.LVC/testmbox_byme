package se.searchofskills.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.domain.Country;
import se.searchofskills.domain.Region;
import se.searchofskills.repository.CountryRepository;
import se.searchofskills.repository.RegionRepository;
import se.searchofskills.service.dto.region.RegionInputDTO;
import se.searchofskills.service.dto.region.RegionOutputDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.utils.Utils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class RegionService {
    private final RegionRepository regionRepository;

    private final CountryRepository countryRepository;

    private final UserACL userACL;

    public RegionService(RegionRepository regionRepository, CountryRepository countryRepository, UserACL userACL) {
        this.regionRepository = regionRepository;
        this.countryRepository = countryRepository;
        this.userACL = userACL;
    }

    public RegionOutputDTO create(RegionInputDTO regionInputDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        String name = regionInputDTO.getName();
        if (name == null || name.isEmpty() || Utils.isAllSpaces(name)) {
            throw new BadRequestException("error.nameEmptyOrBlank", null);
        }
        if (!Utils.nameRegex(name)) {
            throw new BadRequestException("error.regionNameInvalid", null);
        }
        Country country = Utils.requireExists(countryRepository.findById(regionInputDTO.getCountryId()), "error.countryNotFound");
        Optional<Region> regionOptional = regionRepository.findByCountryIdAndName(country.getId() ,name);
        if (regionOptional.isPresent()) {
            throw new BadRequestException("error.regionExisted", null);
        }
        Region region = new Region();
        region.setName(Utils.handleWhitespace(name));
        region.setCountry(country);
        regionRepository.save(region);
        return new RegionOutputDTO(region);
    }

    public RegionOutputDTO update(RegionInputDTO regionInputDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        String name = regionInputDTO.getName();
        Region region = Utils.requireExists(regionRepository.findById(regionInputDTO.getId()), "error.regionNotFound");
        if (name != null) {
            if (name.isEmpty() || Utils.isAllSpaces(name)) {
                throw new BadRequestException("error.nameEmptyOrBlank", null);
            }
            if (!Utils.nameRegex(name)) {
                throw new BadRequestException("error.regionNameInvalid", null);
            }
            Country country = Utils.requireExists(countryRepository.findById(regionInputDTO.getCountryId()), "error.countryNotFound");
            Optional<Region> regionOptionalSameName = regionRepository.findByCountryIdAndName(country.getId(), name);
            if (regionOptionalSameName.isPresent() && !regionOptionalSameName.get().getId().equals(region.getId())) {
                throw new BadRequestException("error.regionExisted", null);
            }
            region.setName(Utils.handleWhitespace(name));
            region.setCountry(country);
            regionRepository.save(region);
        }
        return new RegionOutputDTO(region);
    }

    public void delete(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Region region = Utils.requireExists(regionRepository.findById(id), "error.regionNotFound");
        regionRepository.delete(region);
    }

    public Page<RegionOutputDTO> getAll(Pageable pageable, String name) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Page<RegionOutputDTO> page;
        if (name == null) {
            page = regionRepository.findAll(pageable).map(RegionOutputDTO::new);
        } else {
            page = regionRepository.findAllByNameContaining(pageable, name).map(RegionOutputDTO::new);
        }
        return page;
    }

    public List<RegionOutputDTO> getRegionByCountry(Long countryId) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Country country = Utils.requireExists(countryRepository.findById(countryId), "error.countryNotFound");
        List<RegionOutputDTO> regions = regionRepository.findAllByCountryId(country.getId()).stream().map(RegionOutputDTO::new).collect(Collectors.toList());
        return regions;
    }
}
