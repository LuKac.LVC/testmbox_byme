package se.searchofskills.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.domain.EmailTemplate;
import se.searchofskills.domain.enumeration.CronJobStatus;
import se.searchofskills.domain.enumeration.EmailTemplateType;
import se.searchofskills.repository.EmailTemplateRepository;
import se.searchofskills.service.dto.EmailTemplateDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.utils.Utils;

@Service
@Transactional
public class EmailTemplateService {
    private final Logger log = LoggerFactory.getLogger(EmailTemplateService.class);

    private final EmailTemplateRepository emailTemplateRepository;
    private final UserACL userACL;
    private final String REQUIRED_MAIN_CONTENT = "&lt;reference_number&gt;"; // <reference_number>
    private final String REQUIRED_REMINDER_CONTENT = "&lt;request_date&gt;"; // <request_date>

    public EmailTemplateService(EmailTemplateRepository emailTemplateRepository,
                                UserACL userACL) {
        this.emailTemplateRepository = emailTemplateRepository;
        this.userACL = userACL;
    }

    /**
     * Get all email template by name containing
     * @param pageable: pagination
     * @param name: name of email template, if name is null or empty will find all
     * @return email template data in pagination
     */
    public Page<EmailTemplateDTO> findEmailTemplatesByName(Pageable pageable, String name, EmailTemplateType type) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        } if (name == null || name.isEmpty()) {
            if (type == null) {
                return emailTemplateRepository.findAllByDeleted(pageable, false).map(EmailTemplateDTO::new);
            } else {
                return emailTemplateRepository.findAllByDeletedAndType(pageable, false, type).map(EmailTemplateDTO::new);
            }
        } else {
            if (type == null) {
                return emailTemplateRepository.findAllByDeletedAndNameContainingIgnoreCase(pageable, false, name).map(EmailTemplateDTO::new);
            } else {
                return emailTemplateRepository.findAllByDeletedAndNameContainingIgnoreCaseAndType(pageable, false, name, type).map(EmailTemplateDTO::new);
            }
        }
    }

    /**
     * get one email template
     * @param id: id of email template
     * @return email template data
     */
    public EmailTemplateDTO findOne(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        return Utils.requireExists(emailTemplateRepository.findById(id).map(EmailTemplateDTO::new), "error.notFound");
    }

    /**
     * Create or update email template
     * @param emailTemplateDTO: input
     * @return email template data after saving
     */
    public EmailTemplateDTO createOrUpdate(EmailTemplateDTO emailTemplateDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        EmailTemplate emailTemplate;
        if (emailTemplateDTO.getId() != null) {
            // edit
            emailTemplate = Utils.requireExists(emailTemplateRepository.findById(emailTemplateDTO.getId()), "error.notFound");
        } else {
            // new
            emailTemplate = new EmailTemplate();
        }

        if (Utils.isAllSpaces(emailTemplateDTO.getName())) {
            throw new BadRequestException("error.noWhiteSpace", null);
        }

        if (!StringUtils.isBlank(emailTemplateDTO.getName())) {
            emailTemplate.setName(Utils.handleWhitespace(emailTemplateDTO.getName()));
        }

        if (emailTemplateDTO.getType() != null) {
            emailTemplate.setType(emailTemplateDTO.getType());
        } else {
            emailTemplate.setType(EmailTemplateType.MAIN);
        }

        if (!StringUtils.isBlank(emailTemplateDTO.getContent())) {
            if (emailTemplate.getType().equals(EmailTemplateType.MAIN)) {
                if (!emailTemplateDTO.getContent().contains(REQUIRED_MAIN_CONTENT)) {
                    throw new BadRequestException("error.requiredContentMainTemplate", null);
                }
            } else if (emailTemplate.getType().equals(EmailTemplateType.REMINDER_1) || emailTemplate.getType().equals(EmailTemplateType.REMINDER_2)) {
                if (!emailTemplateDTO.getContent().contains(REQUIRED_REMINDER_CONTENT)) {
                    throw new BadRequestException("error.requiredContentReminderTemplate", null);
                }
            }
            emailTemplate.setContent(emailTemplateDTO.getContent());
        }

        emailTemplateRepository.save(emailTemplate);

        return new EmailTemplateDTO(emailTemplate);
    }

    /**
     * Delete email template by ID
     * @param id: id of email template
     */
    public void deleteEmailTemplate(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        EmailTemplate emailTemplate = Utils.requireExists(emailTemplateRepository.findByIdAndDeletedIsFalse(id), "error.notFound");
        if (isUsingByRunningCronjob(id)) {
            throw new BadRequestException("error.emailTemplateAlreadyInUse", null);
        }
        emailTemplate.setDeleted(true);

        emailTemplateRepository.save(emailTemplate);
    }

    /**
     * check if emailTemplate is using by any running cronjob
     * @param id
     * @return true if using or false if not
     */
    public boolean isUsingByRunningCronjob(long id) {
        long count = emailTemplateRepository.countAllCronJobIsUsingEmailTemplateById(CronJobStatus.RUNNING, id);
        return count > 0;
    }
}
