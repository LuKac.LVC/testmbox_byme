package se.searchofskills.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.domain.Category;
import se.searchofskills.repository.CategoryRepository;
import se.searchofskills.service.dto.category.CategoryCreatedDTO;
import se.searchofskills.service.dto.category.CategoryOutputDTO;
import se.searchofskills.service.dto.category.CategoryUpdateDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CategoryService {
    private final UserACL userACL;

    private final CategoryRepository categoryRepository;

    public CategoryService(UserACL userACL, CategoryRepository categoryRepository) {
        this.userACL = userACL;
        this.categoryRepository = categoryRepository;
    }

    public List<CategoryOutputDTO> create(CategoryCreatedDTO categoryCreatedDTO) {
        // check admin role
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        List<CategoryOutputDTO> categoryDTOS = new ArrayList<>();
        List<String> categoryNames = new ArrayList<>();
        for (int i = 0; i < categoryCreatedDTO.getNames().size(); i++) {
            String categoryName = categoryCreatedDTO.getNames().get(i);
            if (!Utils.nameRegex(categoryName)) {
                throw new BadRequestException("error.categoriesNameInvalid", null);
            }
            if (!Utils.isAllSpaces(categoryName) && !categoryName.isEmpty()) {
                categoryNames.add(Utils.handleWhitespace(categoryName));
            }
        }
        //get tag name remove invalid tag name
        if (categoryNames.size() == 0) {
            throw new BadRequestException("error.listNameEmpty", null);
        }
        List<Category> categoryList = new ArrayList<>();
        for (String categoryName : categoryNames) {
            Optional<Category> categoryOptional = categoryRepository.findByName(categoryName);
            if (categoryOptional.isPresent()) {
                //throw exception category exited
                throw new BadRequestException("error.categoryNameExisted", null);
            }
            Category category = new Category();
            category.setName(categoryName);
            categoryList.add(category);
        }
        categoryRepository.saveAll(categoryList);
        categoryDTOS = categoryList.stream().map(CategoryOutputDTO::new).collect(Collectors.toList());
        return categoryDTOS;
    }

    public CategoryOutputDTO update(CategoryUpdateDTO categoryUpdateDTO) {
        // check admin role
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        // get existed category from id
        Category category = Utils.requireExists(categoryRepository.findById(categoryUpdateDTO.getId()), "error.categoryNotFound");
        String name = categoryUpdateDTO.getName();
        if (!Utils.nameRegex(name)) {
            //throw tag name invalid
            throw new BadRequestException("error.categoriesNameInvalid", null);
        }
        if (Utils.isAllSpaces(name) || name.isEmpty()) {
            //throw empty or blank
            throw new BadRequestException("error.categoryNameEmptyOrBlank", null);
        }
        Optional<Category> categoryOptional = categoryRepository.findByName(name);
        if (categoryOptional.isPresent() && !categoryOptional.get().getId().equals(category.getId())) {
            //throw category if category name existed
            throw new BadRequestException("error.categoryNameExisted", null);
        }
        category.setName(Utils.handleWhitespace(name));
        categoryRepository.save(category);
        return new CategoryOutputDTO(category);
    }

    public void delete(long id) {
        // check admin role
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        // get existed category from id
        Category category = Utils.requireExists(categoryRepository.findById(id), "error.categoryNotFound");
        categoryRepository.delete(category);
    }

    public Page<CategoryOutputDTO> getAll(Pageable pageable, String name) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Page<CategoryOutputDTO> page;
        if (name == null) {
            page = categoryRepository.findAll(pageable).map(CategoryOutputDTO::new);
        }
        else {
            page = categoryRepository.findAllByNameContainingIgnoreCase(pageable, name).map(CategoryOutputDTO::new);
        }
        return page;
    }

    public boolean checkExist(String name) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Optional<Category> categoryOptional = categoryRepository.findByName(name);
        return categoryOptional.isPresent();
    }
}
