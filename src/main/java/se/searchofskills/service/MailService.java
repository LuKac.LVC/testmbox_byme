package se.searchofskills.service;

import io.github.jhipster.config.JHipsterProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.event.EventListener;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import se.searchofskills.config.ApplicationProperties;
import se.searchofskills.domain.EmailTemplate;
import se.searchofskills.domain.EmailTracking;
import se.searchofskills.domain.Recipient;
import se.searchofskills.domain.User;
import se.searchofskills.domain.enumeration.EmailTemplateType;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Service for sending emails.
 * <p>
 * We use the {@link Async} annotation to send emails asynchronously.
 */
@Service
public class MailService {

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    private static final String USER = "user";

    private static final String BASE_URL = "baseUrl";

    private final JHipsterProperties jHipsterProperties;

    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    private final ApplicationProperties applicationProperties;

    public MailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
                       MessageSource messageSource, SpringTemplateEngine templateEngine, ApplicationProperties applicationProperties) {

        this.jHipsterProperties = jHipsterProperties;
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
        this.applicationProperties = applicationProperties;
    }

    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        }  catch (MailException | MessagingException e) {
            log.warn("Email could not be sent to user '{}'", to, e);
        }
    }

    // TODO need to separate this method as compose main and reminder email to able to test
    @EventListener
    @Async
    public void autoSendingEmail(AutoSendEmailEvent event) {
        Recipient recipient = event.getRecipient();
        EmailTemplate emailTemplate = event.getEmailTemplate();
        EmailTemplateType type = event.getType();

        Locale locale = Locale.forLanguageTag("en");
        Context context = new Context(locale);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        String subject = "ID" + recipient.getId() + " - Begäran om kopia på offentliga handlingar - " + recipient.getTitle();
        String content;
        if (type.equals(EmailTemplateType.MAIN)) {
            // &lt;reference_number&gt; = <reference_number> as placeholder of "Main email"
            if (recipient.getReferenceNumber() == null){
                content = emailTemplate.getContent().replace("&lt;reference_number&gt;", "");
            }else {
                content = emailTemplate.getContent().replace("&lt;reference_number&gt;", recipient.getReferenceNumber());
            }
        } else {
            DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime( FormatStyle.MEDIUM )
                .withLocale( Locale.UK )
                .withZone( ZoneId.of("UTC") );

            String mainContent = "";
            if (event.getMainEmailTemplate() != null && event.getMainEmailTemplate().getContent() != null) {
                // &lt;reference_number&gt; = <reference_number> as placeholder of "Main email"
                if(recipient.getReferenceNumber() == null){
                    mainContent = event.getMainEmailTemplate().getContent().replace("&lt;reference_number&gt;", "");
                }else {
                    mainContent = event.getMainEmailTemplate().getContent().replace("&lt;reference_number&gt;", recipient.getReferenceNumber());
                }
            };

            // &lt;request_date&gt; = <request_date> as placeholder of "Reminder email"
            content = emailTemplate.getContent().replace("&lt;request_date&gt;", formatter.format(event.getRequestDate()));

            // get content of reminder 1 to include into reminder 2
            if (emailTemplate.getType().equals(EmailTemplateType.REMINDER_2) && event.getReminder1Template() != null) {
                String reminder1Content = event.getReminder1Template().getContent().replace("&lt;request_date&gt;", formatter.format(event.getRequestDate()));
                content = content.concat("<hr><br><br>" + reminder1Content);
            }

            String attachedMainEmail = "";
            String headAttachedMainEmail = "<hr><br><br><strong>From: </strong>SearchOfSkills &lt;info@branschtaxonomi.se&gt;</p><p>" +
                "<strong>Sent:</strong> " + formatter.format(event.getRequestDate()) + "</p><p>" +
                "<strong>To:</strong> '";
            String footAttachedMainEmail = "&gt;</p>" +
                "<p><strong>Subject:</strong> "+ subject + "<p>&nbsp;</p>" +
                mainContent;

            //check registrar null or not null
            if(recipient.getRegistrar() != null){
                if (recipient.getEmail() == null){
                    attachedMainEmail = headAttachedMainEmail + recipient.getRegistrar() +"' &lt;" + recipient.getRegistrar() + "&gt;</p>" +
                        "<strong>Cc:</strong> '" + recipient.getRegistrar()+ "' &lt;" + recipient.getRegistrar() + footAttachedMainEmail;
                }else {
                    attachedMainEmail = headAttachedMainEmail + recipient.getEmail() +"' &lt;" + recipient.getEmail() + "&gt;</p>" +
                        "<strong>Cc:</strong> '" + recipient.getRegistrar()+ "' &lt;" + recipient.getRegistrar() + footAttachedMainEmail;
                }
            }else {
                attachedMainEmail = headAttachedMainEmail + recipient.getEmail() +"' &lt;" + recipient.getEmail() + footAttachedMainEmail;
            }

            content = content.concat(attachedMainEmail);
        }

        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, StandardCharsets.UTF_8.name());
            String to = "";
            if (event.getType().equals(EmailTemplateType.MAIN)){
                if (recipient.getEmail() == null){
                    to = recipient.getRegistrar();
                }else {
                    to = recipient.getEmail();
                }
                String cc = recipient.getRegistrar();
                if (cc != null && this.isAbleToSendToSpecificEmail(cc)) {
                    message.setCc(cc);
                }
            }else {
                EmailTracking emailTracking = event.getEmailTracking();
                if (emailTracking.getRegistrar() == null){
                    to = recipient.getEmail();
                }else {
                    to = emailTracking.getRegistrar();
                }

            }

            if (this.isAbleToSendToSpecificEmail(to)) {
                message.setTo(to);
            } else {
                log.warn("Can not send to email '{}' by has not added to emails in application properties yet", to);
                return;
            }
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, true);
            javaMailSender.send(mimeMessage);
        }  catch (MailException | MessagingException e) {
            if (recipient.getEmail() != null){
                log.warn("Email could not be sent to user '{}'", recipient.getEmail(), e);
            }else {
                log.warn("Email could not be sent to user '{}'", recipient.getRegistrar(), e);
            }

        }
    }

    @Async
    public void sendEmailFromTemplate(User user, String templateName, String titleKey) {
        if (user.getEmail() == null) {
            log.debug("Email doesn't exist for user '{}'", user.getLogin());
            return;
        }
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    @Async
    public void sendActivationEmail(User user) {
        log.debug("Sending activation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/activationEmail", "email.activation.title");
    }

    @Async
    public void sendCreationEmail(User user) {
        log.debug("Sending creation email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/creationEmail", "email.activation.title");
    }

    @Async
    public void sendPasswordResetMail(User user) {
        log.debug("Sending password reset email to '{}'", user.getEmail());
        sendEmailFromTemplate(user, "mail/passwordResetEmail", "email.reset.title");
    }

    public boolean isAbleToSendToSpecificEmail(String email) {
        if (applicationProperties.isSendAll())
            return true;
        String[] emailList = applicationProperties.getEmails();
        if (emailList == null)
            return false;
        List<String> emails = Arrays.asList(emailList);
        return emails.contains(email);
    }
}
