package se.searchofskills.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.domain.Employer;
import se.searchofskills.domain.ManagerialPosition;
import se.searchofskills.repository.ManagerialPositionRepository;
import se.searchofskills.service.dto.managerial_position.ManagerialPositionCreateDTO;
import se.searchofskills.service.dto.managerial_position.ManagerialPositionOutputDTO;
import se.searchofskills.service.dto.managerial_position.ManagerialPositionUpdateDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ManagerialPositionService {
    private final ManagerialPositionRepository managerialPositionRepository;

    private final UserACL userACL;

    public ManagerialPositionService(ManagerialPositionRepository managerialPositionRepository, UserACL userACL) {
        this.managerialPositionRepository = managerialPositionRepository;
        this.userACL = userACL;
    }

    public List<ManagerialPositionOutputDTO> create(ManagerialPositionCreateDTO managerialPositionCreateDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        List<ManagerialPositionOutputDTO> managerialPositionOutputDTOS = new ArrayList<>();
        List<String> managerialPositionNames = new ArrayList<>();
        for (int i = 0; i < managerialPositionCreateDTO.getNames().size(); i++) {
            String managerialPosition = managerialPositionCreateDTO.getNames().get(i);
            if (!Utils.nameRegex(managerialPosition)) {
                throw new BadRequestException("error.managerialPositionNameInvalid", null);
            }
            if (!Utils.isAllSpaces(managerialPosition) && !managerialPosition.isEmpty()) {
                managerialPositionNames.add(Utils.handleWhitespace(managerialPosition));
            }
        }
        //get managerial position name remove invalid tag name
        if (managerialPositionNames.size() == 0) {
            throw new BadRequestException("error.listNameEmpty", null);
        }
        List<ManagerialPosition> managerialPositions = new ArrayList<>();
        for (String managerialPositionName : managerialPositionNames) {
            ManagerialPosition managerialPosition = new ManagerialPosition();
            managerialPosition.setName(managerialPositionName);
            managerialPositions.add(managerialPosition);
        }
        managerialPositionRepository.saveAll(managerialPositions);
        managerialPositionOutputDTOS = managerialPositions.stream().map(ManagerialPositionOutputDTO::new).collect(Collectors.toList());
        return managerialPositionOutputDTOS;
    }

    public ManagerialPositionOutputDTO update(ManagerialPositionUpdateDTO managerialPositionUpdateDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        // get existed managerial position from id
        ManagerialPosition managerialPosition = Utils.requireExists(managerialPositionRepository.findById(managerialPositionUpdateDTO.getId()), "error.managerialPositionNotFound");
        String name = managerialPositionUpdateDTO.getName();
        if (name.isEmpty() || Utils.isAllSpaces(name)) {
            //throw employer name invalid
            throw new BadRequestException("error.nameEmptyOrBlank", null);
        }
        managerialPosition.setName(Utils.handleWhitespace(name));
        managerialPositionRepository.save(managerialPosition);
        return new ManagerialPositionOutputDTO(managerialPosition);
    }

    public void delete(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        ManagerialPosition managerialPosition = Utils.requireExists(managerialPositionRepository.findById(id), "error.managerialPositionNotFound");
        managerialPositionRepository.delete(managerialPosition);
    }

    public Page<ManagerialPositionOutputDTO> getAll(Pageable pageable, String name) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Page<ManagerialPositionOutputDTO> page;
        if (name == null) {
            page = managerialPositionRepository.findAll(pageable).map(ManagerialPositionOutputDTO::new);
        } else {
            page = managerialPositionRepository.findAllByNameContainingIgnoreCase(pageable, name).map(ManagerialPositionOutputDTO::new);
        }
        return page;
    }
}
