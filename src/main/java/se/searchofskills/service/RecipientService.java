package se.searchofskills.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.domain.Recipient;
import se.searchofskills.domain.RecipientList;
import se.searchofskills.domain.enumeration.CronJobStatus;
import se.searchofskills.repository.RecipientListRepository;
import se.searchofskills.repository.RecipientRepository;
import se.searchofskills.service.dto.RecipientDTO;
import se.searchofskills.service.dto.RecipientListDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.utils.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.apache.commons.collections4.IterableUtils.isEmpty;

@Service
@Transactional
public class RecipientService {
    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final RecipientListRepository recipientListRepository;
    private final RecipientRepository recipientRepository;
    private final UserACL userACL;

    public RecipientService(RecipientListRepository recipientListRepository,
                            RecipientRepository recipientRepository,
                            UserACL userACL) {
        this.recipientListRepository = recipientListRepository;
        this.recipientRepository = recipientRepository;
        this.userACL = userACL;
    }

    /**
     * Import recipient and recipient list via Excel file
     * @param inputStream: input of excel file
     * @param recipientListName: xlsx, xls
     * @return recipient list saved
     */
    public List<Recipient> importRecipientList(InputStream inputStream, String recipientListName) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        List<Recipient> listRecipient = readExcelFileRecipientList(inputStream);

        if (listRecipient.size() > 0) {
            // Create recipient list
            RecipientList recipientList = new RecipientList();
            recipientList.setName(Utils.handleWhitespace(recipientListName));
            recipientList.setDeleted(false);
            recipientList = recipientListRepository.save(recipientList);

            // Add all recipient to the recipient list saved
            for (Recipient recipient: listRecipient) {
                recipient.setRecipientList(recipientList);
            }
            recipientRepository.saveAll(listRecipient);
            return listRecipient;
        } else {
            throw new BadRequestException("error.uploadExcelDataEmpty", null);
        }
    }

    private List<Recipient> readExcelFileRecipientList(InputStream input) {
        List<Recipient> recipientListToAdd = new ArrayList<>();
        try (Workbook workbook = WorkbookFactory.create(input)) {
            // get sheet
            Sheet sheet = workbook.getSheetAt(0);

            // Remove empty row in Excel
            removeEmptyRow(sheet);

            // Get all rows
            for (Row row: sheet) {
                if (row.getRowNum() == 0) {
                    // Ignore header
                    continue;
                }

                // Read cells and set value for book object
                Recipient recipient = new Recipient();
                for (int i = 0; i < 7; i++){

                    Cell cell = row.getCell(i);
                    if(cell == null && i != 4 && i != 2 && i != 5){
                        throw new BadRequestException("error.fieldNullOrEmpty", (row.getRowNum() + 1) +"-"+ (i + 1));
                    }
                    Object cellValue = null;
                    if(!((i == 4 || i == 2 || i == 5) && cell == null)){
                        cellValue = getCellValue(cell);
                    }

                    // Set value for book object
                    int columnIndex = i;
                    switch (columnIndex) {
                        case 0: {
                            recipient.setCounty(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 1: {
                            recipient.setTitle(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 2: {
                            if (cellValue == null){
                                recipient.setReferenceNumber(null);
                                break;
                            }
                            recipient.setReferenceNumber(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 3: {
                            recipient.setProcuringAgency(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 4: {
                            if (cellValue == null){
                                recipient.setRegistrar(null);
                                break;
                            }
                            if (!Utils.isValidEmail(cellValue.toString()) && StringUtils.isNotBlank(cellValue.toString())){
                                throw new BadRequestException("error.invalidEmailFromExcel", (row.getRowNum() + 1) +"-"+ (cell.getColumnIndex() + 1));
                            }
                            recipient.setRegistrar(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 5: {
                            if (cellValue == null && recipient.getRegistrar() == null){
                                throw new BadRequestException("error.bothEmailAndRegisterCannotEmpty", (row.getRowNum() + 1) + "-" + 5);
                            }else {
                                if (cellValue == null){
                                    recipient.setEmail(null);
                                    break;
                                }else {
                                    if (!Utils.isValidEmail(cellValue.toString())){
                                        throw new BadRequestException("error.invalidEmailFromExcel", (row.getRowNum() + 1) +"-"+ (cell.getColumnIndex() + 1));
                                    }
                                    recipient.setEmail(Utils.handleWhitespace(cellValue.toString()));
                                    break;
                                }
                            }
                        }
                        case 6: {
                            recipient.setProcurementUrl(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                    }
                }
                recipientListToAdd.add(recipient);
            }

            workbook.close();
            input.close();

            return recipientListToAdd;
        } catch (IOException e) {
            throw new BadRequestException("error.uploadExcelWrongFormat", null);
        }
    }

    /**
     * Get cell value with cases
     * @param cell: cell to get value
     * @return object contain value of cell
     */
    private static Object getCellValue(Cell cell) {
        CellType cellType = cell.getCellType();
        Object cellValue = null;
        switch (cellType) {
            case BOOLEAN:
                cellValue = cell.getBooleanCellValue();
                break;
            case FORMULA:
                Workbook workbook = cell.getSheet().getWorkbook();
                FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
                cellValue = evaluator.evaluate(cell).getNumberValue();
                break;
            case NUMERIC:
                cellValue = cell.getNumericCellValue();
                break;
            case STRING:
                cellValue = cell.getStringCellValue();
                break;
            case _NONE:
            case BLANK:
                if (cell.getColumnIndex() == 4 || cell.getColumnIndex() == 2 || cell.getColumnIndex() == 5){
                    cellValue = null;
                    break;
                }
            case ERROR:
                throw new BadRequestException("error.cellContentError", (cell.getRowIndex() + 1) + "-" +  (cell.getColumnIndex() + 1));
            default:
                break;
        }

        return cellValue;
    }

    /**
     * Remove empty row in Excel
     * @Param sheet
     * @return sheet
     * */
    public Sheet removeEmptyRow(Sheet sheet){
        for (int i = 0; i <= sheet.getLastRowNum(); i++){
            if (sheet.getRow(i) == null) continue;
            Row row = sheet.getRow(i);
            int cellNumber = 0;
            int lastCellNumber = row.getLastCellNum();
            if (lastCellNumber == -1){
                sheet.removeRow(row);
                continue;
            }
            boolean isValue = false;
            for (int j = 0; j < 7; j++){
                if (row.getCell(j) == null) continue;

                CellType cellType = row.getCell(j).getCellType();
                if (StringUtils.isBlank((cellType == CellType.NUMERIC) ? String.valueOf(row.getCell(j).getNumericCellValue()) : row.getCell(j).getStringCellValue())){
                    cellNumber++;
                    if (cellNumber == 7){
                        sheet.removeRow(row);
                        break;
                    }
                }else{
                    isValue = true;
                }
                if (lastCellNumber-1 == j && !isValue){
                    sheet.removeRow(row);
                    break;
                }
            }
        }
        return sheet;
    }

    /**
     * Get all recipient list by admin
     *
     * @param pageable: pagination
     * @param name: name of recipient list
     * @return recipient lists
     * @throws AccessForbiddenException if user is not admin
     */
    public Page<RecipientListDTO> findAllRecipientListsByName(Pageable pageable, String name) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        if (name == null || name.isEmpty()) {
            return recipientListRepository.findAllByDeleted(pageable, false).map(RecipientListDTO::new);
        } else {
            return recipientListRepository.findAllByDeletedAndNameContainingIgnoreCase(pageable, false, name).map(RecipientListDTO::new);
        }
    }

    /**
     * Get a recipient list by id
     * @param id: id of recipient list
     * @return recipient list DTO
     * @throws AccessForbiddenException if user is not admin
     */
    public RecipientListDTO findOne(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        return Utils.requireExists(recipientListRepository.findById(id).map(RecipientListDTO::new), "error.notFound");
    }

    /**
     * get all recipients belong to a recipient
     *
     * @param pageable: pagination
     * @param recipientListId: recipient list id
     * @return recipients in page
     * @throws AccessForbiddenException if user is not admin
     */
    public Page<RecipientDTO> findAllRecipientsByRecipientListId(Pageable pageable, long recipientListId) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        return recipientRepository.findAllByRecipientListId(pageable, recipientListId).map(RecipientDTO::new);
    }

    /**
     * Update recipient list name by id
     * @param recipientListDTO input
     */
    public RecipientListDTO updateRecipientList(RecipientListDTO recipientListDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        RecipientList recipientList = Utils.requireExists(recipientListRepository.findById(recipientListDTO.getId()), "error.notFound");
        if (Utils.isAllSpaces(recipientListDTO.getName())) {
            throw new BadRequestException("error.noWhiteSpace", null);
        }
        recipientList.setName(Utils.handleWhitespace(recipientListDTO.getName()));
        recipientListRepository.save(recipientList);
        return new RecipientListDTO(recipientList);
    }

    /**
     * Delete recipient list by id
     * @param id of recipient list
     */
    public void deleteRecipientList(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }

        RecipientList recipientList = Utils.requireExists(recipientListRepository.findByIdAndDeletedIsFalse(id), "error.notFound");
        if (isUsingByRunningCronjob(id)) {
            throw new BadRequestException("error.recipientListAlreadyInUse", null);
        }

        recipientList.setDeleted(true);
        recipientListRepository.save(recipientList);
    }

    /**
     * check if recipientList is using by any running cronjob
     * @param id
     * @return true if using or false if not
     */
    public boolean isUsingByRunningCronjob(long id) {
        long count = recipientListRepository.countAllCrobjobIsUsingRecipientById(CronJobStatus.RUNNING, id);
        return count > 0;
    }

}
