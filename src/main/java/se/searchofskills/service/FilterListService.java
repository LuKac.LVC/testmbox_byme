package se.searchofskills.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.domain.Candidate;
import se.searchofskills.domain.CandidateFilterList;
import se.searchofskills.domain.FilterList;
import se.searchofskills.domain.User;
import se.searchofskills.domain.enumeration.Action;
import se.searchofskills.repository.CandidateFilterListRepository;
import se.searchofskills.repository.CandidateRepository;
import se.searchofskills.repository.FilterListRepository;
import se.searchofskills.repository.UserRepository;
import se.searchofskills.security.SecurityUtils;
import se.searchofskills.service.dto.filter_list.FilterListCreateInputDTO;
import se.searchofskills.service.dto.filter_list.FilterListOutputDTO;
import se.searchofskills.service.dto.filter_list.FilterListUpdateInputDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class FilterListService {
    private final UserACL userACL;

    private final UserRepository userRepository;

    private final FilterListRepository filterListRepository;

    private final CandidateFilterListRepository candidateFilterListRepository;

    private final CandidateRepository candidateRepository;

    public FilterListService(UserACL userACL, UserRepository userRepository, FilterListRepository filterListRepository, CandidateFilterListRepository candidateFilterListRepository, CandidateRepository candidateRepository) {
        this.userACL = userACL;
        this.userRepository = userRepository;
        this.filterListRepository = filterListRepository;
        this.candidateFilterListRepository = candidateFilterListRepository;
        this.candidateRepository = candidateRepository;
    }

    public FilterListOutputDTO create(FilterListCreateInputDTO filterListCreateInputDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        User user = Utils.requireExists(SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin), "error.userNotFound");
        FilterList filterList = new FilterList();
        if (filterListCreateInputDTO.getName().isEmpty() || Utils.isAllSpaces(filterListCreateInputDTO.getName()) || filterListCreateInputDTO.getName() == null) {
            throw new BadRequestException("nameEmptyOrBlank", null);
        }
        List<Long> candidateIds = new ArrayList<>();
        filterList.setName(Utils.handleWhitespace(filterListCreateInputDTO.getName()));
        filterList.setUser(user);
        filterListRepository.save(filterList);
        FilterListOutputDTO filterListOutputDTO = new FilterListOutputDTO(filterList);
        filterListOutputDTO.setCandidateIds(candidateIds);
        return filterListOutputDTO;
    }

    public FilterListOutputDTO update(FilterListUpdateInputDTO filterListUpdateInputDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        FilterList filterList = Utils.requireExists(filterListRepository.findById(filterListUpdateInputDTO.getId()), "error.filterListNotFound");
        if (!userACL.canUpdate(filterList.getUser().getId())) {
            throw new AccessForbiddenException("error.notAuthenticateAdmin");
        }
        List<Candidate> candidates = new ArrayList<>();
        for (Long candidateId : filterListUpdateInputDTO.getCandidateIds()) {
            Optional<Candidate> candidateOptional = candidateRepository.findById(candidateId);
            candidateOptional.ifPresent(candidates::add);
        }
        if (candidates.size() == 0) {
            throw new BadRequestException("error.candidateListEmpty", null);
        }
        if (filterListUpdateInputDTO.getAction() != Action.ADD && filterListUpdateInputDTO.getAction() != Action.REMOVE) {
            throw new BadRequestException("error.actionInvalid", null);
        }
        if (filterListUpdateInputDTO.getAction() == Action.ADD) {
            for (Candidate candidate : candidates) {
                Optional<CandidateFilterList> candidateFilterListOptional = candidateFilterListRepository.findByCandidateIdAndFilterListId(candidate.getId(), filterList.getId());
                CandidateFilterList candidateFilterList = candidateFilterListOptional.orElseGet(CandidateFilterList::new);
                candidateFilterList.setCandidate(candidate);
                candidateFilterList.setFilterList(filterList);
                candidateFilterListRepository.save(candidateFilterList);
            }
        }
        else if (filterListUpdateInputDTO.getAction() == Action.REMOVE) {
            for (Candidate candidate : candidates) {
                Optional<CandidateFilterList> candidateFilterListOptional = candidateFilterListRepository.findByCandidateIdAndFilterListId(candidate.getId(), filterList.getId());
                candidateFilterListOptional.ifPresent(candidateFilterListRepository::delete);
            }
        }
        FilterListOutputDTO filterListOutputDTO = new FilterListOutputDTO(filterList);
        List<Long> candidateIds = candidateFilterListRepository.findAllByFilterListId(filterList.getId());
        filterListOutputDTO.setCandidateIds(candidateIds);
        return filterListOutputDTO;
    }

    public void delete(long id) {
        FilterList filterList = Utils.requireExists(filterListRepository.findById(id), "error.filterListNotFound");
        filterListRepository.delete(filterList);
    }

    public Page<FilterListOutputDTO> getAll(Pageable pageable, String name) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        User user = Utils.requireExists(SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByLogin), "error.userNotFound");
        Page<FilterListOutputDTO> page;
        if (name == null) {
            page = filterListRepository.findAllByUserId(pageable, user.getId()).map(FilterListOutputDTO::new);
        } else {
            page = filterListRepository.findAllByUserIdAndNameContainingIgnoreCase(pageable, user.getId(), name).map(FilterListOutputDTO::new);
        }
        for (FilterListOutputDTO filterListOutputDTO : page) {
            List<Long> candidateIds = candidateFilterListRepository.findAllByFilterListId(filterListOutputDTO.getId());
            filterListOutputDTO.setCandidateIds(candidateIds);
        }
        return page;
    }
}
