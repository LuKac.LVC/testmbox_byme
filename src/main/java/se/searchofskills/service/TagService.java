package se.searchofskills.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.domain.Category;
import se.searchofskills.domain.Tag;
import se.searchofskills.domain.TagCategory;
import se.searchofskills.domain.Word;
import se.searchofskills.repository.CategoryRepository;
import se.searchofskills.repository.TagCategoryRepository;
import se.searchofskills.repository.TagRepository;
import se.searchofskills.repository.WordRepository;
import se.searchofskills.service.dto.category.CategoryOutputDTO;
import se.searchofskills.service.dto.tag.TagCreateInputDTO;
import se.searchofskills.service.dto.tag.TagInputFromWordsDTO;
import se.searchofskills.service.dto.tag.TagOutputDTO;
import se.searchofskills.service.dto.tag.TagUpdateInputDTO;
import se.searchofskills.service.error.AccessForbiddenException;
import se.searchofskills.service.error.BadRequestException;
import se.searchofskills.service.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class TagService {
    private final TagRepository tagRepository;

    private final TagCategoryRepository tagCategoryRepository;

    private final CategoryRepository categoryRepository;

    private final UserACL userACL;

    private final WordRepository wordRepository;

    public TagService(TagRepository tagRepository, TagCategoryRepository tagCategoryRepository, CategoryRepository categoryRepository, UserACL userACL, WordRepository wordRepository) {
        this.tagRepository = tagRepository;
        this.tagCategoryRepository = tagCategoryRepository;
        this.categoryRepository = categoryRepository;
        this.userACL = userACL;
        this.wordRepository = wordRepository;
    }

    public List<TagOutputDTO> create(TagCreateInputDTO tagCreateInputDTO) {
        // check admin role
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        List<TagOutputDTO> tagOutputDTOS = new ArrayList<>();
        List<Category> categoryList = new ArrayList<>();
        List<String> tagNames = new ArrayList<>();
        //get tag name remove invalid tag name
        for (int i = 0; i < tagCreateInputDTO.getTags().size(); i++) {
            String tag = tagCreateInputDTO.getTags().get(i);
            if (!Utils.nameRegex(tag)) {
                if (!tagCreateInputDTO.isBlock()) {
                    throw new BadRequestException("error.tagNameInvalid", null);
                }
                else {
                    throw new BadRequestException("error.blockTagNameInvalid", null);
                }
            }
            if (!Utils.isAllSpaces(tag) && !tag.isEmpty()) {
                tagNames.add(Utils.handleWhitespace(tag));
            }
        }
        // throw exception tag name if all is null
        if (tagNames.size() == 0) {
            if (!tagCreateInputDTO.isBlock()) {
                throw new BadRequestException("error.tagListEmpty", null);
            } else {
                throw new BadRequestException("error.blockTagListEmpty", null);
            }
        }
        //get category existed
        for (Long cateId : tagCreateInputDTO.getCategories()) {
            Optional<Category> categoryOptional = categoryRepository.findById(cateId);
            if (!categoryOptional.isPresent()) {
                continue;
            }
            categoryList.add(categoryOptional.get());
        }
        // throw exception if no category existed
        if (categoryList.size() == 0) {
            throw new BadRequestException("error.categoryListEmpty", null);
        }
        List<Tag> tagList = new ArrayList<>();
        for (String tagDTO : tagNames) {
            Optional<Tag> tagOptional = tagRepository.findByName(tagDTO);
            if (tagOptional.isPresent() && tagOptional.get().isActive()) {
                    // throw exception if block tag exited
                if (!tagOptional.get().isBlock()) {
                    throw new BadRequestException("error.tagNameExist", null);
                } else {
                    throw new BadRequestException("error.blockTagNameExist", null);
                }
            }
            Tag tag = tagOptional.orElseGet(Tag::new);
            tag.setName(tagDTO);
            tag.setSearch(0);
            tag.setActive(true);
            tag.setBlock(tagCreateInputDTO.isBlock());
            tagList.add(tag);
        }
        // save all tag
        tagRepository.saveAll(tagList);
        for (Tag tag : tagList) {
            //get category for tag
            List<CategoryOutputDTO> categoryDTOS = categoryList.stream().map(CategoryOutputDTO::new).collect(Collectors.toList());
            for (Category category : categoryList) {
                TagCategory tagCategory;
                Optional<TagCategory> tagCategoryOptional = tagCategoryRepository.findByTagIdAndCategoryId(tag.getId(), category.getId());
                tagCategory = tagCategoryOptional.orElseGet(TagCategory::new);
                tagCategory.setTag(tag);
                tagCategory.setCategory(category);
                tagCategoryRepository.save(tagCategory);
            }
            TagOutputDTO tagOutputDTO = new TagOutputDTO(tag);
            tagOutputDTO.setCategories(categoryDTOS);
            tagOutputDTOS.add(tagOutputDTO);
        }
        return tagOutputDTOS;
    }

    public TagOutputDTO update(TagUpdateInputDTO tagUpdateInputDTO) {
        // check admin role
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        TagOutputDTO tagOutputDTO;
        List<Category> cateIds = new ArrayList<>();
        String tagName = tagUpdateInputDTO.getName();
        // get exist tag from id
        Tag tag = Utils.requireExists(tagRepository.findById(tagUpdateInputDTO.getId()), "error.tagNotFound");
        // throw exception if tag name is not null
        if (Utils.isAllSpaces(tagName) || tagName.isEmpty()) {
            if (!tagUpdateInputDTO.isBlock()) {
                throw new BadRequestException("error.tagNameEmptyOrBlank", null);
            } else {
                throw new BadRequestException("error.blockTagNameEmptyOrBlank", null);
            }
        }
        if (!Utils.nameRegex(tagName)) {
            if (!tagUpdateInputDTO.isBlock()) {
                throw new BadRequestException("error.tagNameInvalid", null);
            } else {
                throw new BadRequestException("error.blockTagNameInvalid", null);
            }
        }
        Optional<Tag> tagOptional = tagRepository.findByName(tagName);
        if (tagOptional.isPresent() && !tagOptional.get().getId().equals(tag.getId())) {
            if (tagOptional.get().isActive()) {
                if (!tagOptional.get().isBlock()) {
                    throw new BadRequestException("error.tagNameExisted", null);
                } else {
                    throw new BadRequestException("error.blockTagNameExisted", null);
                }
                // throw exception if block tag name existed

            }
            // if update tag name duplicate with not activated tag existed, need delete not activated tag to only tag name existed
            else if (!tagOptional.get().isActive()) {
                tagRepository.delete(tagOptional.get());
            }
        }
        //save tag
        tag.setActive(true);
        tag.setBlock(tagUpdateInputDTO.isBlock());
        tag.setName(Utils.handleWhitespace(tagName));
        tagRepository.save(tag);
        for (Long cateId : tagUpdateInputDTO.getCategories()) {
            //get category existed
            Optional<Category> categoryOptional = categoryRepository.findById(cateId);
            if (!categoryOptional.isPresent()) {
                continue;
            }
            cateIds.add(categoryOptional.get());
        }
        tagOutputDTO = new TagOutputDTO(tag);
        List<CategoryOutputDTO> categoryDTOS = new ArrayList<>();
        if (cateIds.size() == 0) {
            tagOutputDTO.setCategories(categoryDTOS);
            return tagOutputDTO;
        }
        categoryDTOS = cateIds.stream().map(CategoryOutputDTO::new).collect(Collectors.toList());
        for (Category category : cateIds) {
            //get category for tag
            TagCategory tagCategory;
            Optional<TagCategory> tagCategoryOptional = tagCategoryRepository.findByTagIdAndCategoryId(tag.getId(), category.getId());
            tagCategory = tagCategoryOptional.orElseGet(TagCategory::new);
            tagCategory.setTag(tag);
            tagCategory.setCategory(category);
            tagCategoryRepository.save(tagCategory);
        }
        tagOutputDTO.setCategories(categoryDTOS);
        return tagOutputDTO;
    }

    public void delete(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Tag tag = Utils.requireExists(tagRepository.findById(id), "error.tagOrBlockTagNotFound");
        tagRepository.delete(tag);
    }

    public Page<TagOutputDTO> getALl(Pageable pageable, boolean block, String name) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Page<TagOutputDTO> page ;
        if (name == null) {
            page = tagRepository.findAllByActiveIsTrueAndBlock(pageable, block).map(TagOutputDTO::new);
        } else {
            page = tagRepository.findAllByActiveIsTrueAndBlockAndNameContainingIgnoreCase(pageable, block, name).map(TagOutputDTO::new);
        }
        for (TagOutputDTO tagOutputDTO : page) {
            //get categories for tag
            List<CategoryOutputDTO> categoryDTOList = this.getCategory(tagOutputDTO);
            tagOutputDTO.setCategories(categoryDTOList);
        }
        return page;
    }

    private List<CategoryOutputDTO> getCategory(TagOutputDTO tagOutputDTO) {
        List<TagCategory> tagCategoryList = tagCategoryRepository.findAllByTagId(tagOutputDTO.getId());
        List<Long> cateIds = tagCategoryList.stream().map(TagCategory::getCategory).map(Category::getId).collect(Collectors.toList());
        List<Category> categoryList = categoryRepository.findAllByIdIn(cateIds);
        return categoryList.stream().map(CategoryOutputDTO::new).collect(Collectors.toList());
    }

    public void block(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Tag tag = Utils.requireExists(tagRepository.findById(id), "error.tagNotExisted");
        tag.setBlock(true);
        tagRepository.save(tag);
    }

    public void unblock(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Tag tag = Utils.requireExists(tagRepository.findById(id), "error.blockTagNotExisted");
        tag.setBlock(false);
        tagRepository.save(tag);
    }

    public void saveWordsToTag(TagInputFromWordsDTO tagInputFromWordsDTO) {
        //get word from database same with list
        List<String> wordsSwitchTag = tagInputFromWordsDTO.getTags().stream().distinct().collect(Collectors.toList());
        List<String> wordsSwitchBlockTag = tagInputFromWordsDTO.getBlockTags().stream().distinct().collect(Collectors.toList());
        Set<Word> wordListToTag = wordRepository.findAllByNameIn(wordsSwitchTag);
        Set<Word> wordsListToBlockTag = wordRepository.findAllByNameIn(wordsSwitchBlockTag);
        Set<String> wordsValueToTag = wordListToTag.stream().map(Word::getName).collect(Collectors.toSet());
        Set<String> wordsValueToBlock = wordsListToBlockTag.stream().map(Word::getName).collect(Collectors.toSet());
        //word to save tag
        List<Tag> tagListNeedSave = new ArrayList<>();
        for (String wordToTag : wordsValueToTag) {
            Optional<Tag> tagOptional = tagRepository.findByName(wordToTag);
            if (tagOptional.isPresent()) {
                continue;
            }
            Tag tag = new Tag();
            tag.setName(wordToTag);
            tag.setActive(true);
            tag.setBlock(false);
            tag.setSearch(0);
            tagListNeedSave.add(tag);
        }
        //word to block tag
        for (String wordToBlockTag : wordsValueToBlock) {
            Optional<Tag> tagOptional = tagRepository.findByName(wordToBlockTag);
            if (tagOptional.isPresent()) {
                continue;
            }
            Tag blockTag = new Tag();
            blockTag.setName(wordToBlockTag);
            blockTag.setActive(true);
            blockTag.setBlock(true);
            blockTag.setSearch(0);
            tagListNeedSave.add(blockTag);
        }
        tagRepository.saveAll(tagListNeedSave);
        //delete all word saved to tag/block tag
        wordListToTag.addAll(wordsListToBlockTag);
        wordRepository.deleteAll(wordListToTag);
    }

    public boolean checkExisted(String name) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Optional<Tag> tagOptional = tagRepository.findByActiveIsTrueAndName(name);
        return tagOptional.isPresent();
    }
}
