package se.searchofskills.service;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.searchofskills.domain.Word;
import se.searchofskills.repository.TagRepository;
import se.searchofskills.repository.WordRepository;
import se.searchofskills.service.dto.word.WordDTO;
import se.searchofskills.service.error.AccessForbiddenException;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class WordService {
    private final WordRepository wordRepository;

    private final TagRepository tagRepository;

    private final UserACL userACL;

    public WordService(WordRepository wordRepository, TagRepository tagRepository, UserACL userACL) {
        this.wordRepository = wordRepository;
        this.tagRepository = tagRepository;
        this.userACL = userACL;
    }

    public Page<WordDTO> getAll(Pageable pageable) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        return wordRepository.findAll(pageable).map(WordDTO::new);
    }

    //reading function
    public void saveWord(String wordsRead) throws IOException {
        String wordsReadWithoutSpecial = wordsRead.replaceAll("[-+.^:,]", " ");
        Set<String> wordSeparate = new HashSet<>(Arrays.asList(wordsReadWithoutSpecial.split("\\s+")));
        Set<String> tagNames = tagRepository.findAllName();
        Set<String> wordInTable = wordRepository.findAllName();
        String invalid = "^[a-zA-ZäöåÄÖÅ]+$";
        Set<String> wordSeparateOnlyAlphabet = wordSeparate.stream().filter(s -> s.matches(invalid)).filter(s -> !tagNames.contains(s)).filter(s -> !wordInTable.contains(s)).collect(Collectors.toSet());
        Set<Word> words = wordSeparateOnlyAlphabet.stream().map(s -> {
            Word word = new Word();
            word.setName(s);
            return word;
        }).collect(Collectors.toSet());
        wordRepository.saveAll(words);
    }

    public String readPDFFile (InputStream inputStream) throws IOException {
        PDDocument document = PDDocument.load(inputStream);
        String text = "";
        if (!document.isEncrypted()) {
            PDFTextStripper stripper = new PDFTextStripper();
            text = stripper.getText(document);
        }
        document.close();
        return text;
    }

}
