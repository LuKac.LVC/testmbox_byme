/**
 * Data Access Objects used by WebSocket services.
 */
package se.searchofskills.web.websocket.dto;
