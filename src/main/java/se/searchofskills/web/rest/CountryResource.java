package se.searchofskills.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.CountryService;
import se.searchofskills.service.dto.country.CountryCreateDTO;
import se.searchofskills.service.dto.country.CountryOutputDTO;
import se.searchofskills.service.dto.country.CountryUpdateDTO;

import java.util.List;

@RestController
@RequestMapping("/api/v2/countries")
public class CountryResource {

    private final CountryService countryService;

    public CountryResource(CountryService countryService) {
        this.countryService = countryService;
    }

    // TODO
    @PostMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CountryOutputDTO> create(@RequestBody CountryCreateDTO countryCreateDTO) {
        return new ResponseEntity<>(this.countryService.create(countryCreateDTO), HttpStatus.CREATED);
    }

    // TODO
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CountryOutputDTO> update(@PathVariable(name = "id") long id, @RequestBody CountryUpdateDTO countryUpdateDTO) {
        countryUpdateDTO.setId(id);
        return new ResponseEntity<>(this.countryService.update(countryUpdateDTO), HttpStatus.OK);
    }

    // TODO
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> delete(@PathVariable(name = "id") long id) {
        this.countryService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // TODO
    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<CountryOutputDTO>> getAll(Pageable pageable, @RequestParam(required = false) String name) {
        Page<CountryOutputDTO> page = countryService.getAll(pageable, name);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
