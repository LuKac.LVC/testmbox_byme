package se.searchofskills.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.CandidateService;
import se.searchofskills.service.dto.candidate.CandidateCreateInputDTO;
import se.searchofskills.service.dto.candidate.CandidateOutputDTO;
import se.searchofskills.service.dto.candidate.CandidateUpdateInputDTO;
import se.searchofskills.service.dto.cv.CvOutputDTO;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/v2/candidates")
public class CandidateResource {

    private final CandidateService candidateService;

    public CandidateResource(CandidateService candidateService) {
        this.candidateService = candidateService;
    }

    // TODO
    @PostMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CandidateOutputDTO> create(@RequestBody CandidateCreateInputDTO candidateCreateInputDTO) {
        return new ResponseEntity<>(this.candidateService.create(candidateCreateInputDTO), HttpStatus.CREATED);
    }

    // TODO
    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CandidateOutputDTO> update(@PathVariable(name = "id") long id, @RequestBody CandidateUpdateInputDTO candidateUpdateInputDTO) {
        candidateUpdateInputDTO.setId(id);
        return new ResponseEntity<>(this.candidateService.update(candidateUpdateInputDTO), HttpStatus.OK);
    }

    // TODO
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> delete(@PathVariable(name = "id") long id) {
        this.candidateService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // TODO
    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<CandidateOutputDTO>> getAll(Pageable pageable, @RequestParam(required = false) String filter, @RequestParam(required = false) List<Long> tags) {
        Page<CandidateOutputDTO> page = candidateService.getAll(pageable, filter, tags);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    // TODO
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CandidateOutputDTO> getOne(@PathVariable(name = "id") long id) {
        return new ResponseEntity<>(this.candidateService.getOne(id), HttpStatus.OK);
    }

    // TODO
    @GetMapping("/columns")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<String>> getColumns() {
        return new ResponseEntity<>(this.candidateService.getColumns(), HttpStatus.OK);
    }

    // TODO
    @PostMapping("/{id}/cvs")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CandidateOutputDTO> uploadCvs(@RequestParam List<MultipartFile> files, @PathVariable long id) throws IOException {
        this.candidateService.uploadCvs(files, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    // TODO
    @DeleteMapping("/{candidateId}/cvs/{cvId}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteCv(@PathVariable long candidateId, @PathVariable long cvId) {
        this.candidateService.deleteCv(candidateId, cvId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // TODO
    @GetMapping("/{candidateId}/cvs/{cvId}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CvOutputDTO> getOneCv(@PathVariable long candidateId, @PathVariable long cvId) {
        return new ResponseEntity<>(this.candidateService.getCv(candidateId, cvId), HttpStatus.OK);
    }

    @GetMapping("/{id}/cv/{fileName}")
    public ResponseEntity<InputStreamResource> viewCv(@PathVariable long id, @PathVariable String fileName) throws FileNotFoundException {
        InputStreamResource inputStreamResource = candidateService.viewPdf(id, fileName);
        return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType("application/pdf"))
            .body(inputStreamResource);
    }

    // TODO
    @GetMapping("/{id}/cvs")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<CvOutputDTO>> getAllCvOfCandidate(Pageable pageable, @PathVariable long id) {
        Page<CvOutputDTO> page = candidateService.getCvs(pageable, id);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    // TODO
    @GetMapping("/cvs/export-pdf")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<InputStreamResource> exportCvs(@RequestParam long filterListId, @RequestParam int numberOfCVs) throws IOException {
        String pathName = "merge.pdf";
        InputStreamResource inputStreamResource = candidateService.exportCvs(filterListId, numberOfCVs, pathName);
        File file = new File(pathName);
        if (file.exists()) {
            file.delete();
        }
        return ResponseEntity.ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=candidate-cv.pdf")
            .contentType(MediaType.parseMediaType("application/pdf"))
            .body(inputStreamResource);
    }


    // TODO
    @GetMapping("/export-excel")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<InputStreamResource> exportExcel(@RequestParam long filterListId, @RequestParam List<String> fieldsSelected) throws Exception {
        ByteArrayInputStream byteArrayInputStream = candidateService.exportExcel(filterListId, fieldsSelected);
        return ResponseEntity.ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=candidate.xlsx")
            .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
            .body(new InputStreamResource(byteArrayInputStream));
    }
}
