package se.searchofskills.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.CategoryService;
import se.searchofskills.service.dto.category.CategoryCreatedDTO;
import se.searchofskills.service.dto.category.CategoryOutputDTO;
import se.searchofskills.service.dto.category.CategoryUpdateDTO;

import java.util.List;

@RestController
@RequestMapping("/api/v2/categories")
public class CategoryResource {
    private final CategoryService categoryService;

    public CategoryResource(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    // TODO
    @PostMapping("")
    public ResponseEntity<List<CategoryOutputDTO>> create(@RequestBody CategoryCreatedDTO categoryCreatedDTO) {
        return new ResponseEntity<>(categoryService.create(categoryCreatedDTO), HttpStatus.CREATED);
    }

    // TODO
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CategoryOutputDTO> update(@PathVariable(name = "id") long id, @RequestBody CategoryUpdateDTO categoryUpdateDTO) {
        categoryUpdateDTO.setId(id);
        return new ResponseEntity<>(this.categoryService.update(categoryUpdateDTO), HttpStatus.OK);
    }

    // TODO
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> delete(@PathVariable(name = "id") long id) {
        this.categoryService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // TODO
    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<CategoryOutputDTO>> getAll(Pageable pageable, @RequestParam(required = false) String name) {
        Page<CategoryOutputDTO> page = categoryService.getAll(pageable, name);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    // TODO
    @GetMapping("/{name}/exist")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Boolean> checkExist(@PathVariable String name) {
        return new ResponseEntity<>(categoryService.checkExist(name), HttpStatus.OK);
    }
}
