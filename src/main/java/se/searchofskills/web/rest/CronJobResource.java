package se.searchofskills.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.CronJobService;
import se.searchofskills.service.dto.CronJobStatisticDTO;
import se.searchofskills.service.dto.CronjobDTO;
import se.searchofskills.service.dto.CronjobDataDTO;

import java.util.List;

import javax.validation.Valid;
import java.time.Instant;

@RestController
@RequestMapping("/api")
public class CronJobResource {
    private final Logger log = LoggerFactory.getLogger(CronJobResource.class);

    private final CronJobService cronJobService;

    public CronJobResource(CronJobService cronJobService) {
        this.cronJobService = cronJobService;
    }


    @ApiOperation(
        value = "Create a cronjob"
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "Created - create successfully"),
        @ApiResponse(code = 400, message = "Bad request - request invalid"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PostMapping("/cronjob")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CronjobDTO> createCronjob(@Valid @RequestBody CronjobDTO cronjobDTO) {
        CronjobDTO result = cronJobService.saveCronjob(cronjobDTO);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }


    @ApiOperation(
        value = "Check if endDate is overlap"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - check endDate overlap successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @GetMapping("/cronjob/getEndDateOverlap")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public Instant getLatestDateOverlapEndDate(@RequestParam Instant endDate) {
        return cronJobService.getLatestDateOverlapEndDate(endDate);
    }

    @ApiOperation(
        value = "Restore a cronjob"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - restore successfully"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 400, message = "Invalid data - cronjob status invalid"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PutMapping("/cronjob/restore")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public void restoreCronJob(@RequestParam long id) {
        cronJobService.restore(id);
    }

    @ApiOperation(
        value = "Cancel a cronjob"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - cancel successfully"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 400, message = "Invalid data - cronjob status invalid"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PutMapping("/cronjob/cancel")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public void cancelCronJob(@RequestParam long id) {
        cronJobService.cancel(id);
    }

    @ApiOperation(
        value = "Resume a cronjob"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - resume successfully"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 400, message = "Invalid data - cronjob status invalid"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PutMapping("/cronjob/resume")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public void resumeCronJob(@RequestParam long id) {
        cronJobService.resume(id);
    }

    @ApiOperation(
        value = "Get cronjob statistic"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - get data successfully"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 400, message = "Invalid data - cronjob status invalid"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @GetMapping("/cronjob/statistic")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public CronJobStatisticDTO getStatisticCronJob(@RequestParam long cronJobId) {
        return cronJobService.getStatistic(cronJobId);
    }

    @ApiOperation(
        value = "Delete a cronjob"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - delete successfully"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 400, message = "Invalid data - cronjob status invalid"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @DeleteMapping("/cronjob")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public void deleteCronJob(@RequestParam long id) {
        cronJobService.delete(id);
    }

    @ApiOperation(
        value = "Get list cronjob"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - get successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @GetMapping("/cronjobs")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<CronjobDataDTO>> getCronJobList(Pageable pageable, @RequestParam(required = false) boolean isArchived) {
        final Page<CronjobDataDTO> page = cronJobService.findCronjobByArchived(pageable, isArchived);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @ApiOperation(
        value = "Archive a cronjob"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - archive successfully"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 400, message = "Invalid data - cronjob status invalid"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PutMapping("/cronjob/archive")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public void archiveCronJob(@RequestParam long id) {
        cronJobService.archive(id);
    }

    @ApiOperation(
        value = "Get list cronjob for search name when creating cronjob"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - get successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    @GetMapping(value = "/cronjob/all")
    public ResponseEntity<List<CronjobDataDTO>> getListCronjob(Pageable pageable,@RequestParam(required = false) String name){
        Page<CronjobDataDTO> page = cronJobService.getCronjobs(pageable, name);
        return new ResponseEntity(page.getContent(), HttpStatus.OK);
    }


}
