package se.searchofskills.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.ManagerialPositionService;
import se.searchofskills.service.dto.managerial_position.ManagerialPositionCreateDTO;
import se.searchofskills.service.dto.managerial_position.ManagerialPositionOutputDTO;
import se.searchofskills.service.dto.managerial_position.ManagerialPositionUpdateDTO;

import java.util.List;

@RestController
@RequestMapping("/api/v2/managerial-positions")
public class ManagerialPositionResource {

    private final ManagerialPositionService managerialPositionService;

    public ManagerialPositionResource(ManagerialPositionService managerialPositionService) {
        this.managerialPositionService = managerialPositionService;
    }

    // TODO
    @PostMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<ManagerialPositionOutputDTO>> create(@RequestBody ManagerialPositionCreateDTO managerialPositionDTO) {
        return new ResponseEntity<>(this.managerialPositionService.create(managerialPositionDTO), HttpStatus.CREATED);
    }

    // TODO
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ManagerialPositionOutputDTO> update(@PathVariable(name = "id") long id, @RequestBody ManagerialPositionUpdateDTO managerialPositionUpdateDTO) {
        managerialPositionUpdateDTO.setId(id);
        return new ResponseEntity<>(this.managerialPositionService.update(managerialPositionUpdateDTO), HttpStatus.OK);
    }

    // TODO
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> delete(@PathVariable(name = "id") long id) {
        this.managerialPositionService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // TODO
    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<ManagerialPositionOutputDTO>> getAll(Pageable pageable, @RequestParam(required = false) String name) {
        Page<ManagerialPositionOutputDTO> page = managerialPositionService.getAll(pageable, name);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
