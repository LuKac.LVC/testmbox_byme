/**
 * View Models used by Spring MVC REST controllers.
 */
package se.searchofskills.web.rest.vm;
