package se.searchofskills.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.searchofskills.domain.enumeration.EmailTemplateType;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.EmailTemplateService;
import se.searchofskills.service.dto.EmailTemplateDTO;
import se.searchofskills.service.dto.RecipientListDTO;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import java.util.List;

@RestController
@RequestMapping("/api")
public class EmailTemplateResource {
    private final Logger log = LoggerFactory.getLogger(EmailTemplateResource.class);

    private final EmailTemplateService emailTemplateService;

    public EmailTemplateResource(EmailTemplateService emailTemplateService) {
        this.emailTemplateService = emailTemplateService;
    }

    @ApiOperation(
        value = "Get email templates with pagination"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - get data successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @GetMapping("/emailTemplates")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<EmailTemplateDTO>> getEmailTemplates(Pageable pageable,@RequestParam(required = false) String name, @RequestParam(required = false) EmailTemplateType type) {
        Page<EmailTemplateDTO> page = emailTemplateService.findEmailTemplatesByName(pageable, name, type);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @ApiOperation(
        value = "Get an email template by id"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - get data successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @GetMapping("/emailTemplate")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public EmailTemplateDTO getOneEmailTemplate(@RequestParam long id) {
        return emailTemplateService.findOne(id);
    }

    @ApiOperation(
        value = "Create or update an email template",
        notes = "Update will be detect if input have 'id' field"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - get data successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PostMapping("/emailTemplate")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public EmailTemplateDTO createOrUpdateEmailTemplate(@Valid @RequestBody EmailTemplateDTO emailTemplateDTO) {
        return emailTemplateService.createOrUpdate(emailTemplateDTO);
    }

    @ApiOperation(
        value = "Delete an email template"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - update data successfully"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @DeleteMapping("/emailTemplate")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public void deleteRecipientList(long id) {
        emailTemplateService.deleteEmailTemplate(id);
    }
}
