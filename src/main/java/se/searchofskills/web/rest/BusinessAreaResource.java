package se.searchofskills.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.BusinessAreaService;
import se.searchofskills.service.dto.business_area.BusinessAreaCreateDTO;
import se.searchofskills.service.dto.business_area.BusinessAreaOutputDTO;
import se.searchofskills.service.dto.business_area.BusinessAreaUpdateDTO;

import java.util.List;

@RestController
@RequestMapping("/api/v2/business-areas")
public class BusinessAreaResource {

    private final BusinessAreaService businessAreaService;

    public BusinessAreaResource(BusinessAreaService businessAreaService) {
        this.businessAreaService = businessAreaService;
    }

    // TODO
    @PostMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<BusinessAreaOutputDTO>> create(@RequestBody BusinessAreaCreateDTO businessAreaCreateDTO) {
        return new ResponseEntity<>(this.businessAreaService.create(businessAreaCreateDTO), HttpStatus.CREATED);
    }

    // TODO
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<BusinessAreaOutputDTO> update(@PathVariable(name = "id") long id, @RequestBody BusinessAreaUpdateDTO businessAreaUpdateDTO) {
        businessAreaUpdateDTO.setId(id);
        return new ResponseEntity<>(this.businessAreaService.update(businessAreaUpdateDTO), HttpStatus.OK);
    }

    // TODO
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> delete(@PathVariable(name = "id") long id) {
        this.businessAreaService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // TODO
    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<BusinessAreaOutputDTO>> getAll(Pageable pageable, @RequestParam(required = false) String name) {
        Page<BusinessAreaOutputDTO> page = businessAreaService.getAll(pageable, name);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
