package se.searchofskills.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.RecipientService;
import se.searchofskills.service.dto.RecipientDTO;
import se.searchofskills.service.dto.RecipientListDTO;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class RecipientResource {
    private final RecipientService recipientService;

    public RecipientResource(RecipientService recipientService) {
        this.recipientService = recipientService;
    }

    @ApiOperation(
        value = "Create a new recipient list",
        notes = "This endpoint to create recipient list and recipient via Excel file"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - import successfully"),
        @ApiResponse(code = 400, message = "Bad Request - Upload excel failed or excel validation caught"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PostMapping("/recipientList/import")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public void importRecipientListViaExcel(@RequestParam("file") MultipartFile file) throws IOException {
        String filePath = file.getOriginalFilename();
        String recipientListName = filePath.substring(0, filePath.lastIndexOf("."));
        recipientService.importRecipientList(file.getInputStream(), recipientListName);
    }

    @ApiOperation(
        value = "Get recipient list with pagination"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - get data successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @GetMapping("/recipientLists")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<RecipientListDTO>> getRecipientLists(Pageable pageable, @RequestParam(required = false) String name) {
        Page<RecipientListDTO> page = recipientService.findAllRecipientListsByName(pageable, name);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @ApiOperation(
        value = "Get a recipient list by id"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - get data successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @GetMapping("/recipientList")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public RecipientListDTO getOneRecipientList(@RequestParam long id) {
        return recipientService.findOne(id);
    }

    @ApiOperation(
        value = "Get recipients by recipient list id"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - get data successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @GetMapping("/recipients")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<RecipientDTO>> getRecipients(Pageable pageable, @RequestParam long recipientListId) {
        Page<RecipientDTO> page = recipientService.findAllRecipientsByRecipientListId(pageable, recipientListId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @ApiOperation(
        value = "Update a recipient list",
        notes = "Recipient list only allow to update 'name' fields"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - update data successfully"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PutMapping("/recipientList")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public RecipientListDTO updateRecipientList(@RequestBody RecipientListDTO recipientListDTO) {
        return recipientService.updateRecipientList(recipientListDTO);
    }

    @ApiOperation(
        value = "Delete a recipient list"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - update data successfully"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @DeleteMapping("/recipientList")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public void deleteRecipientList(@RequestParam long id) {
        recipientService.deleteRecipientList(id);
    }
}
