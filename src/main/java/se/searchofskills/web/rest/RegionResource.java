package se.searchofskills.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.RegionService;
import se.searchofskills.service.dto.region.RegionInputDTO;
import se.searchofskills.service.dto.region.RegionOutputDTO;

import java.util.List;

@RestController
@RequestMapping("/api/v2/regions")
public class RegionResource {

    private final RegionService regionService;

    public RegionResource(RegionService regionService) {
        this.regionService = regionService;
    }

    // TODO
    @PostMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<RegionOutputDTO> create(@RequestBody RegionInputDTO regionInputDTO) {
        return new ResponseEntity<>(this.regionService.create(regionInputDTO), HttpStatus.CREATED);
    }

    // TODO
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<RegionOutputDTO> update(@PathVariable(name = "id") long id, @RequestBody RegionInputDTO regionInputDTO) {
        regionInputDTO.setId(id);
        return new ResponseEntity<>(this.regionService.update(regionInputDTO), HttpStatus.OK);
    }

    // TODO
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> delete(@PathVariable(name = "id") long id) {
        this.regionService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // TODO
    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<RegionOutputDTO>> getAll(Pageable pageable, @RequestParam(required = false) String name) {
        Page<RegionOutputDTO> page = regionService.getAll(pageable, name);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    // TODO
    @GetMapping("/{countryId}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<RegionOutputDTO>> getByCountry(@PathVariable Long countryId) {
        List<RegionOutputDTO> page = regionService.getRegionByCountry(countryId);
        return new ResponseEntity<>(page, HttpStatus.OK);
    }
}
