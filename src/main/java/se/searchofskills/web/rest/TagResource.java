package se.searchofskills.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.TagService;
import se.searchofskills.service.dto.tag.TagCreateInputDTO;
import se.searchofskills.service.dto.tag.TagInputFromWordsDTO;
import se.searchofskills.service.dto.tag.TagOutputDTO;
import se.searchofskills.service.dto.tag.TagUpdateInputDTO;

import java.util.List;

@RestController
@RequestMapping("/api/v2/tags")
public class TagResource {
    private final TagService tagService;

    public TagResource(TagService tagService) {
        this.tagService = tagService;
    }

    // TODO
    @PostMapping("")
    public ResponseEntity<List<TagOutputDTO>> create(@RequestBody TagCreateInputDTO tagCreateInputDTO) {
        return new ResponseEntity<>(this.tagService.create(tagCreateInputDTO), HttpStatus.CREATED);
    }

    // TODO
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<TagOutputDTO> update(@PathVariable(name = "id") long id, @RequestBody TagUpdateInputDTO tagUpdateInputDTO) {
        tagUpdateInputDTO.setId(id);
        return new ResponseEntity<>(this.tagService.update(tagUpdateInputDTO), HttpStatus.OK);
    }

    // TODO
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> delete(@PathVariable(name = "id") long id) {
        this.tagService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // TODO
    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<TagOutputDTO>> getAll(Pageable pageable, @RequestParam boolean block, @RequestParam(required = false) String name) {
        Page<TagOutputDTO> page = tagService.getALl(pageable, block, name);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    // TODO
    @PutMapping("/{id}/block")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<TagOutputDTO> block(@PathVariable(name = "id") long id) {
        tagService.block(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // TODO
    @PutMapping("/{id}/unblock")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<TagOutputDTO> unblock(@PathVariable(name = "id") long id) {
        tagService.unblock(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // TODO
    @PostMapping("/words")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> saveWordsToTag(@RequestBody TagInputFromWordsDTO tagInputFromWordsDTO) {
        this.tagService.saveWordsToTag(tagInputFromWordsDTO);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // TODO
    @GetMapping("/{name}/exist")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Boolean> checkExist(@PathVariable String name) {
        return new ResponseEntity<>(tagService.checkExisted(name), HttpStatus.OK);
    }
}
