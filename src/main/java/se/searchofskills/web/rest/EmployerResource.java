package se.searchofskills.web.rest;


import io.github.jhipster.web.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.searchofskills.security.AuthoritiesConstants;
import se.searchofskills.service.EmployerService;
import se.searchofskills.service.dto.employer.EmployerCreateDTO;
import se.searchofskills.service.dto.employer.EmployerOutputDTO;
import se.searchofskills.service.dto.employer.EmployerUpdateDTO;

import java.util.List;

@RestController
@RequestMapping("/api/v2/employers")
public class EmployerResource {
    private final EmployerService employerService;

    public EmployerResource(EmployerService employerService) {
        this.employerService = employerService;
    }

    // TODO
    @PostMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<EmployerOutputDTO>> create(@RequestBody EmployerCreateDTO employerCreateDTO) {
        return new ResponseEntity<>(this.employerService.create(employerCreateDTO), HttpStatus.CREATED);
    }

    // TODO
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<EmployerOutputDTO> update(@PathVariable(name = "id") long id, @RequestBody EmployerUpdateDTO employerUpdateDTO) {
        employerUpdateDTO.setId(id);
        return new ResponseEntity<>(this.employerService.update(employerUpdateDTO), HttpStatus.OK);
    }

    // TODO
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> delete(@PathVariable(name = "id") long id) {
        this.employerService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // TODO
    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<EmployerOutputDTO>> getAll(Pageable pageable, @RequestParam(required = false) String name) {
        Page<EmployerOutputDTO> page = employerService.getAll(pageable, name);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
