package se.searchofskills.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.Candidate;

import java.util.List;
import java.util.Set;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long> {
    List<Candidate> findAllByIdIn(List<Long> candidateIds);

    @Query("select c.id from Candidate c where upper(c.country.name) like upper(concat('%', ?1, '%'))")
    Set<Long> findAllByCountryNameContaining(String freeText);

    @Query("select c.id from Candidate c where upper(c.region.name) like upper(concat('%', ?1, '%'))")
    Set<Long> findAllByRegionNameContaining(String freeText);
}
