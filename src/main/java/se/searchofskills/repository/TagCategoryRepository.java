package se.searchofskills.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.TagCategory;

import java.util.List;
import java.util.Optional;

@Repository
public interface TagCategoryRepository extends JpaRepository<TagCategory, Long> {
    @Query("select t from TagCategory t where t.tag.id = ?1 and t.category.id = ?2")
    Optional<TagCategory> findByTagIdAndCategoryId(Long tagId, Long categoryId);

    List<TagCategory> findAllByTagId(Long id);
}
