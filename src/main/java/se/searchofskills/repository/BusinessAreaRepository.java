package se.searchofskills.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.BusinessArea;

import java.util.List;

@Repository
public interface BusinessAreaRepository extends JpaRepository<BusinessArea, Long> {
    Page<BusinessArea> findAllByNameContainingIgnoreCase(Pageable pageable, String name);

    List<BusinessArea> findAllByIdIn(List<Long> businessIds);
}
