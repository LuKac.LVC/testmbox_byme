package se.searchofskills.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.Word;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface WordRepository extends JpaRepository<Word, Long> {
    @Query("select w.name from Word w")
    Set<String> findAllName();

    Set<Word> findAllByNameIn(List<String> values);


}
