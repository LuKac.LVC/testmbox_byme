package se.searchofskills.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.Cv;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface CvRepository extends JpaRepository<Cv, Long> {
    Page<Cv> findAllByCandidateIdOrderByCreatedDateDesc(Pageable pageable, Long candidateId);

    List<Cv> findAllByCandidateIdOrderByCreatedDateDesc(Long candidateId);

    Optional<Cv> findTopByCandidateIdOrderByCreatedDateDesc(Long candidateId);

    Optional<Cv> findByCandidateIdAndFileName(Long candidateId, String fileName);

    @Query("select c.candidate.id from Cv c where upper(c.content) like upper(concat('%', ?1, '%'))")
    Set<Long> findAllCandidateIdsByContent(String name);
}
