package se.searchofskills.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.EmailTemplate;
import se.searchofskills.domain.enumeration.CronJobStatus;
import se.searchofskills.domain.enumeration.EmailTemplateType;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmailTemplateRepository extends JpaRepository<EmailTemplate, Long> {
    Page<EmailTemplate> findAllByDeleted(Pageable pageable, boolean deleted);

    Page<EmailTemplate> findAllByDeletedAndNameContainingIgnoreCase(Pageable pageable, boolean deleted, String name);

    @Query(value = "SELECT et FROM EmailTemplate et " +
        "JOIN CronJobEmailTemplate cet ON cet.emailTemplate.id = et.id " +
        "WHERE cet.emailTemplate.type = :type AND cet.cronJob.id = :cronJobId")
    List<EmailTemplate> findMainTemplateOfCronJob(@Param("type") EmailTemplateType type, @Param("cronJobId") long cronJobId);

    @Query(value = "SELECT et FROM EmailTemplate et " +
        "JOIN CronJobEmailTemplate cet ON cet.emailTemplate.id = et.id " +
        "WHERE cet.cronJob.id = :cronJobId AND cet.type = :type")
    List<EmailTemplate> findReminderTemplateOfCronJob(@Param("type") EmailTemplateType type, @Param("cronJobId") long cronJobId);

    @Query(value = "SELECT COUNT (cr.id) from CronJob as cr JOIN CronJobEmailTemplate cet ON cet.emailTemplate.id = :id AND cet.cronJob.id = cr.id where cr.status = :status")
    long countAllCronJobIsUsingEmailTemplateById(@Param("status") CronJobStatus status, @Param("id") long id);

    Optional<EmailTemplate> findByIdAndDeletedIsFalse(long id);

    Page<EmailTemplate> findAllByDeletedAndType(Pageable pageable, boolean deleted, EmailTemplateType type);

    Page<EmailTemplate> findAllByDeletedAndNameContainingIgnoreCaseAndType(Pageable pageable, boolean deleted, String name, EmailTemplateType type);

    Optional<EmailTemplate> findByIdAndType(long id, EmailTemplateType emailTemplateType);
}
