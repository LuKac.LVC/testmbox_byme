package se.searchofskills.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.CandidateEmployer;

import java.util.List;
import java.util.Optional;

@Repository
public interface CandidateEmployerRepository extends JpaRepository<CandidateEmployer, Long> {
    @Query("select c from CandidateEmployer c where c.employer.id = ?1 and c.candidate.id = ?2")
    Optional<CandidateEmployer> findByEmployerIdAndCandidateId(Long employerId, Long candidateId);

    @Query("select c.employer.id from CandidateEmployer c where c.candidate.id = ?1")
    List<Long> findAllByCandidateId(Long candidateId);
}
