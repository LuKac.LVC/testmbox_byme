package se.searchofskills.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.CandidateBusinessArea;

import java.util.List;
import java.util.Optional;

@Repository
public interface CandidateBusinessAreaRepository extends JpaRepository<CandidateBusinessArea, Long> {
    Optional<CandidateBusinessArea> findByBusinessAreaIdAndCandidateId(Long businessId, Long candidateId);

    @Query("select c.businessArea.id from CandidateBusinessArea c where c.candidate.id = ?1")
    List<Long> findAllByCandidateId(Long candidateId);
}
