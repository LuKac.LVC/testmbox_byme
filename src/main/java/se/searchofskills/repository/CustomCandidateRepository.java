package se.searchofskills.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.query.QueryUtils;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.*;
import se.searchofskills.domain.enumeration.CandidateColumn;
import se.searchofskills.domain.enumeration.Operator;
import se.searchofskills.service.dto.filter.FilterDTO;
import se.searchofskills.service.dto.filter.FilterWithConditionDTO;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;

@Repository
public class CustomCandidateRepository {
    private final EntityManager entityManager;

    private final CandidateRepository candidateRepository;

    private final CandidateFilterListRepository candidateFilterListRepository;

    private final CvRepository cvRepository;

    public CustomCandidateRepository(EntityManager entityManager, CandidateRepository candidateRepository, CandidateFilterListRepository candidateFilterListRepository, CvRepository cvRepository) {
        this.entityManager = entityManager;
        this.candidateRepository = candidateRepository;
        this.candidateFilterListRepository = candidateFilterListRepository;
        this.cvRepository = cvRepository;
    }

    public Page<Candidate> searchCandidate(Pageable pageable, FilterDTO filterDTO, FilterList filterList, List<String> tagsName) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Candidate> criteria = builder.createQuery(Candidate.class);
        Root<Candidate> candidateRoot = criteria.from(Candidate.class);

        criteria.select(candidateRoot);

        List<Predicate> predicates = getPredicate(candidateRoot, builder, filterDTO, filterList, tagsName);
        criteria.where(builder.and(predicates.toArray(new Predicate[]{})));
        criteria.orderBy(QueryUtils.toOrders(pageable.getSort(),candidateRoot , builder));

        // This query fetches the Candidate as per the Page Limit
        TypedQuery<Candidate> typedQuery = entityManager.createQuery(criteria);
        typedQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        typedQuery.setMaxResults(pageable.getPageSize());

        // Create Count Query
        CriteriaQuery<Long> countQuery = builder.createQuery(Long.class);
        Root<Candidate> candidateRootCount = countQuery.from(Candidate.class);
        countQuery.select(builder.count(candidateRootCount)).where(builder.and(predicates.toArray(new Predicate[]{})));

        // Fetches the count of all Candidate as per given criteria
        Long count = entityManager.createQuery(countQuery).getSingleResult();
        return new PageImpl<>(typedQuery.getResultList(), pageable, count);
    }

    public List<Predicate> getPredicate(Root<Candidate> candidateRoot, CriteriaBuilder builder, FilterDTO filterDTO, FilterList filterList, List<String> tagsName) {
        List<Predicate> predicates = new ArrayList<>();

        Predicate deletePredicate = builder.equal(candidateRoot.get(Candidate_.DELETED), false);
        predicates.add(deletePredicate);

        //free text
        if (filterDTO.getFreeText() != null || !filterDTO.getFreeText().isEmpty()) {
            String freeText = filterDTO.getFreeText().toUpperCase(Locale.ROOT);
            Predicate predicateFreeText = builder.or(
                builder.like(builder.upper(candidateRoot.get(Candidate_.FIRST_NAME)),'%' + freeText + '%'),
                builder.like(builder.upper(candidateRoot.get(Candidate_.LAST_NAME)),'%' + freeText + '%'),
                builder.like(builder.upper(candidateRoot.get(Candidate_.BIRTH_YEAR)),'%' + freeText + '%'),
                builder.like(builder.upper(candidateRoot.get(Candidate_.CITY)),'%' + freeText + '%'),
                builder.like(builder.upper(candidateRoot.get(Candidate_.PERSONAL_NUMBER)), '%' + freeText + '%'),
                builder.like(builder.upper(candidateRoot.get(Candidate_.LINKED_LN_LINK)), '%' + freeText + '%'),
                builder.like(builder.upper(candidateRoot.get(Candidate_.ZIP_CODE)),'%' + freeText + '%'),
                builder.like(builder.upper(candidateRoot.get(Candidate_.COUNTY)),'%' + freeText + '%'),
                builder.like(builder.upper(candidateRoot.get(Candidate_.STREET_ADDRESS)),'%' + freeText + '%'),
                builder.like(builder.upper(candidateRoot.get(Candidate_.EMAIL_PRIVATE)),'%' + freeText + '%'),
                builder.like(builder.upper(candidateRoot.get(Candidate_.EMAIL_WORK)),'%' + freeText + '%')
            );
            Set<Long> candidateIdsCountryFreeText =  candidateRepository.findAllByCountryNameContaining(freeText);
            Predicate countryFreeText = candidateRoot.get(Candidate_.ID).in(candidateIdsCountryFreeText);

            Set<Long> candidateIdsRegionFreeText =  candidateRepository.findAllByRegionNameContaining(freeText);
            Predicate regionFreeText = candidateRoot.get(Candidate_.ID).in(candidateIdsRegionFreeText);

            Predicate predicateSearchFreeText = builder.or(predicateFreeText, countryFreeText, regionFreeText);
            predicates.add(predicateSearchFreeText);
        }
        // predicate filter list id
        List<Long> allCandidateIds = new ArrayList<>();
        if (filterList != null) {
            List<Long> candidateIdsFromFilterList = candidateFilterListRepository.findAllByFilterListId(filterList.getId());
            allCandidateIds.addAll(candidateIdsFromFilterList);
            Predicate predicateFilterList = candidateRoot.get(Candidate_.ID).in(allCandidateIds);
            predicates.add(predicateFilterList);
        }
        // filterWithCondition
        List<Predicate> filterWithConditionList = new ArrayList<>();
        if (filterDTO.getFilterWithConditions().size() > 0) {
            for (FilterWithConditionDTO filterWithConditionDTO : filterDTO.getFilterWithConditions()) {
                Predicate filterWithConditionPredicate = getPredicateFilterWithConditions(candidateRoot, builder, filterWithConditionDTO);
                filterWithConditionList.add(filterWithConditionPredicate);
            }
            Predicate combinePredicateFilterWithConditions = filterWithConditionList.get(0);
            if (filterWithConditionList.size() > 1) {
                for (int i = 1; i < filterWithConditionList.size(); i++) {
                    for (int j = i - 1; j < filterDTO.getOperatorsOrder().size() ; j++) {
                        Operator operator = filterDTO.getOperatorsOrder().get(j);
                        if (operator == Operator.AND) {
                            combinePredicateFilterWithConditions = builder.and(combinePredicateFilterWithConditions, filterWithConditionList.get(i));
                            break;
                        }
                        else if (operator == Operator.OR) {
                            combinePredicateFilterWithConditions = builder.or(combinePredicateFilterWithConditions, filterWithConditionList.get(i));
                            break;
                        }
                    }
                }
            }
            predicates.add(combinePredicateFilterWithConditions);
        }
        //filter with tag name
        if (!tagsName.isEmpty()) {
            Set<Long> candidateIdsWithCvContent = new HashSet<>();
            for(String tag : tagsName) {
                String tagName = tag.toUpperCase(Locale.ROOT);
                Set<Long> candidateIdsWithCvContentSameTagName = cvRepository.findAllCandidateIdsByContent(tagName);
                candidateIdsWithCvContent.addAll(candidateIdsWithCvContentSameTagName);
            }
            Predicate tagPredicate = candidateRoot.get(Candidate_.ID).in(candidateIdsWithCvContent);
            predicates.add(tagPredicate);
        }
        return predicates;
    }

    private Predicate getPredicateFilterWithConditions(Root<Candidate> candidateRoot, CriteriaBuilder builder, FilterWithConditionDTO filterWithConditionDTO) {
        Predicate predicate = null;
        switch (filterWithConditionDTO.getCondition()) {
            case IS:
                predicate = builder.equal(getCandidatePath(candidateRoot, filterWithConditionDTO.getField()), filterWithConditionDTO.getValue());
                break;
            case IS_NOT:
                predicate = builder.notEqual(getCandidatePath(candidateRoot, filterWithConditionDTO.getField()), filterWithConditionDTO.getValue());
                break;
            case CONTAINS:
                predicate = builder.like(builder.upper(getCandidatePath(candidateRoot, filterWithConditionDTO.getField())),'%' + filterWithConditionDTO.getValue().toUpperCase() + '%');
                break;
            case NOT_CONTAINS:
                predicate = builder.notLike(builder.upper(getCandidatePath(candidateRoot, filterWithConditionDTO.getField())),'%' + filterWithConditionDTO.getValue().toUpperCase() + '%');
                break;
            case GREATER_THAN:
                predicate = builder.greaterThan(getCandidatePath(candidateRoot, filterWithConditionDTO.getField()), filterWithConditionDTO.getValue());
                break;
            case LESS_THAN:
                predicate = builder.lessThan(getCandidatePath(candidateRoot, filterWithConditionDTO.getField()), filterWithConditionDTO.getValue());
                break;
        }
        return predicate;
    }

    private Expression<String> getCandidatePath(Root<Candidate> candidateRoot, CandidateColumn candidateColumn) {
        Expression<String> candidatePath = null;
        switch (candidateColumn) {
            case FIRST_NAME:
                candidatePath =  candidateRoot.get(Candidate_.FIRST_NAME);
                break;
            case LAST_NAME:
                candidatePath = candidateRoot.get(Candidate_.LAST_NAME);
                break;
            case BIRTH_YEAR:
                candidatePath = candidateRoot.get(Candidate_.BIRTH_YEAR);
                break;
            case PERSONAL_NUMBER:
                candidatePath = candidateRoot.get(Candidate_.PERSONAL_NUMBER);
                break;
            case GENDER :
                candidatePath = candidateRoot.get(Candidate_.GENDER);
                break;
            case LINKED_LN_LINK:
                candidatePath = candidateRoot.get(Candidate_.LINKED_LN_LINK);
                break;
            case FIRST_EMPLOYMENT :
                candidatePath = candidateRoot.get(Candidate_.FIRST_EMPLOYMENT);
                break;
            case CITY:
                candidatePath = candidateRoot.get(Candidate_.CITY);
                break;
            case ZIPCODE:
                candidatePath = candidateRoot.get(Candidate_.ZIP_CODE);
                break;
            case COUNTY:
                candidatePath = candidateRoot.get(Candidate_.COUNTY);
                break;
            case STREET_ADDRESS:
                candidatePath = candidateRoot.get(Candidate_.STREET_ADDRESS);
                break;
            case PHONE_NUMBER:
                candidatePath = candidateRoot.get(Candidate_.PHONE_NUMBER);
                break;
            case EMAIL_PRIVATE:
                candidatePath = candidateRoot.get(Candidate_.EMAIL_PRIVATE);
                break;
            case EMAIL_WORK:
                candidatePath = candidateRoot.get(Candidate_.EMAIL_WORK);
                break;
        }
        return candidatePath;
    }
}
