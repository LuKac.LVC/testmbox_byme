package se.searchofskills.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.CandidateFilterList;

import java.util.List;
import java.util.Optional;

@Repository
public interface CandidateFilterListRepository extends JpaRepository<CandidateFilterList, Long> {
    @Query("select c from CandidateFilterList c where c.candidate.id = ?1 and c.filterList.id = ?2")
    Optional<CandidateFilterList> findByCandidateIdAndFilterListId(Long candidateId, Long filterId);

    @Query("select c.candidate.id from CandidateFilterList c where c.filterList.id = ?1")
    List<Long> findAllByFilterListId(Long filterId);
}
