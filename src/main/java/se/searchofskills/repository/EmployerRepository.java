package se.searchofskills.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.Employer;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployerRepository extends JpaRepository<Employer, Long> {
    Optional<Employer> findByName(String name);

    Page<Employer> findAllByNameContainingIgnoreCase(Pageable pageable, String name);

    List<Employer> findAllByIdIn(List<Long> employerIds);
}
