package se.searchofskills.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.Region;

import java.util.List;
import java.util.Optional;

@Repository
public interface RegionRepository extends JpaRepository<Region, Long> {
    @Query("select r from Region r where r.country.id = ?1 and r.name = ?2")
    Optional<Region> findByCountryIdAndName(Long countryId, String name);

    @Query("select r from Region r where upper(r.name) like upper(concat('%', ?1, '%'))")
    Page<Region> findAllByNameContaining(Pageable pageable, String name);

    List<Region> findAllByCountryId(Long countryId);
}
