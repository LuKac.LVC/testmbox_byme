package se.searchofskills.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.Recipient;


@Repository
public interface RecipientRepository extends JpaRepository<Recipient, Long> {
    Page<Recipient> findAllByRecipientListId(Pageable pageable, long id);

    int countAllByRecipientListId(long id);
}
