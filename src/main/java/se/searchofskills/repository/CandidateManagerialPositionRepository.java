package se.searchofskills.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.CandidateManagerialPosition;

import java.util.List;
import java.util.Optional;

@Repository
public interface CandidateManagerialPositionRepository extends JpaRepository<CandidateManagerialPosition, Long> {
    Optional<CandidateManagerialPosition> findByCandidateIdAndManagerialPositionId(Long candidateId, Long managerId);

    @Query("select c.managerialPosition.id from CandidateManagerialPosition c where c.candidate.id = ?1")
    List<Long> findAllByCandidateId(Long candidateId);
}
