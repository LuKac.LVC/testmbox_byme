package se.searchofskills.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.ManagerialPosition;

import java.util.List;

@Repository
public interface ManagerialPositionRepository extends JpaRepository<ManagerialPosition, Long> {
    Page<ManagerialPosition> findAllByNameContainingIgnoreCase(Pageable pageable, String name);

    List<ManagerialPosition> findAllByIdIn(List<Long> managerialPositionIds);
}
