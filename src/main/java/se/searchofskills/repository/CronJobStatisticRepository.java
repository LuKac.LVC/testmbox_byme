package se.searchofskills.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.CronJobStatistic;

import java.util.List;
import java.util.Optional;

@Repository
public interface CronJobStatisticRepository extends JpaRepository<CronJobStatistic, Long> {
    Optional<CronJobStatistic> findByCronJobId(long id);
    List<CronJobStatistic> findAllByCronJobId(long id);
}
