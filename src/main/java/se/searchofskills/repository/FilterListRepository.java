package se.searchofskills.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.FilterList;

import java.util.List;

@Repository
public interface FilterListRepository extends JpaRepository<FilterList, Long> {
    Page<FilterList> findAllByUserIdAndNameContainingIgnoreCase(Pageable pageable, Long userId, String name);

    Page<FilterList> findAllByUserId(Pageable pageable, Long userId);

    List<FilterList> findAllByUserId(Long userId);
}
