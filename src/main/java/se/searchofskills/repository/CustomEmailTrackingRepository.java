package se.searchofskills.repository;

import org.apache.commons.lang3.EnumUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.EmailTracking;
import se.searchofskills.domain.EmailTracking_;
import se.searchofskills.domain.Recipient;
import se.searchofskills.domain.Recipient_;
import se.searchofskills.domain.enumeration.EmailStatus;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomEmailTrackingRepository {

    final private EntityManager entityManager;
    final private CriteriaBuilder criteriaBuilder;


    public CustomEmailTrackingRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    public Page<EmailTracking> findEmailTrackingCriteria(Pageable pageable,
                                                         String keyword,
                                                         boolean archive){
        CriteriaQuery<EmailTracking> criteriaQuery = criteriaBuilder.createQuery(EmailTracking.class);
        Root<EmailTracking> emailTrackingRootRoot = criteriaQuery.from(EmailTracking.class);
        Join<EmailTracking, Recipient> recipient = emailTrackingRootRoot.join(EmailTracking_.RECIPIENT, JoinType.LEFT);
        Predicate predicate = getPredicate(keyword , emailTrackingRootRoot, recipient, archive);

        criteriaQuery.where(predicate, criteriaBuilder.equal(emailTrackingRootRoot.get("archived"), archive), criteriaBuilder.equal(emailTrackingRootRoot.get("deleted"), false));

        TypedQuery<EmailTracking> typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        typedQuery.setMaxResults(pageable.getPageSize());

        Long emailTrackingCount = getCandidateCount(predicate);

        return new PageImpl<>(typedQuery.getResultList(), pageable, emailTrackingCount);
    }

    private Predicate getPredicate(String keyword,
                                   Root<EmailTracking> emailTrackingRoot,
                                   Join<EmailTracking, Recipient> recipient,
                                   boolean archive){
        List<Predicate> predicates = new ArrayList<>();
        Predicate predicate = null;
        predicates.add(criteriaBuilder.like(recipient.get("county"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(recipient.get("title"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(recipient.get("referenceNumber"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(recipient.get("procuringAgency"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(emailTrackingRoot.get("registrar"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(recipient.get("email"), "%" + keyword + "%"));
        if (EnumUtils.isValidEnum(EmailStatus.class, keyword)){
            predicates.add(criteriaBuilder.equal(emailTrackingRoot.get("status"),  EmailStatus.valueOf(keyword)));
        }
        try{
            predicates.add(criteriaBuilder.equal(emailTrackingRoot.get("cronJob"), Integer.parseInt(keyword)));
            predicates.add(criteriaBuilder.equal(emailTrackingRoot.get("recipient"), Integer.parseInt(keyword)));
        }
        finally {
            predicate =  criteriaBuilder.or(predicates.toArray(new Predicate[0]));
            return predicate;
        }
    }

    private long getCandidateCount(Predicate predicate) {
        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(long.class);
        Root<EmailTracking> countRoot = countQuery.from(EmailTracking.class);
        Join<EmailTracking, Recipient> recipient = countRoot.join(EmailTracking_.RECIPIENT, JoinType.LEFT);
        countQuery.select(criteriaBuilder.count(countRoot)).where(predicate);
        return entityManager.createQuery(countQuery).getSingleResult();
    }


}
