package se.searchofskills.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.CronJobEmailTemplate;

import java.util.List;

@Repository
public interface CronJobEmailTemplateRepository extends JpaRepository<CronJobEmailTemplate, Long> {
    List<CronJobEmailTemplate> findAllByCronJob_Id(Long cronjobId);
}
