package se.searchofskills.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import se.searchofskills.domain.Tag;


import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
    Optional<Tag> findByName(String name);

    Page<Tag> findAllByActiveIsTrueAndBlockAndNameContainingIgnoreCase(Pageable pageable, boolean block, String name);

    Page<Tag> findAllByActiveIsTrueAndBlock(Pageable pageable, boolean block);

    Optional<Tag> findByActiveIsTrueAndName(String name);

    @Query("select t.name from Tag t")
    Set<String> findAllName();

    @Query("select t from Tag t where t.id in ?1")
    List<Tag> findAllByIdIn(List<Long> ids);
}
