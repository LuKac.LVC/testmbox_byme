package se.searchofskills.domain.enumeration;

public enum FilterCondition {
    IS,
    IS_NOT,
    CONTAINS,
    NOT_CONTAINS,
    GREATER_THAN,
    LESS_THAN
}
