package se.searchofskills.domain.enumeration;

public enum Frequency {
    ONCE_PER_WEEK, ONCE_PER_TWO_WEEK, ONCE_PER_MONTH
}
