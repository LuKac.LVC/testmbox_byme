package se.searchofskills.domain.enumeration;

public enum CronJobStatus {
    RUNNING, CANCELLED, FINISHED
}
