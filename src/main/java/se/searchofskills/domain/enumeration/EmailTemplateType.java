package se.searchofskills.domain.enumeration;

public enum EmailTemplateType {
    MAIN, REMINDER_1, REMINDER_2
}
