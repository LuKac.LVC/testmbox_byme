package se.searchofskills.domain.enumeration;

public enum Operator {
    OR,
    AND
}
