package se.searchofskills.domain.enumeration;

public enum CandidateColumn {
    ID("id"),
    FIRST_NAME("firstName"),
    LAST_NAME("lastName"),
    BIRTH_YEAR("birthYear"),
    PERSONAL_NUMBER("personalNumber"),
    GENDER("gender"),
    LINKED_LN_LINK("linkedLnLink"),
    FIRST_EMPLOYMENT("firstEmployment"),
    CITY("city"),
    ZIPCODE("zipCode"),
    COUNTY("county"),
    STREET_ADDRESS("streetAddress"),
    PHONE_NUMBER("phoneNumber"),
    SECOND_PHONE_NUMBER("secondPhoneNumber"),
    EMAIL_PRIVATE("emailPrivate"),
    EMAIL_WORK("emailWork"),
    REGION("regionDTO"),
    COUNTRY("countryDTO"),
    EMPLOYERS("employerDTOs"),
    MANAGERIAL_POSITIONS("managerialPositionDTOs"),
    BUSINESS_AREAS("businessAreaDTOs"),
    CV("cvDTO");

    private final String label;

    CandidateColumn(String label) {
        this.label = label;
    }
}
