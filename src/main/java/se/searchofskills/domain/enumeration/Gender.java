package se.searchofskills.domain.enumeration;

public enum Gender {
    MALE, FEMALE, OTHER
}
