package se.searchofskills.domain.enumeration;

public enum Action {
    ADD, REMOVE
}
