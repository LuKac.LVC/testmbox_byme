package se.searchofskills.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "recipient")
public class Recipient extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "recipient_list_id")
    private RecipientList recipientList;

    @Lob
    private String county;

    @Lob
    private String title;

    @Lob
    @Column(name = "procuring_agency")
    private String procuringAgency;

    @Lob
    @Column(name = "reference_number")
    private String referenceNumber;

    private String registrar;

    private String email;

    @Lob
    @Column(name = "procurement_url")
    private String procurementUrl;

    public Recipient() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RecipientList getRecipientList() {
        return recipientList;
    }

    public void setRecipientList(RecipientList recipientList) {
        this.recipientList = recipientList;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProcuringAgency() {
        return procuringAgency;
    }

    public void setProcuringAgency(String procuringAgency) {
        this.procuringAgency = procuringAgency;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProcurementUrl() {
        return procurementUrl;
    }

    public void setProcurementUrl(String procurementUrl) {
        this.procurementUrl = procurementUrl;
    }
}
