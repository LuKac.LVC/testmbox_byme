package se.searchofskills.domain;

import se.searchofskills.domain.enumeration.Gender;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "candidate")
public class Candidate extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "birth_year")
    private String birthYear;

    @Column(name = "personal_number")
    private String personalNumber;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "region_id")
    private Region region;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_id")
    private Country country;

    @Column(name = "linkedln_link")
    private String linkedLnLink;

    @Column(name = "first_employment")
    private int firstEmployment;

    private String city;

    @Column(name = "zip_code")
    private String zipCode;

    private String county;

    @Column(name = "street_address")
    private String streetAddress;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "second_phone_number")
    private String secondPhoneNumber;

    @Column(name = "email_private")
    private String emailPrivate;

    @Column(name = "email_work")
    private String emailWork;

    private boolean deleted;

    @OneToMany(mappedBy = "candidate", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<CandidateEmployer> candidateEmployers;

    @OneToMany(mappedBy = "candidate", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<CandidateBusinessArea> candidateBusinessAreas;

    @OneToMany(mappedBy = "candidate", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<CandidateManagerialPosition> candidateManagerialPositions;

    @OneToMany(mappedBy = "candidate", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<CandidateFilterList> candidateFilterLists;

    @OneToMany(mappedBy = "candidate", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Cv> cvs;

    public Set<CandidateEmployer> getCandidateEmployers() {
        return candidateEmployers;
    }

    public void setCandidateEmployers(Set<CandidateEmployer> candidateEmployers) {
        this.candidateEmployers = candidateEmployers;
    }

    public Set<CandidateBusinessArea> getCandidateBusinessAreas() {
        return candidateBusinessAreas;
    }

    public void setCandidateBusinessAreas(Set<CandidateBusinessArea> candidateBusinessAreas) {
        this.candidateBusinessAreas = candidateBusinessAreas;
    }

    public Set<CandidateManagerialPosition> getCandidateManagerialPositions() {
        return candidateManagerialPositions;
    }

    public void setCandidateManagerialPositions(Set<CandidateManagerialPosition> candidateManagerialPositions) {
        this.candidateManagerialPositions = candidateManagerialPositions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getLinkedLnLink() {
        return linkedLnLink;
    }

    public void setLinkedLnLink(String linkedLnLink) {
        this.linkedLnLink = linkedLnLink;
    }

    public int getFirstEmployment() {
        return firstEmployment;
    }

    public void setFirstEmployment(int firstEmployment) {
        this.firstEmployment = firstEmployment;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSecondPhoneNumber() {
        return secondPhoneNumber;
    }

    public void setSecondPhoneNumber(String secondPhoneNumber) {
        this.secondPhoneNumber = secondPhoneNumber;
    }

    public String getEmailPrivate() {
        return emailPrivate;
    }

    public void setEmailPrivate(String emailPrivate) {
        this.emailPrivate = emailPrivate;
    }

    public String getEmailWork() {
        return emailWork;
    }

    public void setEmailWork(String emailWork) {
        this.emailWork = emailWork;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Set<CandidateFilterList> getCandidateFilterLists() {
        return candidateFilterLists;
    }

    public void setCandidateFilterLists(Set<CandidateFilterList> candidateFilterLists) {
        this.candidateFilterLists = candidateFilterLists;
    }

    public Set<Cv> getCvs() {
        return cvs;
    }

    public void setCvs(Set<Cv> cvs) {
        this.cvs = cvs;
    }
}
