package se.searchofskills.domain;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cronjob_statistic")
public class CronJobStatistic extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "cronjob_id", unique = true)
    private CronJob cronJob;

    @Column(name = "total_email_to_send")
    private int totalEmailToSend;

    @Column(name = "email_sent")
    private int emailSent;

    @Column(name = "reminder_sent_1")
    private int reminderSent1;

    @Column(name = "reminder_sent_2")
    private int reminderSent2;

    @Column(name = "unanswered_email")
    private int unansweredEmail;

    public CronJobStatistic() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CronJob getCronJob() {
        return cronJob;
    }

    public void setCronJob(CronJob cronJob) {
        this.cronJob = cronJob;
    }

    public int getTotalEmailToSend() {
        return totalEmailToSend;
    }

    public void setTotalEmailToSend(int totalEmailToSend) {
        this.totalEmailToSend = totalEmailToSend;
    }

    public int getEmailSent() {
        return emailSent;
    }

    public void setEmailSent(int emailSent) {
        this.emailSent = emailSent;
    }

    public int getReminderSent1() {
        return reminderSent1;
    }

    public void setReminderSent1(int reminderSent1) {
        this.reminderSent1 = reminderSent1;
    }

    public int getReminderSent2() {
        return reminderSent2;
    }

    public void setReminderSent2(int reminderSent2) {
        this.reminderSent2 = reminderSent2;
    }

    public int getUnansweredEmail() {
        return unansweredEmail;
    }

    public void setUnansweredEmail(int unansweredEmail) {
        this.unansweredEmail = unansweredEmail;
    }
}
