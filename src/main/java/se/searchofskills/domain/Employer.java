package se.searchofskills.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "employer")
public class Employer extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "employer", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<CandidateEmployer> candidateEmployers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CandidateEmployer> getCandidateEmployers() {
        return candidateEmployers;
    }

    public void setCandidateEmployers(Set<CandidateEmployer> candidateEmployers) {
        this.candidateEmployers = candidateEmployers;
    }


}
