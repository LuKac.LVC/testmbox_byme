package se.searchofskills.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "business_area")
public class BusinessArea extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "businessArea", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<CandidateBusinessArea> candidateBusinessAreas;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CandidateBusinessArea> getCandidateBusinessAreas() {
        return candidateBusinessAreas;
    }

    public void setCandidateBusinessAreas(Set<CandidateBusinessArea> candidateBusinessAreas) {
        this.candidateBusinessAreas = candidateBusinessAreas;
    }
}
