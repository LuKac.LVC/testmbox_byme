package se.searchofskills.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "candidate_managerial_position")
public class CandidateManagerialPosition extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "candidate_id")
    private Candidate candidate;

    @ManyToOne
    @JoinColumn(name = "managerial_position_id")
    private ManagerialPosition managerialPosition;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public ManagerialPosition getManagerialPosition() {
        return managerialPosition;
    }

    public void setManagerialPosition(ManagerialPosition managerialPosition) {
        this.managerialPosition = managerialPosition;
    }
}
