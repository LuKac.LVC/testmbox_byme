package se.searchofskills.domain;

import se.searchofskills.domain.enumeration.CronJobStatus;
import se.searchofskills.domain.enumeration.Frequency;
import se.searchofskills.domain.enumeration.WeekDay;
import se.searchofskills.service.dto.CronjobDTO;

import javax.persistence.*;
import javax.validation.constraints.Max;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Table(name = "cronjob")
public class CronJob extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "recipient_list_id")
    private RecipientList recipientList;

    @Column(name = "chunk_number")
    private int chunkNumber;

    @Enumerated(EnumType.STRING)
    private WeekDay weekday;

    @Column(name = "send_hour")
    private int sendHour;

    @Enumerated(EnumType.STRING)
    private Frequency frequency;

    @Column(name = "week_of_month")
    @Max(value = 4)
    private int weekOfMonth;

    @Column(name = "end_date")
    private Instant endDate;

    private Instant startDate;

    @Enumerated(EnumType.STRING)
    private CronJobStatus status;

    private boolean deleted;

    private boolean archived;

    @Column(name = "next_send_date")
    private Instant nextSendDate;

    @Column(name = "template_reminder_time_1")
    private int templateReminderTime1;

    @Column(name = "template_reminder_time_2")
    private int templateReminderTime2;

    public CronJob() {
    }

    public CronJob(CronjobDTO cronjobDTO) {
        this.id = cronjobDTO.getId();
        this.chunkNumber = cronjobDTO.getChunkNumber();
        this.sendHour = cronjobDTO.getSendHour();
        this.weekday = cronjobDTO.getWeekday();
        this.frequency = cronjobDTO.getFrequency();
        this.endDate = cronjobDTO.getEndDate();
        this.weekOfMonth = cronjobDTO.getWeekOfMonth();
        this.templateReminderTime1 = cronjobDTO.getTemplateReminderTime1();
        this.templateReminderTime2 = cronjobDTO.getTemplateReminderTime2();
        this.name = cronjobDTO.getName();
        this.startDate = cronjobDTO.getStartDate();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RecipientList getRecipientList() {
        return recipientList;
    }

    public void setRecipientList(RecipientList recipientList) {
        this.recipientList = recipientList;
    }

    public int getChunkNumber() {
        return chunkNumber;
    }

    public void setChunkNumber(int chunkNumber) {
        this.chunkNumber = chunkNumber;
    }

    public WeekDay getWeekday() {
        return weekday;
    }

    public void setWeekday(WeekDay weekday) {
        this.weekday = weekday;
    }

    public int getSendHour() {
        return sendHour;
    }

    public void setSendHour(int sendHour) {
        this.sendHour = sendHour;
    }

    public Frequency getFrequency() {
        return frequency;
    }

    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
    }

    public int getWeekOfMonth() {
        return weekOfMonth;
    }

    public void setWeekOfMonth(int weekOfMonth) {
        this.weekOfMonth = weekOfMonth;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public CronJobStatus getStatus() {
        return status;
    }

    public void setStatus(CronJobStatus status) {
        this.status = status;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Instant getNextSendDate() {
        return nextSendDate;
    }

    public void setNextSendDate(Instant nextSendDate) {
        this.nextSendDate = nextSendDate;
    }

    public int getTemplateReminderTime1() {
        return templateReminderTime1;
    }

    public void setTemplateReminderTime1(int templateReminderTime1) {
        this.templateReminderTime1 = templateReminderTime1;
    }

    public int getTemplateReminderTime2() {
        return templateReminderTime2;
    }

    public void setTemplateReminderTime2(int templateReminderTime2) {
        this.templateReminderTime2 = templateReminderTime2;
    }
}
