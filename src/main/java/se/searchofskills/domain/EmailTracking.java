package se.searchofskills.domain;

import se.searchofskills.domain.enumeration.EmailStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Table(name = "email_tracking")
public class EmailTracking extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cronjob_id")
    private CronJob cronJob;

    @OneToOne
    @JoinColumn(name = "recipient_id", unique = true)
    private Recipient recipient;

    private String registrar;

    @Column(name = "reminder_date_1")
    private Instant reminderDate1;

    @Column(name = "reminder_date_2")
    private Instant reminderDate2;

    @Column(name = "sent_reminder_1")
    private boolean sentReminder1;

    @Column(name = "sent_reminder_2")
    private boolean sentReminder2;

    @Enumerated(EnumType.STRING)
    private EmailStatus status;

    private boolean deleted;

    private boolean archived;

    @Lob
    private String comment;

    public EmailTracking() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CronJob getCronJob() {
        return cronJob;
    }

    public void setCronJob(CronJob cronJob) {
        this.cronJob = cronJob;
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }

    public Instant getReminderDate1() {
        return reminderDate1;
    }

    public void setReminderDate1(Instant reminderDate1) {
        this.reminderDate1 = reminderDate1;
    }

    public Instant getReminderDate2() {
        return reminderDate2;
    }

    public void setReminderDate2(Instant reminderDate2) {
        this.reminderDate2 = reminderDate2;
    }

    public boolean isSentReminder1() {
        return sentReminder1;
    }

    public void setSentReminder1(boolean sentReminder1) {
        this.sentReminder1 = sentReminder1;
    }

    public boolean isSentReminder2() {
        return sentReminder2;
    }

    public void setSentReminder2(boolean sentReminder2) {
        this.sentReminder2 = sentReminder2;
    }

    public EmailStatus getStatus() {
        return status;
    }

    public void setStatus(EmailStatus status) {
        this.status = status;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
