package se.searchofskills.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "managerial_position")
public class ManagerialPosition extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "managerialPosition", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<CandidateManagerialPosition> candidateManagerialPositions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CandidateManagerialPosition> getCandidateManagerialPositions() {
        return candidateManagerialPositions;
    }

    public void setCandidateManagerialPositions(Set<CandidateManagerialPosition> candidateManagerialPositions) {
        this.candidateManagerialPositions = candidateManagerialPositions;
    }
}
