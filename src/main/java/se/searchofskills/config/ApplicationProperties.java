package se.searchofskills.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Properties specific to Search Of Skills.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */

@Configuration
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    private boolean sendAll;
    private String[] emails;
    private Long timeChangeSentToUnanswered;
    private boolean migrate;

    public void setTimeChangeSentToUnanswered(Long timeChangeSentToUnanswered) {
        this.timeChangeSentToUnanswered = timeChangeSentToUnanswered;
    }

    public Long getTimeChangeSentToUnanswered() {
        return timeChangeSentToUnanswered;
    }

    public boolean isSendAll() {
        return sendAll;
    }

    public void setSendAll(boolean sendAll) {
        this.sendAll = sendAll;
    }

    public String[] getEmails() {
        return emails;
    }

    public void setEmails(String[] emails) {
        this.emails = emails;
    }

    public boolean isMigrate() {
        return migrate;
    }

    public void setMigrate(boolean migrate) {
        this.migrate = migrate;
    }
}
