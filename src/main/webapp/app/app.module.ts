import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { SearchOfSkillsSharedModule } from 'app/shared/shared.module';
import { SearchOfSkillsCoreModule } from 'app/core/core.module';
import { SearchOfSkillsAppRoutingModule } from './app-routing.module';
import { SearchOfSkillsHomeModule } from './home/home.module';
import { SearchOfSkillsEntityModule } from './entities/entity.module';
import { NgSelectConfig } from '@ng-select/ng-select';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    BrowserModule,
    SearchOfSkillsSharedModule,
    SearchOfSkillsCoreModule,
    SearchOfSkillsHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    SearchOfSkillsEntityModule,
    SearchOfSkillsAppRoutingModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({ progressBar: true, timeOut: 5000 }), // ToastrModule added
    NgSelectModule,
    FormsModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
  providers: [NgSelectConfig],
})
export class SearchOfSkillsAppModule {}
