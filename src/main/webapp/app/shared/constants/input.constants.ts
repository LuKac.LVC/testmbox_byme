export const DATE_FORMAT = 'YYYY-MM-DD';
export const DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm';

export const CANDIDATE_FIELDS = [
  {
    id: 'FIRST_NAME',
    name: 'FIRST NAME',
  },
  {
    id: 'LAST_NAME',
    name: 'LAST NAME',
  },
  {
    id: 'BIRTH_YEAR',
    name: 'BIRTH YEAR',
  },
  {
    id: 'PERSONAL_NUMBER',
    name: 'PERSONAL NUMBER',
  },
  {
    id: 'GENDER',
    name: 'GENDER',
  },
  {
    id: 'LINKED_LN_LINK',
    name: 'LINKED LN_LINK',
  },
  {
    id: 'FIRST_EMPLOYMENT',
    name: 'FIRST EMPLOYMENT',
  },
  {
    id: 'CITY',
    name: 'CITY',
  },
  {
    id: 'ZIPCODE',
    name: 'ZIPCODE',
  },
  {
    id: 'COUNTY',
    name: 'COUNTY',
  },
  {
    id: 'STREET_ADDRESS',
    name: 'STREET ADDRESS',
  },
  {
    id: 'PHONE_NUMBER',
    name: 'PHONE NUMBER',
  },
  {
    id: 'SECOND_PHONE_NUMBER',
    name: 'SECOND PHONE_NUMBER',
  },
  {
    id: 'EMAIL_PRIVATE',
    name: 'EMAIL PRIVATE',
  },
  {
    id: 'EMAIL_WORK',
    name: 'EMAIL WORK',
  },
];

export const CANDIDATE_CONDITIONS = [
  {
    id: 'IS',
    name: 'IS',
  },
  {
    id: 'IS_NOT',
    name: 'IS NOT',
  },
  {
    id: 'CONTAINS',
    name: 'CONTAINS',
  },
  {
    id: 'NOT_CONTAINS',
    name: 'NOT CONTAINS',
  },
  {
    id: 'GREATER_THAN',
    name: 'GREATER THAN',
  },
  {
    id: 'LESS_THAN',
    name: 'LESS THAN',
  },
];
