export const EMAIL_REGEX = /^[\w]{1,}[\w.+-]{0,}@[\w-]{1,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$/;
export const PHONE_REGEX = /^\+?[0-9]{3}-?[0-9]{5,12}$/;
