import { action, computed, observable } from 'mobx';
import { Injectable } from '@angular/core';

@Injectable()
export class LoadingStore {
  @observable loadingQueueSize = 0;

  constructor() {}

  @computed get isLoading(): any {
    return this.loadingQueueSize > 0;
  }

  @action showLoading(): any {
    this.loadingQueueSize++;
  }

  @action hideLoading(): any {
    if (this.loadingQueueSize > 0) {
      this.loadingQueueSize--;
    }
  }
}
export const loadingStore = new LoadingStore();

export const LOADING_STORE_PROVIDER = [
  {
    provide: LoadingStore,
    useValue: loadingStore,
  },
];
