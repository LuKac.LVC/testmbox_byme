import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'bytesToSize' })
export class BytesToSizePipe implements PipeTransform {
  units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  transform(size: number): string {
    let l = 0,
      n = size;

    while (n >= 1024 && ++l) {
      n = n / 1024;
    }

    return n.toFixed(n < 10 && l > 0 ? 1 : 0) + ' ' + this.units[l];
  }
}
