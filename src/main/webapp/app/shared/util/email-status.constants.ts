import { EmailStatus } from 'app/features/email-tracking-management/models/email-tracking.model';

export const EmailStatusConstants = [
  {
    value: EmailStatus.GOT_ALL,
    content: 'emailTrackingManagement.emailStatus.gotAll',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.gotAll',
  },
  {
    value: EmailStatus.RECEIVED_WITHOUT,
    content: 'emailTrackingManagement.emailStatus.receivedWithout',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.receivedWithout',
  },
  {
    value: EmailStatus.RECEIVED_WITH,
    content: 'emailTrackingManagement.emailStatus.receivedWith',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.receivedWith',
  },
  {
    value: EmailStatus.RECEIVED_COST,
    content: 'emailTrackingManagement.emailStatus.receivedCost',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.receivedCost',
  },
  {
    value: EmailStatus.CONFIDENTIALITY_WITHOUT,
    content: 'emailTrackingManagement.emailStatus.confidentialityWithout',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.confidentialityWithout',
  },
  {
    value: EmailStatus.CONFIDENTIALITY_WITH,
    content: 'emailTrackingManagement.emailStatus.confidentialityWith',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.confidentialityWith',
  },
  {
    value: EmailStatus.APPROVED_BUT_DONT_INCLUDE_ANY_CV,
    content: 'emailTrackingManagement.emailStatus.doNotIncludeCV',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.doNotIncludeCV',
  },
  {
    value: EmailStatus.COMMENT,
    content: 'emailTrackingManagement.emailStatus.comment',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.comment',
  },
  {
    value: EmailStatus.CANCELED,
    content: 'emailTrackingManagement.emailStatus.canceled',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.canceled',
  },
  {
    value: EmailStatus.NOT_READY_YET,
    content: 'emailTrackingManagement.emailStatus.notReadyYet',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.notReadyYet',
  },
  {
    value: EmailStatus.APPEALED,
    content: 'emailTrackingManagement.emailStatus.appealed',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.appealed',
  },
  {
    value: EmailStatus.SENT,
    content: 'emailTrackingManagement.emailStatus.sent',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.sent',
  },
  {
    value: EmailStatus.PAYMENT,
    content: 'emailTrackingManagement.emailStatus.payment',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.payment',
  },
  {
    value: EmailStatus.UNANSWERED,
    content: 'emailTrackingManagement.emailStatus.unanswered',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.unanswered',
  },
  {
    value: EmailStatus.COULD_NOT_BE_SENT,
    content: 'emailTrackingManagement.emailStatus.couldNotBeSent',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.couldNotBeSent',
  },
  {
    value: EmailStatus.BY_MAIL,
    content: 'emailTrackingManagement.emailStatus.byMail',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.byMail',
  },
  {
    value: EmailStatus.INCOMPLETE,
    content: 'emailTrackingManagement.emailStatus.incomplete',
    hoverTitle: 'emailTrackingManagement.emailStatusHoverContent.incomplete',
  },
];
