import { Pipe, PipeTransform } from '@angular/core';
import { WeekDay } from 'app/features/cronjob-management/models/cronjob.model';

@Pipe({ name: 'weekday' })
export class WeekDayPipe implements PipeTransform {
  transform(weekday: WeekDay): string {
    switch (weekday) {
      case 'MON':
        return 'Monday';
      case 'TUE':
        return 'Tuesday';
      case 'WED':
        return 'Wednesday';
      case 'THU':
        return 'Thursday';
      case 'FRI':
        return 'Friday';
      case 'SAT':
        return 'Saturday';
      case 'SUN':
        return 'Sunday';
      default:
        return 'NA';
    }
  }
}
