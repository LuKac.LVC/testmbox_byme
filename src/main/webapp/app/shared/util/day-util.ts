import { WeekDay } from '../../features/cronjob-management/models/cronjob.model';

export const getCurrentDay = (): string => {
  const now = new Date();
  let day = now.getDay();
  if (now.getHours() === 23) {
    day = day + 1;
  }
  switch (day) {
    case 1:
      return 'MON';
    case 2:
      return 'TUE';
    case 3:
      return 'WED';
    case 4:
      return 'THU';
    case 5:
      return 'FRI';
    case 6:
      return 'SAT';
    case 8:
    case 0:
      return 'SUN';
    default:
      return 'MON';
  }
};

export const getCurrentDayValue = (day: string): number => {
  switch (day) {
    case 'MON':
      return 1;
    case 'TUE':
      return 2;
    case 'WED':
      return 3;
    case 'THU':
      return 4;
    case 'FRI':
      return 5;
    case 'SAT':
      return 6;
    case 'SUN':
      return 7;
    default:
      return 1;
  }
};

export const getCurrentDayString = (day: number): WeekDay => {
  switch (day) {
    case 1:
    case 8:
      return WeekDay.MON;
    case 2:
      return WeekDay.TUE;
    case 3:
      return WeekDay.WED;
    case 4:
      return WeekDay.THU;
    case 5:
      return WeekDay.FRI;
    case 6:
      return WeekDay.SAT;
    case 0:
    case 7:
      return WeekDay.SUN;
    default:
      return WeekDay.MON;
  }
};
