import { Pipe, PipeTransform } from '@angular/core';
import { EmailStatus } from 'app/features/email-tracking-management/models/email-tracking.model';
import { EmailStatusConstants } from 'app/shared/util/email-status.constants';

@Pipe({ name: 'emailStatus' })
export class EmailStatusPipe implements PipeTransform {
  transform(emailStatus: EmailStatus): string {
    const status = EmailStatusConstants.filter(elm => elm.value === emailStatus);
    if (status.length > 0) {
      return status[0].content;
    } else return emailStatus.toString();
  }
}
