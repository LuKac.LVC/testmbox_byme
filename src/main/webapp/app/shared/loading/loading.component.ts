import { Component } from '@angular/core';
import { LoadingStore } from '../store/loading.store';

@Component({
  selector: 'jhi-loading',
  templateUrl: './loading.component.html',
})
export class LoadingComponent {
  constructor(
    // eslint-disable-next-line @typescript-eslint/no-parameter-properties
    public readonly loadingStore: LoadingStore
  ) {}
}
