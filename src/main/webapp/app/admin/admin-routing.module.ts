import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
/* jhipster-needle-add-admin-module-import - JHipster will add admin modules imports here */

@NgModule({
  imports: [
    /* jhipster-needle-add-admin-module - JHipster will add admin modules here */
    RouterModule.forChild([
      {
        path: 'user-management',
        loadChildren: () => import('./user-management/user-management.module').then(m => m.UserManagementModule),
        data: {
          pageTitle: 'userManagement.home.title',
        },
      },
      {
        path: 'recipient-management',
        loadChildren: () => import('../features/recipient-management/recipient-management.module').then(m => m.RecipientManagementModule),
        data: {
          pageTitle: 'recipientManagement.home.title',
        },
      },
      {
        path: 'email-template-management',
        loadChildren: () =>
          import('../features/email-template-management/email-template-management.module').then(m => m.EmailTemplateManagementModule),
        data: {
          pageTitle: 'emailTemplateManagement.home.title',
        },
      },
      {
        path: 'cronjob-management',
        loadChildren: () => import('../features/cronjob-management/cronjob-management.module').then(m => m.CronjobManagementModule),
        data: {
          pageTitle: 'cronjobManagement.home.title',
        },
      },
      {
        path: 'archive-cronjob-management',
        loadChildren: () => import('../features/cronjob-management/cronjob-management.module').then(m => m.CronjobManagementModule),
        data: {
          pageTitle: 'cronjobManagement.home.title',
        },
      },
      {
        path: 'email-tracking-management',
        loadChildren: () =>
          import('../features/email-tracking-management/email-tracking-management.module').then(m => m.EmailTrackingManagementModule),
        data: {
          pageTitle: 'emailTrackingManagement.home.title',
        },
      },
      {
        path: 'audits',
        loadChildren: () => import('./audits/audits.module').then(m => m.AuditsModule),
      },
      {
        path: 'configuration',
        loadChildren: () => import('./configuration/configuration.module').then(m => m.ConfigurationModule),
      },
      {
        path: 'docs',
        loadChildren: () => import('./docs/docs.module').then(m => m.DocsModule),
      },
      {
        path: 'health',
        loadChildren: () => import('./health/health.module').then(m => m.HealthModule),
      },
      {
        path: 'logs',
        loadChildren: () => import('./logs/logs.module').then(m => m.LogsModule),
      },
      {
        path: 'tracker',
        loadChildren: () => import('./tracker/tracker.module').then(m => m.TrackerModule),
      },
      {
        path: 'metrics',
        loadChildren: () => import('./metrics/metrics.module').then(m => m.MetricsModule),
      },
      {
        path: 'tag-management',
        loadChildren: () => import('../features/tag-management/tag-management.module').then(m => m.TagManagementModule),
        data: {
          pageTitle: 'tagManagement.home.title',
        },
      },
      {
        path: 'category-management',
        loadChildren: () => import('../features/category-management/category-management.module').then(m => m.CategoryManagementModule),
        data: {
          pageTitle: 'categoryManagement.home.title',
        },
      },
      {
        path: 'employer-management',
        loadChildren: () => import('../features/employer-management/employer-management.module').then(m => m.EmployerManagementModule),
        data: {
          pageTitle: 'employerManagement.home.title',
        },
      },
      {
        path: 'region-management',
        loadChildren: () => import('../features/region-management/region-management.module').then(m => m.RegionManagementModule),
        data: {
          pageTitle: 'regionManagement.home.title',
        },
      },
      {
        path: 'country-management',
        loadChildren: () => import('../features/country-management/country-management.module').then(m => m.CountryManagementModule),
        data: {
          pageTitle: 'countryManagement.home.title',
        },
      },
      {
        path: 'managerial-position',
        loadChildren: () => import('../features/managerial-position/managerial-position.module').then(m => m.ManagerialPositionModule),
        data: {
          pageTitle: 'managerialPosition.home.title',
        },
      },
      {
        path: 'business-areas',
        loadChildren: () => import('../features/business-areas/business-areas.module').then(m => m.BusinessAreaModule),
        data: {
          pageTitle: 'businessAreas.home.title',
        },
      },
      {
        path: 'candidate-management',
        loadChildren: () => import('../features/candidate-management/candidate-management.module').then(m => m.CandidateManagementModule),
        data: {
          pageTitle: 'candidateManagement.home.title',
        },
      },
      {
        path: 'word-management',
        loadChildren: () => import('../features/word-management/word-management.module').then(m => m.WordManagementModule),
        data: {
          pageTitle: 'wordManagement.home.title',
        },
      },
      /* jhipster-needle-add-admin-route - JHipster will add admin routes here */
    ]),
  ],
})
export class AdminRoutingModule {}
