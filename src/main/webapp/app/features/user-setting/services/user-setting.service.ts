import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page } from '../../../shared/models/page.model';
import { createRequestOption, Pagination } from '../../../shared/util/request-util';
import { Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from '../../../app.constants';
import { IFilterListItem } from '../models/user-setting.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';

@Injectable({ providedIn: 'root' })
export default class UserV2Service {
  private resourceUrl = SERVER_API_URL + 'api/v2';

  filterList = new ReplaySubject<IFilterListItem[]>(1);

  constructor(private http: HttpClient) {}

  getFilterList(req: Pagination = { page: 0, size: ITEMS_PER_PAGE, sort: ['id'] }, name?: string): Observable<Page<IFilterListItem>> {
    let options = createRequestOption(req);
    if (name) {
      options = options.set('name', name);
    }
    return this.http
      .get<IFilterListItem[]>(this.resourceUrl + '/users/filters', { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<IFilterListItem> = {
            items: res?.body || [],
            total: Number(res.headers.get('x-total-count')),
          };
          this.filterList.next(page.items);
          return page;
        })
      );
  }

  createFilterList(name: string): Observable<IFilterListItem> {
    return this.http.post<IFilterListItem>(this.resourceUrl + '/users/filters', { name });
  }

  updateCandidateFilterList(id: number, candidateIds: number[], action: 'REMOVE' | 'ADD'): Observable<IFilterListItem> {
    return this.http.put<IFilterListItem>(this.resourceUrl + `/users/filters/${id}`, { candidateIds, action });
  }

  deleteFilterList(id: number): Observable<{}> {
    return this.http.delete(this.resourceUrl + `/api/v2/users/filters/${id}`);
  }

  getCandidateColumnConfig(): Observable<string[]> {
    return this.http.get<string[]>(this.resourceUrl + '/users/candidates-columns-config');
  }

  updateCandidateColumnConfig(fieldConfig: string[]): Observable<string[]> {
    return this.http.put<string[]>(this.resourceUrl + '/users/candidates-columns-config', { fieldConfig });
  }
}
