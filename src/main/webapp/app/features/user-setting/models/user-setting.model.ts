export interface IFilterListItem {
  id: number;
  name: string;
  candidateIds?: number[];
  userId: number;
}
