import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page } from '../../../shared/models/page.model';
import { createRequestOption, Pagination } from '../../../shared/util/request-util';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from '../../../app.constants';
import { ICountry } from '../models/country-management.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';

@Injectable({ providedIn: 'root' })
export default class CountryService {
  private resourceUrl = SERVER_API_URL + 'api/v2';
  constructor(private http: HttpClient) {}

  query(req: Pagination = { page: 0, size: ITEMS_PER_PAGE, sort: ['id'] }, name?: string): Observable<Page<ICountry>> {
    let options = createRequestOption(req);
    if (name) {
      options = options.set('name', name);
    }
    return this.http
      .get<ICountry[]>(this.resourceUrl + '/countries', { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<ICountry> = {
            items: res?.body || [],
            total: Number(res.headers.get('x-total-count')),
          };
          return page;
        })
      );
  }

  create(name: string): Observable<ICountry[]> {
    return this.http.post<ICountry[]>(`${this.resourceUrl}/countries`, { name: name.trim() });
  }

  update(id: number, name: string): Observable<ICountry[]> {
    return this.http.put<ICountry[]>(`${this.resourceUrl}/countries/${id}`, { name: name.trim() });
  }

  delete(id: number): Observable<{}> {
    return this.http.delete(`${this.resourceUrl}/countries/${id}`);
  }
}
