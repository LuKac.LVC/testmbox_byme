import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HandleRequestService } from 'app/core/handle-request/handle-request.service';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { loading } from 'app/shared/rxjs/loading';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { ICountry } from '../models/country-management.model';
import CountryService from '../services/country-management.service';
import CreateCountriesDialogComponent from '../dialog/create-countries-dialog.component';
import { ConfirmDialogComponent } from 'app/shared/dialog/confirm-dialog.component';
import { DialogType } from 'app/shared/dialog/dialog-type.enum';
import EditCountryDialogComponent from '../dialog/edit-country-dialog.component';

@Component({
  selector: 'jhi-country-management',
  templateUrl: './country-management.component.html',
})
export default class CountryManagementComponent implements OnInit {
  countries: ICountry[] = [];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page = 1;
  predicate = 'id';
  ascending = true;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private countryService: CountryService,
    private handleRequestService: HandleRequestService,
    private modalService: NgbModal
  ) {}
  ngOnInit(): void {
    this.handleNavigation();
  }

  private loadCountries(): void {
    this.countryService
      .query({ page: this.page - 1, size: this.itemsPerPage, sort: this.sort() })
      .pipe(loading())
      .subscribe(
        data => {
          this.countries = data.items;
          this.totalItems = data.total;
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }
  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  private handleNavigation(): void {
    combineLatest([this.activatedRoute.data, this.activatedRoute.queryParamMap])
      .pipe(
        map(result => {
          const [data, params] = result;
          const page = params.get('page');
          this.page = page !== null ? +page : 1;
          const sort = (params.get('sort') ?? data['defaultSort'])?.split(',') || [];
          this.predicate = sort[0] || 'id';
          this.ascending = sort[1] === 'asc';
          this.loadCountries();
        })
      )
      .subscribe();
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
      },
    });
  }
  onCreateCountries(name: string): void {
    this.countryService
      .create(name)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadCountries();
          this.handleRequestService.showMessageSuccess('countryManagement.modal.created', '');
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  openCreateModal(): void {
    const modalRef = this.modalService.open(CreateCountriesDialogComponent, { size: 'lg', centered: true });
    modalRef.result.then(
      result => {
        this.onCreateCountries(result.name);
      },
      () => {}
    );
  }

  onEditCountry(id: number, name: string): void {
    this.countryService
      .update(id, name)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadCountries();
          this.handleRequestService.showMessageSuccess('countryManagement.modal.updated', { param: id });
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  openEditModal(id: number, name: string): void {
    const modalRef = this.modalService.open(EditCountryDialogComponent, { size: 'lg', centered: true });
    modalRef.componentInstance.name = name;
    modalRef.result.then(
      result => {
        this.onEditCountry(id, result.name);
      },
      () => {}
    );
  }

  onDeleteCountries(id: number): void {
    this.countryService
      .delete(id)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadCountries();
          this.handleRequestService.showMessageSuccess('countryManagement.modal.deleted', { param: id });
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }
  openDeleteModal(id: number, name: string): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = 'countryManagement.modal.deleteTitle';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'countryManagement.modal.deleteQuestion';
    modalRef.componentInstance.translateValues = { name };
    modalRef.result.then(
      () => {
        this.onDeleteCountries(id);
      },
      () => {}
    );
  }
}
