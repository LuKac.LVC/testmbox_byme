import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-edit-country',
  templateUrl: './edit-country-dialog.component.html',
})
export default class EditCountryDialogComponent {
  @Input() name = '';
  errorRequired = false;
  constructor(public activeModal: NgbActiveModal) {}

  get isDisableSubmit(): boolean {
    return !this.name;
  }

  onSubmit(): void {
    this.activeModal.close({
      name: this.name,
    });
  }

  handleValidate(): void {
    this.errorRequired = !this.name;
  }
}
