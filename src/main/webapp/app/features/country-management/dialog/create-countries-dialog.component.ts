import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-create-countries',
  templateUrl: './create-countries-dialog.component.html',
})
export default class CreateCountriesDialogComponent {
  name = '';
  errorRequired = false;
  errorWhiteSpace = false;
  constructor(public activeModal: NgbActiveModal) {}

  get isDisableSubmit(): boolean {
    return !this.name || this.errorWhiteSpace;
  }

  onSubmit(): void {
    this.activeModal.close({
      name: this.name,
    });
  }

  handleValidate(): void {
    this.errorRequired = !this.name.length;
    this.errorWhiteSpace = !!this.name && !this.name.trim().length;
  }
}
