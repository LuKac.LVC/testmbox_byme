import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SearchOfSkillsSharedModule } from '../../shared/shared.module';
import CountryManagementComponent from './components/country-management.component';
import { countryManagementRoute } from './country-management.route';
import CreateCountriesDialogComponent from './dialog/create-countries-dialog.component';
import EditCountryDialogComponent from './dialog/edit-country-dialog.component';
@NgModule({
  imports: [SearchOfSkillsSharedModule, RouterModule.forChild(countryManagementRoute)],
  declarations: [CountryManagementComponent, CreateCountriesDialogComponent, EditCountryDialogComponent],
})
export class CountryManagementModule {}
