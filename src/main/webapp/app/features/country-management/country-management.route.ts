import { Routes } from '@angular/router';
import CountryManagementComponent from './components/country-management.component';

export const countryManagementRoute: Routes = [
  {
    path: '',
    component: CountryManagementComponent,
  },
];
