import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { EmailTrackingService } from '../services/email-tracking.service';
import { EmailStatus, EmailTracking } from '../models/email-tracking.model';
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons';
import { HandleRequestService } from '../../../core/handle-request/handle-request.service';
import * as moment from 'moment';
import { Moment } from 'moment';
import { EmailStatusConstants } from '../../../shared/util/email-status.constants';

@Component({
  selector: 'jhi-email-tracking-mgmt-update',
  templateUrl: './email-tracking-management-update.component.html',
})
export class EmailTrackingManagementUpdateComponent implements OnInit {
  emailTracking!: EmailTracking;
  emailTrackingId!: number;

  editForm = this.fb.group({
    status: [EmailStatus.SENT, Validators.required],
    reminderDate1: [undefined, Validators.required],
    reminderDate2: [undefined, Validators.required],
    email: ['', [Validators.minLength(5), Validators.maxLength(254), Validators.email]],
  });
  minDate: any;
  faCalendarAlt = faCalendarAlt;
  statusList = EmailStatusConstants;
  constructor(
    private handleRequestService: HandleRequestService,
    private emailTrackingService: EmailTrackingService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    const currentDate = new Date();
    this.minDate = { year: currentDate.getFullYear(), month: currentDate.getMonth() + 1, day: currentDate.getDate() };
    this.emailTrackingId = this.route.params['value']['id'];
    this.emailTrackingService.findById(this.emailTrackingId).subscribe(
      (emailTracking: EmailTracking) => {
        this.emailTracking = emailTracking;
        this.updateForm(this.emailTracking);
      },
      error => {
        this.handleRequestService.showMessageError('error.actionFailed', error.error);
      }
    );
  }

  previousState(): void {
    this.route.queryParams.subscribe(params => {
      const from = params['from'];
      this.router.navigate(['./admin/email-tracking-management'], { queryParams: { page: from } });
    });
  }

  save(): void {
    this.updateEmailTracking(this.emailTracking);
    this.emailTrackingService.edit(this.emailTracking).subscribe(
      () => {
        this.previousState();
        this.handleRequestService.showMessageSuccess('emailTrackingManagement.updated', { param: this.emailTracking.id });
      },
      error => {
        this.handleRequestService.showMessageError('error.actionFailed', error.error);
      }
    );
  }

  private updateForm(emailTracking: EmailTracking): void {
    this.editForm.patchValue({
      id: emailTracking.id,
      reminderDate1: moment(emailTracking.reminderDate1),
      reminderDate2: moment(emailTracking.reminderDate2),
      status: emailTracking.status,
      email: emailTracking.registrar,
    });
    if (emailTracking.sentReminder1) {
      this.editForm.controls['reminderDate1'].disable();
    }
    if (emailTracking.sentReminder2) {
      this.editForm.controls['reminderDate2'].disable();
    }
  }

  private updateEmailTracking(emailTracking: EmailTracking): void {
    emailTracking.status = this.editForm.get('status')?.value;
    const reminderDate1: Moment = this.editForm.get('reminderDate1')?.value;
    emailTracking.reminderDate1 = reminderDate1.format('YYYY-MM-DDTHH:mm:ss') + 'Z';
    const reminderDate2: Moment = this.editForm.get('reminderDate2')?.value;
    emailTracking.reminderDate2 = reminderDate2.format('YYYY-MM-DDTHH:mm:ss') + 'Z';
    emailTracking.registrar = this.editForm.get('email')!.value;
  }
}
