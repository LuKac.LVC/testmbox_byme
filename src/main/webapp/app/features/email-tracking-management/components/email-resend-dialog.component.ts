import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EmailTracking } from '../models/email-tracking.model';
import { FormBuilder, Validators } from '@angular/forms';
import { EmailTrackingService } from '../services/email-tracking.service';
import { HandleRequestService } from '../../../core/handle-request/handle-request.service';
import { ResendEmailTracking } from '../models/resend-email-tracking.model';

@Component({
  selector: 'jhi-email-resend-dialog-mgmt',
  templateUrl: './email-resend-dialog.component.html',
})
export class EmailResendDialogComponent implements OnInit {
  emailTracking!: EmailTracking;
  emailTrackingId!: number;

  editForm = this.fb.group({
    id: [],
    registrar: ['', [Validators.maxLength(255), Validators.pattern(/^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)]],
    purchaser: [
      '',
      [Validators.required, Validators.maxLength(255), Validators.pattern(/^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)],
    ],
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private modalService: NgbActiveModal,
    private emailTrackingService: EmailTrackingService,
    private handleRequestService: HandleRequestService
  ) {}

  ngOnInit(): void {
    this.emailTrackingService.findById(this.emailTrackingId).subscribe(
      (emailTracking: EmailTracking) => {
        this.emailTracking = emailTracking;
        this.updateForm(emailTracking);
      },
      error => {
        this.handleRequestService.showMessageError('error.actionFailed', error.error);
      }
    );
  }

  back(): void {
    window.history.back();
  }

  cancel(): void {
    this.modalService.dismiss();
  }

  save(): void {
    const resendEmailTracking: ResendEmailTracking = {
      id: this.emailTracking.id,
      email: this.editForm.get('purchaser')?.value,
      registrar: this.editForm.get('registrar')?.value,
    };
    this.emailTrackingService.resend(resendEmailTracking).subscribe(
      () => {
        this.modalService.close();
        this.handleRequestService.showMessageSuccess('emailTrackingManagement.resent', { param: resendEmailTracking.id });
      },
      error => {
        this.handleRequestService.showMessageError('error.actionFailed', error.error);
      }
    );
  }

  private updateForm(emailTracking: EmailTracking): void {
    this.editForm.patchValue({
      registrar: emailTracking.recipient!.registrar,
      purchaser: emailTracking.recipient!.email,
    });
  }
}
