import { Routes } from '@angular/router';

import { EmailTrackingManagementComponent } from './components/email-tracking-management.component';
import { EmailTrackingManagementUpdateComponent } from './components/email-tracking-management-update.component';
import { ArchiveEmailTrackingManagementComponent } from './components/archive-email-tracking-management.component';

export const emailTrackingManagementRoute: Routes = [
  {
    path: '',
    component: EmailTrackingManagementComponent,
    data: {
      defaultSort: 'id,asc',
    },
  },
  {
    path: ':id/edit',
    component: EmailTrackingManagementUpdateComponent,
  },
  {
    path: 'archive',
    component: ArchiveEmailTrackingManagementComponent,
    data: {
      defaultSort: 'id,asc',
    },
  },
];
