import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SearchOfSkillsSharedModule } from '../../shared/shared.module';
import { EmailTrackingManagementComponent } from './components/email-tracking-management.component';
import { emailTrackingManagementRoute } from './email-tracking-management.route';
import { EmailTrackingManagementUpdateComponent } from './components/email-tracking-management-update.component';
import { EmailResendDialogComponent } from './components/email-resend-dialog.component';
import { CommentDialogComponent } from './components/comment-dialog.component';
import { UrlDialogComponent } from './components/url-dialog-component';
import { ArchiveEmailTrackingManagementComponent } from './components/archive-email-tracking-management.component';

@NgModule({
  imports: [SearchOfSkillsSharedModule, RouterModule.forChild(emailTrackingManagementRoute)],
  declarations: [
    EmailTrackingManagementComponent,
    ArchiveEmailTrackingManagementComponent,
    EmailTrackingManagementUpdateComponent,
    EmailResendDialogComponent,
    CommentDialogComponent,
    UrlDialogComponent,
  ],
})
export class EmailTrackingManagementModule {}
