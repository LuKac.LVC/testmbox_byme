import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-create-business-areas',
  templateUrl: './create-business-areas-dialog.component.html',
})
export default class CreateBusinessAreasDialogComponent {
  names: string[] = [];
  errorRequired = false;
  constructor(public activeModal: NgbActiveModal) {}

  get isDisableSubmit(): boolean {
    return !this.names.length;
  }

  onSubmit(): void {
    this.activeModal.close({
      names: this.names,
    });
  }

  addTagFn(name: string): string {
    return name;
  }

  handleValidate(): void {
    this.errorRequired = !this.names.length;
  }
}
