import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-edit-business-areas',
  templateUrl: './edit-business-areas-dialog.component.html',
})
export default class EditBusinessAreasDialogComponent {
  @Input() name = '';
  errorRequired = false;
  constructor(public activeModal: NgbActiveModal) {}

  get isDisableSubmit(): boolean {
    return !this.name;
  }

  onSubmit(): void {
    this.activeModal.close({
      name: this.name,
    });
  }

  handleValidate(): void {
    this.errorRequired = !this.name;
  }
}
