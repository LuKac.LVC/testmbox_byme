import { Routes } from '@angular/router';
import BusinessAreasComponent from './components/business-areas.component';

export const businessAreaRoute: Routes = [
  {
    path: '',
    component: BusinessAreasComponent,
  },
];
