import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page } from '../../../shared/models/page.model';
import { createRequestOption, Pagination } from '../../../shared/util/request-util';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from '../../../app.constants';
import { IBusinessArea } from '../models/business-areas.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';

@Injectable({ providedIn: 'root' })
export default class BusinessAreaService {
  private resourceUrl = SERVER_API_URL + 'api/v2';
  constructor(private http: HttpClient) {}

  query(req: Pagination = { page: 0, size: ITEMS_PER_PAGE, sort: ['id'] }, name?: string): Observable<Page<IBusinessArea>> {
    let options = createRequestOption(req);
    if (name) {
      options = options.set('name', name);
    }
    return this.http
      .get<IBusinessArea[]>(this.resourceUrl + '/business-areas', { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<IBusinessArea> = {
            items: res?.body || [],
            total: Number(res.headers.get('x-total-count')),
          };
          return page;
        })
      );
  }

  create(names: string[]): Observable<IBusinessArea[]> {
    return this.http.post<IBusinessArea[]>(`${this.resourceUrl}/business-areas`, { names });
  }

  update(id: number, name: string): Observable<IBusinessArea[]> {
    return this.http.put<IBusinessArea[]>(`${this.resourceUrl}/business-areas/${id}`, { name });
  }

  delete(id: number): Observable<{}> {
    return this.http.delete(`${this.resourceUrl}/business-areas/${id}`);
  }
}
