import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HandleRequestService } from 'app/core/handle-request/handle-request.service';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ConfirmDialogComponent } from 'app/shared/dialog/confirm-dialog.component';
import { DialogType } from 'app/shared/dialog/dialog-type.enum';
import { loading } from 'app/shared/rxjs/loading';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import CreateBusinessAreasDialogComponent from '../dialog/create-business-areas-dialog.component';
import EditBusinessAreasDialogComponent from '../dialog/edit-business-areas-dialog.component';
import { IBusinessArea } from '../models/business-areas.model';
import BusinessAreaService from '../services/business-areas.service';

@Component({
  selector: 'jhi-business-areas',
  templateUrl: './business-areas.component.html',
})
export default class BusinessAreasComponent implements OnInit {
  areas: IBusinessArea[] = [];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page = 1;
  predicate = 'id';
  ascending = true;
  constructor(
    private businessAreaService: BusinessAreaService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private handleRequestService: HandleRequestService,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.handleNavigation();
  }

  private loadBusinessAreas(): void {
    this.businessAreaService
      .query({ page: this.page - 1, size: this.itemsPerPage, sort: this.sort() })
      .pipe(loading())
      .subscribe(
        data => {
          this.areas = data.items;
          this.totalItems = data.total;
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  private handleNavigation(): void {
    combineLatest([this.activatedRoute.data, this.activatedRoute.queryParamMap])
      .pipe(
        map(result => {
          const [data, params] = result;
          const page = params.get('page');
          this.page = page !== null ? +page : 1;
          const sort = (params.get('sort') ?? data['defaultSort'])?.split(',') || [];
          this.predicate = sort[0] || 'id';
          this.ascending = sort[1] === 'asc';
          this.loadBusinessAreas();
        })
      )
      .subscribe();
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
      },
    });
  }

  onCreateBusinessAreas(names: string[]): void {
    this.businessAreaService
      .create(names)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadBusinessAreas();
          this.handleRequestService.showMessageSuccess('businessAreas.modal.created', '');
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  openCreateModal(): void {
    const modalRef = this.modalService.open(CreateBusinessAreasDialogComponent, { size: 'lg', centered: true });
    modalRef.result.then(
      result => {
        this.onCreateBusinessAreas(result.names);
      },
      () => {}
    );
  }

  onEditBusinessAreas(id: number, name: string): void {
    this.businessAreaService
      .update(id, name)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadBusinessAreas();
          this.handleRequestService.showMessageSuccess('businessAreas.modal.updated', { param: id });
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  openEditModal(id: number, name: string): void {
    const modalRef = this.modalService.open(EditBusinessAreasDialogComponent, { size: 'lg', centered: true });
    modalRef.componentInstance.name = name;
    modalRef.result.then(
      result => {
        this.onEditBusinessAreas(id, result.name);
      },
      () => {}
    );
  }

  onDeleteBusinessAreas(id: number): void {
    this.businessAreaService
      .delete(id)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadBusinessAreas();
          this.handleRequestService.showMessageSuccess('businessAreas.modal.deleted', { param: id });
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  openDeleteModal(id: number, name: string): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = 'businessAreas.modal.deleteTitle';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'businessAreas.modal.deleteQuestion';
    modalRef.componentInstance.translateValues = { name };
    modalRef.result.then(
      () => {
        this.onDeleteBusinessAreas(id);
      },
      () => {}
    );
  }
}
