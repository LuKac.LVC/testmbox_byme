import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SearchOfSkillsSharedModule } from '../../shared/shared.module';
import { businessAreaRoute } from './business-areas.route';
import BusinessAreasComponent from './components/business-areas.component';
import EditBusinessAreasDialogComponent from './dialog/edit-business-areas-dialog.component';
import CreateBusinessAreasDialogComponent from './dialog/create-business-areas-dialog.component';
@NgModule({
  imports: [SearchOfSkillsSharedModule, RouterModule.forChild(businessAreaRoute)],
  declarations: [BusinessAreasComponent, EditBusinessAreasDialogComponent, CreateBusinessAreasDialogComponent],
})
export class BusinessAreaModule {}
