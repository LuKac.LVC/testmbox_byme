import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, ParamMap, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HandleRequestService } from 'app/core/handle-request/handle-request.service';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ConfirmDialogComponent } from 'app/shared/dialog/confirm-dialog.component';
import { DialogType } from 'app/shared/dialog/dialog-type.enum';
import { loading } from 'app/shared/rxjs/loading';
import { combineLatest } from 'rxjs';
import CreateEmployersDialogComponent from '../dialog/create-employers-dialog.component';
import EditEmployerDialogComponent from '../dialog/edit-employer-dialog.component';
import { IEmployer } from '../models/employer-management.model';
import EmployerService from '../services/employer.service';
@Component({
  selector: 'jhi-employer-management',
  templateUrl: './employer-management.component.html',
})
export default class EmployerManagementComponent implements OnInit {
  employers: IEmployer[] = [];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page = 1;
  predicate = 'id';
  ascending = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private employerService: EmployerService,
    private handleRequestService: HandleRequestService,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.handleNavigation();
  }

  private loadEmployer(): void {
    this.employerService
      .query({ page: this.page - 1, size: this.itemsPerPage, sort: this.sort() })
      .pipe(loading())
      .subscribe(
        data => {
          this.employers = data.items;
          this.totalItems = data.total;
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  private handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      this.page = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort'])?.split(',') || [];
      this.predicate = sort[0] || 'id';
      this.ascending = sort[1] === 'asc';
      this.loadEmployer();
    }).subscribe();
  }

  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
      },
    });
  }

  onCreateEmployers(names: string[]): void {
    this.employerService
      .create(names)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadEmployer();
          this.handleRequestService.showMessageSuccess('employerManagement.modal.created', '');
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  openCreateModal(): void {
    const modalRef = this.modalService.open(CreateEmployersDialogComponent, { size: 'lg', centered: true });
    modalRef.result.then(
      result => {
        this.onCreateEmployers(result.names);
      },
      () => {}
    );
  }

  onEditEmployers(id: number, name: string): void {
    this.employerService
      .update(id, name)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadEmployer();
          this.handleRequestService.showMessageSuccess('employerManagement.modal.updated', { param: id });
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  openUpdateModal(id: number, name: string): void {
    const modalRef = this.modalService.open(EditEmployerDialogComponent, { size: 'lg', centered: true });
    modalRef.componentInstance.name = name;
    modalRef.result.then(
      result => {
        this.onEditEmployers(id, result.name);
      },
      () => {}
    );
  }

  onDeleteEmployer(id: number): void {
    this.employerService
      .delete(id)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadEmployer();
          this.handleRequestService.showMessageSuccess('employerManagement.modal.deleted', { param: id });
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  openDeleteModal(id: number, name: string): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = 'employerManagement.modal.deleteTitle';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'employerManagement.modal.deleteQuestion';
    modalRef.componentInstance.translateValues = { name };
    modalRef.result.then(
      () => {
        this.onDeleteEmployer(id);
      },
      () => {}
    );
  }
}
