import { Routes } from '@angular/router';
import EmployerManagementComponent from './components/employer-management.component';

export const employerManagementRoute: Routes = [
  {
    path: '',
    component: EmployerManagementComponent,
  },
];
