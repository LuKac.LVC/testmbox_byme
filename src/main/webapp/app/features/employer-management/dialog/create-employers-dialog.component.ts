import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-create-employers',
  templateUrl: './create-employers-dialog.component.html',
})
export default class CreateEmployersDialogComponent {
  employers: string[] = [];
  errorRequired = false;
  constructor(public activeModal: NgbActiveModal) {}

  get isDisableSubmit(): boolean {
    return !this.employers.length;
  }

  onSubmit(): void {
    this.activeModal.close({
      names: this.employers,
    });
  }

  addTagFn(name: string): string {
    return name;
  }

  handleValidate(): void {
    this.errorRequired = !this.employers.length;
  }
}
