import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SearchOfSkillsSharedModule } from '../../shared/shared.module';
import EmployerManagementComponent from './components/employer-management.component';
import { employerManagementRoute } from './employer-management.route';
import CreateEmployersDialogComponent from './dialog/create-employers-dialog.component';
import EditEmployerDialogComponent from './dialog/edit-employer-dialog.component';

@NgModule({
  imports: [SearchOfSkillsSharedModule, RouterModule.forChild(employerManagementRoute)],
  declarations: [EmployerManagementComponent, CreateEmployersDialogComponent, EditEmployerDialogComponent],
  exports: [],
})
export class EmployerManagementModule {}
