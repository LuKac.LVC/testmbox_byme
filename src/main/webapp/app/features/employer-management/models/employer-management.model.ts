export interface IEmployer {
  id: number;
  name: string;
  createdDate?: Date;
}
