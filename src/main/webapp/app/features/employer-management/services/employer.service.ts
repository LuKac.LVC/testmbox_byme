import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { Page } from 'app/shared/models/page.model';
import { createRequestOption, Pagination } from 'app/shared/util/request-util';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from '../../../app.constants';
import { IEmployer } from '../models/employer-management.model';

@Injectable({ providedIn: 'root' })
export default class EmployerService {
  private resourceUrl = SERVER_API_URL + 'api/v2';
  constructor(private http: HttpClient) {}

  query(req: Pagination = { page: 0, size: ITEMS_PER_PAGE, sort: ['id'] }, name?: string): Observable<Page<IEmployer>> {
    let options = createRequestOption(req);
    if (name !== undefined) {
      options = options.set('name', name);
    }
    return this.http
      .get<IEmployer[]>(this.resourceUrl + '/employers', { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<IEmployer> = {
            items: res?.body || [],
            total: Number(res.headers.get('x-total-count')),
          };
          return page;
        })
      );
  }

  create(names: string[]): Observable<IEmployer[]> {
    return this.http.post<IEmployer[]>(`${this.resourceUrl}/employers`, { names });
  }

  update(id: number, name: string): Observable<IEmployer[]> {
    return this.http.put<IEmployer[]>(`${this.resourceUrl}/employers/${id}`, { name });
  }

  delete(id: number): Observable<{}> {
    return this.http.delete(`${this.resourceUrl}/employers/${id}`);
  }
}
