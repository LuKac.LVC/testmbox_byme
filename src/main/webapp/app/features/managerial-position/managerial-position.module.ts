import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { managerialPositionRoute } from './managerial-position.route';
import { SearchOfSkillsSharedModule } from '../../shared/shared.module';
import RegionManagementComponent from './components/managerial-position.component';
import CreateManagerialPositionsDialogComponent from './dialog/create-managerial-positions-dialog.component';
import EditManagerialPositionDialogComponent from './dialog/edit-managerial-position-dialog.component';
@NgModule({
  imports: [SearchOfSkillsSharedModule, RouterModule.forChild(managerialPositionRoute)],
  declarations: [RegionManagementComponent, CreateManagerialPositionsDialogComponent, EditManagerialPositionDialogComponent],
})
export class ManagerialPositionModule {}
