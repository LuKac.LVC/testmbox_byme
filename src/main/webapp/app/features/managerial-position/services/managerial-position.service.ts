import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page } from '../../../shared/models/page.model';
import { createRequestOption, Pagination } from '../../../shared/util/request-util';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from '../../../app.constants';
import { IManagerialPosition } from '../models/managerial-position.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';

@Injectable({ providedIn: 'root' })
export default class ManagerialPositionService {
  private resourceUrl = SERVER_API_URL + 'api/v2';
  constructor(private http: HttpClient) {}

  query(req: Pagination = { page: 0, size: ITEMS_PER_PAGE, sort: ['id'] }, name?: string): Observable<Page<IManagerialPosition>> {
    let options = createRequestOption(req);
    if (name) {
      options = options.set('name', name);
    }
    return this.http
      .get<IManagerialPosition[]>(this.resourceUrl + '/managerial-positions', { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<IManagerialPosition> = {
            items: res?.body || [],
            total: Number(res.headers.get('x-total-count')),
          };
          return page;
        })
      );
  }

  create(names: string[]): Observable<IManagerialPosition[]> {
    return this.http.post<IManagerialPosition[]>(`${this.resourceUrl}/managerial-positions`, { names });
  }

  update(id: number, name: string): Observable<IManagerialPosition[]> {
    return this.http.put<IManagerialPosition[]>(`${this.resourceUrl}/managerial-positions/${id}`, { name: name.trim() });
  }

  delete(id: number): Observable<{}> {
    return this.http.delete(`${this.resourceUrl}/managerial-positions/${id}`);
  }
}
