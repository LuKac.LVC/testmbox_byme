import { Routes } from '@angular/router';
import RegionManagementComponent from './components/managerial-position.component';

export const managerialPositionRoute: Routes = [
  {
    path: '',
    component: RegionManagementComponent,
  },
];
