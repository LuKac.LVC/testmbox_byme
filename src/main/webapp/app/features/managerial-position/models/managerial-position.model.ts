export interface IManagerialPosition {
  id: number;
  name: string;
  createdDate?: Date;
}
