import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HandleRequestService } from 'app/core/handle-request/handle-request.service';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ConfirmDialogComponent } from 'app/shared/dialog/confirm-dialog.component';
import { DialogType } from 'app/shared/dialog/dialog-type.enum';
import { loading } from 'app/shared/rxjs/loading';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import CreateManagerialPositionsDialogComponent from '../dialog/create-managerial-positions-dialog.component';
import EditManagerialPositionDialogComponent from '../dialog/edit-managerial-position-dialog.component';
import { IManagerialPosition } from '../models/managerial-position.model';
import ManagerialPositionService from '../services/managerial-position.service';

@Component({
  selector: 'jhi-managerial-position',
  templateUrl: './managerial-position.component.html',
})
export default class ManagerialPositionComponent implements OnInit {
  managerialPositions: IManagerialPosition[] = [];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page = 1;
  predicate = 'id';
  ascending = true;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private managerialPositionService: ManagerialPositionService,
    private handleRequestService: HandleRequestService,
    private modalService: NgbModal
  ) {}
  ngOnInit(): void {
    this.handleNavigation();
  }

  private loadRegions(): void {
    this.managerialPositionService
      .query({ page: this.page - 1, size: this.itemsPerPage, sort: this.sort() })
      .pipe(loading())
      .subscribe(
        data => {
          this.managerialPositions = data.items;
          this.totalItems = data.total;
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }
  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  private handleNavigation(): void {
    combineLatest([this.activatedRoute.data, this.activatedRoute.queryParamMap])
      .pipe(
        map(result => {
          const [data, params] = result;
          const page = params.get('page');
          this.page = page !== null ? +page : 1;
          const sort = (params.get('sort') ?? data['defaultSort'])?.split(',') || [];
          this.predicate = sort[0] || 'id';
          this.ascending = sort[1] === 'asc';
          this.loadRegions();
        })
      )
      .subscribe();
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
      },
    });
  }

  onCreateManagerialPositions(names: string[]): void {
    this.managerialPositionService
      .create(names)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadRegions();
          this.handleRequestService.showMessageSuccess('managerialPosition.modal.created', '');
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  openCreateModal(): void {
    const modalRef = this.modalService.open(CreateManagerialPositionsDialogComponent, { size: 'lg', centered: true });
    modalRef.result.then(
      result => {
        this.onCreateManagerialPositions(result.names);
      },
      () => {}
    );
  }

  onEditManagerialPositions(id: number, name: string): void {
    this.managerialPositionService
      .update(id, name)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadRegions();
          this.handleRequestService.showMessageSuccess('managerialPosition.modal.updated', { param: id });
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  openEditModal(id: number, name: string): void {
    const modalRef = this.modalService.open(EditManagerialPositionDialogComponent, { size: 'lg', centered: true });
    modalRef.componentInstance.name = name;
    modalRef.result.then(
      result => {
        this.onEditManagerialPositions(id, result.name);
      },
      () => {}
    );
  }

  onDeleteManagerialPositions(id: number): void {
    this.managerialPositionService
      .delete(id)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadRegions();
          this.handleRequestService.showMessageSuccess('managerialPosition.modal.deleted', { param: id });
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }
  openDeleteModal(id: number, name: string): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = 'managerialPosition.modal.deleteTitle';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'managerialPosition.modal.deleteQuestion';
    modalRef.componentInstance.translateValues = { name };
    modalRef.result.then(
      () => {
        this.onDeleteManagerialPositions(id);
      },
      () => {}
    );
  }
}
