import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HandleRequestService } from 'app/core/handle-request/handle-request.service';
import { ICountry } from 'app/features/country-management/models/country-management.model';
import CountryService from 'app/features/country-management/services/country-management.service';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ConfirmDialogComponent } from 'app/shared/dialog/confirm-dialog.component';
import { DialogType } from 'app/shared/dialog/dialog-type.enum';
import { loading } from 'app/shared/rxjs/loading';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import ModifiedRegionDialogComponent from '../dialog/modified-region-dialog.component';
import { IRegion } from '../models/region-management.model';
import RegionService from '../services/region.service';

@Component({
  selector: 'jhi-region-management',
  templateUrl: './region-management.component.html',
})
export default class RegionManagementComponent implements OnInit {
  regions: IRegion[] = [];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page = 1;
  predicate = 'id';
  ascending = true;

  countries: ICountry[] = [];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private regionService: RegionService,
    private handleRequestService: HandleRequestService,
    private modalService: NgbModal,
    private countryService: CountryService
  ) {}

  ngOnInit(): void {
    this.handleNavigation();
    this.loadCountries();
  }

  private loadRegions(): void {
    this.regionService
      .query({ page: this.page - 1, size: this.itemsPerPage, sort: this.sort() })
      .pipe(loading())
      .subscribe(
        data => {
          this.regions = data.items;
          this.totalItems = data.total;
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  private loadCountries(): void {
    this.countryService.query({ page: 0, size: this.itemsPerPage, sort: ['id'] }).subscribe(
      data => {
        this.countries = data.items;
      },
      error => {
        this.handleRequestService.showMessageError('error.actionFailed', error.error);
      }
    );
  }

  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  private handleNavigation(): void {
    combineLatest([this.activatedRoute.data, this.activatedRoute.queryParamMap])
      .pipe(
        map(result => {
          const [data, params] = result;
          const page = params.get('page');
          this.page = page !== null ? +page : 1;
          const sort = (params.get('sort') ?? data['defaultSort'])?.split(',') || [];
          this.predicate = sort[0] || 'id';
          this.ascending = sort[1] === 'asc';
          this.loadRegions();
        })
      )
      .subscribe();
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
      },
    });
  }

  openModifiedModal(region?: IRegion): void {
    const modalRef = this.modalService.open(ModifiedRegionDialogComponent, { size: 'lg', centered: true });
    modalRef.componentInstance.countries = this.countries;
    if (region?.id !== undefined) {
      modalRef.componentInstance.name = region.name;
      modalRef.componentInstance.countryId = region.country.id;
    }

    modalRef.result.then(
      result => {
        const updateApi =
          region?.id !== undefined
            ? this.regionService.update(region.id, result.name, result.countryId)
            : this.regionService.create(result.name, result.countryId);
        updateApi.pipe(loading()).subscribe(
          () => {
            this.loadRegions();
            this.handleRequestService.showMessageSuccess(
              region?.id ? 'regionManagement.modal.updated' : 'regionManagement.modal.created',
              {}
            );
          },
          error => {
            this.handleRequestService.showMessageError('error.actionFailed', error.error);
          }
        );
      },
      () => {}
    );
  }

  onDeleteRegion(id: number): void {
    this.regionService
      .delete(id)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadRegions();
          this.handleRequestService.showMessageSuccess('regionManagement.modal.deleted', { param: id });
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  openDeleteModal(id: number, name: string): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = 'regionManagement.modal.deleteTitle';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'regionManagement.modal.deleteQuestion';
    modalRef.componentInstance.translateValues = { name };
    modalRef.result.then(
      () => {
        this.onDeleteRegion(id);
      },
      () => {}
    );
  }
}
