import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page } from '../../../shared/models/page.model';
import { createRequestOption, Pagination } from '../../../shared/util/request-util';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from '../../../app.constants';
import { IRegion } from '../models/region-management.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';

@Injectable({ providedIn: 'root' })
export default class RegionService {
  private resourceUrl = SERVER_API_URL + 'api/v2';
  constructor(private http: HttpClient) {}

  query(req: Pagination = { page: 0, size: ITEMS_PER_PAGE, sort: ['id'] }, name?: string): Observable<Page<IRegion>> {
    let options = createRequestOption(req);
    if (name) {
      options = options.set('name', name);
    }
    return this.http
      .get<IRegion[]>(this.resourceUrl + '/regions', { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<IRegion> = {
            items: res?.body || [],
            total: Number(res.headers.get('x-total-count')),
          };
          return page;
        })
      );
  }

  create(name: string, countryId: number): Observable<IRegion[]> {
    return this.http.post<IRegion[]>(`${this.resourceUrl}/regions`, { name: name.trim(), countryId });
  }

  update(id: number, name: string, countryId: number): Observable<IRegion[]> {
    return this.http.put<IRegion[]>(`${this.resourceUrl}/regions/${id}`, { name: name.trim(), countryId });
  }

  existed(name: string): Observable<boolean> {
    return this.http.get<boolean>(`${this.resourceUrl}/regions/${name}/exist`);
  }

  delete(id: number): Observable<{}> {
    return this.http.delete(`${this.resourceUrl}/regions/${id}`);
  }

  getByCountry(countryId: number): Observable<IRegion[]> {
    return this.http.get<IRegion[]>(`${this.resourceUrl}/regions/${countryId}`);
  }
}
