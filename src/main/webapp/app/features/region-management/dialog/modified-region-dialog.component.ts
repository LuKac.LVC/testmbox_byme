import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ICountry } from 'app/features/country-management/models/country-management.model';
import CountryService from 'app/features/country-management/services/country-management.service';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { IRegionInput } from '../models/region-management.model';

@Component({
  selector: 'jhi-modified-region-dialog',
  templateUrl: './modified-region-dialog.component.html',
})
export default class ModifiedRegionDialogComponent implements OnInit {
  @Input() name = '';
  @Input() countryId?: number;
  @Input() countries: ICountry[] = [];

  editForm = this.fb.group({
    name: ['', [Validators.required, Validators.maxLength(255)]],
    countryId: [null, [Validators.required]],
  });

  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private countryService: CountryService) {}

  ngOnInit(): void {
    if (this.name && this.countryId) {
      this.updateForm({ name: this.name, countryId: this.countryId });
    }
  }

  private updateForm(formData: IRegionInput): void {
    this.editForm.patchValue({
      name: formData.name,
      countryId: formData.countryId,
    });
  }

  get isDisableSubmit(): boolean {
    return !this.name || !this.countryId;
  }

  onSearchCountries(name: string): void {
    this.countryService
      .query(
        {
          page: 0,
          size: ITEMS_PER_PAGE,
          sort: ['id,asc'],
        },
        name
      )
      .subscribe(data => {
        this.countries = data.items;
      });
  }

  onSubmit(): void {
    this.activeModal.close({
      name: this.editForm.get('name')?.value,
      countryId: this.editForm.get('countryId')?.value,
    });
  }

  addTagFn(name: string): string {
    return name;
  }

  get isNameInvalid(): boolean {
    return !!(this.editForm.get('name')?.invalid && (this.editForm.get('name')?.dirty || this.editForm.get('name')?.touched));
  }

  get isCountryInvalid(): boolean {
    return !!(
      this.editForm.get('countryId')?.invalid &&
      (this.editForm.get('countryId')?.dirty || this.editForm.get('countryId')?.touched)
    );
  }

  get isFormInvalid(): boolean {
    return this.editForm.status === 'INVALID';
  }
}
