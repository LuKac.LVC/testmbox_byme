import { ICountry } from '../../country-management/models/country-management.model';

export interface IRegion {
  id: number;
  name: string;
  country: ICountry;
}

export interface IRegionInput {
  name: string;
  countryId: number;
}
