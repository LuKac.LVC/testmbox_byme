import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { regionManagementRoute } from './region-management.route';
import { SearchOfSkillsSharedModule } from '../../shared/shared.module';
import RegionManagementComponent from './components/region-management.component';
import ModifiedRegionDialogComponent from './dialog/modified-region-dialog.component';

@NgModule({
  imports: [SearchOfSkillsSharedModule, RouterModule.forChild(regionManagementRoute)],
  declarations: [RegionManagementComponent, ModifiedRegionDialogComponent],
})
export class RegionManagementModule {}
