import { Routes } from '@angular/router';
import RegionManagementComponent from './components/region-management.component';

export const regionManagementRoute: Routes = [
  {
    path: '',
    component: RegionManagementComponent,
  },
];
