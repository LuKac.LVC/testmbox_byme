import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SearchOfSkillsSharedModule } from '../../shared/shared.module';
import { RecipientManagementComponent } from './components/recipient-management.component';
import { RecipientManagementDetailComponent } from './components/recipient-management-detail.component';
import { RecipientManagementUpdateComponent } from './components/recipient-management-update.component';
import { recipientManagementRoute } from './recipient-management.route';
import { RecipientDetailDialogComponent } from './components/recipient-detail-dialog.component';

@NgModule({
  imports: [SearchOfSkillsSharedModule, RouterModule.forChild(recipientManagementRoute)],
  declarations: [
    RecipientManagementComponent,
    RecipientManagementDetailComponent,
    RecipientManagementUpdateComponent,
    RecipientDetailDialogComponent,
  ],
})
export class RecipientManagementModule {}
