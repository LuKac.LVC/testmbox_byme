import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Pagination } from 'app/shared/util/request-util';
import { RecipientList } from '../models/recipientList.model';
import { Recipient } from '../models/recipient.model';
import { Page } from '../../../shared/models/page.model';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class RecipientService {
  public resourceUrl = SERVER_API_URL + 'api';

  constructor(private http: HttpClient) {}

  import(formData: FormData): Observable<void> {
    return this.http.post<void>(this.resourceUrl + '/recipientList/import', formData);
  }

  update(recipientList: RecipientList): Observable<RecipientList> {
    return this.http.put<RecipientList>(this.resourceUrl + '/recipientList', recipientList);
  }

  find(id: number): Observable<RecipientList> {
    const options: HttpParams = new HttpParams().append('id', id + '');
    return this.http.get<RecipientList>(this.resourceUrl + '/recipientList', { params: options });
  }

  findAllRecipientsByRecipientListId(req?: Pagination, id?: number): Observable<Page<Recipient>> {
    const options = createRequestOption(req).append('recipientListId', id + '');
    return this.http
      .get<Recipient[]>(this.resourceUrl + '/recipients', { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<Recipient> = {
            items: res.body!,
            total: Number(res.headers.get('X-Total-Count')),
          };
          return page;
        })
      );
  }

  query(req?: Pagination, name?: string): Observable<Page<RecipientList>> {
    let options;
    if (name) {
      options = createRequestOption(req).append('name', name);
    } else {
      options = createRequestOption(req);
    }
    return this.http
      .get<RecipientList[]>(this.resourceUrl + '/recipientLists', { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<RecipientList> = {
            items: res.body!,
            total: Number(res.headers.get('X-Total-Count')),
          };
          return page;
        })
      );
  }

  delete(id: number): Observable<{}> {
    const options: HttpParams = new HttpParams().append('id', id + '');
    return this.http.delete(this.resourceUrl + '/recipientList', { params: options });
  }
}
