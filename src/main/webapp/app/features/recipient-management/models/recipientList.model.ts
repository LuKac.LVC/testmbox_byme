import { Recipient } from './recipient.model';

export interface RecipientList {
  id?: number;
  name?: string;
  deleted?: boolean;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
  recipients?: Recipient[];
}
