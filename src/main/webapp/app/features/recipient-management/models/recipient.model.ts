export interface Recipient {
  id?: number;
  recipientIdentify?: number;
  county?: string;
  title?: string;
  procuringAgency?: string;
  referenceNumber?: string;
  registrar?: string;
  email?: string;
  procurementUrl?: string;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}
