import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipientService } from '../services/recipient.service';
import { Recipient } from '../models/recipient.model';
import { loading } from '../../../shared/rxjs/loading';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RecipientList } from '../models/recipientList.model';
import { Page } from '../../../shared/models/page.model';

@Component({
  selector: 'jhi-recipient-detail-dialog',
  templateUrl: './recipient-detail-dialog.component.html',
})
export class RecipientDetailDialogComponent implements OnInit {
  recipients: Recipient[] = [];
  recipientList!: RecipientList;
  totalItems = 0;
  itemsPerPage = 10;
  page!: number;
  predicate!: string;
  ascending!: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private route: ActivatedRoute,
    private recipientService: RecipientService,
    private router: Router,
    private modalService: NgbActiveModal
  ) {}

  ngOnInit(): void {
    this.page = 1;
    this.ascending = true;
    this.predicate = 'id';
    this.loadAll();
  }

  loadAll(): void {
    this.recipientService
      .findAllRecipientsByRecipientListId(
        {
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort(),
        },
        this.recipientList.id
      )
      .pipe(loading())
      .subscribe((recipientPage: Page<Recipient>) => this.onSuccess(recipientPage));
  }

  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  trackIdentity(index: number, item: Recipient): any {
    return item.id;
  }

  private onSuccess(recipientPage: Page<Recipient>): void {
    this.totalItems = recipientPage.total;
    this.recipients = recipientPage.items;
  }

  transition(): void {
    this.loadAll();
  }

  cancel(): void {
    this.modalService.dismiss();
  }
}
