import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, ParamMap, Router } from '@angular/router';
import { RecipientService } from '../services/recipient.service';
import { Recipient } from '../models/recipient.model';
import { ITEMS_PER_PAGE } from '../../../shared/constants/pagination.constants';
import { loading } from '../../../shared/rxjs/loading';
import { combineLatest } from 'rxjs';
import { Page } from '../../../shared/models/page.model';

@Component({
  selector: 'jhi-recipient-mgmt-detail',
  templateUrl: './recipient-management-detail.component.html',
})
export class RecipientManagementDetailComponent implements OnInit {
  recipients: Recipient[] = [];
  recipientListId!: number;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private route: ActivatedRoute,
    private recipientService: RecipientService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.page = 1;
    this.ascending = true;
    this.predicate = 'id';
    this.recipientListId = this.route.params['value']['id'];
    this.handleNavigation();
    this.loadAll();
  }

  loadAll(): void {
    this.recipientService
      .findAllRecipientsByRecipientListId(
        {
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort(),
        },
        this.recipientListId
      )
      .pipe(loading())
      .subscribe((recipientPage: Page<Recipient>) => this.onSuccess(recipientPage));
  }

  private handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      this.page = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      this.predicate = sort[0];
      this.ascending = sort[1] === 'asc';
      this.loadAll();
    }).subscribe();
  }

  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  trackIdentity(index: number, item: Recipient): any {
    return item.id;
  }

  private onSuccess(recipientPage: Page<Recipient>): void {
    this.totalItems = recipientPage.total;
    this.recipients = recipientPage.items;
  }

  transition(): void {
    this.router.navigate(['./' + this.recipientListId + '/view'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
      },
    });
  }
}
