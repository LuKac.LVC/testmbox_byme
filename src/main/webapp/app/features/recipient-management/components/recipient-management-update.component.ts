import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { RecipientList } from '../models/recipientList.model';
import { RecipientService } from '../services/recipient.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { loading } from '../../../shared/rxjs/loading';
import { HandleRequestService } from '../../../core/handle-request/handle-request.service';

@Component({
  selector: 'jhi-recipient-mgmt-update',
  templateUrl: './recipient-management-update.component.html',
})
export class RecipientManagementUpdateComponent implements OnInit {
  id!: number;
  recipientList!: RecipientList;
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: ['', [Validators.required, Validators.maxLength(255)]],
  });

  constructor(
    public activeModal: NgbActiveModal,
    private recipientService: RecipientService,
    private fb: FormBuilder,
    private handleRequestService: HandleRequestService
  ) {}

  ngOnInit(): void {
    this.recipientService.find(this.id).subscribe(recipientList => {
      this.recipientList = recipientList;
      this.updateForm(recipientList);
    });
  }

  previousState(): void {
    window.history.back();
  }

  private updateForm(recipientList: RecipientList): void {
    this.editForm.patchValue({
      id: recipientList.id,
      name: recipientList.name,
      createdDate: recipientList.createdDate,
    });
  }

  public save(): void {
    this.recipientService
      .update(this.editForm.value)
      .pipe(loading())
      .subscribe(
        recipientList => {
          this.activeModal.close(recipientList);
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  cancel(): void {
    this.activeModal.dismiss();
  }
}
