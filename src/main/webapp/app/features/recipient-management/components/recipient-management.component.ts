import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Data, ParamMap, Router } from '@angular/router';

import { ITEMS_PER_PAGE } from '../../../shared/constants/pagination.constants';
import { loading } from '../../../shared/rxjs/loading';
import { ToastrService } from 'ngx-toastr';
import { RecipientList } from '../models/recipientList.model';
import { RecipientService } from '../services/recipient.service';
import { RecipientManagementUpdateComponent } from './recipient-management-update.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDialogComponent } from '../../../shared/dialog/confirm-dialog.component';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { combineLatest } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { Page } from '../../../shared/models/page.model';
import { HandleRequestService } from '../../../core/handle-request/handle-request.service';

@Component({
  selector: 'jhi-recipient-mgmt',
  templateUrl: './recipient-management.component.html',
})
export class RecipientManagementComponent implements OnInit {
  @ViewChild('file', { static: false }) nameFile?: ElementRef;
  recipientLists: RecipientList[] = [];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  isNotFormatFile = true;
  formData: FormData = new FormData();
  formUpload: FormGroup;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private recipientService: RecipientService,
    private handleRequestService: HandleRequestService,
    private modalService: NgbModal,
    private translatePipe: TranslatePipe
  ) {
    this.formUpload = new FormGroup({
      file: new FormControl(''),
    });
  }

  ngOnInit(): void {
    this.page = 1;
    this.ascending = true;
    this.predicate = 'id';
    this.handleNavigation();
    this.loadAll();
  }

  trackIdentity(index: number, item: RecipientList): any {
    return item.id;
  }

  deleteRecipientList(recipientList: RecipientList): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.delete';
    modalRef.componentInstance.title = 'entity.action.delete';
    modalRef.componentInstance.content = 'recipientManagement.delete.question';
    modalRef.componentInstance.translateValues = { name: recipientList.name };

    modalRef.result.then(
      () => {
        this.recipientService
          .delete(recipientList.id!)
          .pipe(loading())
          .subscribe(
            () => {
              this.recipientLists = this.recipientLists.filter(elm => elm.id !== recipientList.id);
              this.handleRequestService.showMessageSuccess('recipientManagement.deleted', { param: recipientList.id });
            },
            error => {
              this.handleRequestService.showMessageError('error.actionFailed', error.error);
            }
          );
      },
      () => {}
    );
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
      },
    });
  }

  private handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      this.page = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      this.predicate = sort[0];
      this.ascending = sort[1] === 'asc';
      this.loadAll();
    }).subscribe();
  }

  private loadAll(): void {
    this.recipientService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .pipe(loading())
      .subscribe(
        (res: Page<RecipientList>) => this.onSuccess(res),
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  private onSuccess(recipientPage: Page<RecipientList>): void {
    this.totalItems = recipientPage.total;
    this.recipientLists = recipientPage.items;
  }

  editRecipientList(recipientList: RecipientList): void {
    const modalRef = this.modalService.open(RecipientManagementUpdateComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.id = recipientList.id;
    modalRef.result.then(res => {
      const index = this.recipientLists.indexOf(recipientList);
      this.recipientLists[index] = res;
      this.toastrService.success(this.translatePipe.transform('recipientManagement.updated', { param: recipientList.id }));
    });
  }

  uploadRecipientList(event: any): void {
    const fileToUpload = event.target.files[0];
    if (fileToUpload) {
      this.formData.append('file', fileToUpload);
      this.recipientService
        .import(this.formData)
        .pipe(loading())
        .subscribe(
          () => {
            this.handleRequestService.showMessageSuccess('recipientManagement.uploaded', null);
            this.formData = new FormData();
            this.formUpload.get('file')!.reset();
            this.loadAll();
          },
          error => {
            this.formData = new FormData();
            this.formUpload.get('file')!.reset();
            this.handleRequestService.showMessageError('error.uploadExcelError', error.error);
          }
        );
    }
  }
}
