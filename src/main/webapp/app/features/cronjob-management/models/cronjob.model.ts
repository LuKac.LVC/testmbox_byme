import { RecipientList } from '../../recipient-management/models/recipientList.model';
import { EmailTemplate } from '../../email-template-management/models/email-template.model';

export interface CronjobDataDTO {
  id?: number;
  name?: string;
  recipientList?: RecipientList;
  templateMain?: EmailTemplate;
  templateReminder1?: EmailTemplate;
  templateReminder2?: EmailTemplate;
  chunkNumber?: number;
  weekOfMonth?: number;
  weekday?: WeekDay;
  status?: CronJobStatus;
  sendHour?: number;
  startDate?: Date;
  endDate?: Date;
  deleted?: boolean;
  archived?: boolean;
  frequency?: Frequency;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}

export enum CronJobStatus {
  RUNNING = 'RUNNING',
  CANCELLED = 'CANCELLED',
  FINISHED = 'FINISHED',
}

export enum WeekDay {
  MON = 'MON',
  TUE = 'TUE',
  WED = 'WED',
  THU = 'THU',
  FRI = 'FRI',
  SAT = 'SAT',
  SUN = 'SUN',
}

export enum Frequency {
  ONCE_PER_WEEK = 'ONCE_PER_WEEK',
  ONCE_PER_TWO_WEEK = 'ONCE_PER_TWO_WEEK',
  ONCE_PER_MONTH = 'ONCE_PER_MONTH',
}
