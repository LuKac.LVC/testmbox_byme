import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SearchOfSkillsSharedModule } from '../../shared/shared.module';
import { CronjobManagementComponent } from './components/cronjob-management.component';
import { cronjobManagementRoute } from './cronjob-management.route';
import { CronjobManagementUpdateComponent } from './components/cronjob-management-update.component';
import { CronjobStatisticComponent } from './components/cronjob-statistic.component';
import { ArchiveCronjobManagementComponent } from './components/archive-cronjob-management.component';
import { OverlapDialogComponent } from './dialog/overlap-dialog.component';

@NgModule({
  imports: [SearchOfSkillsSharedModule, RouterModule.forChild(cronjobManagementRoute)],
  declarations: [
    CronjobManagementComponent,
    CronjobManagementUpdateComponent,
    CronjobStatisticComponent,
    ArchiveCronjobManagementComponent,
    OverlapDialogComponent,
  ],
  exports: [OverlapDialogComponent],
})
export class CronjobManagementModule {}
