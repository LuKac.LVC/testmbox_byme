import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CronjobStatistic } from '../models/cronjob-statistic.model';
import { CronjobService } from '../services/cronjob.service';
import { HandleRequestService } from '../../../core/handle-request/handle-request.service';
import { loading } from '../../../shared/rxjs/loading';

@Component({
  selector: 'jhi-cronjob-statistic',
  templateUrl: './cronjob-statistic.component.html',
})
export class CronjobStatisticComponent implements OnInit {
  id!: number;
  statistic!: CronjobStatistic;

  constructor(private route: ActivatedRoute, private cronjobService: CronjobService, private handleRequestService: HandleRequestService) {}

  ngOnInit(): void {
    this.id = this.route.params['value']['id'];
    this.cronjobService
      .findStatistic(this.id)
      .pipe(loading())
      .subscribe(
        (cronjobStatistic: CronjobStatistic) => {
          this.statistic = cronjobStatistic;
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  back(): void {
    window.history.back();
  }
}
