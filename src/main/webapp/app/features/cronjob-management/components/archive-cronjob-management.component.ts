import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, ParamMap, Router } from '@angular/router';

import { ITEMS_PER_PAGE } from '../../../shared/constants/pagination.constants';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDialogComponent } from '../../../shared/dialog/confirm-dialog.component';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { combineLatest } from 'rxjs';
import { CronjobDataDTO, CronJobStatus } from '../models/cronjob.model';
import { loading } from '../../../shared/rxjs/loading';
import { Page } from '../../../shared/models/page.model';
import { CronjobService } from '../services/cronjob.service';
import { HandleRequestService } from '../../../core/handle-request/handle-request.service';
import { DialogType } from '../../../shared/dialog/dialog-type.enum';

@Component({
  selector: 'jhi-archive-cronjob-mgmt',
  templateUrl: './archive-cronjob-management.component.html',
})
export class ArchiveCronjobManagementComponent implements OnInit {
  cronjobList: CronjobDataDTO[] = [];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService,
    private translateService: TranslateService,
    private modalService: NgbModal,
    private translatePipe: TranslatePipe,
    private cronjobService: CronjobService,
    private handleRequestService: HandleRequestService
  ) {}

  ngOnInit(): void {
    this.page = 1;
    this.ascending = true;
    this.predicate = 'id';
    this.handleNavigation();
    this.loadAll();
  }

  loadAll(): void {
    this.cronjobService
      .query(
        {
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort(),
        },
        true
      )
      .pipe(loading())
      .subscribe(
        (pageCronjob: Page<CronjobDataDTO>) => this.onSuccess(pageCronjob),
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }
  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  transition(): void {
    this.router.navigate(['./archive'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
      },
    });
  }

  trackIdentity(index: number, item: CronjobDataDTO): any {
    return item.id;
  }

  deleteCronjob(id: number): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.delete';
    modalRef.componentInstance.title = 'entity.action.delete';
    modalRef.componentInstance.content = 'cronjobManagement.delete.question';
    modalRef.componentInstance.translateValues = { param: id };

    modalRef.result.then(
      () => {
        this.cronjobService
          .delete(id)
          .pipe(loading())
          .subscribe(
            () => {
              this.cronjobList = this.cronjobList.filter(elm => elm.id !== id);
              this.handleRequestService.showMessageSuccess('cronjobManagement.deleted', { param: id });
            },
            error => {
              this.handleRequestService.showMessageError('error.actionFailed', error.error);
            }
          );
      },
      () => {}
    );
  }

  private handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      this.page = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      this.predicate = sort[0];
      this.ascending = sort[1] === 'asc';
      this.loadAll();
    }).subscribe();
  }

  getClassByStatus(status: CronJobStatus): string {
    switch (status) {
      case CronJobStatus.CANCELLED:
        return 'cronjob-status-red';
      case CronJobStatus.FINISHED:
        return 'cronjob-status-green';
      case CronJobStatus.RUNNING:
        return 'cronjob-status-orange';
    }
  }

  restore(id: number): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = 'entity.action.ok';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'cronjobManagement.restore.question';
    modalRef.componentInstance.translateValues = { param: id };

    modalRef.result.then(
      () => {
        this.cronjobService
          .restore(id)
          .pipe(loading())
          .subscribe(
            () => {
              this.cronjobList = this.cronjobList.filter(elm => elm.id !== id);
              this.handleRequestService.showMessageSuccess('cronjobManagement.restored', { param: id });
            },
            error => {
              this.handleRequestService.showMessageError('error.actionFailed', error.error);
            }
          );
      },
      () => {}
    );
  }

  private onSuccess(pageCronjob: Page<CronjobDataDTO>): void {
    this.totalItems = pageCronjob.total;
    this.cronjobList = pageCronjob.items;
  }
}
