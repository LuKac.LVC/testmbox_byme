import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, ParamMap, Router } from '@angular/router';

import { ITEMS_PER_PAGE } from '../../../shared/constants/pagination.constants';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDialogComponent } from '../../../shared/dialog/confirm-dialog.component';
import { combineLatest } from 'rxjs';
import { CronjobDataDTO, CronJobStatus } from '../models/cronjob.model';
import { CronjobService } from '../services/cronjob.service';
import { loading } from '../../../shared/rxjs/loading';
import { Page } from '../../../shared/models/page.model';
import { HandleRequestService } from '../../../core/handle-request/handle-request.service';
import { DialogType } from '../../../shared/dialog/dialog-type.enum';
import { CronjobDTO } from '../models/cronjob-dto.model';
import { getCurrentDayString, getCurrentDayValue } from '../../../shared/util/day-util';

@Component({
  selector: 'jhi-cronjob-mgmt',
  templateUrl: './cronjob-management.component.html',
})
export class CronjobManagementComponent implements OnInit {
  cronjobList: CronjobDataDTO[] = [];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private cronjobService: CronjobService,
    private handleRequestService: HandleRequestService
  ) {}

  ngOnInit(): void {
    this.page = 1;
    this.ascending = true;
    this.predicate = 'id';
    this.handleNavigation();
    this.loadAll();
  }

  loadAll(): void {
    this.cronjobService
      .query(
        {
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort(),
        },
        false
      )
      .pipe(loading())
      .subscribe(
        (pageCronjob: Page<CronjobDataDTO>) => this.onSuccess(pageCronjob),
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  private onSuccess(recipientPage: Page<CronjobDataDTO>): void {
    this.totalItems = recipientPage.total;
    this.cronjobList = recipientPage.items;
    this.cronjobList.forEach(elm => this.parseToLocalDate(elm));
  }

  private parseToLocalDate(cronJob: CronjobDTO): CronjobDTO {
    const timeZone = (new Date().getTimezoneOffset() / 60) * -1;
    let sendHour = cronJob.sendHour! + timeZone;
    // convert to UTC time zone
    if (sendHour < 0) {
      sendHour = 24 + sendHour;
      // get last day
      cronJob.weekday = getCurrentDayString(getCurrentDayValue(cronJob.weekday!.toString()) - 1);
    } else if (sendHour > 23) {
      sendHour = sendHour - 24;
      // get next day
      cronJob.weekday = getCurrentDayString(getCurrentDayValue(cronJob.weekday!.toString()) + 1);
    }
    cronJob.sendHour = sendHour;
    return cronJob;
  }

  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
      },
    });
  }

  trackIdentity(index: number, item: CronjobDataDTO): any {
    return item.id;
  }

  private handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      this.page = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      this.predicate = sort[0];
      this.ascending = sort[1] === 'asc';
      this.loadAll();
    }).subscribe();
  }

  archive(id: number): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = 'entity.action.ok';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'cronjobManagement.archive.question';
    modalRef.componentInstance.translateValues = { param: id };

    modalRef.result.then(
      () => {
        this.cronjobService
          .archive(id)
          .pipe(loading())
          .subscribe(
            () => {
              this.cronjobList = this.cronjobList.filter(elm => elm.id !== id);
              this.handleRequestService.showMessageSuccess('cronjobManagement.archived', { param: id });
            },
            error => {
              this.handleRequestService.showMessageError('error.actionFailed', error.error);
            }
          );
      },
      () => {}
    );
  }

  resume(id: number): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = 'entity.action.ok';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'cronjobManagement.resume.question';
    modalRef.componentInstance.translateValues = { param: id };

    modalRef.result.then(
      () => {
        this.cronjobService
          .resume(id)
          .pipe(loading())
          .subscribe(
            () => {
              this.loadAll();
              this.handleRequestService.showMessageSuccess('cronjobManagement.resumed', { param: id });
            },
            error => {
              this.handleRequestService.showMessageError('error.actionFailed', error.error);
            }
          );
      },
      () => {}
    );
  }

  cancel(id: number): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = 'entity.action.cancel';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'cronjobManagement.cancel.question';
    modalRef.componentInstance.translateValues = { param: id };

    modalRef.result.then(
      () => {
        this.cronjobService
          .cancel(id)
          .pipe(loading())
          .subscribe(
            () => {
              this.loadAll();
              this.handleRequestService.showMessageSuccess('cronjobManagement.cancelled', { param: id });
            },
            error => {
              this.handleRequestService.showMessageError('error.actionFailed', error.error);
            }
          );
      },
      () => {}
    );
  }

  getClassByStatus(status: CronJobStatus): string {
    switch (status) {
      case CronJobStatus.CANCELLED:
        return 'cronjob-status-red';
      case CronJobStatus.FINISHED:
        return 'cronjob-status-green';
      case CronJobStatus.RUNNING:
        return 'cronjob-status-orange';
    }
  }
}
