import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page } from '../../../shared/models/page.model';
import { createRequestOption, Pagination } from '../../../shared/util/request-util';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from '../../../app.constants';
import { ICategory } from '../models/category-management.model';

@Injectable({ providedIn: 'root' })
export default class CategoryService {
  private resourceUrl = SERVER_API_URL + 'api/v2';
  constructor(private http: HttpClient) {}

  query(req: Pagination, name?: string): Observable<Page<ICategory>> {
    let options = createRequestOption(req);
    if (name) {
      options = options.set('name', name);
    }
    return this.http
      .get<ICategory[]>(this.resourceUrl + '/categories', { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<ICategory> = {
            items: res?.body || [],
            total: Number(res.headers.get('x-total-count')),
          };
          return page;
        })
      );
  }

  create(names: string[]): Observable<ICategory[]> {
    return this.http.post<ICategory[]>(`${this.resourceUrl}/categories`, { names });
  }

  update(id: number, name: string): Observable<ICategory[]> {
    return this.http.put<ICategory[]>(`${this.resourceUrl}/categories/${id}`, { name: name.trim() });
  }

  existed(name: string): Observable<boolean> {
    return this.http.get<boolean>(`${this.resourceUrl}/categories/${name}/exist`);
  }

  delete(id: number): Observable<{}> {
    return this.http.delete(`${this.resourceUrl}/categories/${id}`);
  }
}
