import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import CategoryService from '../services/category.service';

@Component({
  selector: 'jhi-edit-categories',
  templateUrl: './edit-categories-dialog.component.html',
})
export default class EditCategoriesDialogComponent {
  @Input() category = '';
  errorRequired = false;
  constructor(
    public activeModal: NgbActiveModal,
    private categoryService: CategoryService,
    private toastrService: ToastrService,
    private translateService: TranslateService
  ) {}

  get isDisableSubmit(): boolean {
    return !this.category;
  }

  onSubmit(): void {
    this.activeModal.close({
      name: this.category,
    });
  }

  handleValidate(): void {
    this.errorRequired = !this.category;
  }
}
