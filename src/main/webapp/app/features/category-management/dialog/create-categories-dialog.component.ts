import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import CategoryService from '../services/category.service';

@Component({
  selector: 'jhi-create-categories',
  templateUrl: './create-categories-dialog.component.html',
})
export default class CreateCategoriesDialogComponent {
  categories: string[] = [];
  errorRequired = false;
  constructor(
    public activeModal: NgbActiveModal,
    private categoryService: CategoryService,
    private toastrService: ToastrService,
    private translateService: TranslateService
  ) {}

  get isDisableSubmit(): boolean {
    return !this.categories.length;
  }

  onSubmit(): void {
    this.activeModal.close({
      names: this.categories,
    });
  }

  addTagFn(name: string): string {
    return name;
  }

  onAddCategories(name: string): void {
    this.categoryService.existed(name).subscribe(existed => {
      if (existed) {
        this.categories = this.categories.filter(item => item !== name);
        this.toastrService.warning(this.translateService.instant('tagManagement.modal.existed'));
      }
    });
  }

  handleValidate(): void {
    this.errorRequired = !this.categories.length;
  }
}
