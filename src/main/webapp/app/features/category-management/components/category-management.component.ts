import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, ParamMap, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HandleRequestService } from 'app/core/handle-request/handle-request.service';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ConfirmDialogComponent } from 'app/shared/dialog/confirm-dialog.component';
import { DialogType } from 'app/shared/dialog/dialog-type.enum';
import { loading } from 'app/shared/rxjs/loading';
import { combineLatest } from 'rxjs';
import CreateCategoriesDialogComponent from '../dialog/create-categories-dialog.component';
import EditCategoriesDialogComponent from '../dialog/edit-categories-dialog.component';
import { ICategory } from '../models/category-management.model';
import CategoryService from '../services/category.service';

@Component({
  selector: 'jhi-category-management',
  templateUrl: './category-management.component.html',
})
export default class CategoryManagementComponent implements OnInit {
  categories: ICategory[] = [];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page = 1;
  predicate = 'id';
  ascending = true;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private categoryService: CategoryService,
    private modalService: NgbModal,
    private handleRequestService: HandleRequestService
  ) {}
  ngOnInit(): void {
    this.handleNavigation();
  }

  private loadCategories(): void {
    this.categoryService
      .query({ page: this.page - 1, size: this.itemsPerPage, sort: this.sort() })
      .pipe(loading())
      .subscribe(
        data => {
          this.categories = data.items;
          this.totalItems = data.total;
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }
  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  private handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      this.page = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort'])?.split(',') || [];
      this.predicate = sort[0] || 'id';
      this.ascending = sort[1] === 'asc';
      this.loadCategories();
    }).subscribe();
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
      },
    });
  }

  onCreateCategories(names: string[]): void {
    this.categoryService
      .create(names)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadCategories();
          this.handleRequestService.showMessageSuccess('categoryManagement.modal.created', '');
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  openCreateModal(): void {
    const modalRef = this.modalService.open(CreateCategoriesDialogComponent, { size: 'lg', centered: true });
    modalRef.result.then(
      result => {
        this.onCreateCategories(result.names);
      },
      () => {}
    );
  }

  onEditCategories(id: number, name: string): void {
    this.categoryService
      .update(id, name)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadCategories();
          this.handleRequestService.showMessageSuccess('categoryManagement.modal.updated', { param: id });
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }
  openEditModal(id: number, name: string): void {
    const modalRef = this.modalService.open(EditCategoriesDialogComponent, { size: 'lg', centered: true });
    modalRef.componentInstance.category = name;
    modalRef.result.then(
      result => {
        this.onEditCategories(id, result.name);
      },
      () => {}
    );
  }

  onDeleteCategories(id: number): void {
    this.categoryService
      .delete(id)
      .pipe(loading())
      .subscribe(
        () => {
          this.loadCategories();
          this.handleRequestService.showMessageSuccess('categoryManagement.modal.deleted', { param: id });
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }
  openDeleteModal(id: number, name: string): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = 'categoryManagement.modal.deleteTitle';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'categoryManagement.modal.deleteQuestion';
    modalRef.componentInstance.translateValues = { name };
    modalRef.result.then(
      () => {
        this.onDeleteCategories(id);
      },
      () => {}
    );
  }
}
