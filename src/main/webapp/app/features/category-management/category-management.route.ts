import { Routes } from '@angular/router';
import CategoryManagementComponent from './components/category-management.component';

export const categoryManagementRoute: Routes = [
  {
    path: '',
    component: CategoryManagementComponent,
  },
];
