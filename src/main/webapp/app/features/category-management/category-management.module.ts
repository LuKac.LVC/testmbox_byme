import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { categoryManagementRoute } from './category-management.route';
import { SearchOfSkillsSharedModule } from '../../shared/shared.module';
import CategoryManagementComponent from './components/category-management.component';
import CreateCategoriesDialogComponent from './dialog/create-categories-dialog.component';
import EditCategoriesDialogComponent from './dialog/edit-categories-dialog.component';
@NgModule({
  imports: [SearchOfSkillsSharedModule, RouterModule.forChild(categoryManagementRoute)],
  declarations: [CategoryManagementComponent, CreateCategoriesDialogComponent, EditCategoriesDialogComponent],
})
export class CategoryManagementModule {}
