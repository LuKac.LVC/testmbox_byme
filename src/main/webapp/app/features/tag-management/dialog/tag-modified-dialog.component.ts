import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ICategory } from 'app/features/category-management/models/category-management.model';
import CategoryService from 'app/features/category-management/services/category.service';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ToastrService } from 'ngx-toastr';
import TagService from '../services/tag.service';

@Component({
  selector: 'jhi-tag-modified',
  templateUrl: './tag-modified-dialog.component.html',
})
export default class TagModifiedDialogComponent {
  @Input() isBlock = false;
  @Input() isEdit = false;
  @Input() name = '';
  @Input() selectedCategories: ICategory[] = [];
  @Input() categories: ICategory[] = [];

  names = [];
  page = 0;
  itemsPerPage = ITEMS_PER_PAGE;

  errors = {
    name: {
      required: false,
    },
    categories: {
      required: false,
    },
  };

  constructor(
    public activeModal: NgbActiveModal,
    private categoryService: CategoryService,
    private tagService: TagService,
    private toastrService: ToastrService,
    private translateService: TranslateService
  ) {}

  get modalTitle(): string {
    if (this.isBlock) {
      return this.isEdit ? 'tagManagement.modal.editBlock' : 'tagManagement.modal.createBlock';
    }
    return this.isEdit ? 'tagManagement.modal.edit' : 'tagManagement.modal.create';
  }
  onSearchCategory(name: string): void {
    this.categoryService
      .query(
        {
          page: this.page,
          size: this.itemsPerPage,
          sort: ['id,asc'],
        },
        name
      )
      .subscribe(data => {
        this.categories = data.items;
      });
  }

  get isDisableSubmit(): boolean {
    if (!this.isEdit) {
      return !this.names.length || !this.selectedCategories.length;
    }
    return !this.name || !this.selectedCategories.length;
  }

  onSubmit(): void {
    const categoriesIds = this.selectedCategories.map(c => c.id);
    this.activeModal.close({
      ...(this.isEdit ? { name: this.name } : { names: this.names }),
      categories: categoriesIds,
    });
  }

  addTagFn(name: string): string {
    return name;
  }

  onAddTag(name: string): void {
    this.tagService.existed(name).subscribe(existed => {
      if (existed) {
        this.names = this.names.filter(item => item !== name);
        this.toastrService.warning(
          this.translateService.instant('tagManagement.modal.existed', { type: this.isBlock ? 'block tag' : 'tag' })
        );
      }
    });
  }

  handleValidate(key: 'name' | 'names' | 'categories'): void {
    switch (key) {
      case 'name':
        this.errors.name.required = !this.name;
        break;
      case 'names':
        this.errors.name.required = !this.names.length;
        break;
      case 'categories':
        this.errors.categories.required = !this.selectedCategories.length;
        break;
      default:
        break;
    }
  }
}
