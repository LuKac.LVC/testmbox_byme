import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page } from 'app/shared/models/page.model';
import { createRequestOption, Pagination } from 'app/shared/util/request-util';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from '../../../app.constants';
import { ICreateTagReq, ITag, IUpdateTagReq } from '../models/tag-management.model';

@Injectable({ providedIn: 'root' })
export default class TagService {
  private resourceUrl = SERVER_API_URL + 'api/v2';
  constructor(private http: HttpClient) {}

  query(req?: Pagination, block?: boolean, name?: string): Observable<Page<ITag>> {
    let options = createRequestOption(req);
    if (block !== undefined) {
      options = options.set('block', String(block));
    }
    if (name) {
      options = options.set('name', name);
    }
    return this.http
      .get<ITag[]>(this.resourceUrl + '/tags', { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<ITag> = {
            items: res?.body || [],
            total: Number(res.headers.get('x-total-count')),
          };
          return page;
        })
      );
  }

  create(request: ICreateTagReq): Observable<ITag[]> {
    return this.http.post<ITag[]>(`${this.resourceUrl}/tags`, request);
  }

  update(id: number, body: IUpdateTagReq): Observable<ITag> {
    return this.http.put<ITag>(`${this.resourceUrl}/tags/${id}`, body);
  }

  delete(id: number): Observable<{}> {
    return this.http.delete(`${this.resourceUrl}/tags/${id}`);
  }

  existed(name: string): Observable<boolean> {
    return this.http.get<boolean>(`${this.resourceUrl}/tags/${name}/exist`);
  }

  switchTagToBlock(id: number): Observable<{}> {
    return this.http.put(`${this.resourceUrl}/tags/${id}/block`, {});
  }

  switchBlockToTag(id: number): Observable<{}> {
    return this.http.put(`${this.resourceUrl}/tags/${id}/unblock`, {});
  }

  saveWord(request: { tags: string[]; blockTags: string[] }): Observable<{}> {
    return this.http.post<{}>(`${this.resourceUrl}/tags/words`, request);
  }
}
