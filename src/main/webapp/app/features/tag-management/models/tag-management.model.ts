import { ICategory } from '../../category-management/models/category-management.model';
export interface ITag {
  id: number;
  name: string;
  search: number;
  block: boolean;
  delete: boolean;
  categories: ICategory[];
}

export interface ICreateTagReq {
  tags: string[];
  categories: number[];
  block: boolean;
}

export interface IUpdateTagReq {
  name: string;
  categories: number[];
  block: boolean;
}

export enum TYPE_TAG {
  BLOCK = 'block',
  TAG = 'tag',
}

export interface ITagFilter {
  id: number;
  name: string;
  disable: boolean;
}
