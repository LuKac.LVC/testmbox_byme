import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SearchOfSkillsSharedModule } from '../../shared/shared.module';
import TagManagementComponent from './components/tag-management.component';
import TagModifiedComponent from './dialog/tag-modified-dialog.component';
import { tagManagementRoute } from './tag-management.route';

@NgModule({
  imports: [SearchOfSkillsSharedModule, RouterModule.forChild(tagManagementRoute)],
  declarations: [TagManagementComponent, TagModifiedComponent],
  exports: [TagModifiedComponent],
})
export class TagManagementModule {}
