import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ITag, TYPE_TAG } from '../models/tag-management.model';
import { ICategory } from '../../category-management/models/category-management.model';
import TagService from '../services/tag.service';
import { loading } from '../../../shared/rxjs/loading';
import { Page } from 'app/shared/models/page.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import TagModifiedDialogComponent from '../dialog/tag-modified-dialog.component';
import CategoryService from 'app/features/category-management/services/category.service';
import { ConfirmDialogComponent } from 'app/shared/dialog/confirm-dialog.component';
import { DialogType } from 'app/shared/dialog/dialog-type.enum';
import { HandleRequestService } from 'app/core/handle-request/handle-request.service';

@Component({
  selector: 'jhi-tag-management',
  templateUrl: './tag-management.component.html',
})
export default class TagManagementComponent implements OnInit {
  isBlock = false;
  tagType: TYPE_TAG = TYPE_TAG.TAG;
  tags: ITag[] = [];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page = 1;
  predicate = 'id';
  ascending = true;
  categories: ICategory[] = [];

  selectedTag?: ITag;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private tagService: TagService,
    private modalService: NgbModal,
    private categoryService: CategoryService,
    private handleRequestService: HandleRequestService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(data => {
      this.isBlock = data['type'] === TYPE_TAG.BLOCK;
      this.tagType = data['type'];
      this.handleNavigation();
    });

    this.loadCategories();
  }

  private loadTags(): void {
    this.tagService
      .query(
        {
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort(),
        },
        this.isBlock
      )
      .pipe(loading())
      .subscribe(
        data => this.onSuccess(data),
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  private loadCategories(): void {
    this.categoryService
      .query({
        page: 0,
        size: this.itemsPerPage,
        sort: ['id,asc'],
      })
      .subscribe(data => {
        this.categories = data.items;
      });
  }

  private onSuccess(data: Page<ITag>): void {
    this.tags = data.items;
    this.totalItems = data.total;
  }

  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  private handleNavigation(): void {
    this.activatedRoute.queryParamMap.subscribe(params => {
      const page = params.get('page');
      this.page = page !== null ? +page : 1;
      const sort = (params.get('sort') || '')?.split(',') || [];
      this.predicate = sort[0] || 'id';
      this.ascending = sort[1] === 'asc';
      this.loadTags();
    });
  }

  transition(): void {
    this.router.navigate([`./${this.tagType}/`], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
      },
    });
  }

  renderCategories(categories: ICategory[]): string {
    return categories.map(i => i.name).join(' , ');
  }

  private onSaveModifiedTag({ name, categories, names }: { name: string; categories: number[]; names: string[] }): void {
    if (this.selectedTag) {
      this.tagService
        .update(this.selectedTag.id, { name, categories, block: this.isBlock })
        .pipe(loading())
        .subscribe(
          () => {
            this.loadTags();
            this.handleRequestService.showMessageSuccess('tagManagement.modal.updated', {
              param: name,
              type: this.isBlock ? 'block tag' : 'tag',
            });
          },
          error => {
            this.handleRequestService.showMessageError('error.actionFailed', error.error);
          }
        );
    } else {
      this.tagService
        .create({ tags: names, categories, block: this.isBlock })
        .pipe(loading())
        .subscribe(
          () => {
            this.loadTags();
            this.handleRequestService.showMessageSuccess('tagManagement.modal.created', { type: this.isBlock ? 'block tag' : 'tag' });
          },
          error => {
            this.handleRequestService.showMessageError('error.actionFailed', error.error);
          }
        );
    }
  }

  openModified(tag?: ITag): void {
    const modalRef = this.modalService.open(TagModifiedDialogComponent, { size: 'lg', centered: true });
    modalRef.componentInstance.categories = this.categories;
    modalRef.componentInstance.isBlock = this.isBlock;
    if (tag) {
      this.selectedTag = tag;
      modalRef.componentInstance.name = tag.name;
      modalRef.componentInstance.selectedCategories = tag.categories;
      modalRef.componentInstance.isEdit = true;
    }
    modalRef.result.then(
      result => {
        this.onSaveModifiedTag({ name: result.name, categories: result.categories, names: result.names });
      },
      () => {
        this.selectedTag = undefined;
      }
    );
  }

  openDelete(id: number, name: string): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = this.isBlock ? 'tagManagement.modal.deleteBlock' : 'tagManagement.modal.delete';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'tagManagement.modal.deleteQuestion';
    modalRef.componentInstance.translateValues = { name, type: this.isBlock ? 'block tag' : 'tag' };

    modalRef.result.then(
      () => {
        this.tagService
          .delete(id)
          .pipe(loading())
          .subscribe(
            () => {
              this.loadTags();
              this.handleRequestService.showMessageSuccess('tagManagement.modal.deleted', {
                param: id,
                type: this.isBlock ? 'block tag' : 'tag',
              });
            },
            error => {
              this.handleRequestService.showMessageError('error.actionFailed', error.error);
            }
          );
      },
      () => {}
    );
  }

  openConfirmChangeType(id: number, name: string): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = this.isBlock ? 'tagManagement.home.changeToTag' : 'tagManagement.home.changeToBlock';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'tagManagement.modal.changeQuestion';
    modalRef.componentInstance.translateValues = { name };

    modalRef.result.then(
      () => {
        const switchApi = this.isBlock ? this.tagService.switchBlockToTag(id) : this.tagService.switchTagToBlock(id);
        switchApi.pipe(loading()).subscribe(() => this.loadTags());
      },
      () => {}
    );
  }
}
