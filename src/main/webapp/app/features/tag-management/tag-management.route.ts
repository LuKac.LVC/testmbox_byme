import { Routes } from '@angular/router';
import TagManagementComponent from './components/tag-management.component';

export const tagManagementRoute: Routes = [
  {
    path: '',
    redirectTo: 'tag',
  },
  {
    path: ':type',
    component: TagManagementComponent,
  },
];
