import { Component, OnInit } from '@angular/core';
import { EmailTemplate } from '../models/email-template.model';
import { ActivatedRoute } from '@angular/router';
import { EmailTemplateService } from '../services/email-template.service';
import { HandleRequestService } from '../../../core/handle-request/handle-request.service';
import { loading } from '../../../shared/rxjs/loading';

@Component({
  selector: 'jhi-email-template-mgmt-detail',
  templateUrl: './email-template-management-detail.component.html',
})
export class EmailTemplateManagementDetailComponent implements OnInit {
  emailTemplate!: EmailTemplate;
  emailTemplateId!: number;

  constructor(
    private route: ActivatedRoute,
    private emailTemplateService: EmailTemplateService,
    private handleRequestService: HandleRequestService
  ) {}

  ngOnInit(): void {
    this.emailTemplateId = this.route.params['value']['id'];
    this.emailTemplateService
      .find(this.emailTemplateId)
      .pipe(loading())
      .subscribe(
        (emailTemplate: EmailTemplate) => {
          this.emailTemplate = emailTemplate;
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  back(): void {
    window.history.back();
  }
}
