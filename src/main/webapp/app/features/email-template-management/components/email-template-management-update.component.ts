import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { EmailTemplate, EmailTemplateType } from '../models/email-template.model';
import { EmailTemplateService } from '../services/email-template.service';
import * as ClassicEditor from '../../../build/ckeditor';
import { ToastrService } from 'ngx-toastr';
import { TranslatePipe } from '@ngx-translate/core';
import { loading } from '../../../shared/rxjs/loading';
import { HandleRequestService } from '../../../core/handle-request/handle-request.service';

@Component({
  selector: 'jhi-email-template-mgmt-update',
  templateUrl: './email-template-management-update.component.html',
})
export class EmailTemplateManagementUpdateComponent implements OnInit {
  public Editor = ClassicEditor;
  public DEFAULT_MAIN_CONTENT = '<p>Hej&nbsp;</p><p>Upphandlingsnr: &lt;reference_number&gt;</p><p>&nbsp;</p>';
  public DEFAULT_REMINDER_1_CONTENT =
    '<p>Hej!</p>' +
    '<p>Detta är en påminnelse och avser en tidigare ställd begäran av allmän handling till myndigheten. Den &lt;request_date&gt; skickade jag den ursprungliga begäran av allmän handling till myndigheten enligt nedan.</p>' +
    '<p>Jag har inte mottagit något svar från myndigheten sedan dess och påminner därför myndigheten att handlägga detta ärende skyndsamt. TF 2:13 säger att handlingar ska lämnas ut ”skyndsamt”. Enligt JO ska en myndighet normalt lämna besked i utlämnandefrågan samma dag som en begäran har gjorts. Myndigheten skall alltså antingen lämna ut handlingen eller ge mig besked om att jag inte har rätt att få ut den. Enligt JO får beskedet dröja någon eller några dagar om det är nödvändig för att myndigheten ska kunna ta ställning till om den efterfrågade handlingen är allmän och offentlig. Att en myndighet dröjer två, tre eller flera veckor med att handlägga begäran om utlämnande av allmänna handlingar och/eller lämna besked om huruvida en handling är allmän och offentlig menar JO är oacceptabelt. För kännedom så har JO uttalat sig om skyndsamhetskravet i många beslut, se bland annat JO 4209-09 och JO 5308-11.</p>' +
    '<p>Mvh</p>' +
    '<p>/Liliane</p>';
  public DEFAULT_REMINDER_2_CONTENT =
    '<p>Hej!</p>' +
    '<p>Detta är en andra påminnelse och avser en tidigare ställd begäran av allmän handling till myndigheten. Den &lt;request_date&gt; skickade jag den ursprungliga begäran av allmän handling till myndigheten enligt nedan.</p>' +
    '<p>Jag har inte mottagit något svar från myndigheten sedan dess och påminner därför myndigheten att handlägga detta ärende skyndsamt. TF 2:13 säger att handlingar ska lämnas ut ”skyndsamt”. Enligt JO ska en myndighet normalt lämna besked i utlämnandefrågan samma dag som en begäran har gjorts. Myndigheten skall alltså antingen lämna ut handlingen eller ge mig besked om att jag inte har rätt att få ut den. Enligt JO får beskedet dröja någon eller några dagar om det är nödvändig för att myndigheten ska kunna ta ställning till om den efterfrågade handlingen är allmän och offentlig. Att en myndighet dröjer två, tre eller flera veckor med att handlägga begäran om utlämnande av allmänna handlingar och/eller lämna besked om huruvida en handling är allmän och offentlig menar JO är oacceptabelt. För kännedom så har JO uttalat sig om skyndsamhetskravet i många beslut, se bland annat JO 4209-09 och JO 5308-11.</p>' +
    '<p>Mvh</p>' +
    '<p>/Liliane</p>';
  public REQUIRED_MAIN_CONTENT = '&lt;reference_number&gt;'; // <reference_number>
  public REQUIRED_REMINDER_CONTENT = '&lt;request_date&gt;'; // <request_date>
  emailTemplate!: EmailTemplate;

  editForm = this.fb.group({
    id: [],
    name: ['', [Validators.required, Validators.maxLength(255)]],
    content: [this.DEFAULT_MAIN_CONTENT, [Validators.required, Validators.maxLength(16777215)]],
    type: [EmailTemplateType.MAIN, [Validators.required]],
  });
  isSaving = false;
  configuration = {
    toolbar: ['heading', '|', 'bold', 'italic', 'link', 'numberedList', 'bulletedList', 'undo', 'redo'],
    placeholder: this.translatePipe.transform('emailTemplateManagement.contentPlaceholder'),
  };
  emailTemplateId!: number;
  invalidMainContent = false;
  invalidReminderContent = false;
  requiredContent!: string;

  constructor(
    private translatePipe: TranslatePipe,
    private toastrService: ToastrService,
    private emailTemplateService: EmailTemplateService,
    private route: ActivatedRoute,
    private handleRequestService: HandleRequestService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.emailTemplateId = this.route.params['value']['id'];
    if (this.emailTemplateId) {
      this.emailTemplateService
        .find(this.emailTemplateId)
        .pipe(loading())
        .subscribe(
          (emailTemplate: EmailTemplate) => {
            this.emailTemplate = emailTemplate;
            this.updateForm(this.emailTemplate);
          },
          error => {
            this.handleRequestService.showMessageError('error.actionFailed', error.error);
          }
        );
    } else {
      this.emailTemplate = {
        name: '',
        content: '',
      };
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.updateEmailTemplate(this.emailTemplate);
    this.emailTemplateService
      .createOrUpdate(this.emailTemplate)
      .pipe(loading())
      .subscribe(
        (emailTemplate: EmailTemplate) => this.onSaveSuccess(emailTemplate),
        error => this.onSaveError(error)
      );
  }

  private updateForm(emailTemplate: EmailTemplate): void {
    this.editForm.patchValue({
      id: emailTemplate.id,
      name: emailTemplate.name,
      content: emailTemplate.content,
      type: emailTemplate.type,
    });
  }

  private updateEmailTemplate(emailTemplate: EmailTemplate): void {
    emailTemplate.name = this.editForm.get(['name'])!.value;
    emailTemplate.type = this.editForm.get(['type'])!.value;
    emailTemplate.content = this.editForm.get(['content'])!.value;
  }

  private onSaveSuccess(emailTemplate: EmailTemplate): void {
    if (this.emailTemplate.id) {
      this.handleRequestService.showMessageSuccess('emailTemplateManagement.updated', { param: emailTemplate.id });
    } else {
      this.handleRequestService.showMessageSuccess('emailTemplateManagement.created', { param: emailTemplate.id });
    }
    this.previousState();
  }

  private onSaveError(error: any): void {
    this.handleRequestService.showMessageError('error.actionFailed', error.error);
  }

  changeType(type: string): void {
    let defaultContent = this.DEFAULT_MAIN_CONTENT;
    if (!this.emailTemplate.id) {
      if (type === EmailTemplateType.REMINDER_1) {
        defaultContent = this.DEFAULT_REMINDER_1_CONTENT;
      } else if (type === EmailTemplateType.REMINDER_2) {
        defaultContent = this.DEFAULT_REMINDER_2_CONTENT;
      }
      this.editForm.patchValue({
        content: defaultContent,
      });
    }
  }
}
