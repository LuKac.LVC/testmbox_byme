import { Component, OnInit } from '@angular/core';
import { EmailTemplate } from '../models/email-template.model';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EmailTemplateService } from '../services/email-template.service';

@Component({
  selector: 'jhi-email-template-detail-dialog',
  templateUrl: './email-template-detail-dialog.component.html',
})
export class EmailTemplateDetailDialogComponent implements OnInit {
  emailTemplate!: EmailTemplate;
  emailTemplateId!: number;

  constructor(private route: ActivatedRoute, private modalService: NgbActiveModal, private emailTemplateService: EmailTemplateService) {}

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll(): void {
    this.emailTemplateService.find(this.emailTemplateId).subscribe((emailTemplate: EmailTemplate) => {
      this.emailTemplate = emailTemplate;
    });
  }

  back(): void {
    window.history.back();
  }

  cancel(): void {
    this.modalService.dismiss();
  }
}
