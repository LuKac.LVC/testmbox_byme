export interface EmailTemplate {
  id?: number;
  name?: string;
  content?: string;
  type?: EmailTemplateType;
  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}

export enum EmailTemplateType {
  MAIN = 'MAIN',
  REMINDER_1 = 'REMINDER_1',
  REMINDER_2 = 'REMINDER_2',
}
