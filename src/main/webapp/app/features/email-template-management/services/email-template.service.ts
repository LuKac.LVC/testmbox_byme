import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { RecipientList } from '../../recipient-management/models/recipientList.model';
import { Observable } from 'rxjs';
import { createRequestOption, Pagination } from '../../../shared/util/request-util';
import { Page } from '../../../shared/models/page.model';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from '../../../app.constants';
import { EmailTemplate } from '../models/email-template.model';

@Injectable({ providedIn: 'root' })
export class EmailTemplateService {
  public resourceUrl = SERVER_API_URL + 'api';

  constructor(private http: HttpClient) {}

  createOrUpdate(emailTemplate: EmailTemplate): Observable<EmailTemplate> {
    return this.http.post<RecipientList>(this.resourceUrl + '/emailTemplate', emailTemplate);
  }

  find(id: number): Observable<EmailTemplate> {
    const options: HttpParams = new HttpParams().append('id', id + '');
    return this.http.get<EmailTemplate>(this.resourceUrl + '/emailTemplate', { params: options });
  }

  query(req?: Pagination, name?: string, type?: string): Observable<Page<EmailTemplate>> {
    let options = createRequestOption(req);
    if (name) {
      options = options.set('name', name);
    }
    if (type) {
      options = options.set('type', type);
    }
    return this.http
      .get<EmailTemplate[]>(this.resourceUrl + '/emailTemplates', { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<EmailTemplate> = {
            items: res.body!,
            total: Number(res.headers.get('X-Total-Count')),
          };
          return page;
        })
      );
  }

  delete(id: number): Observable<{}> {
    const options: HttpParams = new HttpParams().append('id', id + '');
    return this.http.delete(this.resourceUrl + '/emailTemplate', { params: options });
  }
}
