export interface IWord {
  id: number;
  value: string;
}
