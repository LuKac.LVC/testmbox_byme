import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { createRequestOption, Pagination } from 'app/shared/util/request-util';
import { HttpClient } from '@angular/common/http';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { Page } from 'app/shared/models/page.model';
import { IWord } from '../models/word.model';
import { SERVER_API_URL } from 'app/app.constants';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export default class WordService {
  private resourceUrl = SERVER_API_URL + 'api/v2';
  constructor(private http: HttpClient) {}
  get(req: Pagination = { page: 0, size: ITEMS_PER_PAGE, sort: ['id'] }): Observable<Page<IWord>> {
    const options = createRequestOption(req);
    return this.http
      .get<IWord[]>(`${this.resourceUrl}/words`, { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<IWord> = {
            items: res?.body || [],
            total: Number(res.headers.get('x-total-count')),
          };
          return page;
        })
      );
  }
}
