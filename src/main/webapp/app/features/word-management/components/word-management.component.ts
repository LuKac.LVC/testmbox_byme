import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { IWord } from '../models/word.model';
import WordService from '../services/word.service';
import { unionWith, isEqual } from 'lodash';
import TagService from 'app/features/tag-management/services/tag.service';
import { HandleRequestService } from 'app/core/handle-request/handle-request.service';
import { loading } from 'app/shared/rxjs/loading';
@Component({
  selector: 'jhi-word-management',
  templateUrl: './word-management.component.html',
  styleUrls: ['./word-management.component.scss'],
})
export default class WordManagementComponent implements OnInit {
  words: IWord[] = [];
  itemsPerPages = [200, 250, 400, 600, 800, 1000];
  totalItems = 0;
  itemsPerPage = 200;
  page = 1;

  wordSelected: IWord[] = [];
  allWords: IWord[] = [];

  constructor(
    private wordService: WordService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private tagService: TagService,
    private handleRequestService: HandleRequestService
  ) {}

  ngOnInit(): void {
    this.handleNavigation();
  }

  private errorHandle = (error: any): void => {
    this.handleRequestService.showMessageError('error.actionFailed', error.error);
  };

  loadWords(): void {
    this.wordService
      .get({ page: this.page - 1, size: this.itemsPerPage, sort: [''] })
      .pipe(loading())
      .subscribe(res => {
        this.words = res.items;
        this.allWords = unionWith(this.allWords, this.words, isEqual);
        this.totalItems = res.total;
      }, this.errorHandle);
  }

  private handleNavigation(): void {
    combineLatest([this.activatedRoute.data, this.activatedRoute.queryParamMap])
      .pipe(
        map(result => {
          const params = result[1];
          const page = params.get('page');
          this.page = page !== null ? +page : 1;
          this.loadWords();
        })
      )
      .subscribe();
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
      },
    });
  }

  onChooseWord(work: IWord): void {
    this.wordSelected.push(work);
  }

  isSelected(id: number): boolean {
    return this.wordSelected.some(i => i.id === id);
  }

  onRemoveWord(id: number): void {
    this.wordSelected = this.wordSelected.filter(i => i.id !== id);
  }

  onSaveWord(): void {
    const blockTags = this.wordSelected.map(i => i.value);
    const tags = this.allWords.filter(i => !this.isSelected(i.id)).map(i => i.value);
    if (blockTags.length || tags.length) {
      this.tagService.saveWord({ tags, blockTags }).subscribe(() => {
        this.handleRequestService.showMessageSuccess('wordManagement.message.saved', '');
        this.wordSelected = [];
        this.allWords = [];
        this.loadWords();
      }, this.errorHandle);
    }
  }
}
