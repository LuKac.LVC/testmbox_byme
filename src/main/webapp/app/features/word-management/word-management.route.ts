import { Routes } from '@angular/router';
import WordManagementComponent from './components/word-management.component';

export const wordManagementRoute: Routes = [
  {
    path: '',
    component: WordManagementComponent,
  },
];
