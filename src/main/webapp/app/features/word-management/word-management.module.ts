import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SearchOfSkillsSharedModule } from '../../shared/shared.module';
import WordManagementComponent from './components/word-management.component';
import { wordManagementRoute } from './word-management.route';

@NgModule({
  imports: [SearchOfSkillsSharedModule, RouterModule.forChild(wordManagementRoute)],
  declarations: [WordManagementComponent],
})
export class WordManagementModule {}
