import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Page } from '../../../shared/models/page.model';
import { createRequestOption, Pagination } from '../../../shared/util/request-util';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from '../../../app.constants';
import { CandidateCreateInputDTO, CandidateOutputDTO, ICvOutputDto } from '../models/candidate.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';

@Injectable({ providedIn: 'root' })
export default class CandidateService {
  private resourceUrl = SERVER_API_URL + 'api/v2';
  constructor(private http: HttpClient) {}

  query(req: Pagination, filter?: string, tags?: number[]): Observable<Page<CandidateOutputDTO>> {
    let options = createRequestOption(req);
    if (filter) {
      options = options.set('filter', filter);
    }
    if (tags) {
      options = options.append('tags', tags.join(','));
    }
    return this.http
      .get<CandidateOutputDTO[]>(this.resourceUrl + '/candidates', { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<CandidateOutputDTO> = {
            items: res?.body || [],
            total: Number(res.headers.get('x-total-count')),
          };
          return page;
        })
      );
  }

  create(data: CandidateCreateInputDTO): Observable<CandidateOutputDTO> {
    return this.http.post<CandidateOutputDTO>(this.resourceUrl + '/candidates', { ...data });
  }

  getColumns(): Observable<string[]> {
    return this.http.get<string[]>(this.resourceUrl + '/candidates/columns');
  }

  updateCVs(id: number, files: File[]): Observable<{}> {
    const formData = new FormData();
    files.forEach(file => {
      formData.append('files', file);
    });
    return this.http.post<{}>(this.resourceUrl + `/candidates/${id}/cvs`, formData);
  }

  detail(id: number): Observable<CandidateOutputDTO> {
    return this.http.get<CandidateOutputDTO>(this.resourceUrl + '/candidates/' + id);
  }

  update(id: number, data: CandidateCreateInputDTO): Observable<CandidateOutputDTO> {
    return this.http.patch<CandidateOutputDTO>(this.resourceUrl + `/candidates/${id}`, { ...data });
  }

  delete(id: number): Observable<{}> {
    return this.http.delete<{}>(this.resourceUrl + '/candidates/' + id);
  }

  getCvs(idCandidate: number, paging: Pagination = { page: 0, size: ITEMS_PER_PAGE, sort: ['id'] }): Observable<Page<ICvOutputDto>> {
    const options = createRequestOption(paging);
    return this.http
      .get<ICvOutputDto[]>(`${this.resourceUrl}/candidates/${idCandidate}/cvs`, { params: options, observe: 'response' })
      .pipe(
        map(res => {
          const page: Page<ICvOutputDto> = {
            items: res?.body || [],
            total: Number(res.headers.get('x-total-count')),
          };
          return page;
        })
      );
  }

  deleteCv(idCandidate: number, idCv: number): Observable<{}> {
    return this.http.delete<{}>(`${this.resourceUrl}/candidates/${idCandidate}/cvs/${idCv}`);
  }

  exportExcel(filterListId: number, fieldsSelected: string[]): Observable<HttpResponse<Blob>> {
    let options: HttpParams = new HttpParams();
    if (filterListId) {
      options = options.set('filterListId', String(filterListId));
    }
    if (fieldsSelected.length) {
      options = options.append('fieldsSelected', fieldsSelected.join(','));
    }

    return this.http.get(`${this.resourceUrl}/candidates/export-excel`, { params: options, observe: 'response', responseType: 'blob' });
  }

  exportPDF(filterListId: number, numberOfCVs: number): Observable<HttpResponse<Blob>> {
    let options: HttpParams = new HttpParams();
    if (filterListId) {
      options = options.set('filterListId', String(filterListId));
    }
    if (numberOfCVs) {
      options = options.set('numberOfCVs', String(numberOfCVs));
    }

    return this.http.get(`${this.resourceUrl}/candidates/cvs/export-pdf`, { params: options, observe: 'response', responseType: 'blob' });
  }
}
