import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'jhi-create-filter-list-dialog',
  templateUrl: './create-filter-list-dialog.component.html',
})
export default class CreateFilterListDialogComponent {
  errorRequired = false;
  errorWhiteSpace = false;
  name = '';
  constructor(public activeModal: NgbActiveModal) {}

  get isDisableSubmit(): boolean {
    return !this.name || this.errorWhiteSpace;
  }

  onSubmit(): void {
    this.activeModal.close({
      name: this.name,
    });
  }

  addTagFn(name: string): string {
    return name;
  }

  handleValidate(): void {
    this.errorRequired = !this.name;
    this.errorWhiteSpace = !!this.name && !this.name.trim().length;
  }
}
