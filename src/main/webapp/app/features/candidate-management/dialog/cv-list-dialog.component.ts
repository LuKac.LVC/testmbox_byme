import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HandleRequestService } from 'app/core/handle-request/handle-request.service';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ICvOutputDto } from '../models/candidate.model';
import CandidateService from '../services/candidate.service';

@Component({
  selector: 'jhi-cv-list-dialog',
  templateUrl: './cv-list-dialog.component.html',
})
export default class CvListDialogComponent implements OnInit {
  @Input() candidateId?: number;
  @Input() candidateName?: string;

  cvs: ICvOutputDto[] = [];

  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page = 1;

  constructor(
    private candidateService: CandidateService,
    private handleRequestService: HandleRequestService,
    public activeModal: NgbActiveModal
  ) {}

  ngOnInit(): void {
    if (this.candidateId) {
      this.loadCvs();
    }
  }
  private loadCvs(): void {
    if (this.candidateId) {
      this.candidateService.getCvs(this.candidateId, { page: this.page - 1, size: this.itemsPerPage, sort: ['id'] }).subscribe(
        data => {
          this.cvs = data.items;
          this.totalItems = data.total;
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
    }
  }
  transition(): void {
    this.loadCvs();
  }
}
