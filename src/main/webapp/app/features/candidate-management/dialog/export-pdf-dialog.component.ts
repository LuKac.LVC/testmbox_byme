import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HandleRequestService } from 'app/core/handle-request/handle-request.service';
import { IFilterListItem } from 'app/features/user-setting/models/user-setting.model';
import UserV2Service from 'app/features/user-setting/services/user-setting.service';
import CandidateService from '../services/candidate.service';

@Component({
  selector: 'jhi-export-pdf-dialog',
  templateUrl: './export-pdf-dialog.component.html',
})
export default class ExportPDFDialogComponent implements OnInit {
  filterList: IFilterListItem[] = [];
  filterListId?: number;

  numberOfCvs = 5;
  errorRequired = false;
  constructor(
    private candidateService: CandidateService,
    private userV2Service: UserV2Service,
    public activeModal: NgbActiveModal,
    private handleRequestService: HandleRequestService
  ) {}

  ngOnInit(): void {
    this.userV2Service.filterList.asObservable().subscribe(data => {
      this.filterList = [...data];
    });
  }

  onSubmit(): void {
    if (this.filterListId) {
      this.candidateService.exportPDF(this.filterListId, this.numberOfCvs).subscribe(
        res => {
          const fileName = res.headers.get('Content-disposition');
          const blob = new Blob([res.body as BlobPart], { type: 'application/vnd.ms-excel' });
          const link = document.createElement('a');
          link.href = window.URL.createObjectURL(blob);
          link.download = fileName?.split('=')[1] || 'candidate.pdf';
          link.click();
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
    }
  }

  get isDisableSubmit(): boolean {
    return !this.filterListId || !this.numberOfCvs;
  }
  handleValidate(): void {
    this.errorRequired = !this.numberOfCvs;
  }
}
