import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxFileDropModule } from 'ngx-file-drop';

import { SearchOfSkillsSharedModule } from '../../shared/shared.module';
import { candidateManagementRoute } from './candidate-management.route';
import CandidateManagementComponent from './components/candidate-management.component';
import CandidateFilterComponent from './components/candidate-filter.component';
import CreateFilterListDialogComponent from './dialog/create-filter-list-dialog.component';
import CreateCandidateComponent from './components/create-candidate.component';
import DetailCandidateComponent from './components/detail-candidate.component';
import CvListDialogComponent from './dialog/cv-list-dialog.component';
import ExportExcelDialogComponent from './dialog/export-excel-dialog.component';
import ExportPDFDialogComponent from './dialog/export-pdf-dialog.component';
@NgModule({
  imports: [SearchOfSkillsSharedModule, RouterModule.forChild(candidateManagementRoute), NgxFileDropModule],
  declarations: [
    CandidateManagementComponent,
    CandidateFilterComponent,
    CreateFilterListDialogComponent,
    CreateCandidateComponent,
    DetailCandidateComponent,
    CvListDialogComponent,
    ExportExcelDialogComponent,
    ExportPDFDialogComponent,
  ],
})
export class CandidateManagementModule {}
