import { Routes } from '@angular/router';
import CandidateManagementComponent from './components/candidate-management.component';
import CreateCandidateComponent from './components/create-candidate.component';
import DetailCandidateComponent from './components/detail-candidate.component';
export const candidateManagementRoute: Routes = [
  {
    path: '',
    component: CandidateManagementComponent,
  },
  {
    path: 'create',
    component: CreateCandidateComponent,
  },
  {
    path: 'edit/:id',
    component: CreateCandidateComponent,
  },
  {
    path: 'detail/:id',
    component: DetailCandidateComponent,
  },
];
