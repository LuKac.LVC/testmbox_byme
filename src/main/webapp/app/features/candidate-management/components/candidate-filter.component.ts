import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { IFilterObj, IFilterWithCondition } from '../models/candidate.model';
import { CANDIDATE_CONDITIONS, CANDIDATE_FIELDS } from 'app/shared/constants/input.constants';
import UserV2Service from '../../user-setting/services/user-setting.service';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { IFilterListItem } from 'app/features/user-setting/models/user-setting.model';
import { HandleRequestService } from 'app/core/handle-request/handle-request.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDialogComponent } from 'app/shared/dialog/confirm-dialog.component';
import { DialogType } from 'app/shared/dialog/dialog-type.enum';
import { loading } from 'app/shared/rxjs/loading';
import { ITagFilter } from 'app/features/tag-management/models/tag-management.model';
import TagService from 'app/features/tag-management/services/tag.service';
@Component({
  selector: 'jhi-candidate-filter',
  templateUrl: './candidate-filter.component.html',
})
export default class CandidateFilterComponent implements OnInit {
  filterWithConditions: IFilterWithCondition[] = [{}];
  fields = CANDIDATE_FIELDS;
  conditions = CANDIDATE_CONDITIONS;
  filterList: IFilterListItem[] = [];
  filterListId = 0;
  freeText = '';
  tagSearchKey = '';
  tags: ITagFilter[] = [];
  tagsSelected: ITagFilter[] = [];

  @Output() filterEvent = new EventEmitter<string>();
  @Output() changeListEvent = new EventEmitter<number>();
  constructor(
    private userV2Service: UserV2Service,
    private handleRequestService: HandleRequestService,
    private modalService: NgbModal,
    private tagService: TagService
  ) {}
  ngOnInit(): void {
    this.loadFilterList();
    this.loadTags();
    this.userV2Service.filterList.asObservable().subscribe(data => {
      this.filterList = [{ id: 0, name: 'All list', candidateIds: [], userId: 0 }, ...data];
    });
  }

  private loadFilterList(): void {
    this.userV2Service.getFilterList({ page: 0, size: ITEMS_PER_PAGE, sort: ['id'] }).subscribe(
      () => {},
      error => {
        this.handleRequestService.showMessageError('error.actionFailed', error.error);
      }
    );
  }

  addFormFilter(): void {
    if (this.filterWithConditions.length < 5) {
      this.filterWithConditions.push({});
    }
  }

  removeFormFilter(index: number): void {
    this.filterWithConditions.splice(index, 1);
  }

  get tsa(): string {
    return JSON.stringify(this.filterWithConditions);
  }

  onSubmitFilter(): void {
    const operators = this.filterWithConditions.map(i => i.operators);
    const operatorsOrder = operators.filter(i => i !== undefined) as Array<'AND' | 'OR'>;
    const filterWithConditions: Array<Omit<IFilterWithCondition, 'operators'>> = this.filterWithConditions
      .filter(i => !!i.value)
      .map(i => {
        return { field: i.field || '', condition: i.condition || '', value: i.value || '' };
      });
    filterWithConditions.forEach((_, key) => {
      if (key < filterWithConditions.length - 1) {
        operatorsOrder[key] = operatorsOrder[key] || 'AND';
      }
    });

    const tagsToFilter = this.tagsSelected.filter(i => !i.disable).map(i => i.id);
    const filter: IFilterObj = {
      filterListId: this.filterListId,
      filterWithConditions,
      operatorsOrder,
      freeText: this.freeText,
      tags: tagsToFilter,
    };
    this.filterEvent.emit(JSON.stringify(filter));
  }

  onClearFilter(): void {
    this.filterWithConditions = [{}];
    this.filterListId = 0;
    this.freeText = '';
    this.filterEvent.emit('');
  }

  onChangeFilterList($event: number): void {
    this.changeListEvent.emit(Number($event));
  }

  openRemoveList(id: number, name: string): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = 'candidateManagement.dialog.deleteListTitle';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'candidateManagement.dialog.deleteListQuestion';
    modalRef.componentInstance.translateValues = { name };
    modalRef.result.then(
      () => {
        this.userV2Service
          .deleteFilterList(id)
          .pipe(loading())
          .subscribe(
            () => {
              this.handleRequestService.showMessageSuccess('candidateManagement.message.deletedList', {});
              this.loadFilterList();
            },
            error => {
              this.handleRequestService.showMessageError('error.actionFailed', error.error);
            }
          );
      },
      () => {}
    );
  }

  loadTags(name?: string): void {
    this.tagService.query({ page: 0, size: ITEMS_PER_PAGE, sort: [] }, false, name).subscribe(data => {
      const newTags: ITagFilter[] = data.items
        .filter(i => !this.tags.some(curTag => curTag.id === i.id))
        .map(i => ({ id: i.id, name: i.name, disable: false }));
      this.tags = this.tags.concat(newTags);
    });
  }

  toggleTagFilter(id: number): void {
    this.tags = this.tags.map(i => ({ ...i, disable: i.id === id ? !i?.disable : i.disable }));
    this.tagsSelected = this.tagsSelected.map(i => ({ ...i, disable: i.id === id ? !i?.disable : i.disable }));
  }

  deleteTagFilter(id: number): void {
    this.tags = this.tags.map(i => ({ ...i, disable: i.id === id ? !i?.disable : i.disable }));
    this.tagsSelected = this.tagsSelected.filter(i => i.id !== id);
  }
}
