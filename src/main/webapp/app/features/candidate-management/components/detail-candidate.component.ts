import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HandleRequestService } from 'app/core/handle-request/handle-request.service';
import { loading } from 'app/shared/rxjs/loading';
import CvListDialogComponent from '../dialog/cv-list-dialog.component';
import { CandidateOutputDTO, ICvOutputDto } from '../models/candidate.model';
import CandidateService from '../services/candidate.service';
@Component({
  selector: 'jhi-detail-candidate',
  templateUrl: './detail-candidate.component.html',
  styleUrls: ['./detail-candidate.component.scss'],
})
export default class DetailCandidateComponent implements OnInit {
  candidate?: CandidateOutputDTO;
  idCandidate?: number;
  cvsExited?: ICvOutputDto[];
  fromPage = 1;
  constructor(
    private candidateService: CandidateService,
    private activatedRoute: ActivatedRoute,
    private handleRequestService: HandleRequestService,
    private router: Router,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(data => {
      this.idCandidate = data['id'] ? Number(data['id']) : undefined;
      if (this.idCandidate) {
        this.loadCandidate();
        this.loadCvExisted();
      }
    });
  }

  private errorHandle = (error: any): void => {
    this.handleRequestService.showMessageError('error.actionFailed', error.error);
  };

  private loadCandidate(): void {
    if (this.idCandidate) {
      this.candidateService
        .detail(this.idCandidate)
        .pipe(loading())
        .subscribe(data => {
          this.candidate = data;
        }, this.errorHandle);
    }
  }

  private loadCvExisted(): void {
    if (this.idCandidate) {
      this.candidateService
        .getCvs(this.idCandidate)
        .pipe(loading())
        .subscribe(cvs => {
          this.cvsExited = cvs.items;
        }, this.errorHandle);
    }
  }

  get employersName(): string {
    return (this.candidate?.employerDTOs || []).map(i => i.name).join(', ');
  }

  get managerialPositionsName(): string {
    return (this.candidate?.managerialPositionDTOs || []).map(i => i.name).join(', ');
  }

  get businessAreasName(): string {
    return (this.candidate?.businessAreaDTOs || []).map(i => i.name).join(', ');
  }

  backToList(): void {
    this.router.navigate([`./admin/candidate-management`], { queryParams: { page: this.fromPage } });
  }

  viewCvList(): void {
    const modalRef = this.modalService.open(CvListDialogComponent, { size: 'xl', centered: true });
    modalRef.componentInstance.candidateId = this.idCandidate;
    modalRef.componentInstance.candidateName = this.candidate?.firstName;
    modalRef.result.then(
      () => {},
      () => {}
    );
  }
}
