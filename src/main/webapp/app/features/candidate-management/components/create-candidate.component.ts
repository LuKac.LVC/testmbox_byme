import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { FileSystemFileEntry, NgxFileDropEntry } from 'ngx-file-drop';
import { unionWith, isEqual } from 'lodash';

import { HandleRequestService } from 'app/core/handle-request/handle-request.service';
import { IBusinessArea } from 'app/features/business-areas/models/business-areas.model';
import BusinessAreaService from 'app/features/business-areas/services/business-areas.service';
import { ICountry } from 'app/features/country-management/models/country-management.model';
import CountryService from 'app/features/country-management/services/country-management.service';
import { IEmployer } from 'app/features/employer-management/models/employer-management.model';
import EmployerService from 'app/features/employer-management/services/employer.service';
import { IManagerialPosition } from 'app/features/managerial-position/models/managerial-position.model';
import ManagerialPositionService from 'app/features/managerial-position/services/managerial-position.service';
import { IRegion } from 'app/features/region-management/models/region-management.model';
import RegionService from 'app/features/region-management/services/region.service';
import { EMAIL_REGEX, PHONE_REGEX } from 'app/shared/constants/regex.constants';
import CandidateService from '../services/candidate.service';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { ICvOutputDto } from '../models/candidate.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDialogComponent } from 'app/shared/dialog/confirm-dialog.component';
import { DialogType } from 'app/shared/dialog/dialog-type.enum';
import { loading } from 'app/shared/rxjs/loading';
@Component({
  selector: 'jhi-create-candidate',
  templateUrl: './create-candidate.component.html',
  styleUrls: ['./create-candidate.component.scss'],
})
export default class CreateCandidateComponent implements OnInit {
  idCandidate?: number;
  candidateForm = this.fb.group({
    firstName: ['', [Validators.required, Validators.maxLength(255)]],
    lastName: ['', [Validators.required, Validators.maxLength(255)]],
    birthYear: [null],
    personalNumber: [null, [Validators.required, Validators.minLength(12), Validators.maxLength(12), Validators.pattern(/^[0-9]\d*$/)]],
    gender: [null, [Validators.required]],
    regionId: [null, [Validators.required]],
    countryId: [null, [Validators.required]],
    linkedLnLink: [''],
    employerIds: [null, [Validators.required]],
    firstEmployment: [null, [Validators.required, Validators.min(0), Validators.pattern(/^[0-9]\d*$/)]],
    managerialPositionIds: [null, [Validators.required]],
    businessAreaIds: [[]],
    city: ['', [Validators.required]],
    zipCode: ['', [Validators.required]],
    county: ['', [Validators.required]],
    streetAddress: ['', [Validators.required]],
    phoneNumber: [null, [Validators.required, Validators.pattern(PHONE_REGEX)]],
    secondPhoneNumber: [null, [Validators.pattern(PHONE_REGEX)]],
    emailPrivate: ['', [Validators.required, Validators.pattern(EMAIL_REGEX)]],
    emailWork: ['', [Validators.required, Validators.pattern(EMAIL_REGEX)]],
  });

  years = Array.from({ length: 150 }, (_, k) => moment().year() - k - 1);
  genders = ['MALE', 'FEMALE', 'OTHER'];
  regions: IRegion[] = [];
  countries: ICountry[] = [];
  employers: IEmployer[] = [];
  managerialPositions: IManagerialPosition[] = [];
  businessAreas: IBusinessArea[] = [];

  cvs: Array<{ file: File; name: string; valid: boolean; size: number }> = [];
  cvsExited: Array<ICvOutputDto> = [];

  constructor(
    private fb: FormBuilder,
    private regionService: RegionService,
    private handleRequestService: HandleRequestService,
    private countryService: CountryService,
    private employerService: EmployerService,
    private managerialPositionService: ManagerialPositionService,
    private businessAreaService: BusinessAreaService,
    private candidateService: CandidateService,
    private activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(data => {
      this.idCandidate = data['id'] ? Number(data['id']) : undefined;

      if (this.idCandidate) {
        this.candidateService
          .detail(this.idCandidate)
          .pipe(
            map(res => {
              const { employerDTOs, managerialPositionDTOs, businessAreaDTOs, countryDTO, regionDTO, ...candidate } = res;
              const dataDefault = {
                ...candidate,
                employerIds: (employerDTOs || []).map(i => i.id),
                managerialPositionIds: (managerialPositionDTOs || []).map(i => i.id),
                businessAreaIds: (businessAreaDTOs || []).map(i => i.id),
                countryId: countryDTO.id,
                regionId: regionDTO.id,
              };

              this.candidateForm.patchValue({ ...dataDefault });
            })
          )
          .subscribe(() => {}, this.errorHandle);

        this.loadCvExisted();
      }
    });
    this.loadCountries();
    this.loadEmployers();
    this.loadManagerialPositions();
    this.loadBusinessAreas();
    this.candidateForm.get('countryId')?.valueChanges.subscribe(value => {
      if (value) {
        this.candidateForm.get('regionId')?.setValue(null);
        this.loadRegions(Number(value));
      } else {
        this.regions = [];
      }
    });
  }

  private errorHandle = (error: any): void => {
    this.handleRequestService.showMessageError('error.actionFailed', error.error);
  };

  public loadRegions(countryId: number): void {
    this.regionService.getByCountry(countryId).subscribe(data => {
      this.regions = data;
    }, this.errorHandle);
  }

  public loadCountries(name?: string): void {
    this.countryService.query(undefined, name).subscribe(data => {
      this.countries = data.items;
    }, this.errorHandle);
  }

  public loadEmployers(name?: string): void {
    this.employerService.query(undefined, name).subscribe(data => {
      this.employers = name ? unionWith(this.employers, data.items, isEqual) : data.items;
    }, this.errorHandle);
  }

  public loadManagerialPositions(name?: string): void {
    this.managerialPositionService.query(undefined, name).subscribe(data => {
      this.managerialPositions = name ? unionWith(this.managerialPositions, data.items, isEqual) : data.items;
    }, this.errorHandle);
  }

  public loadBusinessAreas(name?: string): void {
    this.businessAreaService.query(undefined, name).subscribe(data => {
      this.businessAreas = data.items;
    }, this.errorHandle);
  }
  private loadCvExisted(): void {
    if (this.idCandidate) {
      this.candidateService.getCvs(this.idCandidate).subscribe(cvs => {
        this.cvsExited = cvs.items;
      });
    }
  }
  onSelectBusinessArea(event: Event): void {
    const target = event.target as HTMLInputElement;
    const currentAreas = this.candidateForm.get('businessAreaIds')?.value || [];
    this.candidateForm.get('businessAreaIds')?.markAsDirty();
    if (target.checked) {
      this.candidateForm.get('businessAreaIds')?.setValue([...currentAreas, Number(target.value || '')]);
    } else {
      this.candidateForm.get('businessAreaIds')?.setValue(currentAreas.filter((i: number) => i !== Number(target.value || '')));
    }
    if (!this.candidateForm.get('businessAreaIds')?.value.length) {
      this.candidateForm.get('businessAreaIds')?.setErrors({ incorrect: true });
    }
  }

  isCheckedArea(id: number): boolean {
    const currentAreas = this.candidateForm.get('businessAreaIds')?.value as number[];
    return (currentAreas || []).includes(id);
  }

  public dropped(files: NgxFileDropEntry[]): void {
    for (const droppedFile of files) {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.cvs = this.cvs.filter(i => i.name !== droppedFile.relativePath);
          this.cvs.push({ file, name: droppedFile.relativePath, valid: this.isFileAllowed(file.name), size: file.size });
        });
      }
    }
  }

  isFileAllowed(fileName: string): boolean {
    let isFileAllowed = false;
    const allowedFiles = ['.pdf'];
    const regex = /(?:\.([^.]+))?$/;
    const extension = regex.exec(fileName);

    if (undefined !== extension && null !== extension) {
      for (const ext of allowedFiles) {
        if (ext === extension[0]) {
          isFileAllowed = true;
        }
      }
    }
    return isFileAllowed;
  }

  get isCsvInvalid(): boolean {
    return this.cvs.some(i => !i.valid);
  }

  removeCv(file: string): void {
    this.cvs = this.cvs.filter(i => i.name !== file);
  }

  get isDisableSubmit(): boolean {
    return !this.candidateForm.valid || this.isCsvInvalid || !(this.candidateForm.get('businessAreaIds')?.value || []).length;
  }

  clearFrom(): void {
    if (this.idCandidate) {
      this.router.navigate(['./admin/candidate-management']);
    } else {
      this.candidateForm.reset();
      this.candidateForm.markAsPristine();
      this.candidateForm.markAsUntouched();
    }
  }

  onSubmit(): void {
    if (!this.isDisableSubmit) {
      const data = this.candidateForm.getRawValue();

      Object.keys(data).forEach(key => {
        if (!data[key]) {
          delete data[key];
        }
      });
      const api = this.idCandidate ? this.candidateService.update(this.idCandidate, data) : this.candidateService.create(data);
      api.pipe(loading()).subscribe(res => {
        this.handleRequestService.showMessageSuccess(
          this.idCandidate ? 'candidateManagement.message.updated' : 'candidateManagement.message.created',
          {}
        );
        this.clearFrom();
        if (this.cvs.length) {
          this.candidateService
            .updateCVs(
              res.id,
              this.cvs.map(i => i.file)
            )
            .subscribe(() => {
              this.handleRequestService.showMessageSuccess('candidateManagement.message.uploadedCv', {});
              this.cvs = [];
            }, this.errorHandle);
        }
      }, this.errorHandle);
    }
  }

  removeCvExited(id: number): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = 'candidateManagement.dialog.deleteCv';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'candidateManagement.dialog.deleteCv';
    modalRef.componentInstance.translateValues = { name };
    modalRef.result.then(
      () => {
        this.candidateService.deleteCv(Number(this.idCandidate), id).subscribe(() => {
          this.handleRequestService.showMessageSuccess('candidateManagement.message.deletedCv', {});
          this.loadCvExisted();
        }, this.errorHandle);
      },
      () => {}
    );
  }

  addEmployer(employer: IEmployer): void {
    if (!employer.id) {
      this.employerService
        .create([employer.name])
        .pipe(loading())
        .subscribe(data => {
          this.handleRequestService.showMessageSuccess('employerManagement.modal.created', '');
          const currentEmployers = this.candidateForm.get('employerIds')?.value as Array<number | undefined>;
          this.candidateForm.get('employerIds')?.setValue(currentEmployers.map(i => i || data[0].id));
          this.loadEmployers(employer.name);
        }, this.errorHandle);
    }
  }

  addManagerialPosition(managerialPosition: IManagerialPosition): void {
    if (!managerialPosition.id) {
      this.managerialPositionService
        .create([managerialPosition.name])
        .pipe(loading())
        .subscribe(data => {
          this.handleRequestService.showMessageSuccess('managerialPosition.modal.created', '');
          const currentManagerialPositions = this.candidateForm.get('managerialPositionIds')?.value as Array<number | undefined>;
          this.candidateForm.get('managerialPositionIds')?.setValue(currentManagerialPositions.map(i => i || data[0].id));
          this.loadManagerialPositions(managerialPosition.name);
        }, this.errorHandle);
    }
  }
}
