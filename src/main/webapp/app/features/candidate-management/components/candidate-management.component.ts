import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HandleRequestService } from 'app/core/handle-request/handle-request.service';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { loading } from 'app/shared/rxjs/loading';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { ICandidate, ICandidateColumn, IFilterObj } from '../models/candidate.model';
import CandidateService from '../services/candidate.service';
import UserV2Service from 'app/features/user-setting/services/user-setting.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import CreateFilterListDialogComponent from '../dialog/create-filter-list-dialog.component';
import CvListDialogComponent from '../dialog/cv-list-dialog.component';
import ExportExcelDialogComponent from '../dialog/export-excel-dialog.component';
import ExportPDFDialogComponent from '../dialog/export-pdf-dialog.component';
import { ConfirmDialogComponent } from 'app/shared/dialog/confirm-dialog.component';
import { DialogType } from 'app/shared/dialog/dialog-type.enum';
@Component({
  selector: 'jhi-candidate-management',
  templateUrl: './candidate-management.component.html',
})
export default class CandidateManagementComponent implements OnInit {
  candidates: ICandidate[] = [];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page = 1;
  predicate = 'id';
  ascending = true;

  columns: ICandidateColumn[] = [];
  columnsConfigDisplay: ICandidateColumn[] = [];
  searchColumnKey = '';
  columnsKeyShow: string[] = [];

  filterJson = '';
  mode: 'VIEW' | 'ACTION' = 'VIEW';
  filtered = false;
  filterListId = 0;
  selectedCandidateIds: number[] = [];

  constructor(
    private candidateService: CandidateService,
    private userV2Service: UserV2Service,
    private handleRequestService: HandleRequestService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.handleNavigation();
    this.loadColumns();
  }
  private loadCandidates(filter?: string, tags?: number[]): void {
    this.candidateService
      .query({ page: this.page - 1, size: this.itemsPerPage, sort: this.sort() }, filter, tags)
      .pipe(loading())
      .subscribe(
        data => {
          this.candidates = data.items;
          this.totalItems = data.total;
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  private loadColumns(): void {
    combineLatest([this.userV2Service.getCandidateColumnConfig(), this.candidateService.getColumns()])
      .pipe(
        map(result => {
          const [userColumn, defaultColumn] = result;
          this.columnsKeyShow = userColumn;
          const columns = defaultColumn.map(column => ({
            key: column,
            label: column,
            sort: true,
            show: userColumn.includes(column),
            fixed: column === 'id' || column === 'firstName' || column === 'lastName',
          }));
          this.columns = [...columns];
          this.columnsConfigDisplay = [...columns];
        })
      )
      .subscribe();
  }

  onSearchColumn(key: string): void {
    if (key) {
      this.columnsConfigDisplay = this.columns.filter(column => column.key.toLowerCase().includes(key));
    } else {
      this.columnsConfigDisplay = [...this.columns];
    }
  }

  onSelectColumnConfig(event: Event): void {
    const target = event.target as HTMLInputElement;
    if (target.checked) {
      this.columnsKeyShow.push(target.value);
    } else {
      this.columnsKeyShow = this.columnsKeyShow.filter(i => i !== target.value);
    }
  }

  saveColumnsConfig(): void {
    this.userV2Service.updateCandidateColumnConfig(this.columnsKeyShow).subscribe(
      () => {
        this.loadColumns();
      },
      error => {
        this.handleRequestService.showMessageError('error.actionFailed', error.error);
      }
    );
  }

  cancelColumnsConfig(): void {
    this.loadColumns();
  }

  setToDefaultConfigColumn(): void {
    this.userV2Service.updateCandidateColumnConfig([]).subscribe(
      () => {
        this.loadColumns();
      },
      error => {
        this.handleRequestService.showMessageError('error.actionFailed', error.error);
      }
    );
  }

  changeModeToView(): void {
    this.mode = 'VIEW';
    this.selectedCandidateIds = [];
  }
  changeModeToEdit(): void {
    this.mode = 'ACTION';
    this.selectedCandidateIds = [];
  }

  isSelectedItem(id: number): boolean {
    return this.selectedCandidateIds.includes(id);
  }

  onSelectCandidate(event: Event): void {
    const target = event.target as HTMLInputElement;
    if (target.checked) {
      this.selectedCandidateIds.push(Number(target.value));
    } else {
      this.selectedCandidateIds = this.selectedCandidateIds.filter(i => i !== Number(target.value));
    }
  }
  private sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  private handleNavigation(): void {
    combineLatest([this.activatedRoute.data, this.activatedRoute.queryParamMap])
      .pipe(
        map(result => {
          const [data, params] = result;
          const page = params.get('page');
          this.page = page !== null ? +page : 1;
          const sort = (params.get('sort') ?? data['defaultSort'])?.split(',') || [];
          this.predicate = sort[0] || 'id';
          this.ascending = sort[1] === 'asc';
          this.loadCandidates();
        })
      )
      .subscribe();
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
      },
    });
  }
  onFilter(filter: string): void {
    const filterObj: IFilterObj = JSON.parse(filter);
    const { tags, ...filterStr } = filterObj;
    this.loadCandidates(JSON.stringify(filterStr), tags);
    this.filterJson = filter;
    if (!filter) {
      this.filtered = false;
      this.filterListId = 0;
    } else {
      this.filtered = !!filter && this.filterListId !== 0;
    }
  }
  onChangeList(listId: number): void {
    if (this.filterListId && this.filterListId !== listId && this.filtered) {
      this.loadCandidates();
      this.selectedCandidateIds = [];
      this.filtered = false;
    }
    this.filterListId = Number(listId);
  }

  openCreateList(): void {
    const modalRef = this.modalService.open(CreateFilterListDialogComponent, { size: 'lg', centered: true });
    modalRef.result.then(
      result => {
        this.userV2Service
          .createFilterList(result.name)
          .pipe(loading())
          .subscribe(
            () => {
              this.handleRequestService.showMessageSuccess('candidateManagement.message.createdList', '');
              this.userV2Service.getFilterList().subscribe();
            },
            error => {
              this.handleRequestService.showMessageError('error.actionFailed', error.error);
            }
          );
      },
      () => {}
    );
  }

  setCandidateToList(): void {
    this.userV2Service
      .updateCandidateFilterList(this.filterListId, this.selectedCandidateIds, 'ADD')
      .pipe(loading())
      .subscribe(
        () => {
          this.handleRequestService.showMessageSuccess('candidateManagement.message.addedToList', {});
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  removeCandidateFromList(): void {
    this.userV2Service
      .updateCandidateFilterList(this.filterListId, this.selectedCandidateIds, 'REMOVE')
      .pipe(loading())
      .subscribe(
        () => {
          this.handleRequestService.showMessageSuccess('candidateManagement.message.removedFromList', {});
          this.loadCandidates(this.filterJson);
        },
        error => {
          this.handleRequestService.showMessageError('error.actionFailed', error.error);
        }
      );
  }

  renderValueCandidate(value: any): string {
    if (typeof value === 'string' || typeof value === 'number') {
      return String(value);
    }
    if (Array.isArray(value)) {
      return value.map(i => i.name).join(', ');
    }
    return value ? value?.name || '' : '';
  }

  onExportExcel(): void {
    const modalRef = this.modalService.open(ExportExcelDialogComponent, { size: 'lg', centered: true });
    modalRef.componentInstance.columns = this.columns;
    modalRef.componentInstance.columnsKeyShow = this.columnsKeyShow;

    modalRef.result.then(
      () => {},
      () => {}
    );
  }

  viewCvList(id: number, name: string): void {
    const modalRef = this.modalService.open(CvListDialogComponent, { size: 'xl', centered: true });
    modalRef.componentInstance.candidateId = id;
    modalRef.componentInstance.candidateName = name;
    modalRef.result.then(
      () => {},
      () => {}
    );
  }

  gotoEdit(id: number): void {
    this.router.navigate(['./admin/candidate-management/edit/' + id]);
  }

  openExportPdf(): void {
    const modalRef = this.modalService.open(ExportPDFDialogComponent, { size: 'xl', centered: true });
    modalRef.result.then(
      () => {},
      () => {}
    );
  }

  openDeleteCandidate(id: number, name: string): void {
    const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg' });
    modalRef.componentInstance.rightButton = 'entity.action.ok';
    modalRef.componentInstance.title = 'candidateManagement.dialog.deleteCandidate';
    modalRef.componentInstance.type = DialogType.CONFIRM;
    modalRef.componentInstance.content = 'candidateManagement.dialog.deleteCandidateQuestion';
    modalRef.componentInstance.translateValues = { name };
    modalRef.result.then(
      () => {
        this.candidateService.delete(id).subscribe(
          () => {
            this.handleRequestService.showMessageSuccess('candidateManagement.message.deleted', {});
          },
          error => {
            this.handleRequestService.showMessageError('error.actionFailed', error.error);
          }
        );
      },
      () => {}
    );
  }
}
