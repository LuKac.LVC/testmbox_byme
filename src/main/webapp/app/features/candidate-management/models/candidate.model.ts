export interface IFilterWithCondition {
  field?: string;
  condition?: string;
  value?: string;
  operators?: 'AND' | 'OR';
}

export interface IFilterObj {
  filterWithConditions: Array<Omit<IFilterWithCondition, 'operators'>>;
  operatorsOrder: Array<'AND' | 'OR'>;
  freeText?: string;
  filterListId: number;
  tags?: number[];
}

export interface ICandidate {
  id: number;
  firstName: string;
  lastName: string;
  birthYear: number;
  personalNumber?: string;
  gender?: string;
  regionId?: number;
  countryId?: number;
  linkedLnLink?: string;
  employerIds?: number[];
  firstEmployment?: number;
  managerialPositionIds?: number[];
  businessAreaIds?: number[];
  city?: string;
  zipCode?: string;
  county?: string;
  streetAddress?: string;
  phoneNumber?: string;
  secondPhoneNumber?: string;
  emailPrivate?: string;
  emailWork?: string;
  currentEmployer?: string;
}

export interface CandidateOutputDTO {
  id: number;

  firstName: string;

  lastName: string;

  birthYear: number;

  personalNumber: string;

  gender: 'MALE' | 'FEMALE' | 'OTHER';

  regionDTO: { id: number; name: string };

  countryDTO: { id: number; name: string };

  linkedLnLink: string;

  employerDTOs: Array<{ id: number; name: string }>;

  firstEmployment: number;

  managerialPositionDTOs: Array<{ id: number; name: string }>;

  businessAreaDTOs: Array<{ id: number; name: string }>;

  city: string;

  zipCode: string;

  county: string;

  streetAddress: string;

  phoneNumber: string;

  secondPhoneNumber: string;

  emailPrivate: string;

  emailWork: string;

  cvDTO: ICvOutputDto;

  createdBy?: string;
  createdDate?: Date;
  lastModifiedBy?: string;
  lastModifiedDate?: Date;
}
export interface ICandidateColumn {
  key: string;
  label?: string;
  sort?: boolean;
  show?: boolean;
  fixed?: boolean;
}

export interface IOperatorsOrderObj {
  [index: number]: 'AND' | 'OR';
}

export interface CandidateCreateInputDTO {
  id: number;

  firstName: string;

  lastName: string;

  birthYear?: number;

  personalNumber: string;

  gender: 'MALE' | 'FEMALE' | 'OTHER';

  regionId: number;

  countryId: number;

  linkedLnLink?: string;

  employerIds: number[];

  firstEmployment: number;

  managerialPositionIds: number[];

  businessAreaIds: number[];

  city: string;

  zipCode: string;

  county: string;

  streetAddress: string;

  phoneNumber: string;

  secondPhoneNumber?: string;

  emailPrivate: string;

  emailWork: string;
}

export interface ICvOutputDto {
  id: number;
  fileName: string;
  url: string;
  createdDate?: Date;
}
