import { Injectable } from '@angular/core';

import { TranslatePipe } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({ providedIn: 'root' })
export class HandleRequestService {
  constructor(private translatePipe: TranslatePipe, private toastrService: ToastrService) {}

  showMessageError(defaultMessageKey: string, error: any): void {
    let status = 0;
    if (error && error.status) {
      status = error.status;
    }
    switch (status) {
      case 400:
        this.toastrService.error(
          this.translatePipe.transform(
            error.title,
            error.detail ? { row: error.detail.split('-')[0], column: error.detail.split('-')[1] } : {}
          )
        );
        break;
      case 403:
      case 404:
        this.toastrService.error(this.translatePipe.transform(error.detail));
        break;
      case 405:
        this.toastrService.error(this.translatePipe.transform(error.message));
        break;
      default:
        this.toastrService.error(this.translatePipe.transform(defaultMessageKey));
    }
  }

  showMessageSuccess(defaultMessageKey: string, param: any): void {
    this.toastrService.success(this.translatePipe.transform(defaultMessageKey, param));
  }
}
